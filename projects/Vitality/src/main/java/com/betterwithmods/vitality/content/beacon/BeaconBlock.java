/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.vitality.content.beacon;

import com.betterwithmods.vitality.modules.beacons.BeaconEffect;
import com.betterwithmods.vitality.modules.beacons.EnhancedBeacons;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

public class BeaconBlock extends net.minecraft.block.BeaconBlock {

    public BeaconBlock(Properties builder) {
        super(builder);
    }

    @Override
    public TileEntity createNewTileEntity(IBlockReader world) {
        return new BeaconTile();
    }

    @Override
    public boolean onBlockActivated(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockRayTraceResult hit) {
        TileEntity tileEntity = world.getTileEntity(pos);

        if (tileEntity instanceof BeaconTile) {
            BeaconTile tileBeacon = (BeaconTile) tileEntity;
            BeaconEffect effect = tileBeacon.getCurrentEffect();
            if (effect != null) {
                effect.onInteract(world, pos, player, hand, player.getHeldItem(hand));
            }

            if (player.isCreative()) {
                ItemStack handStack = player.getHeldItem(hand);
                if (handStack.getItem() instanceof BlockItem) {


                    Block block = ((BlockItem) handStack.getItem()).getBlock();
                    BlockState blockState = block.getDefaultState();

                    if (EnhancedBeacons.beaconEffects.stream().map(BeaconEffect::getStructureTag).anyMatch(block::isIn)) {
                        int r;
                        for (r = 1; r <= 4; r++) {
                            for (int x = -r; x <= r; x++) {
                                for (int z = -r; z <= r; z++) {
                                    world.setBlockState(pos.add(x, -r, z), blockState);
                                }
                            }
                        }
                    }
                }
            }
        }
        return super.onBlockActivated(state, world, pos, player, hand, hit);
    }


    @Override
    public void onPlayerDestroy(IWorld world, BlockPos pos, BlockState state) {
        TileEntity tileEntity = world.getTileEntity(pos);

        if (tileEntity instanceof BeaconTile) {
            BeaconTile tileBeacon = (BeaconTile) tileEntity;
            BeaconEffect effect = tileBeacon.getCurrentEffect();
            if (effect != null) {
                effect.onBreak(world.getWorld(), pos);
            }
        }
        super.onPlayerDestroy(world, pos, state);
    }


}
