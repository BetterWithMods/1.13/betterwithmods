/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.vitality.generator;

import com.betterwithmods.core.Tags;
import com.betterwithmods.core.base.providers.DataProviders;
import com.betterwithmods.core.generator.RecipeProvider;
import com.betterwithmods.vitality.Registrars;
import net.minecraft.block.Blocks;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.IFinishedRecipe;
import net.minecraft.data.ShapedRecipeBuilder;
import net.minecraft.item.Items;
import net.minecraft.tags.BlockTags;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;

import java.util.function.Consumer;

public class VitalityDataProviders extends DataProviders {
    @Override
    public void onGatherData(GatherDataEvent event) {
        DataGenerator generator = event.getGenerator();
        if (event.includeServer()) {
            generator.addProvider(new BlockTagsProvider(generator));
            generator.addProvider(new Recipes(generator));
        }
    }

    private static class BlockTagsProvider extends net.minecraft.data.BlockTagsProvider {

        public BlockTagsProvider(DataGenerator generatorIn) {
            super(generatorIn);
        }

        @Override
        protected void registerTags() {
            getBuilder(Tags.Blocks.VERY_FAST);
            getBuilder(Tags.Blocks.FAST).add(Blocks.GRASS_PATH, Blocks.GRAVEL).add(BlockTags.STONE_BRICKS).add(BlockTags.PLANKS);
            getBuilder(Tags.Blocks.NORMAL);
            getBuilder(Tags.Blocks.SLOW).add(Blocks.SOUL_SAND);
            getBuilder(Tags.Blocks.VERY_SLOW);


            getBuilder(Tags.Blocks.BLINDNESS).add(net.minecraftforge.common.Tags.Blocks.STORAGE_BLOCKS_COAL);
            getBuilder(Tags.Blocks.COSMETIC).add(Blocks.GLASS);
            getBuilder(Tags.Blocks.FIRE_RESIST);
            getBuilder(Tags.Blocks.FORTUNE).add(net.minecraftforge.common.Tags.Blocks.STORAGE_BLOCKS_DIAMOND);
            getBuilder(Tags.Blocks.GLOWING).add(net.minecraftforge.common.Tags.Blocks.STORAGE_BLOCKS_IRON);
            getBuilder(Tags.Blocks.JUMP_BOOST).add(Blocks.SLIME_BLOCK);
            getBuilder(Tags.Blocks.LEVITATE);
            getBuilder(Tags.Blocks.LOOTING).add(net.minecraftforge.common.Tags.Blocks.STORAGE_BLOCKS_EMERALD);
            getBuilder(Tags.Blocks.NIGHT_VISON).add(Blocks.GLOWSTONE);
            getBuilder(Tags.Blocks.SLOWFALL);
            getBuilder(Tags.Blocks.TRUESIGHT).add(net.minecraftforge.common.Tags.Blocks.STORAGE_BLOCKS_LAPIS);
            getBuilder(Tags.Blocks.WATER_BREATHING);

        }
    }

    private static class Recipes extends RecipeProvider {

        public Recipes(DataGenerator generatorIn) {
            super(generatorIn);
        }

        @Override
        protected void registerRecipes(Consumer<IFinishedRecipe> consumer) {

            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.BEACON)
                    .key('S', Items.NETHER_STAR)
                    .key('G', Blocks.GLASS)
                    .key('O', Blocks.OBSIDIAN)
                    .patternLine("GGG")
                    .patternLine("GSG")
                    .patternLine("OOO")
                    .addCriterion("has_nether_star", this.hasItem(Items.NETHER_STAR)).build(consumer);

        }
    }
}
