/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.vitality.modules.beacons;

import com.betterwithmods.core.References;
import com.betterwithmods.core.base.setup.ModuleBase;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import net.minecraft.block.Blocks;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.DyeColor;
import net.minecraft.item.Item;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

public class EnhancedBeacons extends ModuleBase {

    public static final List<BeaconEffect> beaconEffects = buildBeaconEffects();
    private static Map<ResourceLocation, List<ForgeConfigSpec.ConfigValue>> beaconConfigs = Maps.newHashMap();


    public EnhancedBeacons() {
        super("enhanced_beacons");

    }

    private static List<BeaconEffect> buildBeaconEffects() {
        List<BeaconEffect> beaconEffects = Lists.newArrayList();
        beaconEffects.add(new BeaconEffect("cosmetic"));
        beaconEffects.add(new BeaconEffect("haste")
                .withScalingPotionEffect(new EffectInstance(Effects.HASTE, 200, 0))
                .withBeamColor(DyeColor.YELLOW));

        beaconEffects.add(new BeaconEffect("glowing")
                .withScalingPotionEffect(new EffectInstance(Effects.GLOWING, 200, 0))
                .withApplicationCriteria(beaconEffect -> entityLivingBase -> {
                    if(entityLivingBase instanceof MonsterEntity) {
                        Optional<ForgeConfigSpec.ConfigValue> applicationChance = getConfigSpec(beaconEffect, "applicationChance");
                        if(applicationChance.isPresent()) {
                            return entityLivingBase.getRNG().nextInt(100) <= (int) applicationChance.get().get();
                        }
                    }

                    return false;
                })
                .setAddConfig((builder, configValues) -> {
                    configValues.add(builder.defineInRange("applicationChance", 5, 0, 100));
                })
                .withBeamColor(DyeColor.GRAY));

        //TODO - Finish these by adding their appropriate potion effects
        beaconEffects.add(new BeaconEffect("looting")
                .withBeamColor(DyeColor.GREEN));

        beaconEffects.add(new BeaconEffect("fortune")
                .withBeamColor(DyeColor.LIGHT_BLUE));

        beaconEffects.add(new BeaconEffect("night_vision")
                .withBeamColor(DyeColor.BLUE));

        beaconEffects.add(new BeaconEffect("blindness")
                .withScalingPotionEffect(new EffectInstance(Effects.BLINDNESS, 200, 0))
                .withApplicationCriteria(isPlayerNotWearing(EquipmentSlotType.HEAD, new ResourceLocation(References.MODID_MECHANIST, "soulforged_steel_armor_helm")))
                .withBeamColor(DyeColor.BLACK));

        beaconEffects.add(new BeaconEffect("slime")
                .withScalingPotionEffect(new EffectInstance(Effects.JUMP_BOOST, 200, 0))
                .withBeamColor(DyeColor.GREEN));

        beaconEffects.add(new BeaconEffect("fire_resist")
                .setOnCreate(beaconEffect -> (world, blockPos) -> {
                    BlockPos firePosition;
                    for (int radius = 1; radius <= beaconEffect.getCurrentLevel(); radius++) {
                        for (int x = -radius; x <= radius; x++) {
                            for (int z = -radius; z <= radius; z++) {
                                firePosition = blockPos.add(x, -radius + 1, z);
                                if(world.isAirBlock(firePosition)) {
                                    world.setBlockState(firePosition, Blocks.FIRE.getDefaultState());
                                }
                            }
                        }
                    }
                }));

        beaconEffects.add(new BeaconEffect("levitate")
            .withScalingPotionEffect(new EffectInstance(Effects.LEVITATION, 200, 0))
            .withRange(level -> level * 4));
        return beaconEffects;
    }


    private static Optional<ForgeConfigSpec.ConfigValue> getConfigSpec(BeaconEffect beaconEffect, String path) {
        List<ForgeConfigSpec.ConfigValue> effectConfigs = beaconConfigs.getOrDefault(beaconEffect.getTagResource(), Lists.newArrayList());
        return effectConfigs.stream()
                .filter(configValue -> {
                    String valuePath = (String) configValue.getPath().get(configValue.getPath().size() - 1);
                    return valuePath.equals(path);
                })
                .findFirst();
    }

    private static Function<BeaconEffect, Predicate<LivingEntity>> isPlayerNotWearing(EquipmentSlotType slot, ResourceLocation itemLocation) {
        return beaconEffect -> entityLivingBase -> {
            if(entityLivingBase instanceof PlayerEntity) {
                if(entityLivingBase.hasItemInSlot(slot)) {
                    Item slotItem = entityLivingBase.getItemStackFromSlot(slot).getItem();
                    Item findItem = ForgeRegistries.ITEMS.getValue(itemLocation);
                    if(findItem != null) {
                        return !slotItem.equals(findItem);
                    }
                }
            }

            return true;
        };
    }

    @Override
    public void configs(ForgeConfigSpec.Builder builder) {
        beaconEffects.forEach(beaconEffect -> {
            List<ForgeConfigSpec.ConfigValue> configs = beaconConfigs.getOrDefault(beaconEffect.getTagResource(), Lists.newArrayList());
            builder.push(beaconEffect.getTagResource().getPath());
            configs.add(builder.define("enabled", true));
            beaconEffect.addConfigs.accept(builder, configs);
            beaconConfigs.putIfAbsent(beaconEffect.getTagResource(), configs);
            builder.pop();
        });
    }

    @Override
    public void commonSetup(FMLCommonSetupEvent event) {
        super.commonSetup(event);
        beaconEffects.forEach(beaconEffect -> beaconEffect.applyConfigs(beaconConfigs.getOrDefault(beaconEffect.getTagResource(), Lists.newArrayList())));
    }

    @Override
    public String getDescription() {
        return "Enchances beacons such that they provide unique effects depending on the block the base is made of. Removes vanilla beacon mechanics";
    }
}
