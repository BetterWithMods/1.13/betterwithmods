/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.vitality;

import com.betterwithmods.core.References;
import com.betterwithmods.core.base.setup.ModBase;
import com.betterwithmods.core.base.setup.ModConfiguration;
import com.betterwithmods.vitality.config.Client;
import com.betterwithmods.vitality.config.Common;
import com.betterwithmods.vitality.generator.VitalityDataProviders;
import com.betterwithmods.vitality.modules.Insomnia;
import com.betterwithmods.vitality.modules.Movement;
import com.betterwithmods.vitality.modules.beacons.EnhancedBeacons;
import net.minecraftforge.fml.common.Mod;

@Mod(References.MODID_VITALITY)
public class Vitality extends ModBase {
    public Vitality() {
        super(References.MODID_VITALITY, References.NAME_VITALITY);
        new Registrars(References.MODID_VITALITY, getLogger());
        addSetup(new VitalityDataProviders());
        addModule(new Insomnia());
        addModule(new Movement());
        addModule(new EnhancedBeacons());
        new ModConfiguration<>(Client::new, Common::new);
    }

}
