/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.vitality;

import com.betterwithmods.core.References;
import com.betterwithmods.core.base.game.ConstantProperties;
import com.betterwithmods.core.base.game.item.BaseItem;
import com.betterwithmods.core.base.game.item.ItemProperties;
import com.betterwithmods.core.base.registration.BlockRegistrar;
import com.betterwithmods.core.base.registration.CreativeTab;
import com.betterwithmods.core.base.registration.ItemRegistrar;
import com.betterwithmods.core.base.registration.TileEntityRegistrar;
import com.betterwithmods.vitality.content.beacon.BeaconBlock;
import com.betterwithmods.vitality.content.beacon.BeaconTile;
import net.minecraft.block.Block;
import net.minecraft.item.Food;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.ObjectHolder;
import org.apache.logging.log4j.Logger;

@Mod.EventBusSubscriber
public class Registrars {

    public static Registrars INSTANCE;

    private static String MODID;

    public static final CreativeTab ITEM_GROUP = new CreativeTab(References.MODID_VITALITY, "items", () -> new ItemStack(Items.WITCH_WART));

    public Registrars(String modid, Logger logger) {
        MODID = modid;
        INSTANCE = this;
        new BlockRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<Block> registry) {
                //TODO - Override minecraft beacon. Currently gives error if you try.
                register(registry, "beacon", new BeaconBlock(ConstantProperties.ROCK));
            }
        };

        new TileEntityRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<TileEntityType<?>> registry) {
                register(registry, "beacon", TileEntityType.Builder.create(BeaconTile::new, Blocks.BEACON).build(null));
            }
        };
        new ItemRegistrar(modid, logger) {


            @Override
            public void registerAll(IForgeRegistry<Item> registry) {
                register(registry, "healing_gland", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "poison_sac", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "witch_wart", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "cocoa_powder", new BaseItem(new ItemProperties().group(ITEM_GROUP)));

                EffectInstance foodPoison = new EffectInstance(Effects.HUNGER, 600, 0);

                Food donut = new Food.Builder().hunger(2).saturation(0.25f).build();
                Food creeper_oysters = new Food.Builder().hunger(2).saturation(0).effect(new EffectInstance(Effects.POISON, 100, 0), 1).build();
                Food egg = new Food.Builder().hunger(2).saturation(0.2f).effect(foodPoison, 0.3f).build();
                Food cooked_egg = new Food.Builder().hunger(2).saturation(0.25f).build();
                Food scrambled_egg = new Food.Builder().hunger(4).saturation(0.3f).effect(foodPoison, 0.3f).build();
                Food cooked_scrambled_egg = new Food.Builder().hunger(5).saturation(0.5f).build();
                Food omelet = new Food.Builder().hunger(3).saturation(0.5f).build();
                Food cooked_omelet = new Food.Builder().hunger(4).saturation(1).build();
                Food ham_and_eggs = new Food.Builder().hunger(2).saturation(0.25f).build();
                Food tasty_sandwich = new Food.Builder().hunger(7).saturation(0.7f).build();
                Food beef_dinner = new Food.Builder().hunger(8).saturation(0.6f).build();
                Food beef_potatoes = new Food.Builder().hunger(7).saturation(0.5f).build();
                Food chocolate = new Food.Builder().hunger(2).saturation(0.2f).build();
                Food kebab = new Food.Builder().hunger(4).saturation(0.3f).effect(foodPoison, 0.3f).build();
                Food cooked_kebab = new Food.Builder().hunger(8).saturation(0.4f).build();
                Food pork_dinner = new Food.Builder().hunger(8).saturation(0.6f).build();
                Food wolf_chop = new Food.Builder().effect(foodPoison, 0.3f).build();
                Food cooked_wolf_chop = new Food.Builder().hunger(8).saturation(0.3f).build();
                Food kibble = new Food.Builder().hunger(3).saturation(0).build();
                Food apple_pie = new Food.Builder().hunger(8).saturation(0.3f).build();
                Food mystery_meat = new Food.Builder().hunger(2).saturation(0.3f).build();
                Food cooked_mystery_meat = new Food.Builder().hunger(6).saturation(0.8f).effect(foodPoison, 0.3f).build();
                Food bat_wing = new Food.Builder().hunger(2).saturation(0.3f).effect(foodPoison, 0.3f).build();
                Food cooked_bat_wing = new Food.Builder().hunger(4).saturation(0.6f).build();
//                Food chicken_soup = new Food.Builder().hunger(4).saturation(0.6f).build();



                register(registry, "donut", new Item(new ItemProperties().group(ITEM_GROUP).food(donut)));
                register(registry, "creeper_oysters", new Item(new ItemProperties().group(ITEM_GROUP).food(creeper_oysters)));
                register(registry, "egg", new Item(new ItemProperties().group(ITEM_GROUP).food(egg)));
                register(registry, "cooked_egg", new Item(new ItemProperties().group(ITEM_GROUP).food(cooked_egg)));
                register(registry, "scrambled_egg", new Item(new ItemProperties().group(ITEM_GROUP).food(scrambled_egg)));
                register(registry, "cooked_scrambled_egg", new Item(new ItemProperties().group(ITEM_GROUP).food(cooked_scrambled_egg)));
                register(registry, "omelet", new Item(new ItemProperties().group(ITEM_GROUP).food(omelet)));
                register(registry, "cooked_omelet", new Item(new ItemProperties().group(ITEM_GROUP).food(cooked_omelet)));
                register(registry, "ham_and_eggs", new Item(new ItemProperties().group(ITEM_GROUP).food(ham_and_eggs)));
                register(registry, "tasty_sandwich", new Item(new ItemProperties().group(ITEM_GROUP).food(tasty_sandwich)));
                register(registry, "beef_dinner", new Item(new ItemProperties().group(ITEM_GROUP).food(beef_dinner)));
                register(registry, "beef_potatoes", new Item(new ItemProperties().group(ITEM_GROUP).food(beef_potatoes)));
                register(registry, "chocolate", new Item(new ItemProperties().group(ITEM_GROUP).food(chocolate)));
                register(registry, "kebab", new Item(new ItemProperties().group(ITEM_GROUP).food(kebab)));
                register(registry, "cooked_kebab", new Item(new ItemProperties().group(ITEM_GROUP).food(cooked_kebab)));
                register(registry, "pork_dinner", new Item(new ItemProperties().group(ITEM_GROUP).food(pork_dinner)));
                register(registry, "wolf_chop", new Item(new ItemProperties().group(ITEM_GROUP).food(wolf_chop)));
                register(registry, "cooked_wolf_chop", new Item(new ItemProperties().group(ITEM_GROUP).food(cooked_wolf_chop)));
                register(registry, "kibble", new Item(new ItemProperties().group(ITEM_GROUP).food(kibble)));
                register(registry, "apple_pie", new Item(new ItemProperties().group(ITEM_GROUP).food(apple_pie)));
                register(registry, "mystery_meat", new Item(new ItemProperties().group(ITEM_GROUP).food(mystery_meat)));
                register(registry, "cooked_mystery_meat", new Item(new ItemProperties().group(ITEM_GROUP).food(cooked_mystery_meat)));
                register(registry, "bat_wing", new Item(new ItemProperties().group(ITEM_GROUP).food(bat_wing)));
                register(registry, "cooked_bat_wing", new Item(new ItemProperties().group(ITEM_GROUP).food(cooked_bat_wing)));

                //TODO - Soup
                //register(registry, "chowder", new Item(new ItemProperties().group(ITEM_GROUP).healAmount(5).saturationAmount(0.0f)));
                //register(registry, "hearty_stew", new Item(new ItemProperties().group(ITEM_GROUP).healAmount(10).saturationAmount(0.0f)));
//                register(registry, "chicken_soup", new Item(new ItemProperties().group(ITEM_GROUP).healAmount(2).saturationAmount(0.25f)));

                register(registry, Blocks.BEACON, ITEM_GROUP);
            }
        };
    }

    @ObjectHolder(References.MODID_VITALITY)
    public static class Items {
        public static final Item WITCH_WART = null;
        public static final Item HEALING_GLAND = null;
        public static final Item POISON_SAC = null;
    }

    @ObjectHolder(References.MODID_VITALITY)
    public static class Blocks {
        public static final Block BEACON = null;
    }

    @ObjectHolder(References.MODID_VITALITY)
    public static class Tiles {
        public static final TileEntityType<BeaconTile> BEACON = null;
    }
}
