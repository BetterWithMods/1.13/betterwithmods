/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.phenomenons.modules.creature;

import com.betterwithmods.core.Tags;
import com.betterwithmods.core.base.setup.ModuleBase;
import com.betterwithmods.core.utilties.EntityUtils;
import com.google.common.collect.ImmutableMap;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.tags.Tag;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.Map;

public class HostilesHuntPassives extends ModuleBase {

    public HostilesHuntPassives() {
        super("hostilesHuntPassives");
    }

    @Override
    public void configs(ForgeConfigSpec.Builder builder) {
        getLoader().addCondition("hostilesHuntPassives", this::isEnabled);
    }

    @Override
    public String getDescription() {
        return "Hostile mobs will actively seek out and kill passive mobs in the world.";
    }

    private static final Map<EntityType<?>, Tag<EntityType<?>>> map = ImmutableMap.<EntityType<?>, Tag<EntityType<?>>>builder()
            .put(EntityType.ZOMBIE, Tags.Entities.ZOMBIE_TARGETS)
            .put(EntityType.SPIDER, Tags.Entities.SPIDER_TARGETS)
            .put(EntityType.SKELETON, Tags.Entities.SKELETON_TARGETS)
            .build();


    @SubscribeEvent
    public void addAI(EntityJoinWorldEvent event) {

        if (event.getEntity() instanceof MobEntity) {
            MobEntity entity = (MobEntity) event.getEntity();
            Tag<EntityType<?>> tag = map.get(entity.getType());
            if(tag != null) {
                entity.goalSelector.addGoal(0, new NearestAttackableTargetGoal<>(entity, LivingEntity.class,
                        1,
                        false,
                        false,
                        e -> EntityUtils.filter(e, tag)));
            }
        }
    }

}
