/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.phenomenons.generator;

import com.betterwithmods.core.References;
import com.betterwithmods.core.Relationships;
import com.betterwithmods.core.Tags;
import com.betterwithmods.core.base.game.ingredient.TagIngredient;
import com.betterwithmods.core.base.game.ingredient.ToolIngredient;
import com.betterwithmods.core.base.providers.DataProviders;
import com.betterwithmods.core.generator.RecipeProvider;
import com.betterwithmods.core.generator.ToolRecipeBuilder;
import com.betterwithmods.core.relationary.Relation;
import net.minecraft.block.Blocks;
import net.minecraft.data.*;
import net.minecraft.entity.EntityType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.Tag;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvents;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.common.crafting.ConditionalRecipe;
import net.minecraftforge.common.crafting.conditions.ICondition;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;

import java.util.function.Consumer;

public class PhenonemonsDataProviders extends DataProviders {
    @Override
    public void onGatherData(GatherDataEvent event) {
        DataGenerator generator = event.getGenerator();
        if (event.includeServer()) {
            generator.addProvider(new EntityTypeTagsProvider(generator) {
                @Override
                protected void registerTags() {

                    getBuilder(Tags.Entities.ZOMBIE_TARGETS).add(EntityType.COW, EntityType.SHEEP, EntityType.PIG, EntityType.LLAMA);
                    getBuilder(Tags.Entities.SKELETON_TARGETS).add(EntityType.BAT);
                    getBuilder(Tags.Entities.SPIDER_TARGETS).add(EntityType.CHICKEN, EntityType.PARROT);
                    getBuilder(Tags.Entities.SQUID_TARGETS).add(EntityType.COW, EntityType.SHEEP, EntityType.PIG, EntityType.LLAMA, EntityType.CHICKEN);


                }
            });

            generator.addProvider(new ItemTagsProvider(generator) {
                @Override
                protected void registerTags() {
                    getBuilder(Tags.Items.AUTOFEED_CHICKEN).add(Tags.Items.SEEDS);
                    getBuilder(Tags.Items.AUTOFEED_COW).add(Items.WHEAT);
                    getBuilder(Tags.Items.AUTOFEED_HORSE).add(Items.APPLE, Items.GOLDEN_CARROT);
                    getBuilder(Tags.Items.AUTOFEED_LLAMA).add(Items.HAY_BLOCK);
                    getBuilder(Tags.Items.AUTOFEED_OCELOT).add(Tags.Items.COOKED_MEAT, Tags.Items.UNCOOKED_MEAT);
                    getBuilder(Tags.Items.AUTOFEED_PIG).add(Items.CARROT);
                    getBuilder(Tags.Items.AUTOFEED_RABBIT).add(Items.CARROT);
                    getBuilder(Tags.Items.AUTOFEED_SHEEP).add(Items.WHEAT);
                    getBuilder(Tags.Items.AUTOFEED_SPIDER).add(Tags.Items.COOKED_MEAT, Tags.Items.UNCOOKED_MEAT);
                    getBuilder(Tags.Items.AUTOFEED_WOLF).add(Tags.Items.COOKED_MEAT, Tags.Items.UNCOOKED_MEAT);
                    getBuilder(Tags.Items.AUTOFEED_ZOMBIE).add(Tags.Items.COOKED_MEAT, Tags.Items.UNCOOKED_MEAT);

                    getBuilder(Tags.Items.SEED_BLACKLIST).add(Items.WHEAT_SEEDS);
                }
            });

            generator.addProvider(new Recipes(generator));
        }
    }

    private static class Recipes extends RecipeProvider  {

        public Recipes(DataGenerator generatorIn) {
            super(generatorIn);
        }

        private static final Ingredient axe = new ToolIngredient(ToolType.AXE);

        private ICondition LUMBER_CHOPPING_ENABLED = moduleLoaded(References.MODID_PHENOMENONS, "lumber_chopping");
        private ICondition LUMBER_CHOPPING_DISABLED = not(LUMBER_CHOPPING_ENABLED);

        private void addLogRecipe(Relation logRelation, Tag<Item> log, IItemProvider plank, Consumer<IFinishedRecipe> consumer) {
            Ingredient ingredientLog = new TagIngredient(log);
            ResourceLocation id = new ResourceLocation(References.MODID_PHENOMENONS, "chopping/" + plank.asItem().getRegistryName().getPath());

            ConditionalRecipe.builder()
                    .addCondition(LUMBER_CHOPPING_ENABLED)
                    .addRecipe(
                            ToolRecipeBuilder.recipe(new ItemStack(plank, 3))
                                    .tool(axe)
                                    .input(ingredientLog)
                                    .damageTool()
                                    .group("planks")
                                    .sound(SoundEvents.ENTITY_ZOMBIE_ATTACK_WOODEN_DOOR)
                                    .addDrop(logRelation, Relationships.BARK, 1)
                                    .addDrop(logRelation, Relationships.DUST, 1)
                                    .addCriterion("has_log", hasItemProvider(ItemTags.LOGS))::build
                    )
                    .addCondition(LUMBER_CHOPPING_DISABLED)
                    .addRecipe(
                            ShapelessRecipeBuilder
                                    .shapelessRecipe(plank, 4)
                                    .addIngredient(ingredientLog)
                                    .setGroup("planks")
                                    .addCriterion("has_logs", this.hasItemProvider(log))
                                    ::build
                    )
                    .build(consumer, id);

        }

        @Override
        protected void registerRecipes(Consumer<IFinishedRecipe> consumer) {
            addLogRecipe(Relationships.ACACIA_WOOD, ItemTags.ACACIA_LOGS, Blocks.ACACIA_PLANKS, consumer);
            addLogRecipe(Relationships.BIRCH_WOOD, ItemTags.BIRCH_LOGS, Blocks.BIRCH_PLANKS, consumer);
            addLogRecipe(Relationships.DARK_OAK_WOOD, ItemTags.DARK_OAK_LOGS, Blocks.DARK_OAK_PLANKS, consumer);
            addLogRecipe(Relationships.JUNGLE_WOOD, ItemTags.JUNGLE_LOGS, Blocks.JUNGLE_PLANKS, consumer);
            addLogRecipe(Relationships.OAK_WOOD, ItemTags.OAK_LOGS, Blocks.OAK_PLANKS, consumer);
            addLogRecipe(Relationships.SPRUCE_WOOD, ItemTags.SPRUCE_LOGS, Blocks.SPRUCE_PLANKS, consumer);
        }
    }
}
