/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.phenomenons.modules.creature.squid;

import net.minecraft.entity.MobEntity;
import net.minecraft.entity.passive.SquidEntity;
import net.minecraft.network.DebugPacketSender;
import net.minecraft.pathfinding.PathFinder;
import net.minecraft.pathfinding.PathNavigator;
import net.minecraft.pathfinding.SwimNodeProcessor;
import net.minecraft.util.Util;
import net.minecraft.util.math.*;
import net.minecraft.world.World;

import javax.annotation.Nonnull;


public class PathNavigateSquid extends PathNavigator {

    public PathNavigateSquid(MobEntity entityIn, World world) {
        super(entityIn, world);
    }


    @Nonnull
    @Override
    protected PathFinder getPathFinder(int cooldown) {
        this.nodeProcessor = new SwimNodeProcessor(false);
        return new PathFinder(this.nodeProcessor, cooldown);
    }

    @Override
    public void tick() {
        ++this.totalTicks;
        if (this.tryUpdatePath) {
            this.updatePath();
        }

        if (!this.noPath()) {
            if (this.canNavigate()) {
                this.pathFollow();
            } else if (this.currentPath != null && this.currentPath.getCurrentPathIndex() < this.currentPath.getCurrentPathLength()) {
                Vec3d vec3d = this.currentPath.getVectorFromIndex(this.entity, this.currentPath.getCurrentPathIndex());
                if (MathHelper.floor(this.entity.posX) == MathHelper.floor(vec3d.x) && MathHelper.floor(this.entity.posY) == MathHelper.floor(vec3d.y) && MathHelper.floor(this.entity.posZ) == MathHelper.floor(vec3d.z)) {
                    this.currentPath.setCurrentPathIndex(this.currentPath.getCurrentPathIndex() + 1);
                }
            }

            DebugPacketSender.func_218803_a(this.world, this.entity, this.currentPath, this.maxDistanceToWaypoint);
            if (!this.noPath()) {
                Vec3d vec3d1 = this.currentPath.getPosition(this.entity);
                SquidEntity squid = (SquidEntity) this.entity;
                Vec3d nextPos = this.currentPath.getPosition(this.entity);
                Vec3d currentPos = new Vec3d(squid.posX, squid.posY, squid.posZ);
                Vec3d vec = nextPos.subtract(currentPos).normalize().scale(0.2);
                squid.setMovementVector((float) vec.x, (float) vec.y, (float) vec.z);
                this.entity.getMoveHelper().setMoveTo(vec3d1.x, vec3d1.y, vec3d1.z, this.speed);
            }
        }
    }

    @Override
    protected void pathFollow() {
        if (this.currentPath != null) {
            Vec3d vec3d = this.getEntityPosition();
            float f = this.entity.getWidth();
            float f1 = f > 0.75F ? f / 2.0F : 0.75F - f / 2.0F;
            Vec3d vec3d1 = this.entity.getMotion();
            if (Math.abs(vec3d1.x) > 0.2D || Math.abs(vec3d1.z) > 0.2D) {
                f1 = (float) ((double) f1 * vec3d1.length() * 6.0D);
            }

            int i = 6;
            Vec3d vec3d2 = this.currentPath.getCurrentPos();
            if (Math.abs(this.entity.posX - (vec3d2.x + 0.5D)) < (double) f1 && Math.abs(this.entity.posZ - (vec3d2.z + 0.5D)) < (double) f1 && Math.abs(this.entity.posY - vec3d2.y) < (double) (f1 * 2.0F)) {
                this.currentPath.incrementPathIndex();
            }

            for (int j = Math.min(this.currentPath.getCurrentPathIndex() + 6, this.currentPath.getCurrentPathLength() - 1); j > this.currentPath.getCurrentPathIndex(); --j) {
                vec3d2 = this.currentPath.getVectorFromIndex(this.entity, j);
                if (!(vec3d2.squareDistanceTo(vec3d) > 36.0D) && this.isDirectPathBetweenPoints(vec3d, vec3d2, 0, 0, 0)) {
                    this.currentPath.setCurrentPathIndex(j);
                    break;
                }
            }

            this.checkForStuck(vec3d);
        }
    }

    @Override
    protected void checkForStuck(@Nonnull Vec3d positionVec3) {
        if (this.totalTicks - this.ticksAtLastPos > 100) {
            if (positionVec3.squareDistanceTo(this.lastPosCheck) < 2.25D) {
                this.clearPath();
            }

            this.ticksAtLastPos = this.totalTicks;
            this.lastPosCheck = positionVec3;
        }

        if (this.currentPath != null && !this.currentPath.isFinished()) {
            Vec3d vec3d = this.currentPath.getCurrentPos();
            if (vec3d.equals(this.timeoutCachedNode)) {
                this.timeoutTimer += Util.milliTime() - this.lastTimeoutCheck;
            } else {
                this.timeoutCachedNode = vec3d;
                double d0 = positionVec3.distanceTo(this.timeoutCachedNode);
                this.timeoutLimit = this.entity.getAIMoveSpeed() > 0.0F ? d0 / (double) this.entity.getAIMoveSpeed() * 100.0D : 0.0D;
            }

            if (this.timeoutLimit > 0.0D && (double) this.timeoutTimer > this.timeoutLimit * 2.0D) {
                this.timeoutCachedNode = Vec3d.ZERO;
                this.timeoutTimer = 0L;
                this.timeoutLimit = 0.0D;
                this.clearPath();
            }

            this.lastTimeoutCheck = Util.milliTime();
        }

    }

    @Nonnull
    @Override
    protected Vec3d getEntityPosition() {
        return new Vec3d(this.entity.posX, this.entity.posY + (double) this.entity.getHeight() * 0.5D, this.entity.posZ);
    }

    @Override
    protected boolean canNavigate() {
        return this.isInLiquid();
    }

    @Override
    protected boolean isDirectPathBetweenPoints(Vec3d posVec31, Vec3d posVec32, int sizeX, int sizeY, int sizeZ) {
        Vec3d vec3d = new Vec3d(posVec32.x, posVec32.y + (double) this.entity.getHeight() * 0.5D, posVec32.z);
        return this.world.rayTraceBlocks(new RayTraceContext(posVec31, vec3d, RayTraceContext.BlockMode.COLLIDER, RayTraceContext.FluidMode.NONE, this.entity)).getType() == RayTraceResult.Type.MISS;
    }
    @Override
    public boolean canEntityStandOnPos(BlockPos pos) {
        return !this.world.getBlockState(pos).isOpaqueCube(this.world, pos);
    }

    @Override
    public void setCanSwim(boolean canSwim) {
    }


}
