/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.phenomenons.modules.creature;

import com.betterwithmods.core.base.setup.ModuleBase;
import com.betterwithmods.core.utilties.ConfigUtils;
import com.betterwithmods.core.utilties.EntityUtils;
import com.betterwithmods.core.utilties.ReflectionHelper;
import com.google.common.collect.Lists;
import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.ai.goal.HurtByTargetGoal;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.List;
import java.util.Optional;

public class GroupAgro extends ModuleBase {

    private ForgeConfigSpec.ConfigValue<List<? extends String>> groupAgroList;

    private List<EntityType<?>> groupAgroTypes;

    public GroupAgro() {
        super("group_agro");
    }

    @Override
    public void configs(ForgeConfigSpec.Builder builder) {
        this.groupAgroList = builder.comment("The list of entities that will agro in groups, similar to Pigmen")
                .translation("config.betterwithmods_phenomenons.groupAgroList")
                .defineList("groupAgroList", Lists.newArrayList("minecraft:enderman"), o -> true);
    }

    public boolean shouldGroupAgro(Entity entity) {
        if (groupAgroTypes == null) {
            groupAgroTypes = ConfigUtils.getEntityTypes(this.groupAgroList.get());
        }
        for (EntityType<?> type : groupAgroTypes) {
            if (entity.getType() == type)
                return true;
        }
        return false;
    }

    private void reflectShouldCallForHelp(MobEntity entity) {
        Optional<HurtByTargetGoal> optionalAI = EntityUtils.findGoals(HurtByTargetGoal.class, entity.targetSelector);
        if (optionalAI.isPresent()) {
            HurtByTargetGoal goals = optionalAI.get();
            ReflectionHelper.setValue(HurtByTargetGoal.class, goals, "entityCallsForHelp", true);
        } else {
            getLogger().debug(entity.getType().getRegistryName() + " can not group aggro targets");
        }
    }

    @SubscribeEvent
    public void addAI(EntityJoinWorldEvent event) {
        if (event.getEntity() instanceof CreatureEntity) {
            CreatureEntity creature = (CreatureEntity) event.getEntity();
            if (shouldGroupAgro(creature)) {
                reflectShouldCallForHelp(creature);
            }
        }
    }

    @Override
    public String getDescription() {
        return "Add AI for mobs to call for help from surrounding mobs of the same type";
    }
}
