/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.phenomenons.modules.tools;

import com.betterwithmods.core.References;
import com.betterwithmods.core.Tags;
import com.betterwithmods.core.base.setup.ModuleBase;
import com.betterwithmods.core.utilties.ItemUtils;
import com.betterwithmods.core.utilties.StackEjector;
import net.minecraft.block.BlockState;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.tags.Tag;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.event.entity.player.UseHoeEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class SeedsRequireHoes extends ModuleBase {

    private static final Tag<Item> SEED_BLACKLIST = Tags.Items.tag(References.MODID_PHENOMENONS, "seed_blacklist");

    private ForgeConfigSpec.DoubleValue seedDropChance;

    public SeedsRequireHoes() {
        super("seedsRequireHoes");
    }

    @Override
    public void configs(ForgeConfigSpec.Builder builder) {
        seedDropChance = builder.comment("How often will seeds drop from a tilled block (x/100)")
                .defineInRange("seedDropChance", 12.5, 0, 100);
    }

    @SubscribeEvent
    public void onHarvest(BlockEvent.HarvestDropsEvent event) {
        if(event.getState().isIn(Tags.Blocks.GRASS_PLANTS)) {
             event.getDrops().removeIf(itemStack -> itemStack.getItem().isIn(Tags.Items.SEEDS));
        }
    }

    @SubscribeEvent
    public void onUseHoe(UseHoeEvent event) {
        World world = event.getContext().getWorld();
        if(!world.isRemote) {
            BlockPos tilledPos = event.getContext().getPos();
            if(world.isAirBlock(tilledPos.up())) {
                BlockState tilledState = world.getBlockState(tilledPos);
                StackEjector ejector = ItemUtils.EJECT_OFFSET;

                ItemStack seedDrop = determineSeedDrop(event.getContext());
                ejector.setStack(seedDrop);
                ejector.ejectStack(world, tilledPos, BlockPos.ZERO);

            }
        }
    }

    private ItemStack determineSeedDrop(ItemUseContext context) {
        Tag<Item> seedTag = Tags.Items.SEEDS;
        if(seedTag.getAllElements().size() > 0) {
            Item seed = seedTag.getRandomElement(context.getWorld().getRandom());
            if(!seed.isIn(SEED_BLACKLIST) && context.getWorld().getRandom().nextInt(100) <= seedDropChance.get()) {
                return new ItemStack(seed, 1);
            }
        }


        return ItemStack.EMPTY;
    }

    @Override
    public String getDescription() {
        return "Seeds no longer drop from blocks. You must till grass with a hoe to have a chance at getting a random seed";
    }
}
