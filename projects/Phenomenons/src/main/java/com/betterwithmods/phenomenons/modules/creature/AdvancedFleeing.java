/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.phenomenons.modules.creature;

import com.betterwithmods.core.base.game.goals.AdvancedPanicGoal;
import com.betterwithmods.core.base.setup.ModuleBase;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.goal.PanicGoal;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.List;

public class AdvancedFleeing extends ModuleBase {

    private ForgeConfigSpec.IntValue maximumFleeDistance;
    private ForgeConfigSpec.BooleanValue groupFlee;
    private ForgeConfigSpec.IntValue groupFleeRange;

    private ForgeConfigSpec.BooleanValue breakBlockCausesFlee;
    private ForgeConfigSpec.IntValue breakBlockRange;

    private ForgeConfigSpec.BooleanValue placeBlockCausesFlee;
    private ForgeConfigSpec.IntValue placeBlockRange;


    public AdvancedFleeing() {
        super("advancedFleeing");
    }

    @Override
    public void configs(ForgeConfigSpec.Builder builder) {
        getLoader().addCondition("advancedFleeing", this::isEnabled);
        this.maximumFleeDistance = builder.comment("The maximum distance an animals will flee from an attacker.")
                .translation("config.betterwithmods_phenomenons.maximumFleeDistance")
                .defineInRange("maxiumumFleeDistance", 20, 0, 44);

        this.groupFlee = builder.comment("Should a fleeing entity cause other nearby animals to flee.")
                .translation("config.betterwithmods_phenomenons.groupFlee")
                .define("groupFlee", true);

        this.groupFleeRange = builder.comment("How large of a range should a fleeing animals cause other nearby entities to flee.")
                .translation("config.betterwithmods_phenomenons.groupFleeRange")
                .defineInRange("groupFleeRange", 10, 0, Integer.MAX_VALUE);

        this.breakBlockCausesFlee = builder.comment("Breaking a block will cause nearby animals to flee.")
                .translation("config.betterwithmods_pgenomenons.breakBlockCausesFlee")
                .define("breakBlockCausesFlee", true);

        this.breakBlockRange = builder.comment("How large of a range from the player does breaking a block cause animals to flee.")
                .translation("config.betterwithmods_phenomenons.breakBlockRange")
                .defineInRange("breakBlockRange", 15, 0, Integer.MAX_VALUE);

        this.placeBlockCausesFlee = builder.comment("Placing a block will cause nearby animals to flee.")
                .translation("config.betterwithmods_pgenomenons.placeBlockCausesFlee")
                .define("placeBlockCausesFlee", true);

        this.placeBlockRange = builder.comment("How large of a range from the player does placing a block cause animals to flee.")
                .translation("config.betterwithmods_phenomenons.placeBlockRange")
                .defineInRange("placeBlockRange", 15, 0, Integer.MAX_VALUE);
    }


    @Override
    public String getDescription() {
        return "When a passive mob is attacked it will attempt to flee from the attacker";
    }

    @SubscribeEvent
    public void addAI(EntityJoinWorldEvent event) {
        if (event.getEntity() instanceof AnimalEntity) {
            AnimalEntity entity = (AnimalEntity) event.getEntity();
            entity.goalSelector.goals.removeIf(prioritizedGoal -> prioritizedGoal.getGoal() instanceof PanicGoal);
            entity.goalSelector.addGoal(0, new AdvancedPanicGoal(entity, maximumFleeDistance.get(), 2.0f));
        }
    }

    @SubscribeEvent
    public void onBlockBreak(BlockEvent.BreakEvent event) {
        if(breakBlockCausesFlee.get()) {
            setTargetForAnimalsInRange(event.getPlayer(), event.getPlayer().getPosition(), breakBlockRange.get());
        }
    }

    @SubscribeEvent
    public void onBlockPlace(BlockEvent.EntityPlaceEvent event) {
        if(placeBlockCausesFlee.get() && event.getEntity() instanceof LivingEntity) {
            setTargetForAnimalsInRange((LivingEntity) event.getEntity(), event.getEntity().getPosition(), placeBlockRange.get());

        }
    }


    @SubscribeEvent
    public void onEntityAttacked(LivingDamageEvent event) {
        if(event.getEntity() instanceof AnimalEntity && event.getSource().getTrueSource() instanceof LivingEntity) {
            AnimalEntity entity = (AnimalEntity) event.getEntity();
            entity.setRevengeTarget((LivingEntity) event.getSource().getTrueSource());

            if(groupFlee.get()) {
                setTargetForAnimalsInRange((LivingEntity) event.getSource().getTrueSource(), entity.getPosition(), groupFleeRange.get());
            }
        }
    }

    private static List<AnimalEntity> getAllAnimalsInRange(IWorld world, BlockPos centerPos, int range) {
        AxisAlignedBB aabb = new AxisAlignedBB(centerPos);
        return world.getEntitiesWithinAABB(AnimalEntity.class, aabb.grow(range));
    }

    private static void setTargetForAnimalsInRange(LivingEntity target, BlockPos centerPos, int range) {
        getAllAnimalsInRange(target.getEntityWorld(), centerPos, range).forEach(animalEntity -> animalEntity.setRevengeTarget(target));

    }
}
