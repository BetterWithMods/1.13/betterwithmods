/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.phenomenons;

import com.betterwithmods.core.References;
import com.betterwithmods.core.base.setup.ModBase;
import com.betterwithmods.core.base.setup.ModConfiguration;
import com.betterwithmods.phenomenons.config.Client;
import com.betterwithmods.phenomenons.config.Common;
import com.betterwithmods.phenomenons.generator.PhenonemonsDataProviders;
import com.betterwithmods.phenomenons.modules.creature.AdvancedFleeing;
import com.betterwithmods.phenomenons.modules.creature.AutoFeed;
import com.betterwithmods.phenomenons.modules.creature.GroupAgro;
import com.betterwithmods.phenomenons.modules.creature.HostilesHuntPassives;
import com.betterwithmods.phenomenons.modules.creature.squid.HostileSquids;
import com.betterwithmods.phenomenons.modules.tools.LogShattering;
import com.betterwithmods.phenomenons.modules.tools.LumberChopping;
import com.betterwithmods.phenomenons.modules.tools.SeedsRequireHoes;
import com.betterwithmods.phenomenons.modules.tools.TreeStumps;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;

@Mod(References.MODID_PHENOMENONS)
public class Phenomenons extends ModBase {

    public Phenomenons() {
        super(References.MODID_PHENOMENONS, References.NAME_PHENOMENONS);
        DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> {
            bus().addListener(this::onTextureSwitchPre);
            bus().addListener(this::onModelBake);
        });

        addSetup(new PhenonemonsDataProviders());
        //Monsters
        addModule(new HostilesHuntPassives());
        addModule(new HostileSquids());
        addModule(new GroupAgro());
        //Animals
        addModule(new AdvancedFleeing());
        addModule(new AutoFeed());
        //Tools
        addModule(new TreeStumps());
        addModule(new SeedsRequireHoes());
        addModule(new LogShattering());
        addModule(new LumberChopping());

        new ModConfiguration<>(Client::new, Common::new);
    }

}
