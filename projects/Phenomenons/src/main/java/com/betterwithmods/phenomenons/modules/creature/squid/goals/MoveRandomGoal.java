/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.phenomenons.modules.creature.squid.goals;

import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.passive.SquidEntity;
import net.minecraft.util.math.MathHelper;

public class MoveRandomGoal extends Goal {
    private final SquidEntity squid;

    public MoveRandomGoal(SquidEntity p_i48823_2_) {
        this.squid = p_i48823_2_;
    }

    public boolean shouldExecute() {
        return true;
    }

    public void tick() {
        int lvt_1_1_ = this.squid.getIdleTime();
        if (lvt_1_1_ > 100) {
            this.squid.setMovementVector(0.0F, 0.0F, 0.0F);
        } else if (this.squid.getRNG().nextInt(50) == 0 || !this.squid.isInWater() || !this.squid.hasMovementVector()) {
            float lvt_2_1_ = this.squid.getRNG().nextFloat() * 6.2831855F;
            float lvt_3_1_ = MathHelper.cos(lvt_2_1_) * 0.2F;
            float lvt_4_1_ = -0.1F + this.squid.getRNG().nextFloat() * 0.2F;
            float lvt_5_1_ = MathHelper.sin(lvt_2_1_) * 0.2F;
            this.squid.setMovementVector(lvt_3_1_, lvt_4_1_, lvt_5_1_);
        }

    }
}
