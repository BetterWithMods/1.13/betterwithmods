/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.phenomenons.modules.creature.squid;

import com.betterwithmods.core.Tags;
import com.betterwithmods.core.base.setup.ModuleBase;
import com.betterwithmods.core.utilties.EntityUtils;
import com.betterwithmods.core.utilties.ReflectionHelper;
import com.betterwithmods.phenomenons.modules.creature.squid.goals.MountGoal;
import com.betterwithmods.phenomenons.modules.creature.squid.goals.MoveRandomGoal;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.entity.ai.goal.MoveTowardsTargetGoal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.passive.SquidEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.event.entity.EntityEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class HostileSquids extends ModuleBase {

    private ForgeConfigSpec.ConfigValue<Boolean> squidTargetPlayers;

    public HostileSquids() {
        super("hostileSquids");
    }

    @Override
    public void configs(ForgeConfigSpec.Builder builder) {
        this.squidTargetPlayers = builder.comment("Decide whether squids will target players")
                .translation("config.betterwithmods_phenomenons.squidHostileToPlayers")
                .define("squidHostileToPlayers", true);
    }

    @SubscribeEvent
    public void onConstruction(EntityEvent.EntityConstructing event) {
        if(event.getEntity() instanceof SquidEntity) {
            SquidEntity entity = (SquidEntity) event.getEntity();
            entity.getAttributes().registerAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(2f);
        }
    }

    @SubscribeEvent
    public void addAI(EntityJoinWorldEvent event) {
        if (event.getEntity() instanceof SquidEntity) {
            SquidEntity squid = (SquidEntity) event.getEntity();
            //Give squid navigator
            ReflectionHelper.setValue(MobEntity.class, squid, "navigator", new PathNavigateSquid(squid, squid.world));

            EntityUtils.clear(squid.goalSelector);

            squid.targetSelector.addGoal(0, new NearestAttackableTargetGoal<>(squid, LivingEntity.class,
                    1,
                    false,
                    false,
                    e ->  EntityUtils.filter(e,  Tags.Entities.SQUID_TARGETS)));

            if(squidTargetPlayers.get()) {
                squid.targetSelector.addGoal(0, new NearestAttackableTargetGoal<>(squid, PlayerEntity.class, false));
            }


            squid.goalSelector.addGoal(1, new MountGoal(squid, 1, false));
            squid.goalSelector.addGoal(2, new MeleeAttackGoal(squid, 1, false));
            squid.goalSelector.addGoal(3, new MoveTowardsTargetGoal(squid, 1, 16));
            squid.goalSelector.addGoal(5, new MoveRandomGoal(squid));
        }
    }

    @Override
    public String getDescription() {
        return "Squids are now a hostile mobs with some interesting properties.";
    }
}
