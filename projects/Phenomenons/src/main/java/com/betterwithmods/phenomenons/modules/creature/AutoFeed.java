/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.phenomenons.modules.creature;

import com.betterwithmods.core.References;
import com.betterwithmods.core.Tags;
import com.betterwithmods.core.base.game.goals.FeedTargetGoal;
import com.betterwithmods.core.base.game.goals.PickupFeedGoal;
import com.betterwithmods.core.base.setup.ModuleBase;
import com.google.common.collect.Maps;
import net.minecraft.entity.CreatureEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.tags.Tag;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class AutoFeed extends ModuleBase {

    private Map<String, ForgeConfigSpec.ConfigValue<List<? extends String>>> feedIngredients;
    private ForgeConfigSpec.IntValue feedCooldownTime;

    private ForgeConfigSpec.BooleanValue mobsPathTowardFood;

    public AutoFeed() {
        super("autoFeed");
    }

    @Override
    public void configs(ForgeConfigSpec.Builder builder) {
        this.feedIngredients = Maps.newHashMap();
        this.feedCooldownTime = builder.comment("How long does a mob wait before eating again.")
                                    .translation("config.betterwithmods_phenomenons.autoFeed.feedCooldownTime")
                                    .defineInRange("feedCooldownTime", 3600, 0, Integer.MAX_VALUE);

        this.mobsPathTowardFood = builder.comment("Mobs will attempt to pathfind to the nearest food.")
                                    .translation("config.betterwithmods_phenomenons.autoFeed.mobsPathTowardFood")
                                    .define("mobsPathTowardFood", true);


    }

    private void addFeedConfig(ForgeConfigSpec.Builder builder, String entityName, Ingredient feed) {
        List<String> feedNames = Arrays.stream(feed.getMatchingStacks())
                .map(ItemStack::getItem)
                .map(Item::getRegistryName)
                .map(ResourceLocation::toString)
                .collect(Collectors.toList());

        this.feedIngredients.put(entityName, builder.defineList(entityName, feedNames, o -> true));
    }

    @SubscribeEvent
    public void addEntityAI(EntityJoinWorldEvent event) {
        if(event.getEntity() instanceof CreatureEntity) {
            ResourceLocation entityResource = event.getEntity().getType().getRegistryName();
            Tag<Item> entityFeedTag = Tags.Items.tag(References.MODID_PHENOMENONS, String.format("autofeed/%s/%s", entityResource.getNamespace(), entityResource.getPath()));
            if(entityFeedTag.getAllElements().size() != 0) {
                CreatureEntity entity = (CreatureEntity) event.getEntity();
                PickupFeedGoal pickupTask = new PickupFeedGoal(entity, entityFeedTag, feedCooldownTime.get());
                entity.goalSelector.addGoal(5, pickupTask);
                if(this.mobsPathTowardFood.get()) {
                    entity.targetSelector.addGoal(3, new FeedTargetGoal(entity, entityFeedTag, pickupTask,1));
                }
            }
        }
    }

    @Override
    public String getDescription() {
        return "Mobs will automatically eat appropriate food off the ground.";
    }
}
