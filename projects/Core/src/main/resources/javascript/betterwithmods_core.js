var Opcodes = Java.type("org.objectweb.asm.Opcodes");
var InsnList = Java.type("org.objectweb.asm.tree.InsnList");
var LabelNode = Java.type("org.objectweb.asm.tree.LabelNode");
var VarInsnNode = Java.type("org.objectweb.asm.tree.VarInsnNode");
var MethodInsnNode = Java.type("org.objectweb.asm.tree.MethodInsnNode");
var JumpInsnNode = Java.type("org.objectweb.asm.tree.JumpInsnNode");
var InsnNode = Java.type("org.objectweb.asm.tree.InsnNode");


var ASMAPI = Java.type("net.minecraftforge.coremod.api.ASMAPI");

var MethodSig = function (obfName, desc) {
    return {
        name: ASMAPI.mapMethod(obfName),
        desc: desc,
        equals: function (method) {
            return (method.name === this.name) && method.desc === this.desc;
        }
    }
};

var HOOKS = 'com/betterwithmods/core/base/game/Hooks';


function info(text) {
    print('[BWM Core] ' + text)
}

function transformBuoyancy(method) {


    var newInstructions = new InsnList();
    var ifSkipReturn = new LabelNode();

    try {
        newInstructions.add(new VarInsnNode(Opcodes.ALOAD, 0)); //Load `this` ItemEntity
        newInstructions.add(new MethodInsnNode(Opcodes.INVOKESTATIC, HOOKS, 'onItemEntityFloat', '(Lnet/minecraft/entity/item/ItemEntity;)Z', false));
        newInstructions.add(new JumpInsnNode(Opcodes.IFEQ, ifSkipReturn));
        newInstructions.add(new LabelNode());
        newInstructions.add(new InsnNode(Opcodes.RETURN));
        newInstructions.add(ifSkipReturn);

        method.instructions.insertBefore(method.instructions.getFirst(), newInstructions);
        info('Successfully Transformed ItemEntity.' + method.name);
    } catch (e) {

    }
}



function transformPlayerList(method) {
    var targetOwner = "net/minecraft/network/play/ServerPlayNetHandler";
    var targetName = "sendPacket";
    var targetDesc = "(Lnet/minecraft/network/IPacket;)V";

    var packetCount = 0;

    var afterThis = null;
    info(method.instructions.size());
    for (var i = 0; i < method.instructions.size(); i++) {
        var instruction = method.instructions.get(i);
        var type = instruction.getType();
        if(type === 5) { //METHOD INSN NODE
            var name = instruction.name;
            if(name === targetName) {
                if(packetCount === 4) {
                    afterThis = instruction;
                    break;
                }
                packetCount++;
            }
        }
    }
    info("Insertion Point:" + afterThis);
    if(afterThis != null) {
        var newInstructions = new InsnList();
        newInstructions.add(new VarInsnNode(Opcodes.ALOAD, 2)); //Load playerEntity
        newInstructions.add(new MethodInsnNode(Opcodes.INVOKESTATIC, HOOKS, 'onPlayerLoggingIn', '(Lnet/minecraft/entity/player/PlayerEntity;)V', false));
        method.instructions.insert(afterThis, newInstructions);
        info('Successfully Transformed PlayerList with onPlayerLoggingIn hook.' + method.name);
    }
}

function initializeCoreMod() {
    return {
        "ItemEntityTransformer": {
            "target": {
                "type": "CLASS",
                "name": "net.minecraft.entity.item.ItemEntity"
            },
            "transformer": function (classNode) {
                //public net.minecraft.entity.item.ItemEntity func_203043_v()V # applyFloatMotion
                var sig = MethodSig("func_203043_v", "()V");

                for (var i in classNode.methods) {
                    var method = classNode.methods[i];
                    if (sig.equals(method)) {
                        transformBuoyancy(method);
                    }
                }
                return classNode;
            }
        },
        "PlayerList": {
            "target": {
                "type": "CLASS",
                "name": "net.minecraft.server.management.PlayerList"
            },
            "transformer": function (classNode) {
                var sig = MethodSig("func_72355_a", "(Lnet/minecraft/network/NetworkManager;Lnet/minecraft/entity/player/ServerPlayerEntity;)V");
                for (var i in classNode.methods) {
                    var method = classNode.methods[i];
                    if (sig.equals(method)) {
                        info(sig.name + "," + method.name);
                        transformPlayerList(method);
                    }
                }
                return classNode;
            }
        }
    }
}
