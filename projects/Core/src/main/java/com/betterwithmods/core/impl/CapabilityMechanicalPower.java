/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.impl;

import com.betterwithmods.core.api.mech.IMechanicalPower;
import com.betterwithmods.core.api.mech.IPower;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class CapabilityMechanicalPower  {

    @CapabilityInject(IMechanicalPower.class)
    public static Capability<IMechanicalPower> MECHANICAL_POWER = null;

    public static void register() {
        CapabilityManager.INSTANCE.register(IMechanicalPower.class, new Capability.IStorage<IMechanicalPower>() {

            @Nullable
            @Override
            public INBT writeNBT(Capability<IMechanicalPower> capability, IMechanicalPower instance, Direction side) {
                return null;
            }

            @Override
            public void readNBT(Capability<IMechanicalPower> capability, IMechanicalPower instance, Direction side, INBT nbt) {

            }
        }, MechanicalPower::new);
    }

    public static class MechanicalPower implements IMechanicalPower {

        @Nullable
        @Override
        public IPower getOutput(@Nonnull IWorld world, @Nonnull BlockPos pos, @Nonnull Direction facing) {
            return Power.ZERO;
        }

        @Nullable
        @Override
        public IPower getInput(@Nonnull IWorld world, @Nonnull BlockPos pos, @Nonnull Direction facing) {
            return Power.ZERO;
        }

        @Nonnull
        @Override
        public IPower getMaximumInput(@Nonnull Direction facing) {
            return Power.ZERO;
        }

        @Nonnull
        @Override
        public IPower getMinimumInput() {
            return Power.ZERO;
        }

        @Override
        public boolean canInputFrom(Direction facing) {
            return false;
        }

        @Override
        public boolean overpower(World world, BlockPos pos) {
            return false;
        }
    }

}
