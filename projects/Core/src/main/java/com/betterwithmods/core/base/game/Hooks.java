/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game;

import com.betterwithmods.core.base.game.events.ItemEntityFloatEvent;
import com.betterwithmods.core.base.game.events.PlayerLoggingInEvent;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraftforge.common.MinecraftForge;

public class Hooks {

    public static boolean onItemEntityFloat(ItemEntity itemEntity) {
        return MinecraftForge.EVENT_BUS.post(new ItemEntityFloatEvent(itemEntity));
    }

    public static void onPlayerLoggingIn(PlayerEntity playerEntity) {
        MinecraftForge.EVENT_BUS.post(new PlayerLoggingInEvent(playerEntity));
    }

//    public static void onTextureRegistration(AtlasTexture map, Set<ResourceLocation> textures) {
//        ModLoader.get().postEvent(new TextureRegistrationEvent(map, textures));
//    }

//    //FIXME remove immedaitely when forge changes TextureStitchEvent to do this
//    @Deprecated
//    public static class TextureRegistrationEvent extends Event {
//        private final AtlasTexture map;
//        private final Set<ResourceLocation> textures;
//
//        public TextureRegistrationEvent(AtlasTexture map, Set<ResourceLocation> textures) {
//            this.map = map;
//            this.textures = textures;
//        }
//
//        public Set<ResourceLocation> getTextures() {
//            return textures;
//        }
//
//        public AtlasTexture getMap() {
//            return map;
//        }
//
//        public void registerTexture(ResourceLocation texture) {
//            this.textures.add(texture);
//        }
//    }

}
