/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class ReflectionHelper {

    public static Object invokeMethod(Class<?> clazz, Object target, String methodName, Object... parameters) {
        try {
            Method method = null;
            if (parameters != null && parameters.length > 0) {
                Class<?>[] clazzes = (Class<?>[]) Arrays.stream(parameters).map(Object::getClass).toArray();
                method = clazz.getDeclaredMethod(methodName, clazzes);
            } else {
                method = clazz.getDeclaredMethod(methodName);
            }
            if (method != null) {
                return getObjectMethod(method, target, parameters);
            }
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object getValue(Class<?> clazz, Object target, String fieldName) {
        try {
            Field field = clazz.getDeclaredField(fieldName);
            return getObjectField(field, target);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setValue(Class<?> clazz, Object target, String fieldName, Object value) {
        try {
            Field field = clazz.getDeclaredField(fieldName);
            if (target == null) {
                setFinalStatic(field, value);
            } else {
                setObjectField(field, target, value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Object getStaticField(Class<?> clazz, Object target, String fieldName) {
        try {
            Field field = clazz.getDeclaredField(fieldName);
            if (target == null) {
                return getFinalStatic(field);
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setFinalStatic(Field field, Object newValue) throws Exception {
        field.setAccessible(true);
        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        field.set(null, newValue);
    }

    public static void setObjectField(Field field, Object target, Object newValue) throws IllegalAccessException {
        field.setAccessible(true);
        field.set(target, newValue);
    }


    public static Object getObjectField(Field field, Object target) throws IllegalAccessException {
        field.setAccessible(true);
        return field.get(target);
    }




    public static Object getObjectMethod(Method method, Object target, Object... parameters) throws InvocationTargetException, IllegalAccessException {
        method.setAccessible(true);
        return method.invoke(target, parameters);
    }

    public static Object getFinalStatic(Field field) throws SecurityException, NoSuchFieldException, IllegalAccessException {
        field.setAccessible(true);
        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        return field.get(null);
    }

}
