/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.block.plant;

import com.betterwithmods.core.utilties.BooleanOperator;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.RedstoneLampBlock;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldReader;

import java.util.Arrays;
import java.util.Collection;

public class GrowthConditions {

    public static final BooleanOperator
            OR = (a, b) -> a || b,
            AND = (a, b) -> a && b;


    public static boolean hasSun(IWorldReader world, BlockPos pos, BlockState state) {
        return world.canBlockSeeSky(pos);
    }

    public static boolean hasLamp(IWorldReader world, BlockPos pos, BlockState state) {
        BlockState above = world.getBlockState(pos.up(2));
        return above.getBlock() == Blocks.REDSTONE_LAMP && above.get(RedstoneLampBlock.LIT);
    }

    public static boolean isFertileGround(IWorldReader world, BlockPos pos, BlockState state) {
        return world.getBlockState(pos.down()).isFertile(world, pos.down());
    }

    public static IGrowthCondition aggregate(BooleanOperator reducer, Collection<IGrowthCondition> conditions) {
        return (world, pos, state) -> conditions.stream().map(c -> c.canGrow(world, pos, state)).reduce(reducer).orElse(false);
    }

    public static IGrowthCondition aggregate(BooleanOperator reducer, IGrowthCondition... conditions) {
        return aggregate(reducer, Arrays.asList(conditions));
    }


    public static class LightLevel implements IGrowthCondition {
        private int light;

        public LightLevel(int light) {
            this.light = light;
        }

        @Override
        public boolean canGrow(IWorldReader world, BlockPos pos, BlockState state) {
            return world.getLightSubtracted(pos.up(), 0) >= light;
        }
    }

}
