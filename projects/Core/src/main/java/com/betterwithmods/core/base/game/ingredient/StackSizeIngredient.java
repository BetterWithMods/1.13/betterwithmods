/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.ingredient;

import com.betterwithmods.core.Core;
import com.betterwithmods.core.References;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.IIngredientSerializer;
import net.minecraftforge.registries.ForgeRegistries;

import javax.annotation.Nullable;
import java.util.function.BiPredicate;
import java.util.stream.Stream;

public class StackSizeIngredient extends Ingredient {

    private static final Multimap<Integer, Item> ITEM_STACKSIZE = MultimapBuilder.hashKeys().hashSetValues().build();
    private ItemStack[] matchingStacks;

    private int size;
    private Condition condition;

    public StackSizeIngredient(int size, Condition condition) {
        super(Stream.empty());
        this.size = size;
        this.condition = condition;

        findItemWithStackSize(size);
    }

    public static class Serializer implements IIngredientSerializer<StackSizeIngredient> {
        public static ResourceLocation NAME = new ResourceLocation(References.MODID_CORE, "stacksize_ingredient");

        @Override
        public StackSizeIngredient parse(PacketBuffer buffer) {
            int count = buffer.readByte();
            Condition condition = Condition.getValue(buffer.readString());
            return new StackSizeIngredient(count, condition);
        }

        @Override
        public StackSizeIngredient parse(JsonObject json) {
            int count = JSONUtils.getInt(json, "size");
            Condition condition = Condition.getValue(JSONUtils.getString(json, "condition"));
            return new StackSizeIngredient(count, condition);
        }

        @Override
        public void write(PacketBuffer buffer, StackSizeIngredient ingredient) {
            buffer.writeByte(ingredient.size);
            buffer.writeString(ingredient.condition.getName());
        }

        public static void write(StackSizeIngredient ingredient, JsonObject object) {
            object.addProperty("size", ingredient.size);
            object.addProperty("condition", ingredient.condition.getName());
        }

    }

    @Override
    public JsonElement serialize() {
        JsonObject jsonobject = new JsonObject();
        jsonobject.addProperty("type", StackSizeIngredient.Serializer.NAME.toString());
        Serializer.write(this, jsonobject);
        return jsonobject;
    }

    @Override
    public boolean test(@Nullable ItemStack stack) {
        if (stack != null) {
            return condition.test(stack.getMaxStackSize(), size);
        }
        return false;
    }


    private void determineMatchingStacks() {
        if (this.matchingStacks == null) {
            this.matchingStacks = ITEM_STACKSIZE.get(size).stream().map(ItemStack::new).toArray(ItemStack[]::new);
        }
    }

    @Override
    public ItemStack[] getMatchingStacks() {
        determineMatchingStacks();
        return matchingStacks;
    }

    @Override
    public IIngredientSerializer<? extends Ingredient> getSerializer() {
        return Core.STACKSIZE_INGREDIENT;
    }

    private static void findItemWithStackSize(int size) {
        if(!ITEM_STACKSIZE.containsKey(size)) {
            for (Item item : ForgeRegistries.ITEMS.getValues()) {
                if (item.getMaxStackSize() == size) {
                    ITEM_STACKSIZE.put(size, item);
                }
            }
        }
    }

    public enum Condition implements IStringSerializable {
        GREATER((i, j) -> i > j),
        LESS((i, j) -> i < j),
        GREATER_EQUAL((i, j) -> i >= j),
        LESS_EQUAL((i, j) -> i <= j),
        EQUAL(Integer::equals);

        private BiPredicate<Integer, Integer> condition;

        Condition(BiPredicate<Integer, Integer> condition) {
            this.condition = condition;
        }

        public boolean test(int current, int size) {
            return condition.test(current, size);
        }

        @Override
        public String getName() {
            return name().toUpperCase();
        }

        public static Condition getValue(String name) {
            return valueOf(name.toUpperCase());
        }


    }

}
