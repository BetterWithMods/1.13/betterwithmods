/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.ingredient;

import com.betterwithmods.core.Core;
import com.betterwithmods.core.References;
import com.betterwithmods.core.utilties.ItemUtils;
import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.IIngredientSerializer;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StackIngredient extends Ingredient implements IStackIngredient {

    private final List<IngredientStack> ingredients;
    private ItemStack[] cachedStacks;

    public StackIngredient(List<IngredientStack> ingredients) {
        super(Stream.of());
        this.ingredients = ingredients;
    }

    public static StackIngredient from(IItemProvider provider, int count) {
        return from(new IngredientStack(provider,count));
    }

    public static StackIngredient from(IngredientStack... stacks) {
        return new StackIngredient(Lists.newArrayList(stacks));
    }

    public static StackIngredient fromStacks(ItemStack... stacks) {
        List<IngredientStack> s = Lists.newArrayList();
        for (ItemStack stack : stacks) {
            s.add(new IngredientStack(Ingredient.fromStacks(stack), stack.getCount()));
        }
        return new StackIngredient(s);
    }

    @Nonnull
    @Override
    public ItemStack[] getMatchingStacks() {
        if (cachedStacks == null) {
            List<ItemStack> stacks = Lists.newArrayList();
            for (IngredientStack i : ingredients) {
                Arrays.stream(i.getIngredient().getMatchingStacks())
                        .map(s -> ItemUtils.withCount(s, i.getCount()))
                        .collect(Collectors.toCollection(() -> stacks));
            }
            cachedStacks = new ItemStack[stacks.size()];
            stacks.toArray(cachedStacks);
        }
        return cachedStacks;
    }

    @Override
    public boolean test(@Nullable ItemStack stack) {
        if (stack != null) {
            for (IngredientStack i : ingredients) {
                if (i.test(stack)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public int getCount(@Nonnull ItemStack stack) {
        for (IngredientStack i : ingredients) {
            if (i.test(stack)) {
                return i.getCount();
            }
        }
        return 0;
    }

    public static class Serializer implements IIngredientSerializer<StackIngredient> {
        public static ResourceLocation NAME = new ResourceLocation(References.MODID_CORE, "stack_ingredient");

        @Nonnull
        @Override
        public StackIngredient parse(PacketBuffer buffer) {
            int size = buffer.readInt();
            List<IngredientStack> stacks = Lists.newArrayList();
            for (int i = 0; i < size; i++) {
                stacks.add(IngredientStack.read(buffer));
            }
            return new StackIngredient(stacks);
        }

        @Nonnull
        @Override
        public StackIngredient parse(JsonObject json) {
            JsonArray stacks = json.get("stacks").getAsJsonArray();
            List<IngredientStack> ingredientStacks = Lists.newArrayList();
            for (JsonElement e : stacks) {
                IngredientStack s = IngredientStack.read(e);
                ingredientStacks.add(s);
            }
            return new StackIngredient(ingredientStacks);
        }

        @Override
        public void write(PacketBuffer buffer, StackIngredient ingredient) {
            int size = ingredient.ingredients.size();
            buffer.writeInt(size);
            for (IngredientStack stack : ingredient.ingredients) {
                IngredientStack.write(buffer, stack);
            }
        }

        public static void write(StackIngredient ingredient, JsonObject object) {
            JsonArray stacks = new JsonArray();
            for (IngredientStack stack : ingredient.ingredients) {
                JsonObject o = new JsonObject();
                IngredientStack.write(stack, o);
                stacks.add(o);
            }
            object.add("stacks", stacks);
        }
    }

    @Nonnull
    @Override
    public JsonElement serialize() {
        JsonObject object = new JsonObject();
        object.addProperty("type", Serializer.NAME.toString());
        Serializer.write(this, object);
        return object;
    }

    @Override
    public IIngredientSerializer<? extends Ingredient> getSerializer() {
        return Core.STACK_INGREDIENT;
    }




}
