/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.ingredient;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mojang.datafixers.util.Pair;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tags.Tag;
import net.minecraft.util.IItemProvider;

import java.util.function.Predicate;

public class IngredientStack extends Pair<Ingredient, Integer> implements Predicate<ItemStack> {

    @Deprecated //Only use for DataProviders
    public IngredientStack(Tag<Item> tag, int c) {
        this(new TagIngredient(tag), c);
    }

    public IngredientStack(IItemProvider item, int c) {
        this(Ingredient.fromItems(item), c);
    }

    public IngredientStack(Ingredient i, int c) {
        super(i, c);
    }

    public Ingredient getIngredient() {
        return getFirst();
    }

    public int getCount() {
        return getSecond();
    }

    public static IngredientStack read(JsonElement json) {
        JsonObject object = json.getAsJsonObject();
        Ingredient parent = Ingredient.deserialize(object.get("parent"));
        int count = object.get("count").getAsInt();
        return new IngredientStack(parent, count);
    }

    public static IngredientStack read(PacketBuffer buffer) {
        Ingredient parent = Ingredient.read(buffer);
        int count = buffer.readInt();
        return new IngredientStack(parent, count);
    }

    public static void write(PacketBuffer buffer, IngredientStack ingredientStack) {
        ingredientStack.getIngredient().write(buffer);
        buffer.writeInt(ingredientStack.getCount());
    }

    public static <T extends IngredientStack> void write(T t, JsonElement jsonElement) {
        JsonObject object = jsonElement.getAsJsonObject();
        object.add("parent", t.getIngredient().serialize());
        object.addProperty("count", t.getCount());
    }

    @Override
    public boolean test(ItemStack stack) {
        return getIngredient().test(stack) && stack.getCount() >= getCount();
    }
}
