/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.setup;

import com.google.common.collect.Lists;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.ColorHandlerEvent;
import net.minecraftforge.client.event.ModelBakeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLLoadCompleteEvent;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;

import java.util.Collection;

public class SetupLoader implements ISetup {
    private Collection<ISetup> collection = Lists.newArrayList();

    @Override
    public void onCommonSetup(FMLCommonSetupEvent event) {
        collection.forEach(setup -> setup.onCommonSetup(event));
    }

    @Override
    public void onClientSetup(FMLClientSetupEvent event) {
        collection.forEach(setup -> setup.onClientSetup(event));
    }

    @Override
    public void onLoadComplete(FMLLoadCompleteEvent event) {
        collection.forEach(setup -> setup.onLoadComplete(event));
    }

    @Override
    public void onServerStarting(FMLServerStartingEvent event) {
        collection.forEach(setup -> setup.onServerStarting(event));
    }

    @Override
    public void onGatherData(GatherDataEvent event) {
        collection.forEach(setup -> setup.onGatherData(event));
    }

    @Override
    public void newRegistries(RegistryEvent.NewRegistry event) {
        collection.forEach(setup -> setup.newRegistries(event));
    }

    @Override
    public void registerModels(ModelRegistryEvent event) {
        collection.forEach(setup -> setup.registerModels(event));
    }

    @Override
    public void onBlockColors(ColorHandlerEvent.Block event) {
        collection.forEach(setup -> setup.onBlockColors(event));
    }

    @Override
    public void onItemColors(ColorHandlerEvent.Item event) {
        collection.forEach(setup -> setup.onItemColors(event));
    }


    @OnlyIn(Dist.CLIENT)
    @Override
    public void onTextureSwitchPre(TextureStitchEvent.Pre event) {
        collection.forEach(setup -> setup.onTextureSwitchPre(event));
    }

    @Override
    public void onModelBake(ModelBakeEvent event) {
        collection.forEach(setup -> setup.onModelBake(event));
    }

    @Override
    public void onConfigure(CollectConfigEvent event) {
        collection.forEach(setup -> setup.onConfigure(event));
    }

    public void addSetup(ISetup setup) {
        this.collection.add(setup);
    }
}
