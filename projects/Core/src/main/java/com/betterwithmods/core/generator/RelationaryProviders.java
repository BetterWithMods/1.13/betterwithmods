/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.generator;

import com.betterwithmods.core.relationary.Relation;
import com.betterwithmods.core.relationary.Shape;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.util.ResourceLocation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;

public abstract class RelationaryProviders implements IDataProvider {
    private static final Set<ResourceLocation> shapes = Sets.newHashSet();


    private static final Logger LOGGER = LogManager.getLogger();
    private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().create();
    private final DataGenerator generator;

    public RelationaryProviders(DataGenerator generatorIn) {
        this.generator = generatorIn;
    }

    @Override
    public void act(DirectoryCache cache) throws IOException {
        Path path = this.generator.getOutputFolder();
        registerShapes(shape -> {
            if (!shapes.add(shape.getRegistryName())) {
                throw new IllegalStateException("Duplicate shape registered:" + shape.getRegistryName());
            } else {
                this.saveJson(cache, shape.read(), path.resolve("data/" + shape.getRegistryName().getNamespace() + "/shapes/" + shape.getRegistryName().getPath() + ".json"));
            }
        });
        Set<ResourceLocation> relations = Sets.newHashSet();
        registerRelations(relation -> {
            if (!relations.add(relation.getRegistryName())) {
                throw new IllegalStateException("Duplicate relation registered:" + relation.getRegistryName());
            } else {
                this.saveJson(cache, relation.write(shapes), path.resolve("data/" + relation.getRegistryName().getNamespace() + "/relations/" + relation.getRegistryName().getPath() + ".json"));
            }
        });
    }

    private void saveJson(DirectoryCache cache, JsonObject json, Path pathIn) {
        try {
            String s = GSON.toJson(json);
            String s1 = HASH_FUNCTION.hashUnencodedChars(s).toString();
            if (!Objects.equals(cache.getPreviousHash(pathIn), s1) || !Files.exists(pathIn)) {
                Files.createDirectories(pathIn.getParent());
                try (BufferedWriter bufferedwriter = Files.newBufferedWriter(pathIn)) {
                    bufferedwriter.write(s);
                }
            }
            cache.func_208316_a(pathIn, s1);
        } catch (IOException ioexception) {
            LOGGER.error("Couldn't save relation {}", pathIn, ioexception);
        }
    }


    public abstract void registerRelations(Consumer<Relation> consumer);

    public abstract void registerShapes(Consumer<Shape> consumer);

    @Override
    public String getName() {
        return "Relationary";
    }
}
