/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.impl.crafting.input;

import com.betterwithmods.core.Registrars;
import com.betterwithmods.core.api.crafting.input.IInput;
import com.betterwithmods.core.api.crafting.input.IInputSerializer;
import com.betterwithmods.core.api.crafting.input.IRecipeContext;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipe;
import com.betterwithmods.core.base.game.ingredient.BlockStateIngredient;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

import javax.annotation.Nonnull;

public class BlockStateInput<R extends IMultiplexRecipe<BlockState, R>> implements IInput<BlockState, R> {

    @Nonnull
    private final Ingredient base;
    @Nonnull
    private final BlockStateIngredient input;

    public BlockStateInput(@Nonnull ItemStack stack) {
        this(Ingredient.fromStacks(stack));
    }

    public BlockStateInput(@Nonnull Ingredient base) {
        this.base = base;
        this.input = new BlockStateIngredient(base);
    }

    @Override
    public boolean matches(IWorld world, BlockPos pos, R recipe, IRecipeContext<BlockState, R> context) {
        BlockState state = context.getState(world, pos);
        return input.test(state);
    }

    @Override
    public NonNullList<Ingredient> getIngredients() {
        NonNullList<Ingredient> list = NonNullList.create();
        list.add(getBase());
        return list;
    }

    @Override
    public void consume(World world, BlockPos pos, R recipe, IRecipeContext<BlockState, R> context) {
        world.removeBlock(pos, false);
    }

    @Override
    public IInputSerializer<?, ?> getSerializer() {
        return Registrars.InputSerializers.BLOCKSTATE;
    }

    @Nonnull
    public Ingredient getBase() {
        return base;
    }
}
