/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.registration;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public abstract class Registrar<T extends IForgeRegistryEntry<T>> implements IRegistrar<T> {

    private static final List<IRegistrar<?>> REGISTRARS = Lists.newArrayList();
    private final Set<T> entries = Sets.newHashSet();
    protected final Logger logger;

    protected final String modid;

    public Registrar(String modid, Logger logger) {
        this.modid = modid;
        this.logger = logger;
        FMLJavaModLoadingContext.get().getModEventBus().register(this);
        REGISTRARS.add(this);
    }

    public void onRegister(RegistryEvent.Register<T> event) {
        registerAll(event.getRegistry());
    }

    public abstract void registerAll(IForgeRegistry<T> registry);

    public T register(IForgeRegistry<T> registry, ResourceLocation registryName, T value) {
        registry.register(value.setRegistryName(registryName));
        entries.add(value);
        return value;
    }

    public T register(IForgeRegistry<T> registry, String entryName, T value) {
        return this.register(registry, new ResourceLocation(this.modid, entryName), value);
    }

    public Logger getLogger() {
        return logger;
    }

    public Set<T> getEntries() {
        return entries;
    }
}

