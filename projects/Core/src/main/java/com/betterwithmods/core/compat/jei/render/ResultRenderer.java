/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.compat.jei.render;

import com.betterwithmods.core.api.crafting.result.IResult;
import com.betterwithmods.core.api.crafting.result.IResultRenderer;
import mezz.jei.api.ingredients.IIngredientRenderer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;

import javax.annotation.Nullable;
import java.util.List;
import java.util.stream.Collectors;

public class ResultRenderer<T extends IResult<?>> implements IIngredientRenderer<T> {

    @Override
    public void render(int x, int y, @Nullable T result) {
        Minecraft minecraft = Minecraft.getInstance();
        if (result != null) {
            IResultRenderer renderer = result.getRenderer();
            FontRenderer font = getFontRenderer(minecraft, result);
            if (renderer != null) {
                renderer.render(x, y, result, font);
            }
        }
    }

    @Override
    public List<String> getTooltip(T result, ITooltipFlag tooltipFlag) {
        ItemStack ingredient = result.displayStack();
        List<ITextComponent> textComponents = result.tooltip();
        textComponents.addAll(ingredient.getTooltip(null, tooltipFlag));
        return textComponents.stream().map(ITextComponent::getFormattedText).collect(Collectors.toList());
    }


}
