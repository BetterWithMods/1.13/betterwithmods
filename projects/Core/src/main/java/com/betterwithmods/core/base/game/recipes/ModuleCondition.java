package com.betterwithmods.core.base.game.recipes;

import com.betterwithmods.core.References;
import com.betterwithmods.core.base.setup.ModuleLoader;
import com.google.gson.JsonObject;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.conditions.ICondition;
import net.minecraftforge.common.crafting.conditions.IConditionSerializer;


public class ModuleCondition implements ICondition {

    private static final ResourceLocation NAME = new ResourceLocation(References.MODID_CORE, "module");

    private final String mod;
    private String module;

    public ModuleCondition(String loader, String module) {
        this.mod = loader;
        this.module = module;
    }

    @Override
    public ResourceLocation getID() {
        return NAME;
    }

    @Override
    public boolean test() {
        return ModuleLoader.getModuleCondition(mod,module);
    }

    public static class Serializer implements IConditionSerializer<ModuleCondition> {

        public static final Serializer INSTANCE = new Serializer();

        @Override
        public void write(JsonObject json, ModuleCondition value) {
            json.addProperty("mod", value.mod);
            json.addProperty("module", value.module);
        }

        @Override
        public ModuleCondition read(JsonObject json) {
            return new ModuleCondition(json.get("mod").getAsString(), json.get("module").getAsString());
        }

        @Override
        public ResourceLocation getID() {
            return NAME;
        }
    }
}
