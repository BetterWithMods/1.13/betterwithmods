/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.relationary;

import com.betterwithmods.core.utilties.DeserializeUtils;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.ShapedRecipe;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@SuppressWarnings("UnstableApiUsage")
public class Relation {
    private static final Logger LOGGER = LogManager.getLogger("Relation");

    protected static int VERSION;

    private ResourceLocation registryName;

    private final Multimap<Shape, ItemStack> shapeToItemstacks;
    private final Multimap<HashableItemStack, Shape> itemStackToShape;

    public Relation(String modid, String name) {
        this(new ResourceLocation(modid, name));
    }

    public Relation(ResourceLocation id) {
        this.registryName = id;
        shapeToItemstacks = MultimapBuilder.hashKeys().arrayListValues().build();
        itemStackToShape = MultimapBuilder.hashKeys().hashSetValues().build();
    }

    private static void registerEntry(Relation relation, JsonObject entry) {

        ResourceLocation shapeName = new ResourceLocation(entry.get("shape").getAsString());
        if (!RelationaryManager.SHAPES.containsKey(shapeName)) {
            throw new IllegalStateException("Unregistered shape:" + shapeName + " used in relationship: " + relation.getRegistryName());
        }

        List<ItemStack> stacks = Lists.newArrayList();
        JsonArray items = JSONUtils.getJsonArray(entry, "items");
        for (JsonElement i : items) {
            JsonObject item = i.getAsJsonObject();
            try {
                ItemStack stack = ShapedRecipe.deserializeItem(item);
                stacks.add(stack);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
        Shape shape = RelationaryManager.SHAPES.get(shapeName);
        relation.addShape(shape, stacks);
    }


    public Collection<ItemStack> fromShape(Shape shape) {
        Collection<ItemStack> stacks = getStacks(shape);
        return stacks.stream().map(ItemStack::copy).collect(ImmutableList.toImmutableList());
    }

    public ItemStack first(Shape shape, int count) {
        ItemStack first = fromShape(shape).stream().findFirst().orElse(ItemStack.EMPTY);
        first.setCount(count);
        return first;
    }

    public Collection<Shape> fromStack(ItemStack stack) {
        return itemStackToShape.get(new HashableItemStack(stack));
    }

    public Relation merge(Relation relation) {
        for (Shape shape : relation.shapeToItemstacks.keySet()) {
            addShape(shape, relation.getStacks(shape));
        }
        return this;
    }

    public <T> Relation addShape(Shape shape, Collection<T> providers, Function<T, ItemStack> stackFunction) {
        return addShape(shape, providers.stream().map(stackFunction).collect(Collectors.toList()));
    }

    public Relation addShape(Shape shape, ItemStack... stacks) {
        return addShape(shape, Lists.newArrayList(stacks));
    }

    public Relation addShape(@Nonnull Shape shape, Collection<ItemStack> stacks) {
        this.shapeToItemstacks.putAll(shape, stacks);
        for (ItemStack stack : stacks) {
            HashableItemStack hashed = new HashableItemStack(stack);
            itemStackToShape.put(hashed, shape);
            RelationaryManager.ITEM_STACK_TO_RELATION.put(hashed, this);
        }
        return this;
    }


    @Override
    public int hashCode() {
        return getRegistryName().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Relation) {
            return ((Relation) o).getRegistryName().equals(this.getRegistryName());
        }
        return false;
    }

    public ResourceLocation getRegistryName() {
        return registryName;
    }

    public Collection<ItemStack> getStacks(Shape shape) {
        return shapeToItemstacks.get(shape).stream().filter(s -> !s.isEmpty()).collect(Collectors.toList());
    }

    public void write(PacketBuffer buffer) {
        buffer.writeResourceLocation(getRegistryName());
        LOGGER.debug("Write");
        LOGGER.debug(getRegistryName());
        buffer.writeInt(shapeToItemstacks.keySet().size());

        for (Shape shape : shapeToItemstacks.keySet()) {
            LOGGER.debug("\tStart Shape");
            buffer.writeResourceLocation(shape.getRegistryName());
            LOGGER.debug("\t" + shape.getRegistryName());
            Collection<ItemStack> stacks = getStacks(shape);
            LOGGER.debug("\tStacks:" + stacks.size());
            buffer.writeInt(stacks.size());
            for (ItemStack stack : stacks) {
                LOGGER.debug("\t\t" + stack);
                buffer.writeItemStack(stack);
            }
        }
        LOGGER.debug("End Write");
    }

    public static Relation read(PacketBuffer buffer) {
        LOGGER.debug("Read");
        Relation relation = new Relation(buffer.readResourceLocation());
        int shapeCount = buffer.readInt();
        LOGGER.debug(relation.getRegistryName());
        LOGGER.debug("Shapes:" + shapeCount);
        for (int i = 0; i < shapeCount; i++) {
            Shape shape = RelationaryManager.getOrCreateShape(buffer.readResourceLocation());
            LOGGER.debug("\t" + shape.getRegistryName());
            List<ItemStack> stacks = Lists.newArrayList();
            int stackCount = buffer.readInt();
            LOGGER.debug("\tStacks:" + stackCount);
            for (int j = 0; j < stackCount; j++) {
                ItemStack stack = buffer.readItemStack();
                LOGGER.debug("\t\t" + i + ":" + stack.getItem().getRegistryName());
                stacks.add(stack);
            }
            relation.addShape(shape, stacks);
        }
        LOGGER.debug("End Read");
        return relation;
    }

    public static void read(Relation relation, JsonObject jsonobject) {
        JsonArray entries = jsonobject.get("relations").getAsJsonArray();
        for (JsonElement e : entries) {
            JsonObject entry = e.getAsJsonObject();
            registerEntry(relation, entry);
        }
    }

    public JsonObject write(Collection<ResourceLocation> validShapes) {
        JsonObject object = new JsonObject();
        JsonArray relations = new JsonArray();
        for (Shape shape : shapeToItemstacks.keySet()) {
            if (!validShapes.contains(shape.getRegistryName()))
                throw new IllegalStateException("Attempt to add relation for unregistered shape: " + shape.getRegistryName());
            JsonObject e = new JsonObject();
            e.addProperty("shape", shape.getRegistryName().toString());
            Collection<ItemStack> stacks = shapeToItemstacks.get(shape);
            JsonArray items = new JsonArray();
            for (ItemStack stack : stacks) {
                JsonObject o = new JsonObject();
                DeserializeUtils.serializeItemStack(stack, o);
                items.add(o);
            }
            e.add("items", items);
            relations.add(e);
        }
        object.add("relations", relations);
        return object;
    }


    public static class Wrapper extends Relation {
        private int lastVersion = -1;

        private Relation cachedRelation;

        public Wrapper(ResourceLocation id) {
            super(id);
        }

        private void validateCache() {
            if (lastVersion != VERSION) {
                this.cachedRelation = RelationaryManager.getOrCreateRelation(getRegistryName());
                this.lastVersion = VERSION;
            }
        }

        @Override
        public Collection<ItemStack> fromShape(Shape shape) {
            validateCache();
            return cachedRelation.fromShape(shape);
        }

        @Override
        public Collection<Shape> fromStack(ItemStack stack) {
            validateCache();
            return cachedRelation.fromStack(stack);
        }
    }

}
