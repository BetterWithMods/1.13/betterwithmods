/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.modules;

import com.betterwithmods.core.base.setup.ModuleBase;
import com.betterwithmods.core.utilties.ReflectionHelper;
import net.minecraft.client.gui.screen.EditStructureScreen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.tileentity.StructureBlockTileEntity;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class FixStructureBlocks extends ModuleBase {
    public FixStructureBlocks() {
        super("fix_structure_blocks");
    }

    @Override
    public void configs(ForgeConfigSpec.Builder builder) {

    }

    @OnlyIn(Dist.CLIENT)
    @SubscribeEvent
    public void onOpenScreen(GuiScreenEvent event) {
        if(event.getGui() instanceof EditStructureScreen) {
            EditStructureScreen screen = (EditStructureScreen) event.getGui();
            TextFieldWidget widget = (TextFieldWidget) ReflectionHelper.getValue(EditStructureScreen.class, screen, "nameEdit");
            if(widget != null) {
                widget.setMaxStringLength(10000);
                StructureBlockTileEntity tile = (StructureBlockTileEntity) ReflectionHelper.getValue(EditStructureScreen.class, screen, "tileStructure");
                if(tile != null && !tile.getName().isEmpty()) {
                    widget.setText(tile.getName());
                }
            }

        }
    }

    @Override
    public String getDescription() {
        return "The text box has a character limit...";
    }
}
