/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.generator;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mojang.datafixers.util.Pair;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.loot.LootParameterSet;
import net.minecraft.world.storage.loot.LootTable;
import net.minecraft.world.storage.loot.LootTableManager;
import net.minecraft.world.storage.loot.ValidationResults;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

public abstract class LootTableProvider implements IDataProvider {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();
    private final DataGenerator generator;
    public List<Pair<Supplier<Consumer<BiConsumer<ResourceLocation, LootTable.Builder>>>, LootParameterSet>> loottables;

    public LootTableProvider(DataGenerator generator) {
        this.generator = generator;
        loottables = Lists.newArrayList();
        registerTables();
    }

    public abstract void registerTables();

    public void add(Supplier<Consumer<BiConsumer<ResourceLocation, LootTable.Builder>>> loottable, LootParameterSet parameters) {
        loottables.add(Pair.of(loottable, parameters));
    }

    @Override
    public void act(DirectoryCache cache) {
        Path path = this.generator.getOutputFolder();
        Map<ResourceLocation, LootTable> registry = Maps.newHashMap();
        this.loottables.forEach((pair) -> pair.getFirst().get().accept((a, b) -> {
            if (registry.put(a, b.setParameterSet(pair.getSecond()).build()) != null) {
                throw new IllegalStateException("Duplicate loot table " + a);
            }
        }));
        ValidationResults validator = new ValidationResults();
        registry.forEach((a, b) -> LootTableManager.func_215302_a(validator, a, b, registry::get));
        Multimap<String, String> problems = validator.getProblems();
        if (!problems.isEmpty()) {
            problems.forEach((p_218435_0_, p_218435_1_) -> LOGGER.warn("Found validation problems in " + p_218435_0_ + ": " + p_218435_1_));
            throw new IllegalStateException("Failed to validate loot tables, see logs");
        } else {
            registry.forEach((name, lootTable) -> {
                Path newPath = getPath(path, name);
                try {
                    IDataProvider.save(GSON, cache, LootTableManager.toJson(lootTable), newPath);
                } catch (IOException var6) {
                    LOGGER.error("Couldn't save loot table {}", newPath, var6);
                }

            });
        }
    }

    private static Path getPath(Path path, ResourceLocation name) {
        return path.resolve("data/" + name.getNamespace() + "/loot_tables/" + name.getPath() + ".json");
    }

    public String getName() {
        return "LootTables";
    }
}
