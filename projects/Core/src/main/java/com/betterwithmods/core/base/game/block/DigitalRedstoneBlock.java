/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;

import javax.annotation.Nullable;

@SuppressWarnings("deprecation")
public abstract class DigitalRedstoneBlock extends BaseRedstoneBlock {

    public DigitalRedstoneBlock(Properties properties) {
        super(properties);
    }

    protected boolean isCurrentlyValid(World world, BlockPos pos, BlockState state) {
        boolean lit = isLit(world, pos);
        boolean powered = world.getRedstonePowerFromNeighbors(pos) > 0;
        return lit == powered;
    }

    protected boolean isLit(IBlockReader world, BlockPos pos) {
        return super.isLit(world.getBlockState(pos));
    }

    @Override
    public void onBlockPlacedBy(World world, BlockPos pos, BlockState state, @Nullable LivingEntity placer, ItemStack stack) {
        handleChanges(state, world, pos);
    }

    public void handleChanges(BlockState state, World world, BlockPos pos) {
        if (!world.isRemote) {
            if (!isCurrentlyValid(world, pos, state)) {
                boolean lit = isLit(world, pos);
                world.setBlockState(pos, state.with(LIT, !lit), 2);
                if (isLit(world, pos)) {
                    onPowered(state, world, pos);
                } else {
                    onUnpowered(state, world, pos);
                }
            }

        }
    }

    @Override
    public void neighborChanged(BlockState state, World world, BlockPos pos, Block fromBlock, BlockPos fromPos, boolean idk) {
        handleChanges(state, world, pos);
        onChanged(state, world, pos, fromBlock, fromPos);
    }


    public int tickRate(IWorldReader world) {
        return 4;
    }

    public abstract void onPowered(BlockState state, World world, BlockPos pos);

    public abstract void onUnpowered(BlockState state, World world, BlockPos pos);

    protected void onChanged(BlockState state, World world, BlockPos pos, Block blockIn, BlockPos fromPos) {
    }


}
