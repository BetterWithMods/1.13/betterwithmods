/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core;

import com.google.common.collect.ImmutableList;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityType;
import net.minecraft.fluid.Fluid;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tags.*;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.ResourceLocation;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class Tags {

    public static class Blocks {
        public static final Tag<Block> WOOD = tag(References.MODID_CORE, "wood");
        public static final Tag<Block> LOGS = tag(References.MODID_CORE, "logs");
        public static final Tag<Block> HANDCRANKABLE = tag(References.MODID_MECHANIST, "handcrankable");
        public static final Tag<Block> BARK_LOGS = tag(References.MODID_CORE, "bark_logs");
        public static final Tag<Block> GRASS_PLANTS = tag(References.MODID_CORE, "grass_plants");
        public static final Tag<Block> VOID_HOLD_BLACKLIST = Tags.Blocks.tag(References.MODID_ARCANIUM, "void_hole_blacklist");
        public static final Tag<Block> CHOPPING_BLOCKS = tag(References.MODID_MECHANIST, "chopping_blocks");
        public static final Tag<Block> REDSTONE_LAMP = tag(References.MODID_MECHANIST, "redstone_lamp");
        public static final Tag<Block> NORMAL_FIRE = tag(References.MODID_MECHANIST, "heat_source/normal");
        public static final Tag<Block> STOKED_FIRE = tag(References.MODID_MECHANIST, "heat_source/stoked");
        public static final Tag<Block> SUPPORTED_STOKED_FIRE = tag(References.MODID_MECHANIST, "supports_stoked_fire");
        public static final Tag<Block> KILN_BLOCKS = tag(References.MODID_MECHANIST, "kiln_blocks");

        public static final Tag<Block> VERY_FAST = tag(References.MODID_VITALITY, "movement/very_fast");
        public static final Tag<Block> FAST = tag(References.MODID_VITALITY, "movement/fast");
        public static final Tag<Block> NORMAL = tag(References.MODID_VITALITY, "movement/normal");
        public static final Tag<Block> SLOW = tag(References.MODID_VITALITY, "movement/slow");
        public static final Tag<Block> VERY_SLOW = tag(References.MODID_VITALITY, "movement/very_slow");

        public static final Tag<Block> BLINDNESS = tag(References.MODID_VITALITY, "beacons/blindness");
        public static final Tag<Block> COSMETIC = tag(References.MODID_VITALITY, "beacons/cosmetic");
        public static final Tag<Block> FIRE_RESIST = tag(References.MODID_VITALITY, "beacons/fire_resist");
        public static final Tag<Block> FORTUNE = tag(References.MODID_VITALITY, "beacons/fortune");
        public static final Tag<Block> GLOWING = tag(References.MODID_VITALITY, "beacons/glowing");
        public static final Tag<Block> JUMP_BOOST = tag(References.MODID_VITALITY, "beacons/jump_boost");
        public static final Tag<Block> LEVITATE = tag(References.MODID_VITALITY, "beacons/levitate");
        public static final Tag<Block> LOOTING = tag(References.MODID_VITALITY, "beacons/looting");
        public static final Tag<Block> NIGHT_VISON = tag(References.MODID_VITALITY, "beacons/night_vison");
        public static final Tag<Block> SLOWFALL = tag(References.MODID_VITALITY, "beacons/slowfall");
        public static final Tag<Block> TRUESIGHT = tag(References.MODID_VITALITY, "beacons/truesight");
        public static final Tag<Block> WATER_BREATHING = tag(References.MODID_VITALITY, "beacons/water_breathing");



        public static Tag<Block> tag(String modid, String name) {
            return tag(new ResourceLocation(modid, name));
        }

        public static Tag<Block> tag(ResourceLocation tagLocation) {
            return new BlockTags.Wrapper(tagLocation);
        }
    }

    public static class Items {

        public static final Tag<Item> INGOT_IRON = tag(References.FORGE, "ingots/iron");
        public static final Tag<Item> DUST_HELLFIRE = tag(References.FORGE, "dusts/hellfire");
        public static final Tag<Item> DUST_WOOD = tag(References.FORGE, "dusts/wood");
        public static final Tag<Item> DUST_COAL = tag(References.FORGE, "dusts/coal");
        public static final Tag<Item> DUST_SULFUR = tag(References.FORGE, "dusts/sulfur");
        public static final Tag<Item> DUST_SALTPETER = tag(References.FORGE, "dusts/saltpeter");
        public static final Tag<Item> DUSTS_BLAZE = tag(References.FORGE, "dusts/blaze");
        public static final Tag<Item> DUSTS_GUNPOWDER = tag(References.FORGE, "dusts/gunpowder");
        public static final Tag<Item> INGOT_DIAMOND = tag(References.FORGE, "ingots/diamond");
        public static final Tag<Item> INGOT_SOULFORGED_STEEL = tag(References.FORGE, "ingots/soulforged_steel");
        public static final Tag<Item> NUGGETS_SOULFORGED_STEEL = tag(References.FORGE, "nuggets/soulforged_steel");
        public static final Tag<Item> GEAR_WOOD = tag(References.FORGE, "gears/wood");

        public static final Tag<Item> SEEDS = tag(References.MODID_CORE, "seeds");
        public static final Tag<Item> FIRESTARTERS = tag(References.MODID_CORE, "tools/firestarters");
        public static final Tag<Item> FLOATS = tag(References.MODID_CORE, "buoyancy/floats");
        public static final Tag<Item> SINKS = tag(References.MODID_CORE, "buoyancy/sink");
        public static final Tag<Item> EQUILIBRIUM = tag(References.MODID_CORE, "buoyancy/equilibrium");
        public static final Tag<Item> UNCOOKED_MEAT = tag(References.MODID_CORE, "meats/uncooked");
        public static final Tag<Item> COOKED_MEAT = tag(References.MODID_CORE, "meats/cooked");

        public static final Tag<Item> PORK = tag(References.MODID_CORE, "meats/pork");
        public static final Tag<Item> BEEF = tag(References.MODID_CORE, "meats/beef");
        public static final Tag<Item> MUTTON = tag(References.MODID_CORE, "meats/mutton");
        public static final Tag<Item> CHICKEN = tag(References.MODID_CORE, "meats/chicken");
        public static final Tag<Item> FISH = tag(References.MODID_CORE, "meats/fish");
        public static final Tag<Item> RABBIT = tag(References.MODID_CORE, "meats/rabbit");
        public static final Tag<Item> ROTTEN = tag(References.MODID_CORE, "meats/rotten");

        public static final Tag<Item> SOAP = tag(References.MODID_MECHANIST, "soap");
        public static final Tag<Item> TIPPING_BLACKLIST = tag(References.MODID_MECHANIST, "tipping_blacklist");
        public static final Tag<Item> ADHESIVE = tag(References.MODID_MECHANIST, "adhesive");
        public static final Tag<Item> FIBER_HEMP = tag(References.MODID_MECHANIST, "fiber_hemp");

        public static final Tag<Item> TANNED_LEATHER = tag(References.MODID_MECHANIST, "tanned_leather");

        public static final Tag<Item> AUTOFEED_CHICKEN = tag(References.MODID_PHENOMENONS, "autofeed/minecraft/chicken");
        public static final Tag<Item> AUTOFEED_COW = tag(References.MODID_PHENOMENONS, "autofeed/minecraft/cow");
        public static final Tag<Item> AUTOFEED_HORSE = tag(References.MODID_PHENOMENONS, "autofeed/minecraft/horse");
        public static final Tag<Item> AUTOFEED_LLAMA = tag(References.MODID_PHENOMENONS, "autofeed/minecraft/llama");
        public static final Tag<Item> AUTOFEED_OCELOT = tag(References.MODID_PHENOMENONS, "autofeed/minecraft/ocelot");
        public static final Tag<Item> AUTOFEED_PIG = tag(References.MODID_PHENOMENONS, "autofeed/minecraft/pig");
        public static final Tag<Item> AUTOFEED_RABBIT = tag(References.MODID_PHENOMENONS, "autofeed/minecraft/rabbit");
        public static final Tag<Item> AUTOFEED_SHEEP = tag(References.MODID_PHENOMENONS, "autofeed/minecraft/sheep");
        public static final Tag<Item> AUTOFEED_SPIDER = tag(References.MODID_PHENOMENONS, "autofeed/minecraft/spider");
        public static final Tag<Item> AUTOFEED_WOLF = tag(References.MODID_PHENOMENONS, "autofeed/minecraft/wolf");
        public static final Tag<Item> AUTOFEED_ZOMBIE = tag(References.MODID_PHENOMENONS, "autofeed/minecraft/zombie");

        public static final Tag<Item> REDSTONE_LAMP = tag(References.MODID_MECHANIST, "redstone_lamp");

        public static final Tag<Item> SEED_BLACKLIST = tag(References.MODID_PHENOMENONS, "seed_blacklist");

        public static Tag<Item> tag(String modid, String name) {
            return tag(new ResourceLocation(modid, name));
        }

        public static Tag<Item> tag(ResourceLocation tagLocation) {
            return new ItemTags.Wrapper(tagLocation);
        }
    }

    public static class Entities {
        public static final Tag<EntityType<?>> SQUID_TARGETS = tag(References.MODID_PHENOMENONS, "squid_targets");

        public static final Tag<EntityType<?>> ZOMBIE_TARGETS = tag(References.MODID_PHENOMENONS, "zombie_targets");
        public static final Tag<EntityType<?>> SPIDER_TARGETS = tag(References.MODID_PHENOMENONS, "spider_targets");
        public static final Tag<EntityType<?>> SKELETON_TARGETS = tag(References.MODID_PHENOMENONS, "skeleton_targets");


        public static Tag<EntityType<?>> tag(String modid, String name) {
            return tag(new ResourceLocation(modid, name));
        }

        public static Tag<EntityType<?>> tag(ResourceLocation tagLocation) {
            return new EntityTypeTags.Wrapper(tagLocation);
        }
    }

    public static class Fluids {

        public static final Tag<Fluid> WATERWHEEL = tag(References.MODID_MECHANIST, "waterwheel");

        public static Tag<Fluid> tag(String modid, String name) {
            return tag(new ResourceLocation(modid, name));
        }

        public static Tag<Fluid> tag(ResourceLocation tagLocation) {
            return new FluidTags.Wrapper(tagLocation);
        }

    }

    public static <T extends IItemProvider> Optional<ItemStack> getItemStack(Tag<T> source, int count, String namespace) {
        return getItemStack(source, count, Comparator.comparing(o -> o.asItem().getRegistryName().getNamespace().equals(namespace)));
    }

    public static <T extends IItemProvider> Optional<ItemStack> getItemStack(Tag<T> source, int count, Comparator<T> comparator) {
        List<T> items = ImmutableList.sortedCopyOf(comparator, source.getAllElements());
        return items.stream().findFirst().map(i -> new ItemStack(i, count));
    }
}


