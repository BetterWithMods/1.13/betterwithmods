/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.registration;

import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.registries.IForgeRegistry;
import org.apache.logging.log4j.Logger;

import java.util.function.BiFunction;

public abstract class ItemRegistrar extends Registrar<Item> {

    public ItemRegistrar(String modid, Logger logger) {
        super(modid, logger);
    }

    @SubscribeEvent
    @Override
    public void onRegister(RegistryEvent.Register<Item> event) {
        super.onRegister(event);
    }

    public Item register(IForgeRegistry<Item> registry, BiFunction<Block, Item.Properties, BlockItem> itemBlock, Block block, Item.Properties properties) {
        return register(registry, block.getRegistryName(), itemBlock.apply(block, properties));
    }

    public Item register(IForgeRegistry<Item> registry, BiFunction<Block, Item.Properties, BlockItem> itemBlock, Block block, ItemGroup group) {
        Item.Properties properties = new Item.Properties().group(group);
        return register(registry, itemBlock, block, properties);
    }

    public Item register(IForgeRegistry<Item> registry, Block block, ItemGroup group) {
       return register(registry, BlockItem::new, block, group);
    }
}
