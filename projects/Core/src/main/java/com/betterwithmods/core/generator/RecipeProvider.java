/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.generator;

import com.betterwithmods.core.base.game.recipes.ModuleCondition;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.advancements.criterion.EnterBlockTrigger;
import net.minecraft.advancements.criterion.InventoryChangeTrigger;
import net.minecraft.advancements.criterion.ItemPredicate;
import net.minecraft.advancements.criterion.MinMaxBounds;
import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.data.IFinishedRecipe;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.tags.Tag;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.conditions.ICondition;
import net.minecraftforge.common.crafting.conditions.IConditionBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Consumer;

public class RecipeProvider implements IDataProvider, IConditionBuilder {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().create();
    private final DataGenerator generator;

    private Map<Item, Tag<Item>> replacements = new HashMap<>();
    private Set<ResourceLocation> excludes = new HashSet<>();


    public RecipeProvider(DataGenerator generatorIn) {
        this.generator = generatorIn;
    }

    /**
     * Saves a recipe to a file.
     */
    private void saveRecipe(DirectoryCache cache, JsonObject recipeJson, Path pathIn) {
        try {
            String s = GSON.toJson((JsonElement) recipeJson);
            String s1 = HASH_FUNCTION.hashUnencodedChars(s).toString();
            if (!Objects.equals(cache.getPreviousHash(pathIn), s1) || !Files.exists(pathIn)) {
                Files.createDirectories(pathIn.getParent());

                try (BufferedWriter bufferedwriter = Files.newBufferedWriter(pathIn)) {
                    bufferedwriter.write(s);
                }
            }

            cache.func_208316_a(pathIn, s1);
        } catch (IOException ioexception) {
            LOGGER.error("Couldn't save recipe {}", pathIn, ioexception);
        }

    }

    /**
     * Saves an advancement to a file.
     */
    protected void saveRecipeAdvancement(DirectoryCache cache, JsonObject advancementJson, Path pathIn) {
        try {
            String s = GSON.toJson((JsonElement) advancementJson);
            String s1 = HASH_FUNCTION.hashUnencodedChars(s).toString();
            if (!Objects.equals(cache.getPreviousHash(pathIn), s1) || !Files.exists(pathIn)) {
                Files.createDirectories(pathIn.getParent());

                try (BufferedWriter bufferedwriter = Files.newBufferedWriter(pathIn)) {
                    bufferedwriter.write(s);
                }
            }

            cache.func_208316_a(pathIn, s1);
        } catch (IOException ioexception) {
            LOGGER.error("Couldn't save recipe advancement {}", pathIn, ioexception);
        }

    }

    @Override
    public void act(DirectoryCache cache) throws IOException {
        Path path = this.generator.getOutputFolder();
        Set<ResourceLocation> set = Sets.newHashSet();
        this.registerRecipes((recipe) -> {
            if (!set.add(recipe.getID())) {
                throw new IllegalStateException("Duplicate recipe " + recipe.getID());
            } else {
                this.saveRecipe(cache, recipe.getRecipeJson(), path.resolve("data/" + recipe.getID().getNamespace() + "/recipes/" + recipe.getID().getPath() + ".json"));
                JsonObject jsonobject = recipe.getAdvancementJson();
                if (jsonobject != null) {
                    //TODO advancements
//                    this.saveRecipeAdvancement(cache, jsonobject, path.resolve("data/" + recipe.getID().getNamespace() + "/advancements/" + recipe.getAdvancementID().getPath() + ".json"));
                }

            }
        });
//        this.saveRecipeAdvancement(cache, Advancement.Builder.builder().withCriterion("impossible", new ImpossibleTrigger.Instance()).serialize(), path.resolve("data/minecraft/advancements/recipes/root.json"));
    }

    protected void exclude(IItemProvider item) {
        excludes.add(item.asItem().getRegistryName());
    }

    protected void replace(IItemProvider item, Tag<Item> tag) {
        replacements.put(item.asItem(), tag);
    }

    private Ingredient enhance(ResourceLocation name, Ingredient vanilla) {
        if (excludes.contains(name))
            return null;

        boolean modified = false;
        List<Ingredient.IItemList> items = new ArrayList<>();
        Ingredient.IItemList[] vanillaItems = getField(Ingredient.class, vanilla, 3);
        for (Ingredient.IItemList entry : vanillaItems) {
            if (entry instanceof Ingredient.SingleItemList) {
                ItemStack stack = entry.getStacks().stream().findFirst().orElse(ItemStack.EMPTY);
                Tag<Item> replacement = replacements.get(stack.getItem());
                if (replacement != null) {
                    items.add(new Ingredient.TagList(replacement));
                    modified = true;
                } else
                    items.add(entry);
            } else
                items.add(entry);
        }
        return modified ? Ingredient.fromItemListStream(items.stream()) : null;
    }

    @SuppressWarnings("unchecked")
    private <T, R> R getField(Class<T> clz, T inst, int index) {
        Field fld = clz.getDeclaredFields()[index];
        fld.setAccessible(true);
        try {
            return (R) fld.get(inst);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public EnterBlockTrigger.Instance enteredBlock(Block blockIn) {
        return new EnterBlockTrigger.Instance(blockIn, null);
    }

    public InventoryChangeTrigger.Instance hasItemProvider(MinMaxBounds.IntBound amount, IItemProvider itemIn) {
        return this.hasItemProvider(ItemPredicate.Builder.create().item(itemIn).count(amount).build());
    }

    public InventoryChangeTrigger.Instance hasItem(Item itemIn) {
        return hasItemProvider(itemIn);
    }

    public InventoryChangeTrigger.Instance hasItemProvider(IItemProvider itemIn) {
        return this.hasItemProvider(ItemPredicate.Builder.create().item(itemIn).build());
    }

    public InventoryChangeTrigger.Instance hasItemProvider(Tag<Item> tagIn) {
        return this.hasItemProvider(ItemPredicate.Builder.create().tag(tagIn).build());
    }

    public InventoryChangeTrigger.Instance hasItemProvider(ItemPredicate... predicates) {
        return new InventoryChangeTrigger.Instance(MinMaxBounds.IntBound.UNBOUNDED, MinMaxBounds.IntBound.UNBOUNDED, MinMaxBounds.IntBound.UNBOUNDED, predicates);
    }

    protected void registerRecipes(Consumer<IFinishedRecipe> consumer) {}

    public ICondition moduleLoaded(String mod, String module)
    {
        return new ModuleCondition(mod, module);
    }


    @Override
    public String getName() {
        return "Recipes";
    }
}
