package com.betterwithmods.core.api.crafting.result;

import net.minecraft.client.gui.FontRenderer;

import javax.annotation.Nullable;

public interface IResultRenderer<T> {

    void render(int x, int y, @Nullable IResult<T> result, FontRenderer font);

}
