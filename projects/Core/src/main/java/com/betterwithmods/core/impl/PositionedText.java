/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.impl;

import com.betterwithmods.core.api.IPositionedText;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.Style;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.stream.Stream;

public class PositionedText implements IPositionedText {

    private ITextComponent parent;
    private double x,y, scale;

    public PositionedText(ITextComponent parent, double x, double y, double scale) {
        this.parent = parent;
        this.x = x;
        this.y = y;
        this.scale = scale;
    }


    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getScale() {
        return scale;
    }

    @Override
    public PositionedText setStyle(Style style) {
        parent.setStyle(style);
        return this;
    }

    @Override
    public Style getStyle() {
        return parent.getStyle();
    }

    @Override
    public PositionedText appendSibling(ITextComponent component) {
        parent.appendSibling(component);
        return this;
    }

    @Override
    public String getUnformattedComponentText() {
        return parent.getUnformattedComponentText();
    }

    @Override
    public List<ITextComponent> getSiblings() {
        return parent.getSiblings();
    }

    @Override
    public Stream<ITextComponent> stream() {
        return parent.stream();
    }

    @Nonnull
    @Override
    public PositionedText shallowCopy() {
        return new PositionedText(parent.shallowCopy(), x,y, scale);
    }
}
