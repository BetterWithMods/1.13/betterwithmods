/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.impl.crafting.result;

import com.betterwithmods.core.Registrars;
import com.betterwithmods.core.api.IPositionedText;
import com.betterwithmods.core.api.crafting.result.IResult;
import com.betterwithmods.core.api.crafting.result.IResultRenderer;
import com.betterwithmods.core.api.crafting.result.IResultSerializer;
import com.betterwithmods.core.api.crafting.result.count.ICount;
import com.betterwithmods.core.relationary.Relation;
import com.betterwithmods.core.relationary.RelationaryManager;
import com.betterwithmods.core.relationary.Shape;
import com.betterwithmods.core.utilties.ItemUtils;
import com.google.common.collect.Lists;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;

public class RelationshipResult extends BaseStackResult {

    private Relation relation; //Purely for JEI display
    private Shape fromShape, toShape;
    private ICount count;

    public RelationshipResult(Relation relation, Shape fromShape, Shape toShape, ICount count) {
        this.relation = relation;
        this.fromShape = fromShape;
        this.toShape = toShape;
        this.count = count;
    }

    public Relation getRelation() {
        return relation;
    }

    public Shape getFromShape() {
        return fromShape;
    }

    public Shape getToShape() {
        return toShape;
    }

    public ICount getCount() {
        return count;
    }

    @Override
    public IResultSerializer<?> getSerializer() {
        return Registrars.ResultSerializers.RELATIONSHIP;
    }


    @Override
    public <T> ItemStack get(@Nullable T input) {
        Relation relation = this.relation;
        if (input instanceof BlockState) {
            ItemStack stack = ItemUtils.getStack((BlockState) input);
            relation = RelationaryManager.getRelationWithShape(stack, fromShape);
        }
        if (relation != null) {
            return relation.first(toShape, getCount().get());
        }
        return ItemStack.EMPTY;
    }

    @Override
    public boolean equals(IResult result) {
        if (result instanceof RelationshipResult) {
            RelationshipResult r = (RelationshipResult) result;
            return r.relation.equals(this.relation) && r.fromShape.equals(this.fromShape) && r.count.equals(this.count);
        }
        return false;
    }

    @Override
    public RelationshipResult copy() {
        return new RelationshipResult(relation, fromShape, toShape, count);
    }

    @Override
    public ItemStack displayStack() {
        Collection<ItemStack> stacks = relation.fromShape(toShape);
        return stacks.stream().findFirst().orElse(ItemStack.EMPTY);
    }

    @Override
    public List<IPositionedText> displayCount() {
        return count.getText();
    }

    @Override
    public List<ITextComponent> tooltip() {
        return Lists.newArrayList(new StringTextComponent("Returns the correctly related material."));
    }

    @Override
    public boolean isDynamic() {
        return true;
    }

    @Override
    public Class<ItemStack> getType() {
        return ItemStack.class;
    }

    @Override
    public ITextComponent displayText() {
        return displayStack().getTextComponent();
    }

    @Override
    public IResultRenderer<ItemStack> getRenderer() {
        return StackResultRenderer.INSTANCE;
    }
}
