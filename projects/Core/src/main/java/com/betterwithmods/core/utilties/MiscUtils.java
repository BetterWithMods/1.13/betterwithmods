/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.IForgeRegistryEntry;

import javax.vecmath.Vector2d;
import javax.vecmath.Vector3d;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collector;

public class MiscUtils {

    public static ResourceLocation stripPath(Pattern pathPattern, ResourceLocation location) {
        String fullPath = location.getPath();
        Matcher matcher = pathPattern.matcher(fullPath);
        if (matcher.matches())
            return new ResourceLocation(location.getNamespace(), matcher.group(1));
        return null;
    }

    public static double[] parseArray(JsonArray array, int size) {
        assert array.size() == size;
        double[] vec = new double[size];
        for (int i = 0; i < size; i++) {
            JsonElement e = array.get(i);
            if (e.isJsonPrimitive()) {
                vec[i] = e.getAsDouble();
            }
        }
        return vec;
    }

    public static Vector3d parseVec3d(JsonArray array) {
        return new Vector3d(parseArray(array, 3));
    }

    public static Vector2d parseVec2d(JsonArray array) {
        return new Vector2d(parseArray(array, 2));
    }

    public static boolean matches(IForgeRegistryEntry<?> a, IForgeRegistryEntry<?> b) {
        return a.getRegistryName().equals(b.getRegistryName());
    }

    public static boolean notEmpty(ItemStack stack) {
        return !stack.isEmpty();
    }

    public static <T> NonNullList<T> nonnulllist(Collection<T> collection) {
        NonNullList<T> list = NonNullList.create();
        list.addAll(collection);
        return list;
    }

    public static <T> Collector<T, NonNullList<T>, NonNullList<T>> nonnulllist() {
        return Collector.of(NonNullList::create, NonNullList::add, (list, item) -> {
            list.addAll(item);
            return list;
        });

    }


    public static <T> void ifPresentOrElse(Optional<T> optional, Consumer<? super T> action , Runnable emptyAction) {
        if(!optional.isPresent()) {
            emptyAction.run();
        } else {
            optional.ifPresent(action);
        }
    }

    public static <T> boolean ifIsPresent(Optional<T> optional, Consumer<? super  T> action) {
        optional.ifPresent(action);
        return optional.isPresent();
    }

    public static <T> boolean ifIsPresentOrElse(Optional<T> optional, Consumer<? super  T> action, Runnable emptyAction) {
        ifPresentOrElse(optional, action, emptyAction);
        return optional.isPresent();
    }
}
