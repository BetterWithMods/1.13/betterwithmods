/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties;

import com.betterwithmods.core.base.game.recipes.multiplex.ITimedRecipe;
import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.common.util.INBTSerializable;

import java.util.concurrent.atomic.AtomicInteger;

public class Timer implements INBTSerializable<CompoundNBT> {

    private final AtomicInteger time = new AtomicInteger();
    private final AtomicInteger goalTime = new AtomicInteger();

    public Timer(int goal) {
        setGoalTime(goal);
    }

    public Timer() {
    }

    public void reset() {
        time.set(0);
        goalTime.set(0);
    }

    public boolean ready() {
        return getTime().get() >= getGoalTime().get();
    }

    public void setGoalTime(int goalTime) {
        this.goalTime.set(goalTime);
    }

    public void setTime(int time) {
        this.time.set(time);
    }

    public void incrementTime(int increment) {
        this.time.addAndGet(increment);
    }

    public void incrementTime() {
        this.incrementTime(1);
    }

    public AtomicInteger getTime() {
        return time;
    }

    public AtomicInteger getGoalTime() {
        return goalTime;
    }

    public void update(ITimedRecipe recipe) {
        int t = recipe.getRecipeTime();
        if (t != getGoalTime().get()) {
            setGoalTime(t);
        }
    }

    private static final String TIME = "time", GOAL_TIME = "goalTime";

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        tag.putInt(TIME, time.get());
        tag.putInt(GOAL_TIME, goalTime.get());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        time.set(nbt.getInt(TIME));
        goalTime.set(nbt.getInt(GOAL_TIME));
    }
}
