/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.block.plant;

import com.betterwithmods.core.utilties.BooleanOperator;
import com.betterwithmods.core.utilties.Rand;
import com.google.common.collect.Lists;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.BushBlock;
import net.minecraft.block.IGrowable;
import net.minecraft.item.ItemStack;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.minecraft.world.storage.loot.LootContext;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.PlantType;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public abstract class BlockPlant extends BushBlock implements IGrowable, IPlantable, IPlant {

    private static VoxelShape[] SHAPE_BY_AGE = new VoxelShape[]{
            Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 2.0D, 16.0D),
            Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 4.0D, 16.0D),
            Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 6.0D, 16.0D),
            Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 8.0D, 16.0D),
            Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 10.0D, 16.0D),
            Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 12.0D, 16.0D),
            Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 14.0D, 16.0D),
            Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 16.0D, 16.0D)
    };


    private final int maxAge;

    private final Collection<IGrowthCondition> growthConditions;
    private BooleanOperator growthConditionReducer;

    public BlockPlant(Block.Properties builder, int maxAge) {
        super(builder);
        this.maxAge = maxAge;
        this.growthConditions = Lists.newArrayList();
        this.growthConditionReducer = (a, b) -> a && b;
    }


    public BlockPlant growthConditions(IGrowthCondition... conditions) {
        this.growthConditions.addAll(Arrays.asList(conditions));
        return this;
    }

    public BlockPlant growthConditionReducer(BooleanOperator reducer) {
        this.growthConditionReducer = reducer;
        return this;
    }

    public abstract IItemProvider getSeed();

    public abstract IItemProvider getCrop();

    public abstract IntegerProperty getAgeProperty();

    protected int getMaxAge() {
        return maxAge;
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(getAgeProperty());
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader world, BlockPos pos, ISelectionContext context) {
        return SHAPE_BY_AGE[state.get(this.getAgeProperty())];
    }


    protected int getAge(BlockState state) {
        return state.get(this.getAgeProperty());
    }

    protected boolean isMaxAge(BlockState state) {
        return state.get(this.getAgeProperty()) >= this.getMaxAge();
    }

    protected BlockState withAge(int age) {
        return this.getDefaultState().with(this.getAgeProperty(), age);
    }

    @Override
    public boolean canGrow(IBlockReader world, BlockPos pos, BlockState state, boolean isClient) {
        if (isMaxAge(state))
            return false;
        return isSatisfied((IWorldReader) world, pos, state);
    }

    @Override
    public boolean canUseBonemeal(World world, Random rand, BlockPos pos, BlockState state) {
        return true;
    }

    @Override
    public void grow(World world, Random rand, BlockPos pos, BlockState state) {
        int newAge = Math.min(this.getAge(state) + 1, getMaxAge());
        world.setBlockState(pos, withAge(newAge), 2);
    }

    @Override
    public PlantType getPlantType(IBlockReader world, BlockPos pos) {
        return PlantType.Crop;
    }

    public boolean isSatisfied(IWorldReader world, BlockPos pos, BlockState state) {
        return GrowthConditions.aggregate(growthConditionReducer, growthConditions).canGrow(world, pos, state);
    }

    @Override
    protected boolean isValidGround(BlockState state, IBlockReader world, BlockPos pos) {
        return state.getBlock().isFertile(state, world, pos);
    }

    @Override
    public List<ItemStack> getDrops(BlockState state, LootContext.Builder builder) {
        return super.getDrops(state, builder);
    }
//
//    @Override
//    public void getDrops(BlockState state, net.minecraft.util.NonNullList<ItemStack> drops, World world, BlockPos pos, int fortune) {
//        drops.add(new ItemStack(this.getSeed()));
//
//        if (isMaxAge(state)) {
//            if (Rand.chance(world.rand, 0.3)) {
//                drops.add(new ItemStack(this.getSeed()));
//            }
//            drops.add(new ItemStack(getCrop()));
//        }
//    }


    @Override
    public void tick(BlockState state, World world, BlockPos pos, Random random) {
        super.tick(state, world, pos, random);
        if (!world.isAreaLoaded(pos, 1))
            return; // Forge: prevent loading unloaded chunks when checking neighbor's light
        if (canGrow(world, pos, state, false)) {
            boolean chance = shouldGrow(world, pos);
            if (net.minecraftforge.common.ForgeHooks.onCropsGrowPre(world, pos, state, chance)) {
                this.grow(world, random, pos, state);
                net.minecraftforge.common.ForgeHooks.onCropsGrowPost(world, pos, state);
            }
        }
    }

    public boolean shouldGrow(World world, BlockPos pos) {
        return Rand.chance(world.rand, 0.2);
    }

}
