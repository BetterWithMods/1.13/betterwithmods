/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.loottables.conditions;

import com.betterwithmods.core.base.setup.ModuleBase;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.loot.*;
import net.minecraft.world.storage.loot.conditions.ILootCondition;

import java.util.Set;
import java.util.function.Function;

public class ModuleLootCondition implements ILootCondition {
    private final ILootCondition onEnabled, onDisabled;
    private final String module;

    private ModuleLootCondition(String module, ILootCondition onEnabled, ILootCondition onDisabled) {
        this.onEnabled = onEnabled;
        this.onDisabled = onDisabled;
        this.module = module;
    }

    public final boolean test(LootContext context) {
        return ModuleBase.isEnabled(this.module) ? onEnabled.test(context) : onDisabled.test(context);
    }

    public Set<LootParameter<?>> getRequiredParameters() {
        return this.onEnabled.getRequiredParameters();
    }

    public void func_215856_a(ValidationResults validate, Function<ResourceLocation, LootTable> p_215856_2_, Set<ResourceLocation> p_215856_3_, LootParameterSet p_215856_4_) {
        ILootCondition.super.func_215856_a(validate, p_215856_2_, p_215856_3_, p_215856_4_);
        this.onEnabled.func_215856_a(validate, p_215856_2_, p_215856_3_, p_215856_4_);
    }

    public static ILootCondition.IBuilder builder(String module, ILootCondition.IBuilder onEnabled, ILootCondition.IBuilder onDisabled) {

        return () -> new ModuleLootCondition(module, onEnabled.build(), onDisabled.build());
    }

    public static class Serializer extends ILootCondition.AbstractSerializer<ModuleLootCondition> {
        public Serializer() {
            super(new ResourceLocation("module"), ModuleLootCondition.class);
        }

        public void serialize(JsonObject json, ModuleLootCondition value, JsonSerializationContext context) {
            json.add("module", context.serialize(value.module));
            json.add("onEnabled", context.serialize(value.onEnabled));
            json.add("onDisabled", context.serialize(value.onDisabled));
        }

        public ModuleLootCondition deserialize(JsonObject json, JsonDeserializationContext context) {
            String module = JSONUtils.getString(json, "module");
            ILootCondition onEnabled = JSONUtils.deserializeClass(json, "onEnabled", context, ILootCondition.class);
            ILootCondition onDisabled = JSONUtils.deserializeClass(json, "onDisabled", context, ILootCondition.class);
            return new ModuleLootCondition(module, onEnabled, onDisabled);
        }
    }
}
