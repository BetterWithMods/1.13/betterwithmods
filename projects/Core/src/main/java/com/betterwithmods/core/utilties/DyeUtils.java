/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties;

import com.google.common.collect.ImmutableMap;
import net.minecraft.block.Blocks;
import net.minecraft.item.DyeColor;
import net.minecraft.item.Item;
import net.minecraft.tags.Tag;
import net.minecraft.util.IItemProvider;
import net.minecraftforge.common.Tags;

import java.util.Map;

public class DyeUtils {

    public static Map<DyeColor, Tag<Item>> DYE_TAGS = ImmutableMap.<DyeColor, Tag<Item>>builder()
            .put(DyeColor.WHITE, Tags.Items.DYES_WHITE)
            .put(DyeColor.ORANGE, Tags.Items.DYES_ORANGE)
            .put(DyeColor.MAGENTA, Tags.Items.DYES_MAGENTA)
            .put(DyeColor.LIGHT_BLUE, Tags.Items.DYES_LIGHT_BLUE)
            .put(DyeColor.YELLOW, Tags.Items.DYES_YELLOW)
            .put(DyeColor.LIME, Tags.Items.DYES_LIME)
            .put(DyeColor.PINK, Tags.Items.DYES_PINK)
            .put(DyeColor.GRAY, Tags.Items.DYES_GRAY)
            .put(DyeColor.LIGHT_GRAY, Tags.Items.DYES_LIGHT_GRAY)
            .put(DyeColor.CYAN, Tags.Items.DYES_CYAN)
            .put(DyeColor.PURPLE, Tags.Items.DYES_PURPLE)
            .put(DyeColor.BLUE, Tags.Items.DYES_BLUE)
            .put(DyeColor.BROWN, Tags.Items.DYES_BROWN)
            .put(DyeColor.GREEN, Tags.Items.DYES_GREEN)
            .put(DyeColor.RED, Tags.Items.DYES_RED)
            .put(DyeColor.BLACK, Tags.Items.DYES_BLACK)
            .build();

    public static Map<DyeColor, IItemProvider> STAINED_GLASS_PANES = ImmutableMap.<DyeColor, IItemProvider>builder()
            .put(DyeColor.WHITE, Blocks.WHITE_STAINED_GLASS_PANE)
            .put(DyeColor.ORANGE, Blocks.ORANGE_STAINED_GLASS_PANE)
            .put(DyeColor.MAGENTA, Blocks.MAGENTA_STAINED_GLASS_PANE)
            .put(DyeColor.LIGHT_BLUE, Blocks.LIGHT_BLUE_STAINED_GLASS_PANE)
            .put(DyeColor.YELLOW, Blocks.YELLOW_STAINED_GLASS_PANE)
            .put(DyeColor.LIME, Blocks.LIME_STAINED_GLASS_PANE)
            .put(DyeColor.PINK, Blocks.PINK_STAINED_GLASS_PANE)
            .put(DyeColor.GRAY, Blocks.GRAY_STAINED_GLASS_PANE)
            .put(DyeColor.LIGHT_GRAY, Blocks.LIGHT_GRAY_STAINED_GLASS_PANE)
            .put(DyeColor.CYAN, Blocks.CYAN_STAINED_GLASS_PANE)
            .put(DyeColor.PURPLE, Blocks.PURPLE_STAINED_GLASS_PANE)
            .put(DyeColor.BLUE, Blocks.BLUE_STAINED_GLASS_PANE)
            .put(DyeColor.BROWN, Blocks.BROWN_STAINED_GLASS_PANE)
            .put(DyeColor.GREEN, Blocks.GREEN_STAINED_GLASS_PANE)
            .put(DyeColor.RED, Blocks.RED_STAINED_GLASS_PANE)
            .put(DyeColor.BLACK, Blocks.BLACK_STAINED_GLASS_PANE)
            .build();


    public static Tag<Item> getDye(DyeColor color) {
        return DYE_TAGS.get(color);
    }

    public static IItemProvider getGlassPane(DyeColor color) {
        return STAINED_GLASS_PANES.get(color);
    }
}
