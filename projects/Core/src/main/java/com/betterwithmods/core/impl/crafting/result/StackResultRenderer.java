package com.betterwithmods.core.impl.crafting.result;

import com.betterwithmods.core.api.crafting.result.IResult;
import com.betterwithmods.core.api.crafting.result.IResultRenderer;
import com.betterwithmods.core.utilties.RenderUtils;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.ItemStack;

import javax.annotation.Nullable;

public class StackResultRenderer implements IResultRenderer<ItemStack> {

    public static final StackResultRenderer INSTANCE = new StackResultRenderer();

    @Override
    public void render(int x, int y, @Nullable IResult result, FontRenderer font) {
        ItemStack stack = result.displayStack();
        GlStateManager.enableDepthTest();
        RenderHelper.enableGUIStandardItemLighting();
        Minecraft minecraft = Minecraft.getInstance();
        ItemRenderer itemRenderer = minecraft.getItemRenderer();
        itemRenderer.renderItemAndEffectIntoGUI(null, stack, x, y);
        RenderUtils.renderItemOverlayIntoGUI(font, stack, x, y, result.displayCount());
        GlStateManager.disableBlend();
        RenderHelper.disableStandardItemLighting();
    }

}
