/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.generator;

import com.google.common.collect.Maps;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.loot.LootTable;

import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public abstract class LootTables implements Consumer<BiConsumer<ResourceLocation, LootTable.Builder>> {
    protected final Map<ResourceLocation, LootTable.Builder> builders = Maps.newHashMap();

    protected void registerLootTable(ILootTableSource source, LootTable.Builder builder) {
        this.builders.put(source.getLootTable(), builder);
    }

    protected void registerLootTable(ResourceLocation name, LootTable.Builder p_218507_2_) {
        this.builders.put(name, p_218507_2_);
    }

    public abstract void register();

    @FunctionalInterface
    public interface ILootTableSource {
        ResourceLocation getLootTable();
    }
}
