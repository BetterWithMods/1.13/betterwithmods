/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.impl.crafting.input;

import com.betterwithmods.core.Registrars;
import com.betterwithmods.core.api.crafting.input.IInput;
import com.betterwithmods.core.api.crafting.input.IInputSerializer;
import com.betterwithmods.core.api.crafting.input.IRecipeContext;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipe;
import com.betterwithmods.core.utilties.ItemUtils;
import com.betterwithmods.core.utilties.inventory.InventoryUtils;
import com.betterwithmods.core.utilties.inventory.ItemSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.items.IItemHandlerModifiable;

public class InventoryInput<R extends IMultiplexRecipe<IItemHandlerModifiable, R>> implements IInput<IItemHandlerModifiable, R> {

    private NonNullList<Ingredient> ingredients;

    public InventoryInput(NonNullList<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public boolean matches(IWorld world, BlockPos pos, R recipe, IRecipeContext<IItemHandlerModifiable, R> context) {
        IItemHandlerModifiable inventory = context.getState(world, pos);
        return getIngredients().stream().allMatch(ingredient -> InventoryUtils.stream(inventory).map(ItemSlot::getStack).anyMatch(ingredient));
    }

    @Override
    public NonNullList<Ingredient> getIngredients() {
        return ingredients;
    }



    @Override
    public void consume(World world, BlockPos pos, R recipe, IRecipeContext<IItemHandlerModifiable, R> context) {
        IItemHandlerModifiable inventory = context.getState(world, pos);
        NonNullList<ItemStack> containers = NonNullList.create();
        ingredients.forEach(ingredient -> InventoryUtils.consumeFromInventory(inventory, ingredient, true, containers));
        if (handleContainers()) {
            InventoryUtils.addToInventoryOrDrop(inventory, containers, world, pos.up(), ItemUtils.EJECT_OFFSET);
        }
    }

    public boolean handleContainers() {
        return true;
    }

    @Override
    public IInputSerializer<?, ?> getSerializer() {
        return Registrars.InputSerializers.INVENTORY;
    }
}
