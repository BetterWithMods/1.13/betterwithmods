/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.client.gui;

import com.betterwithmods.core.base.game.container.BaseContainer;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

public class BaseContainerScreen<T extends BaseContainer> extends ContainerScreen<T> {

    private final ResourceLocation background;

    public BaseContainerScreen(T container, PlayerInventory inventory, ITextComponent title, ResourceLocation background) {
        super(container, inventory, title);
        this.background = background;
    }

    public void drawName(int nameY, int playerX, int playerY) {
        String name = getTitle().getFormattedText();
        int nameWidth = this.font.getStringWidth(name) / 2;
        this.font.drawString(name, (float) (this.xSize / 2 - nameWidth), nameY, 0x404040);
        this.font.drawString(this.playerInventory.getDisplayName().getFormattedText(), playerX, (float) (this.ySize - playerY), 4210752);
    }

    @Override
    public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
        this.renderBackground();
        super.render(p_render_1_, p_render_2_, p_render_3_);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.minecraft.getTextureManager().bindTexture(background);
        this.blit(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);
        int centerX = (this.width - this.xSize) / 2, centerY = (this.height - this.ySize) / 2;
        drawExtras(partialTicks, mouseX, mouseY, centerX, centerY);
    }


    protected void drawExtras(float partialTicks, int mouseX, int mouseY, int centerX, int centerY) {
    }

    public void setWidth(int w) {
        this.xSize = w;
    }

    public void setHeight(int h) {
        this.ySize = h;
    }

}
