/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.ingredient;

import com.betterwithmods.core.Core;
import com.betterwithmods.core.References;
import com.betterwithmods.core.relationary.Relation;
import com.betterwithmods.core.relationary.RelationaryManager;
import com.betterwithmods.core.relationary.Shape;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.IIngredientSerializer;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.stream.Stream;

public class RelationIngredient extends Ingredient {
    private Relation relation;
    private Shape shape;

    private ItemStack[] matchingStacks;

    public RelationIngredient(Relation relation, Shape shape) {
        super(Stream.of());
        this.relation = relation;
        this.shape = shape;
    }

    @Override
    public ItemStack[] getMatchingStacks() {
        Collection<ItemStack> stacks = relation.getStacks(shape);
        matchingStacks = new ItemStack[stacks.size()];
        stacks.toArray(matchingStacks);
        return matchingStacks;
    }

    @Override
    public boolean test(@Nullable ItemStack stack) {
        return RelationaryManager.isShape(relation, stack, shape);
    }

    public Relation getRelation() {
        return relation;
    }

    public Shape getShape() {
        return shape;
    }

    public static class Serializer implements IIngredientSerializer<RelationIngredient> {
        public static ResourceLocation NAME = new ResourceLocation(References.MODID_CORE, "relation_ingredient");

        @Override
        public RelationIngredient parse(PacketBuffer buffer) {
            Relation relation = RelationaryManager.getOrCreateRelation(buffer.readResourceLocation());
            Shape shape = RelationaryManager.getOrCreateShape(buffer.readResourceLocation());
            return new RelationIngredient(relation, shape);
        }

        @Override
        public RelationIngredient parse(JsonObject json) {
            Relation relation = RelationaryManager.getOrCreateRelation(new ResourceLocation(JSONUtils.getString(json, "relation", "")));
            Shape shape = RelationaryManager.getOrCreateShape(new ResourceLocation(JSONUtils.getString(json, "shape", "")));
            return new RelationIngredient(relation, shape);
        }

        @Override
        public void write(PacketBuffer buffer, RelationIngredient ingredient) {
            buffer.writeResourceLocation(ingredient.getRelation().getRegistryName());
            buffer.writeResourceLocation(ingredient.getShape().getRegistryName());
        }
    }

    @Nonnull
    @Override
    public IIngredientSerializer<? extends Ingredient> getSerializer() {
        return Core.RELATION_INGREDIENT;
    }

    @Override
    public boolean isSimple() {
        return false;
    }

    @Override
    public JsonElement serialize() {
        JsonObject object = new JsonObject();
        object.addProperty("type", Serializer.NAME.toString());
        object.addProperty("relation", relation.getRegistryName().toString());
        object.addProperty("shape", shape.getRegistryName().toString());
        return object;
    }
}
