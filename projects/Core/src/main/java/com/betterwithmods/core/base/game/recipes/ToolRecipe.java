/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.recipes;

import com.betterwithmods.core.Registrars;
import com.betterwithmods.core.utilties.EnvironmentUtils;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.ICraftingRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import javax.annotation.Nonnull;
import java.util.Random;

public class ToolRecipe implements ICraftingRecipe {
    private final ResourceLocation id;
    private final String group;
    private final NonNullList<Ingredient> ingredients = NonNullList.create();
    protected final Ingredient tool;
    protected final Ingredient input;
    protected final ItemStack result;
    protected final NonNullList<ItemStack> additionalDrops;
    protected boolean damageTool;
    protected SoundEvent sound;

    public ToolRecipe(ResourceLocation id, String group, Ingredient tool, Ingredient input, ItemStack result, NonNullList<ItemStack> additionalDrops, SoundEvent sound, boolean damageTool) {
        this.id = id;
        this.group = group;
        this.tool = tool;
        this.input = input;
        this.result = result;
        this.additionalDrops = additionalDrops;
        this.sound = sound;
        this.damageTool = damageTool;
        if (damageTool) {
            MinecraftForge.EVENT_BUS.register(this);
        }
        this.ingredients.add(tool);
        this.ingredients.add(input);
    }

    @Override
    public boolean matches(@Nonnull CraftingInventory inv, @Nonnull World world) {
        return isMatch(inv, world);
    }

    public boolean isMatch(IInventory inv, World world) {
        boolean hasTool = false, hasInput = false;
        for (int x = 0; x < inv.getSizeInventory(); x++) {
            boolean inRecipe = false;
            ItemStack slot = inv.getStackInSlot(x);

            if (!slot.isEmpty()) {
                if (tool.test(slot)) {
                    if (!hasTool) {
                        hasTool = true;
                        inRecipe = true;
                    } else {
                        return false;
                    }
                } else if (input.test(slot)) {
                    if (!hasInput) {
                        hasInput = true;
                        inRecipe = true;
                    } else {
                        return false;
                    }
                }
                if (!inRecipe)
                    return false;
            }
        }
        return hasTool && hasInput;
    }


    @Override
    public ItemStack getCraftingResult(@Nonnull CraftingInventory inv) {
        return getRecipeOutput().copy();
    }

    @Override
    public boolean canFit(int width, int height) {
        return width * height >= 2;
    }

    @Nonnull
    @Override
    public ItemStack getRecipeOutput() {
        return result;
    }

    @Nonnull
    @Override
    public ResourceLocation getId() {
        return id;
    }

    @Nonnull
    @Override
    public IRecipeSerializer<?> getSerializer() {
        return Registrars.RecipeSerializers.TOOL_RECIPE;
    }

    @Nonnull
    @Override
    public IRecipeType<?> getType() {
        return IRecipeType.CRAFTING;
    }


    @Override
    public NonNullList<ItemStack> getRemainingItems(CraftingInventory inv) {
        NonNullList<ItemStack> nonnulllist = NonNullList.withSize(inv.getSizeInventory(), ItemStack.EMPTY);
        for (int i = 0; i < nonnulllist.size(); ++i) {
            ItemStack item = inv.getStackInSlot(i);
            if (tool.test(item)) {
                if (damageTool) {
                    item.attemptDamageItem(1, new Random(), null);
                }
                nonnulllist.set(i, item.copy());
            } else if (item.hasContainerItem()) {
                nonnulllist.set(i, item.getContainerItem());
            }
        }
        return nonnulllist;
    }

    @Override
    public String getGroup() {
        return group;
    }

    @SubscribeEvent
    public void dropExtra(PlayerEvent.ItemCraftedEvent event) {
        if (event.getPlayer() == null)
            return;
        PlayerEntity player = event.getPlayer();
        World world = player.getEntityWorld();
        if (isMatch(event.getInventory(), world)) {
            playSound(world, player.getPosition());
            if (!world.isRemote) {
                for (ItemStack drops : additionalDrops) {
                    player.entityDropItem(drops.copy());
                }
            }
        }
    }

    public void playSound(World world, BlockPos pos) {
        if (sound != null) {
            EnvironmentUtils.playSound(world, pos, sound, SoundCategory.PLAYERS, 0.25F, 2.5F);
        }
    }

    @Override
    public boolean isDynamic() {
        return false;
    }

    @Override
    public NonNullList<Ingredient> getIngredients() {
        return ingredients;
    }
}
