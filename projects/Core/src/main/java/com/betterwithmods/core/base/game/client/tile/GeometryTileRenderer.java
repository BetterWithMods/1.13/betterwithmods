/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.client.tile;

import com.betterwithmods.core.Core;
import com.betterwithmods.core.base.model.ModelGeometry;
import com.google.common.collect.Maps;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import java.util.Map;

public abstract class GeometryTileRenderer<T extends TileEntity> extends TileEntityRenderer<T> {

    private static Map<ResourceLocation, ModelGeometry> models = Maps.newHashMap();

    public abstract void renderModel(T tile, double x, double y, double z, float partialTicks, int destroyStage);

    @Override
    public void render(T tile, double x, double y, double z, float partialTicks, int destroyStage) {
        GlStateManager.pushMatrix();
        {
            GlStateManager.translated(x + 0.5D, y + 0.5, z + 0.5D);
            GlStateManager.enableRescaleNormal();
            GlStateManager.rotatef(180, 1, 0, 0);
            renderModel(tile, x, y, z, partialTicks, destroyStage);
        }
        GlStateManager.popMatrix();
    }


    public ModelGeometry get(ResourceLocation name) {
        ModelGeometry model = models.getOrDefault(name, new ModelGeometry(name));
        if (!models.containsKey(name)) {
            models.put(name, model);
        }
        if(model.getGeometry().getBones().size() < 1) {
            Core.LOGGER.error("Model missing geometry {}",name);
        }
        return model;
    }

}
