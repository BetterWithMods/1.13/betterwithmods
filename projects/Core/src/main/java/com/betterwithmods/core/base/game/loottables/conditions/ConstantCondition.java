/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.loottables.conditions;

import com.betterwithmods.core.References;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.loot.LootContext;
import net.minecraft.world.storage.loot.conditions.ILootCondition;

public class ConstantCondition<T> implements ILootCondition {
    private T value;

    public ConstantCondition(T value) {
        this.value = value;
    }

    @Override
    public boolean test(LootContext lootContext) {
        return false;
    }


    public static ILootCondition.IBuilder booleanBuilder(Boolean value) {
        return () -> new BooleanConstantCondition(value);
    }

    public T getValue() {
        return value;
    }

    public static class BooleanConstantCondition extends ConstantCondition<Boolean> {
        public BooleanConstantCondition(Boolean value) {
            super(value);
        }
    }

    public static class BooleanSerializer extends ILootCondition.AbstractSerializer<BooleanConstantCondition> {

        public BooleanSerializer() {
            super(new ResourceLocation(References.MODID_CORE, "boolean"), BooleanConstantCondition.class);
        }

        @Override
        public void serialize(JsonObject json, BooleanConstantCondition value, JsonSerializationContext context) {
            json.addProperty("value", value.getValue());
        }

        @Override
        public BooleanConstantCondition deserialize(JsonObject json, JsonDeserializationContext context) {
            boolean value = JSONUtils.getBoolean(json, "value");
            return new BooleanConstantCondition(value);
        }
    }


}
