/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core;

import com.betterwithmods.core.api.crafting.input.IInputSerializer;
import com.betterwithmods.core.api.crafting.output.IOutputSerializer;
import com.betterwithmods.core.api.crafting.result.IResultSerializer;
import com.betterwithmods.core.api.crafting.result.count.ICountSerializer;
import com.betterwithmods.core.base.game.recipes.ModuleCondition;
import com.betterwithmods.core.base.game.recipes.ToolRecipe;
import com.betterwithmods.core.base.game.recipes.ToolRecipeSerializer;
import com.betterwithmods.core.base.registration.RecipeSerializerRegistrar;
import com.betterwithmods.core.base.registration.custom.CountSerializerRegistrar;
import com.betterwithmods.core.base.registration.custom.InputSerializerRegistrar;
import com.betterwithmods.core.base.registration.custom.OutputSerializerRegistrar;
import com.betterwithmods.core.base.registration.custom.ResultSerializerRegistrar;
import com.betterwithmods.core.impl.crafting.input.BlockStateInputSerializer;
import com.betterwithmods.core.impl.crafting.input.InventoryInputSerializer;
import com.betterwithmods.core.impl.crafting.output.OutputList;
import com.betterwithmods.core.impl.crafting.output.OutputListSerializer;
import com.betterwithmods.core.impl.crafting.result.RelationshipResultSerializer;
import com.betterwithmods.core.impl.crafting.result.StackResultSerializer;
import com.betterwithmods.core.impl.crafting.result.count.Chance;
import com.betterwithmods.core.impl.crafting.result.count.Digit;
import com.betterwithmods.core.impl.crafting.result.count.Range;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.ObjectHolder;
import org.apache.logging.log4j.Logger;

@Mod.EventBusSubscriber
public class Registrars {

    public Registrars(String modid, Logger logger) {
        new RecipeSerializerRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<IRecipeSerializer<?>> registry) {
                CraftingHelper.register(ModuleCondition.Serializer.INSTANCE);
                register(registry, "tool_recipe", new ToolRecipeSerializer());
            }
        };

        new CountSerializerRegistrar(modid, logger) {

            @Override
            public void registerAll(IForgeRegistry<ICountSerializer<?>> registry) {
                register(registry, "chance", new Chance.ChanceSerializer());
                register(registry, "digit", new Digit.DigitSerializer());
                register(registry, "range", new Range.Serializer());
            }
        };

        new InputSerializerRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<IInputSerializer<?, ?>> registry) {
                register(registry, "blockstate", new BlockStateInputSerializer());
                register(registry, "inventory", new InventoryInputSerializer());
            }
        };

        new OutputSerializerRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<IOutputSerializer<?>> registry) {
                register(registry, "list", new OutputListSerializer());
            }
        };

        new ResultSerializerRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<IResultSerializer<?>> registry) {
                register(registry, "stack", new StackResultSerializer());
                register(registry, "relationship", new RelationshipResultSerializer());
            }
        };

    }

    @ObjectHolder(References.MODID_CORE)
    public static class RecipeSerializers {
        public static final IRecipeSerializer<ToolRecipe> TOOL_RECIPE = null;
    }

    @ObjectHolder(References.MODID_CORE)
    public static class ResultSerializers {
        public static final StackResultSerializer STACK = null;
        public static final RelationshipResultSerializer RELATIONSHIP = null;
    }

    @ObjectHolder(References.MODID_CORE)
    public static class InputSerializers {
        public static final BlockStateInputSerializer BLOCKSTATE = null;
        public static final InventoryInputSerializer INVENTORY = null;
    }

    @ObjectHolder(References.MODID_CORE)
    public static class CountSerializers {
        public static final ICountSerializer<Digit> DIGIT = null;
        public static final ICountSerializer<Range> RANGE = null;
        public static final ICountSerializer<Chance> CHANCE = null;
    }

    @ObjectHolder(References.MODID_CORE)
    public static class OutputSerializers {
        public static final IOutputSerializer<OutputList> LIST = null;
    }

}
