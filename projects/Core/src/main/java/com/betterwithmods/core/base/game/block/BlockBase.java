/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.block;

import com.betterwithmods.core.base.game.tile.TileBase;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.registries.ForgeRegistries;

import javax.annotation.Nullable;

public class BlockBase extends Block {
    private TileEntityType<?> tileEntityType;
    private boolean hasNoTileEntity;
    private ToolType harvestTool;
    private int harvestLevel;

    private BlockRenderLayer renderLayer = BlockRenderLayer.SOLID;

    public BlockBase(Properties properties) {
        super(properties);
    }

    protected void setRenderLayer(BlockRenderLayer renderLayer) {
        this.renderLayer = renderLayer;
    }

    public BlockRenderLayer getRenderLayer() {
        return this.renderLayer;
    }

    public void setHarvestTool(ToolType harvestTool) {
        this.harvestTool = harvestTool;
    }

    public void setHarvestLevel(int harvestLevel) {
        this.harvestLevel = harvestLevel;
    }

    @Nullable
    @Override
    public ToolType getHarvestTool(BlockState state) {
        return this.harvestTool;
    }

    @Override
    public int getHarvestLevel(BlockState state) {
        return this.harvestLevel;
    }


    @Override
    public void onBlockPlacedBy(World world, BlockPos pos, BlockState state, @Nullable LivingEntity placer, ItemStack stack) {
        onChanged(world, pos, state);
    }


    @Override
    public void neighborChanged(BlockState state, World world, BlockPos pos, Block fromBlock, BlockPos fromPos, boolean idk) {
        onChanged(world, pos, state);
        super.neighborChanged(state, world, pos, fromBlock, fromPos, idk);
    }

    @Override
    public void onReplaced(BlockState state, World world, BlockPos pos, BlockState newState, boolean isMoving) {
        if (state.getBlock() != newState.getBlock()) {
            if (hasTileEntity(state)) {
                TileEntity tile = world.getTileEntity(pos);
                if (tile instanceof TileBase) {
                    ((TileBase) tile).onRemoved();
                }

            }
        }
        super.onReplaced(state, world, pos, newState, isMoving);
        onChanged(world, pos, state);

    }

    public void onChanged(World world, BlockPos pos, BlockState state) {
        TileEntity tile = world.getTileEntity(pos);
        if (tile instanceof TileBase) {
            ((TileBase) tile).onChanged();
        }
    }

    @Override
    public boolean eventReceived(BlockState state, World world, BlockPos pos, int id, int param) {
        TileEntity tileentity = world.getTileEntity(pos);
        return tileentity != null && tileentity.receiveClientEvent(id, param);
    }

    @Nullable
    public INamedContainerProvider getContainerProvider(World world, BlockPos pos) {
        TileEntity tile = world.getTileEntity(pos);
        if (tile instanceof INamedContainerProvider) {
            return (INamedContainerProvider) tile;
        }
        return null;
    }


    @Override
    public BlockState rotate(BlockState state, IWorld world, BlockPos pos, Rotation rot) {
        if (state.has(BlockStateProperties.FACING)) {
            return state.with(BlockStateProperties.FACING, rot.rotate(state.get(BlockStateProperties.FACING)));
        } else if (state.has(BlockStateProperties.HORIZONTAL_FACING)) {
            return state.with(BlockStateProperties.HORIZONTAL_FACING, rot.rotate(state.get(BlockStateProperties.HORIZONTAL_FACING)));
        }
        return state;
    }


    public void findTileEntityType() {
        if (!hasNoTileEntity && this.tileEntityType == null) {
            for (TileEntityType<?> type : ForgeRegistries.TILE_ENTITIES.getValues()) {
                if (type.isValidBlock(this)) {
                    this.tileEntityType = type;
                    this.hasNoTileEntity = false;
                    return;
                }
            }
            //If we made it this far it probably doesn't have a TE
            this.hasNoTileEntity = true;
        }
    }

    public TileEntityType<?> getTileEntityType() {
        findTileEntityType();
        return tileEntityType;
    }

    @Override
    public boolean hasTileEntity(BlockState state) {
        return getTileEntityType() != null;
    }

    @Nullable
    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
        if (hasTileEntity(state)) {
            return getTileEntityType().create();
        }
        return null;
    }
}
