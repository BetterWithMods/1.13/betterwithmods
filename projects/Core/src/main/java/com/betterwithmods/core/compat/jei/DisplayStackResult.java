/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.compat.jei;

import com.betterwithmods.core.api.IPositionedText;
import com.betterwithmods.core.api.crafting.result.IResult;
import com.betterwithmods.core.api.crafting.result.IResultRenderer;
import com.betterwithmods.core.api.crafting.result.IResultSerializer;
import com.betterwithmods.core.impl.PositionedText;
import com.betterwithmods.core.impl.crafting.result.BaseStackResult;
import com.betterwithmods.core.impl.crafting.result.StackResultRenderer;
import com.google.common.collect.Lists;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;

import javax.annotation.Nullable;
import java.util.List;

//THIS SHOULD ONLY EVER BE USED FOR JEI.
public class DisplayStackResult extends BaseStackResult {

    private ItemStack stack;

    public DisplayStackResult(ItemStack stack) {
        this.stack = stack;
    }

    @Override
    public boolean equals(IResult result) {
        if (result instanceof DisplayStackResult) {
            return ((DisplayStackResult) result).stack.isItemEqual(stack);
        }
        return false;
    }

    @Override
    public <T> ItemStack get(@Nullable T input) {
        return stack;
    }

    @Override
    public IResult copy() {
        return new DisplayStackResult(stack);
    }

    @Override
    public IResultSerializer<?> getSerializer() {
        throw new IllegalStateException("This method should never be called for any reason");
    }

    @Override
    public ItemStack displayStack() {
        return stack.copy();
    }

    @Override
    public List<IPositionedText> displayCount() {
        List<IPositionedText> text = Lists.newArrayList();
        int digit = stack.getCount();
        if (digit > 1) {
            text.add(new PositionedText(new StringTextComponent(Integer.toString(digit)), 14, 9, 1));
        }
        return text;
    }

    @Override
    public ResourceLocation getId() {
        return stack.getItem().getRegistryName();
    }

    @Override
    public Class<ItemStack> getType() {
        return ItemStack.class;
    }

    @Override
    public ITextComponent displayText() {
        return displayStack().getTextComponent();
    }

    @Override
    public IResultRenderer<ItemStack> getRenderer() {
        return new StackResultRenderer();
    }
}
