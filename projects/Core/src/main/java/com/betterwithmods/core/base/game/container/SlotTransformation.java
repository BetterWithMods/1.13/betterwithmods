/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.container;

import com.betterwithmods.core.api.base.container.ISlot;
import com.betterwithmods.core.api.base.container.ISlotRange;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;

import java.util.Optional;

public class SlotTransformation {

    private ResourceLocation startRange, endRange;

    private Optional<Ingredient> filter;
    private boolean direction;

    public SlotTransformation(ResourceLocation startRange, ResourceLocation endRange, boolean direction, Optional<Ingredient> filter) {
        this.startRange = startRange;
        this.endRange = endRange;
        this.filter = filter;
        this.direction = direction;
    }

    public SlotTransformation(ResourceLocation startRange, ResourceLocation endRange, Optional<Ingredient> filter) {
        this(startRange, endRange, false, filter);
    }

    public SlotTransformation(ResourceLocation startRange, ResourceLocation endRange, boolean direction) {
        this(startRange, endRange, direction, Optional.empty());
    }

    public SlotTransformation(ResourceLocation startRange, ResourceLocation endRange) {
        this(startRange, endRange, false);
    }

    public void onPostTransfer(net.minecraft.inventory.container.Slot slot, ItemStack start, ItemStack end) {
        //NO-OP
    }

    public void onPreTransfer(net.minecraft.inventory.container.Slot slot, ItemStack start) {
        //NO-OP
    }


    public ItemStack transferStackRange(InventoryContainer container, ItemStack stack, int index) {
        ISlotRange start = container.getRange(startRange);
        ISlotRange end = container.getRange(endRange);
        ISlot slot = new Slot(index);
        ItemStack copy = stack.copy();
        if (start != null && end != null) {
            if (start.contains(slot)) {
                if (filter.map(i -> i.test(stack)).orElse(true)) {
                    net.minecraft.inventory.container.Slot s = container.getSlot(index);
                    onPreTransfer(s, stack);
                    if (!container.mergeItemStack(stack, end.getStart(), end.getEnd(), direction)) {
                        return ItemStack.EMPTY;
                    }
                    onPostTransfer(s, stack, copy);
                }
            }
        }
        return stack;
    }
}
