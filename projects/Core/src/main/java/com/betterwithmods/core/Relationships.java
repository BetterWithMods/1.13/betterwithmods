/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core;

import com.betterwithmods.core.relationary.Relation;
import com.betterwithmods.core.relationary.Shape;
import net.minecraft.util.ResourceLocation;

public class Relationships {


    public static final Relation ACACIA_WOOD = relation(References.MINECRAFT, "acacia_wood");
    public static final Relation BIRCH_WOOD = relation(References.MINECRAFT, "birch_wood");
    public static final Relation DARK_OAK_WOOD = relation(References.MINECRAFT, "dark_oak_wood");
    public static final Relation JUNGLE_WOOD = relation(References.MINECRAFT, "jungle_wood");
    public static final Relation OAK_WOOD = relation(References.MINECRAFT, "oak_wood");
    public static final Relation SPRUCE_WOOD = relation(References.MINECRAFT, "spruce_wood");

    public static final Relation STRIPPED_ACACIA_WOOD = relation(References.MINECRAFT, "stripped_acacia_wood");
    public static final Relation STRIPPED_BIRCH_WOOD = relation(References.MINECRAFT, "stripped_birch_wood");
    public static final Relation STRIPPED_DARK_OAK_WOOD = relation(References.MINECRAFT, "stripped_dark_oak_wood");
    public static final Relation STRIPPED_JUNGLE_WOOD = relation(References.MINECRAFT, "stripped_jungle_wood");
    public static final Relation STRIPPED_OAK_WOOD = relation(References.MINECRAFT, "stripped_oak_wood");
    public static final Relation STRIPPED_SPRUCE_WOOD = relation(References.MINECRAFT, "stripped_spruce_wood");

    public static final Relation SOULFORGED_STEEL = relation(References.MODID_MECHANIST, "soulforged_steel");

    public static final Shape BLOCK = shape(References.MODID_CORE, "block");
    public static final Shape LOG = shape(References.MODID_CORE, "log");
    public static final Shape BARK = shape(References.MODID_CORE, "bark");
    public static final Shape PLANK = shape(References.MODID_CORE, "plank");
    public static final Shape SIDING = shape(References.MODID_CORE, "siding");
    public static final Shape MOULDING = shape(References.MODID_CORE, "moulding");
    public static final Shape CORNER = shape(References.MODID_CORE, "corner");

    public static final Shape TABLE = shape(References.MODID_CORE, "table");
    public static final Shape CHAIR = shape(References.MODID_CORE, "chair");

    public static final Shape DUST = shape(References.FORGE, "dust");
    public static final Shape INGOT = shape(References.FORGE, "ingot");
    public static final Shape NUGGET = shape(References.FORGE, "nugget");


    public static Relation.Wrapper relation(String mod, String name) {
        return relation(new ResourceLocation(mod, name));
    }

    public static Relation.Wrapper relation(ResourceLocation name) {
        return new Relation.Wrapper(name);
    }

    public static Shape shape(String mod, String name) {
        return shape(new ResourceLocation(mod, name));
    }

    public static Shape shape(ResourceLocation name) {
        return new Shape(name);
    }


}
