/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.impl;

import com.betterwithmods.core.api.souls.ISoulStorage;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;

public class CapabilitySoulHolder {

    @CapabilityInject(ISoulStorage.class)
    public static Capability<ISoulStorage> SOUL = null;

    public static void register() {
//        CapabilityManager.INSTANCE.register(ISoulStorage.class, new Capability.IStorage<ISoulStorage>() {
//            @Nullable
//            @Override
//            public INBT writeNBT(Capability<ISoulStorage> capability, ISoulStorage instance, Direction enumFacing) {
//                return instance.serializeNBT();
//            }
//
//            @Override
//            public void readNBT(Capability<ISoulStorage> capability, ISoulStorage instance, Direction enumFacing, INBT tag) {
//                instance.deserializeNBT((CompoundNBT) tag);
//            }
//        }, SoulStorage::new);
    }
}
