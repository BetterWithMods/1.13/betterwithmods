/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.relationary;

import com.google.common.collect.Maps;
import net.minecraft.util.ResourceLocation;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;

public class Container<T> {
    private Map<ResourceLocation, T> registry = Maps.newHashMap();

    private Function<T, ResourceLocation> getRegistryName;

    public Container(Collection<T> list, Function<T, ResourceLocation> getRegistryName) {
       this(getRegistryName);
        addAll(list);
    }

    public Container(Function<T, ResourceLocation> getRegistryName) {
        this.getRegistryName = getRegistryName;
    }

    public void addAll(Collection<T> collection) {
        for(T t: collection)
            add(t);;
    }

    public void add(T t) {
        registry.put(getRegistryName.apply(t), t);
    }

    public T get(ResourceLocation location) {
        return registry.get(location);
    }

    public T getOrDefault(ResourceLocation location, T t) {
        return registry.getOrDefault(location, t);
    }

    public boolean containsKey(ResourceLocation location) {
        return registry.containsKey(location);
    }

    public void clear() {
        this.registry.clear();
    }

    public Collection<T> values() {
        return registry.values();
    }
}

