/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.RedstoneWireBlock;
import net.minecraft.entity.Entity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.LightType;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;

import java.lang.reflect.Field;
import java.util.function.BiPredicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class WorldUtils {


    public static Vec3d fromEntity(Entity entity) {
        return new Vec3d(entity.posX, entity.posY, entity.posZ);
    }

    public static int getWorldUpdateLCG(World world) {
        try {
            Field field = world.getClass().getDeclaredField("updateLCG");
            return (int) field.get(world);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return -1;
    }


    public static boolean isValidForSnow(World world, BlockPos pos) {
        Biome biome = world.getBiome(pos);
        if (biome.getTemperature(pos) >= 0.15F) {
            return false;
        }
        return pos.getY() >= 0 && pos.getY() < 256 && world.getLightFor(LightType.BLOCK, pos) < 10;

    }

    public static boolean isPowered(World world, BlockPos pos) {
        return calculateNeighborStrength(world, pos, world.getBlockState(pos)) > 0;
    }

    public static int calculateNeighborStrength(World world, BlockPos pos, BlockState state) {
        int max = 0;
        for (Direction facing : DirectionUtils.FACINGS) {
            int i = calculateInputStrength(world, pos, state, facing);
            if (i > max) {
                max = i;
            }
        }
        return max;
    }

    public static int calculateInputStrength(World world, BlockPos pos, BlockState state, Direction facing) {
        BlockPos blockpos = pos.offset(facing);
        int i = world.getRedstonePower(blockpos, facing);
        if (i > 0) {
            return i;
        } else {
            BlockState iblockstate = world.getBlockState(blockpos);
            return Math.max(i, iblockstate.getBlock() == Blocks.REDSTONE_WIRE ? iblockstate.get(RedstoneWireBlock.POWER) : 0);
        }
    }

    public static Iterable<BlockPos> getAllBlocks(Vec3d to, Vec3d from) {
        BlockPos fromPos = new BlockPos(to.x, to.y, to.z);
        BlockPos toPos = new BlockPos(Math.ceil(from.x), Math.ceil(from.y), Math.ceil(from.z));
        return BlockPos.PooledMutableBlockPos.getAllInBoxMutable(fromPos, toPos);
    }


    public static Iterable<BlockPos> getAllBlocks(AxisAlignedBB bb) {
        BlockPos from = new BlockPos(Math.ceil(bb.minX), Math.ceil(bb.minY), Math.ceil(bb.minZ));
        BlockPos to = new BlockPos(Math.floor(bb.maxX - 0.5), Math.floor(bb.maxY - 0.5), Math.floor(bb.maxZ - 0.5));
        return BlockPos.PooledMutableBlockPos.getAllInBoxMutable(to, from);
    }

    public static Stream<BlockPos> streamArea(Iterable<BlockPos> iterable) {
        return StreamSupport.stream(iterable.spliterator(), false);
    }

    public static Stream<BlockPos> streamArea(AxisAlignedBB bb) {
        return StreamSupport.stream(getAllBlocks(bb).spliterator(), false);
    }


    public static boolean applyAll(IWorldReader world, Iterable<BlockPos> positions, BiPredicate<IWorldReader, BlockPos> test) {
        return streamArea(positions).allMatch(pos -> test.test(world, pos));
    }

    public static boolean onAxis(BlockPos base, BlockPos test, Direction.Axis axis) {
        switch (axis) {
            case X:
                return base.getZ() == test.getZ() && base.getY() == test.getY();
            case Y:
                return base.getZ() == test.getZ() && base.getX() == test.getX();
            case Z:
                return base.getX() == test.getX() && base.getY() == test.getY();
            default:
                return false;
        }
    }


    public static boolean isSideSolid(IWorldReader world, BlockPos pos, Direction side) {
        return Block.func_220055_a(world, pos, side);
    }
}

