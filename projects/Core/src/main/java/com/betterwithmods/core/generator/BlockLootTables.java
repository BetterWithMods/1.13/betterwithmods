/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.generator;

import com.google.common.collect.Sets;
import net.minecraft.block.Block;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.storage.loot.ConstantRange;
import net.minecraft.world.storage.loot.ItemLootEntry;
import net.minecraft.world.storage.loot.LootPool;
import net.minecraft.world.storage.loot.LootTable;
import net.minecraft.world.storage.loot.conditions.SurvivesExplosion;

import java.util.Set;
import java.util.function.BiConsumer;

public abstract class BlockLootTables extends LootTables {

    private final Set<Block> blocks;

    public BlockLootTables(Set<Block> blocks) {
        this.blocks = blocks;
    }

    public static LootTable.Builder basicBlock(IItemProvider item) {
        return LootTable.builder().addLootPool(
                pool("main")
                        .acceptCondition(SurvivesExplosion.builder())
                        .rolls(ConstantRange.of(1))
                        .addEntry(ItemLootEntry.builder(item)));
    }


    public static LootPool.Builder pool(String name) {
        return LootPool.builder().name(name);
    }

    protected void registerBasicBlock(Block block) {
        registerBasicBlock(block, block);
    }

    protected void registerBasicBlock(Block block, IItemProvider item) {
        registerLootTable(block, basicBlock(item));
    }

    protected void registerLootTable(Block block, LootTable.Builder builder) {
        registerLootTable(block::getLootTable, builder);
    }

    @Override
    public void accept(BiConsumer<ResourceLocation, LootTable.Builder> factory) {
        register();
        Set<ResourceLocation> names = Sets.newHashSet();

        for (Block block : blocks) {
            ResourceLocation lootTable = block.getLootTable();
            if (lootTable != net.minecraft.world.storage.loot.LootTables.EMPTY && names.add(lootTable)) {
                LootTable.Builder builder = this.builders.remove(lootTable);
                if (builder == null) {
                    throw new IllegalStateException(String.format("Missing loottable '%s' for '%s'", lootTable, Registry.BLOCK.getKey(block)));
                }
                factory.accept(lootTable, builder);
            }
        }

    }
}

