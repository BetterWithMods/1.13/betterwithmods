/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties;

import com.betterwithmods.core.Registrars;
import com.betterwithmods.core.api.crafting.input.IInput;
import com.betterwithmods.core.api.crafting.input.IInputSerializer;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipe;
import com.betterwithmods.core.api.crafting.output.IOutput;
import com.betterwithmods.core.api.crafting.output.IOutputSerializer;
import com.betterwithmods.core.api.crafting.result.IResult;
import com.betterwithmods.core.api.crafting.result.IResultSerializer;
import com.betterwithmods.core.api.crafting.result.count.ICount;
import com.betterwithmods.core.api.crafting.result.count.ICountSerializer;
import com.betterwithmods.core.impl.Registries;
import com.betterwithmods.core.impl.crafting.ItemStackBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.registries.ForgeRegistries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;

public class DeserializeUtils {
    public static Logger LOGGER = LogManager.getLogger();

    public static ResourceLocation getRL(JsonObject json, String key) {
        return new ResourceLocation(json.get(key).getAsString());
    }

    public static void writeBlockState(JsonObject stateObject, BlockState state) {
        String blockName = state.getBlock().getRegistryName().toString();
        stateObject.addProperty("block", blockName);
    }

    public static BlockState readBlockState(JsonObject json, String key) {
        JsonObject stateObject = JSONUtils.getJsonObject(json, key);
        ResourceLocation blockName = getRL(stateObject, "block");
        //TODO properties
        return Registries.get(ForgeRegistries.BLOCKS, blockName).map(Block::getDefaultState).orElse(Blocks.AIR.getDefaultState());
    }

    public static void serializeItemStack(ItemStack stack, JsonObject object) {
        object.addProperty("item", stack.getItem().getRegistryName().toString());
        object.addProperty("count", stack.getCount());
        CompoundNBT tag = stack.getTag();
        if (tag != null) {
            object.addProperty("nbt", tag.toString());
        }
    }

    public static NonNullList<IResult> getResults(JsonArray resultsArray) {
        NonNullList<IResult> results = NonNullList.create();
        for (JsonElement element : resultsArray) {
            JsonObject object = element.getAsJsonObject();
            IResultSerializer<?> resultSerializer = Registries.getOptionalSerializer(Registries.RESULT_SERIALIZERS, object).orElse(Registrars.ResultSerializers.STACK);
            IResult result = resultSerializer.read(object);
            results.add(result);
        }
        return results;
    }


    public static void writeResults(PacketBuffer buffer, Collection<IResult> results) {
        LOGGER.debug("Writing Results: {}", results.size());
        buffer.writeInt(results.size());
        for (IResult result : results) {
            writeResult(buffer, result);
        }
    }

    public static <T extends IResult> void writeResult(PacketBuffer buffer, T result) {
        IResultSerializer<T> serializer = (IResultSerializer<T>) result.getSerializer();
        LOGGER.debug("Writing Result: {}", serializer.getRegistryName());
        buffer.writeResourceLocation(serializer.getRegistryName());
        serializer.write(buffer, result);
    }

    public static NonNullList<IResult> readResults(PacketBuffer buffer) {
        int size = buffer.readInt();
        LOGGER.debug("Reading Results: {}", size);
        NonNullList<IResult> results = NonNullList.create();
        for (int i = 0; i < size; i++) {
            results.add(readResult(buffer));
        }
        return results;
    }

    public static IResult readResult(PacketBuffer buffer) {
        ResourceLocation id = buffer.readResourceLocation();
        LOGGER.debug("Reading Result:{}", id);
        IResultSerializer<?> resultSerializer = Registries.get(Registries.RESULT_SERIALIZERS, id).orElse(Registrars.ResultSerializers.STACK);
        return resultSerializer.read(buffer);
    }

    public static <T extends IOutput> void writeIOutput(T output, JsonObject json) {
        if(output != null) {
            IOutputSerializer<T> serializer = (IOutputSerializer<T>) output.getSerializer();
            json.addProperty("type", serializer.getRegistryName().toString());
            serializer.write(output, json);
        }
    }


    public static <T extends IOutput> void writeIOutput(PacketBuffer buffer, T output) {
        buffer.writeBoolean(output != null);
        if(output != null) {
            IOutputSerializer<T> serializer = (IOutputSerializer<T>) output.getSerializer();
            buffer.writeResourceLocation(serializer.getRegistryName());
            serializer.write(buffer, output);
        }
    }

    public static <T extends IOutput> T readIOutput(JsonObject json) {
        if(!DeserializeUtils.isEmpty(json)) {
            ResourceLocation serializerId = DeserializeUtils.getRL(json, "type");
            IOutputSerializer<T> serializer = (IOutputSerializer<T>) Registries.get(Registries.OUTPUT_SERIALIZERS, serializerId).orElseThrow(() -> new IllegalStateException("Unable to find OUtput Serialzier:" + serializerId));
            return serializer.read(json);
        }
        return null;
    }

    public static <T extends IOutput> T readIOutput(PacketBuffer buffer) {
        if(buffer.readBoolean()) {
            ResourceLocation serializerId = buffer.readResourceLocation();
            IOutputSerializer<T> serializer = (IOutputSerializer<T>) Registries.get(Registries.OUTPUT_SERIALIZERS, serializerId).orElseThrow(() -> new IllegalStateException("Unable to find OUtput Serialzier:" + serializerId));
            return serializer.read(buffer);
        }
        return null;
    }

    public static <T, I extends IInput<T, ?>> void writeIInput(PacketBuffer buffer, I input) {
        IInputSerializer<T, I> serializer = (IInputSerializer<T, I>) input.getSerializer();
        buffer.writeResourceLocation(serializer.getRegistryName());
        serializer.write(buffer, input);
    }

    public static <T, R extends IMultiplexRecipe<T, R>, I extends IInput<T, R>> I readIInput(JsonObject json) {
        ResourceLocation serializerId = DeserializeUtils.getRL(json, "type");
        IInputSerializer<T, I> serializer = (IInputSerializer<T, I>) Registries.get(Registries.INPUT_SERIALIZERS, serializerId).orElseThrow(() -> new IllegalStateException("Unable to find Input Serialzier:" + serializerId));
        return serializer.read(json);
    }


    public static <T, R extends IMultiplexRecipe<T, R>, I extends IInput<T, R>> I readIInput(PacketBuffer buffer) {
        ResourceLocation serializerId = buffer.readResourceLocation();
        IInputSerializer<T, I> serializer = (IInputSerializer<T, I>) Registries.get(Registries.INPUT_SERIALIZERS, serializerId).orElseThrow(() -> new IllegalStateException("Unable to find Input Serialzier:" + serializerId));
        return serializer.read(buffer);
    }

    public static ICount readCount(JsonElement parent) {
        if (parent.isJsonObject()) {
            JsonObject object = parent.getAsJsonObject();

            if (JSONUtils.isJsonPrimitive(object, "count")) {
                return Registrars.CountSerializers.DIGIT.read(object.get("count"));
            }

            JsonObject countObject = JSONUtils.getJsonObject(object, "count");
            return Registries.getSerializer(Registries.COUNT_SERIALIZERS, countObject).read(countObject);
        }
        return null;
    }


    public static <C extends ICount> void writeCount(C count, JsonObject json) {
        json.addProperty("type", count.getSerializer().getRegistryName().toString());
        ICountSerializer<C> serializer = (ICountSerializer<C>) count.getSerializer();
        serializer.write(count, json);
    }

    public static  <T, I extends IInput<T, ?>> void writeInput(I input, JsonObject object) {
        JsonObject jsonInput = new JsonObject();
        DeserializeUtils.writeIInput(input, jsonInput);
        object.add("input", jsonInput);
    }

    public static <T extends IOutput> void writeOutput(T outputs, JsonObject object) {
        JsonObject jsonInput = new JsonObject();
        DeserializeUtils.writeIOutput(outputs, jsonInput);
        object.add("result", jsonInput);
    }

    public static <T, I extends IInput<T, ?>> void writeIInput(I input, JsonObject object) {
        IInputSerializer<T, I> serializer = (IInputSerializer<T, I>) input.getSerializer();
        object.addProperty("type", serializer.getRegistryName().toString());
        serializer.write(input, object);
    }

    public static void writeItemStackBuilder(ItemStackBuilder builder, JsonObject json) {

        Item item = builder.getItem();
        CompoundNBT tag = builder.getTag();
        ICount count = builder.getCountProvider();
        json.addProperty("item", item.getRegistryName().toString());
        if (tag != null) {
            json.addProperty("tag", tag.toString());
        }
        JsonObject jsonCount = new JsonObject();
        DeserializeUtils.writeCount(count, jsonCount);
        json.add("count", jsonCount);
    }

    public static void writeResults(Collection<IResult> results, JsonObject object) {
        JsonArray outputs = new JsonArray();
        for (IResult result : results) {
            JsonObject jsonOutput = new JsonObject();
            writeResults(result, jsonOutput);
            outputs.add(jsonOutput);
        }
        object.add("results", outputs);
    }

    public static void writeResults(IResult result, JsonObject object) {
        IResultSerializer resultSerializer = result.getSerializer();
        object.addProperty("type", resultSerializer.getRegistryName().toString());
        resultSerializer.write(result, object);
    }


    public static void writeResultContainer(Collection<IResult> results, JsonObject object) {
        JsonObject jsonResult = new JsonObject();
        jsonResult.addProperty("type", "betterwithmods_core:list");
        DeserializeUtils.writeResults(results, jsonResult);
        object.add("result", jsonResult);
    }

    public static NonNullList<Ingredient> readIngredients(JsonObject object) {
        JsonArray inputs = JSONUtils.getJsonArray(object, "inputs");
        NonNullList<Ingredient> ingredients = NonNullList.create();
        for (JsonElement element : inputs) {
            //Have to pass the JsonElement as ingredients can be either arrays or objects.
            ingredients.add(Ingredient.deserialize(element));
        }
        return ingredients;
    }

    public static NonNullList<Ingredient> readIngredients(PacketBuffer buffer) {
        NonNullList<Ingredient> ingredients = NonNullList.create();
        int size = buffer.readInt();
        for (int i = 0; i < size; i++) {
            ingredients.add(Ingredient.read(buffer));
        }
        return ingredients;
    }

    public static void writeIngredients(PacketBuffer buffer, NonNullList<Ingredient> ingredients) {
        buffer.writeInt(ingredients.size());
        for (Ingredient ingredient : ingredients) {
            ingredient.write(buffer);
        }
    }

    public static void writeIngredients(JsonObject json, NonNullList<Ingredient> ingredients, String keyArray) {
        JsonArray inputs = new JsonArray();
        for (Ingredient ingredient : ingredients) {
            inputs.add(ingredient.serialize());
        }
        json.add(keyArray, inputs);
    }

    public static boolean isEmpty(JsonElement element) {
        if(element.isJsonObject()) {
            return element.getAsJsonObject().entrySet().isEmpty();
        }
        return false;
    }

    public static JsonArray toArray(JsonObject json, Vec3d vec3d) {
        JsonArray array = new JsonArray();
        array.add(vec3d.getX());
        array.add(vec3d.getY());
        array.add(vec3d.getZ());
        return array;
    }

    public static Vec3d fromArray(JsonArray array) {
        return new Vec3d(array.get(0).getAsDouble(), array.get(1).getAsDouble(),array.get(2).getAsDouble());
    }

}
