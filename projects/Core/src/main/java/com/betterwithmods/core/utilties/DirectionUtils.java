/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.Rotation;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;

public class DirectionUtils {

    private static final Direction[][] AXIS_TO_FACING = new Direction[][]{
            new Direction[]{Direction.WEST, Direction.EAST},
            new Direction[]{Direction.UP, Direction.DOWN},
            new Direction[]{Direction.NORTH, Direction.SOUTH}
    };

    public static Direction[] FACINGS = Direction.values();

    public static Iterable<Direction> fromAxis(Direction.Axis axis) {
        if (axis != null)
            return Arrays.asList(AXIS_TO_FACING[axis.ordinal()]);
        return Collections.emptyList();
    }

    public static EnumSet<Direction> FACING_SET = EnumSet.allOf(Direction.class);

    public static EnumSet<Direction> HORIZONTAL_SET = EnumSet.complementOf(EnumSet.of(Direction.DOWN, Direction.UP));
    public static EnumSet<Direction> ALL_EXCEPT_DOWN = EnumSet.complementOf(EnumSet.of(Direction.DOWN));

    public static Iterable<Direction> excluding(Direction facing) {
        return EnumSet.complementOf(EnumSet.of(facing));
    }

    public static Iterable<Direction> excluding(Direction f, Direction... f2) {
        return EnumSet.complementOf(EnumSet.of(f, f2));
    }


    public static Direction rotation(Direction direction, Rotation rotation) {
        if (rotation == Rotation.CLOCKWISE_90) {
            return direction.rotateY();
        } else if (rotation == Rotation.COUNTERCLOCKWISE_90) {
            return direction.rotateYCCW();
        } else if (rotation == Rotation.CLOCKWISE_180) {
            return direction.rotateY().rotateY();
        }
        return direction;
    }

    public static void write(CompoundNBT compound, String tag, @Nullable Direction facing) {
        if (facing != null) {
            compound.putInt(tag, facing.getIndex());
        }
    }

    @Nullable
    public static Direction read(CompoundNBT compound, String tag) {
        if (compound.contains(tag)) {
            return FACINGS[compound.getInt(tag)];
        }
        return null;
    }

}
