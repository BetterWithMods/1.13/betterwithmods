/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.model;

import com.betterwithmods.core.utilties.MiscUtils;
import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.util.JSONUtils;

import javax.vecmath.Vector3d;
import java.util.List;

public class Bone {

    private final String name;
    private final Vector3d pivot, rotation;
    private final boolean mirror;

    private final List<Cube> cubes;

    public Bone(String name, Vector3d pivot, Vector3d rotation, boolean mirror, List<Cube> cubes) {
        this.name = name;
        this.pivot = pivot;
        this.rotation = rotation;
        this.mirror = mirror;
        this.cubes = cubes;
    }

    public String getName() {
        return name;
    }

    public Vector3d getPivot() {
        return pivot;
    }

    public Vector3d getRotation() {
        return rotation;
    }

    public boolean isMirror() {
        return mirror;
    }

    public List<Cube> getCubes() {
        return cubes;
    }

    public static Bone deserialize(JsonObject object) {

        String name = JSONUtils.getString(object, "name", "invalid");

        Vector3d pivot = new Vector3d(0, 0, 0);
        if (JSONUtils.hasField(object, "pivot")) {
            pivot = MiscUtils.parseVec3d(JSONUtils.getJsonArray(object, "pivot"));
        }

        Vector3d rotation = new Vector3d(0, 0, 0);
        if (JSONUtils.hasField(object, "rotation")) {
            rotation = MiscUtils.parseVec3d(JSONUtils.getJsonArray(object, "rotation"));
        }

        boolean mirror = JSONUtils.getBoolean(object, "mirror", false);

        List<Cube> cubes = Lists.newArrayList();

        JsonArray cubesArray = JSONUtils.getJsonArray(object, "cubes", new JsonArray());
        for (JsonElement element : cubesArray) {
            if (element.isJsonObject()) {
                JsonObject o = element.getAsJsonObject();

                cubes.add(Cube.deserialize(o));
            }
        }

        return new Bone(name, pivot, rotation, mirror, cubes);
    }
}
