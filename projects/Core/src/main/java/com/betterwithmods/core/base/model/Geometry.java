/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.model;

import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Geometry {

    private final ResourceLocation registryName;
    private final int textureWidth, textureHeight;
    private final Map<String, Bone> bones;

    protected static int VERSION;

    public Geometry(ResourceLocation registryName) {
        this(registryName, 0, 0, Lists.newArrayList());
    }

    public Geometry(ResourceLocation registryName, int textureWidth, int textureHeight, List<Bone> bones) {
        this.registryName = registryName;
        this.textureWidth = textureWidth;
        this.textureHeight = textureHeight;
        this.bones = bones.stream().collect(Collectors.toMap(Bone::getName, Function.identity()));
    }

    public ResourceLocation getRegistryName() {
        return registryName;
    }

    public int getTextureWidth() {
        return textureWidth;
    }

    public int getTextureHeight() {
        return textureHeight;
    }

    public Collection<Bone> getBones() {
        return bones.values();
    }

    public Bone getBone(String name) {
        return bones.get(name);
    }

    public static Geometry deserialize(ResourceLocation registryName, JsonObject object) {
        String path = registryName.getPath();
        String geoEntity = String.format("geometry.%s", path);

        if (!JSONUtils.hasField(object, geoEntity)) {
            throw new IllegalStateException("Model must contain object: " + geoEntity);
        }

        JsonObject geometry = JSONUtils.getJsonObject(object, geoEntity);


        int w = JSONUtils.getInt(geometry, "texturewidth", 16);
        int h = JSONUtils.getInt(geometry, "textureheight", 16);

        JsonArray boneArray = JSONUtils.getJsonArray(geometry, "bones", new JsonArray());
        List<Bone> bones = Lists.newArrayList();
        for (JsonElement element : boneArray) {
            if (element.isJsonObject()) {
                JsonObject o = element.getAsJsonObject();
                bones.add(Bone.deserialize(o));
            }
        }
        return new Geometry(registryName, w, h, bones);
    }


    public static class Wrapper extends Geometry {
        private int lastVersion = -1;

        private Geometry cachedGeometry;

        private Runnable callback;

        public Wrapper(ResourceLocation id) {
            super(id);
        }

        private void validateCache() {
            if (lastVersion != VERSION) {
                this.cachedGeometry = GeometryManager.getOrCreate(getRegistryName());
                this.lastVersion = VERSION;
                this.callback.run();
            }
        }

        @Override
        public int getTextureHeight() {
            validateCache();
            return cachedGeometry.getTextureHeight();
        }

        @Override
        public int getTextureWidth() {
            validateCache();
            return cachedGeometry.getTextureWidth();
        }

        @Override
        public Collection<Bone> getBones() {
            validateCache();
            return cachedGeometry.getBones();
        }

        @Override
        public Bone getBone(String name) {
            return cachedGeometry.getBone(name);
        }

        public void setCallback(Runnable callback) {
            this.callback = callback;
        }
    }
}
