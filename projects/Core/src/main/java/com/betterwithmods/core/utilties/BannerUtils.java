/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties;


import com.google.common.collect.Lists;
import net.minecraft.client.renderer.BannerTextures;
import net.minecraft.item.BannerItem;
import net.minecraft.item.DyeColor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.tileentity.BannerPattern;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.util.INBTSerializable;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BannerUtils {

    @OnlyIn(Dist.CLIENT)
    public static BannerTextures.Cache makeCache(String modid, String id, String baseTexture, String path) {
        return new BannerTextures.Cache(new ResourceLocation(modid, id).toString(), new ResourceLocation(modid, baseTexture), new ResourceLocation(modid, path).toString());
    }

    public static void readArray(BannerData[] array, CompoundNBT tag) {
        ListNBT list = tag.getList("Banners", 10);
        for (int i = 0; i < array.length; i++) {
            if (list.size() > i) {
                BannerData data = new BannerData();
                data.deserializeNBT(list.getCompound(i));
                array[i] = data;
            }
        }
    }

    public static void writeArray(BannerData[] banners, CompoundNBT tag) {
        ListNBT list = new ListNBT();
        for (BannerData data : banners) {
            if (data != null)
                list.add(data.serializeNBT());
        }
        tag.put("Banners", list);
    }

    public static Optional<BannerData> fromStack(ItemStack stack) {
        if (stack.getItem() instanceof BannerItem) {
            return Optional.of(new BannerData(stack));
        }
        return Optional.empty();
    }

    public static BannerData fromPatternColors(PatternColor... data) {
        return new BannerData(data);
    }

    public static class BannerData implements INBTSerializable<CompoundNBT> {
        //initialize list with base
        private PatternColor base = new PatternColor(BannerPattern.BASE, DyeColor.WHITE);

        List<PatternColor> patternColors = Lists.newArrayList();

        private BannerData(PatternColor... patternColors) {
            this.patternColors.addAll(Lists.newArrayList(patternColors));
        }

        private BannerData(ItemStack stack) {
            if (stack.getItem() instanceof BannerItem) {
                BannerItem item = (BannerItem) stack.getItem();
                this.base = new PatternColor(BannerPattern.BASE, item.getColor());
                CompoundNBT tag = stack.getChildTag("BlockEntityTag");
                if (tag != null) {
                    deserializeNBT(tag);
                }
            }
        }

        private BannerData() {
        }

        @Override
        public CompoundNBT serializeNBT() {
            CompoundNBT tag = new CompoundNBT();

            ListNBT list = new ListNBT();

            for (PatternColor pc : patternColors) {
                list.add(pc.serializeNBT());
            }
            tag.putInt("Base", base.getColor().getId());
            tag.put("Patterns", list);
            return tag;
        }


        @Override
        public void deserializeNBT(CompoundNBT tag) {
            if (tag != null && tag.contains("Patterns")) {
                if (tag.contains("Base")) {
                    base = new PatternColor(BannerPattern.BASE, DyeColor.byId(tag.getInt("Base")));
                }
                ListNBT list = tag.getList("Patterns", 10);
                for (int i = 0; i < list.size(); i++) {
                    CompoundNBT e = list.getCompound(i);
                    PatternColor pc = new PatternColor();
                    pc.deserializeNBT(e);
                    patternColors.add(pc);
                }
            }
        }

        private List<PatternColor> getPatternColors() {
            List<PatternColor> l = Lists.newArrayList(base);
            l.addAll(patternColors);
            return l;
        }

        private List<BannerPattern> getPatternList() {
            return getPatternColors().stream().map(PatternColor::getPattern).collect(Collectors.toList());
        }

        private List<DyeColor> getColorList() {
            return getPatternColors().stream().map(PatternColor::getColor).collect(Collectors.toList());
        }

        public ResourceLocation getTexture(BannerTextures.Cache cache) {
            return cache.getResourceLocation(getPatternID(), getPatternList(), getColorList());
        }

        private String getPatternID() {
            StringBuilder builder = new StringBuilder();
            for (PatternColor pc : getPatternColors()) {
                builder.append(pc.pattern.getHashname()).append(pc.color.getId());
            }
            return builder.toString();
        }
    }

    public static class PatternColor implements INBTSerializable<CompoundNBT> {
        private DyeColor color;
        private BannerPattern pattern;

        private PatternColor(BannerPattern pattern, DyeColor color) {
            this.pattern = pattern;
            this.color = color;
        }

        private PatternColor() {
        }

        @Override
        public CompoundNBT serializeNBT() {
            CompoundNBT entry = new CompoundNBT();
            entry.putInt("Color", color.getId());
            entry.putString("Pattern", pattern.getHashname());
            return entry;
        }

        @Override
        public void deserializeNBT(CompoundNBT nbt) {
            pattern = BannerPattern.byHash(nbt.getString("Pattern"));
            color = DyeColor.byId(nbt.getInt("Color"));

        }

        public DyeColor getColor() {
            return color;
        }

        public BannerPattern getPattern() {
            return pattern;
        }
    }

}
