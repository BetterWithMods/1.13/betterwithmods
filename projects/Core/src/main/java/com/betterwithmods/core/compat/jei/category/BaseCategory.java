/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.compat.jei.category;

import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IGuiIngredientGroup;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nonnull;

public abstract class BaseCategory<T> implements IRecipeCategory<T> {

    protected final IDrawable background;
    protected final IDrawable icon;
    protected final String localizedName;
    protected final ResourceLocation uid;
    protected final IGuiHelper guiHelper;

    public BaseCategory(IGuiHelper guiHelper, IDrawable background, String translationKey, IDrawable icon, ResourceLocation uid) {
        this.guiHelper = guiHelper;
        this.background = background;
        this.icon = icon;
        this.uid = uid;
        this.localizedName = I18n.format(translationKey);
    }

    @Nonnull
    @Override
    public ResourceLocation getUid() {
        return uid;
    }

    @Override
    public String getTitle() {
        return localizedName;
    }

    @Override
    public IDrawable getBackground() {
        return background;
    }

    @Override
    public IDrawable getIcon() {
        return icon;
    }

    protected static int SLOT_WIDTH = 18;

    public static <T> void createSlot(IGuiIngredientGroup<T> group, boolean input, int i, int x, int y) {
        group.init(i, input, x, y);
    }

    public static <T> void createSlotsHorizontal(IGuiIngredientGroup<T> group, boolean input, int count, int start, int x, int y) {
        for (int i = 0; i < count; i++) {
            group.init(i + start, input, x + (i * SLOT_WIDTH), y);
        }
    }

    public static <T> void createSlotsGrid(IGuiIngredientGroup<T> group, boolean input, int count, int rows, int startIndex, int startX, int startY) {
        int columns = count / rows;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                int index = j + i * columns + startIndex;
                int x = startX + j * SLOT_WIDTH;
                int y = startY + i * SLOT_WIDTH;
                group.init(index, input, x, y);
            }
        }
    }

}
