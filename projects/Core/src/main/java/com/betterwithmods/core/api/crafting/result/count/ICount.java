/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.api.crafting.result.count;

import com.betterwithmods.core.api.IPositionedText;
import com.betterwithmods.core.impl.Registries;
import com.google.common.collect.Lists;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nullable;
import java.util.List;
import java.util.function.Supplier;

public interface ICount extends Supplier<Integer> {

    ICountSerializer<?> getSerializer();

    @Nullable
    static ICount read(PacketBuffer buffer) {
        ResourceLocation id = buffer.readResourceLocation();
        return Registries.get(Registries.COUNT_SERIALIZERS, id).map(s -> s.read(buffer)).orElseThrow(() -> new IllegalArgumentException("Can not deserialize unknown Count type: " + id));
    }

    static <T extends ICount> void write(PacketBuffer buffer, T count) {
        ICountSerializer<T> serializer = (ICountSerializer<T>) count.getSerializer();
//        System.out.println(serializer.getRegistryName());
        buffer.writeResourceLocation(serializer.getRegistryName());
        serializer.write(buffer, count);
    }

    default boolean equals(ICount count) {
        return false;
    }

    default List<IPositionedText> getText() {
        return Lists.newArrayList();
    }
}
