/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.impl.crafting.result.count;

import com.betterwithmods.core.Registrars;
import com.betterwithmods.core.api.IPositionedText;
import com.betterwithmods.core.api.ISerializer;
import com.betterwithmods.core.api.crafting.result.count.ICount;
import com.betterwithmods.core.api.crafting.result.count.ICountSerializer;
import com.betterwithmods.core.impl.PositionedText;
import com.betterwithmods.core.utilties.Rand;
import com.google.common.collect.Lists;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.registries.ForgeRegistryEntry;

import java.util.List;

public class Range implements ICount {
    private int min, max;

    public Range(int min, int max) {
        this.max = MathHelper.clamp(max, min, 64);
        this.min = MathHelper.clamp(min, 0, this.max);
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    @Override
    public Integer get() {
        return Rand.between(min, max);
    }

    @Override
    public ICountSerializer<?> getSerializer() {
        return Registrars.CountSerializers.RANGE;
    }

    public static class Serializer extends ForgeRegistryEntry<ICountSerializer<?>> implements ICountSerializer<Range>, ISerializer<Range> {

        @Override
        public Range read(JsonElement json) {
            int min = JSONUtils.getInt(json.getAsJsonObject(), "min", 0);
            int max = JSONUtils.getInt(json.getAsJsonObject(), "max", 0);
            return new Range(min, max);
        }

        @Override
        public Range read(PacketBuffer buffer) {
            int min = buffer.readInt();
            int max = buffer.readInt();
            return new Range(min, max);
        }

        @Override
        public void write(PacketBuffer buffer, Range range) {
            buffer.writeInt(range.min);
            buffer.writeInt(range.max);
        }

        @Override
        public <V extends Range> void write(V range, JsonElement jsonElement) {
            JsonObject json = jsonElement.getAsJsonObject();
            json.addProperty("min", range.getMin());
            json.addProperty("max", range.getMax());
        }
    }

    @Override
    public boolean equals(ICount count) {
        return count instanceof Range && ((Range) count).min == this.min && ((Range) count).max == this.max;
    }

    @Override
    public List<IPositionedText> getText() {
        return Lists.<IPositionedText>newArrayList(new PositionedText(new StringTextComponent(String.format("%d-%d", min, max)), 7, 9, 0.75));
    }
}
