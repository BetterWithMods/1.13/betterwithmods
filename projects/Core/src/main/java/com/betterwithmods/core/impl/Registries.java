/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.impl;

import com.betterwithmods.core.api.crafting.input.IInputSerializer;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipeSerializer;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipeType;
import com.betterwithmods.core.api.crafting.output.IOutputSerializer;
import com.betterwithmods.core.api.crafting.result.IResultSerializer;
import com.betterwithmods.core.api.crafting.result.count.ICountSerializer;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;
import net.minecraftforge.registries.RegistryManager;

import java.util.Optional;

public class Registries {

    //TODO Find a way to not let this class load until after the NewRegistry event
    public static final IForgeRegistry<IMultiplexRecipeSerializer<?>> MULTIPLEX_RECIPE_SERIALIZERS = RegistryManager.ACTIVE.getRegistry(IMultiplexRecipeSerializer.class);
    public static final IForgeRegistry<IMultiplexRecipeType<?, ?>> MULTIPLEX_RECIPE_TYPES = RegistryManager.ACTIVE.getRegistry(IMultiplexRecipeType.class);
    public static final IForgeRegistry<IOutputSerializer<?>> OUTPUT_SERIALIZERS = RegistryManager.ACTIVE.getRegistry(IOutputSerializer.class);
    public static final IForgeRegistry<IResultSerializer<?>> RESULT_SERIALIZERS = RegistryManager.ACTIVE.getRegistry(IResultSerializer.class);
    public static final IForgeRegistry<IInputSerializer<?,?>> INPUT_SERIALIZERS = RegistryManager.ACTIVE.getRegistry(IInputSerializer.class);
    public static final IForgeRegistry<ICountSerializer<?>> COUNT_SERIALIZERS = RegistryManager.ACTIVE.getRegistry(ICountSerializer.class);


    public static <T extends IForgeRegistryEntry<T>> Optional<T> get(IForgeRegistry<T> registry, ResourceLocation value) {
        return Optional.ofNullable(registry.getValue(value));
    }

    public static <T extends IForgeRegistryEntry<T>> T getSerializer(IForgeRegistry<T> registry, JsonObject json) {
        ResourceLocation type = new ResourceLocation(JSONUtils.getString(json, "type"));
        return getOptionalSerializer(registry, json).orElseThrow(() -> new JsonSyntaxException(String.format("Invalid or unsupported %s type '%s'", registry.getRegistryName(), type)));
    }

    public static <T extends IForgeRegistryEntry<T>> Optional<T> getOptionalSerializer(IForgeRegistry<T> registry, JsonObject json) {
        ResourceLocation type = new ResourceLocation(JSONUtils.getString(json, "type"));
        return get(registry, type);
    }

    public static <T extends IForgeRegistryEntry<T>> Optional<T> getOptionalSerializer(IForgeRegistry<T> registry, JsonObject json, String key, String fallback) {
        ResourceLocation type = new ResourceLocation(JSONUtils.getString(json, key, fallback));
        return get(registry, type);
    }

}

