/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.compat.jei.category;

import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipe;
import com.betterwithmods.core.api.crafting.output.IOutput;
import com.betterwithmods.core.api.crafting.result.IResult;
import com.betterwithmods.core.compat.jei.CoreJEI;
import com.google.common.collect.Lists;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredientType;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

import java.util.List;

public abstract class AbstractMultiplexCategory<T, R extends IMultiplexRecipe<T, R>> extends BaseCategory<R> {

    public AbstractMultiplexCategory(IGuiHelper guiHelper, IDrawable background, String translationKey, IDrawable icon, ResourceLocation uid) {
        super(guiHelper, background, translationKey, icon, uid);
    }

    public static <T> List<IResult<T>> getOutputs(IOutput output, Class<T> clazz) {
        if(output != null) {
            List<IResult<T>> results = output.getDisplayResults(clazz);
            if (!results.isEmpty()) {
                return results;
            }
        }
        return Lists.newArrayList();
    }

    protected static <T> void setOutputs(IIngredients ingredients, IOutput output, IIngredientType<IResult<T>> type, Class<T> clazz) {
        ingredients.setOutputs(type, getOutputs(output,clazz));
    }

    @Override
    public void setIngredients(R recipe, IIngredients ingredients) {
        ingredients.setInputIngredients(recipe.getInputs());
        setOutputs(ingredients, recipe.getOutput(), CoreJEI.STACK_RESULT, ItemStack.class);
    }

}
