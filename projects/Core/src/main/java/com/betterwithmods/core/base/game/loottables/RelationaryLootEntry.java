/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.loottables;

import com.betterwithmods.core.References;
import com.betterwithmods.core.relationary.Relation;
import com.betterwithmods.core.relationary.RelationaryManager;
import com.betterwithmods.core.relationary.Shape;
import com.betterwithmods.core.utilties.ItemUtils;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.loot.ItemLootEntry;
import net.minecraft.world.storage.loot.LootContext;
import net.minecraft.world.storage.loot.LootParameters;
import net.minecraft.world.storage.loot.StandaloneLootEntry;
import net.minecraft.world.storage.loot.conditions.ILootCondition;
import net.minecraft.world.storage.loot.functions.ILootFunction;

import java.util.function.Consumer;

public class RelationaryLootEntry extends StandaloneLootEntry {

    private Shape inputShape, outputShape;

    protected RelationaryLootEntry(Shape inputShape, Shape outputShape, int weight, int quality, ILootCondition[] conditions, ILootFunction[] functions) {
        super(weight, quality, conditions, functions);
        this.inputShape = inputShape;
        this.outputShape = outputShape;
    }

    @Override
    protected void func_216154_a(Consumer<ItemStack> consumer, LootContext lootContext) {
        BlockState state = lootContext.get(LootParameters.BLOCK_STATE);
        ItemStack stack = ItemUtils.getStack(state);
        Relation relation = RelationaryManager.getRelationWithShape(stack, inputShape);
        if (relation != null) {
            consumer.accept(relation.first(outputShape, 1));
        }
    }

    public static StandaloneLootEntry.Builder<?> create(Shape inputShape, Shape outputShape) {
        return builder((weight, quality, conditions, functions) -> new RelationaryLootEntry(inputShape, outputShape, weight, quality, conditions, functions));
    }
    public static class Serializer extends net.minecraft.world.storage.loot.StandaloneLootEntry.Serializer<RelationaryLootEntry> {

        public Serializer() {
            super(new ResourceLocation(References.MODID_CORE, "relationary"), RelationaryLootEntry.class);
        }

        @Override
        public void serialize(JsonObject json, RelationaryLootEntry entry, JsonSerializationContext context) {
            super.serialize(json, entry, context);
            json.addProperty("inputShape", entry.inputShape.getRegistryName().toString());
            json.addProperty("outputShape", entry.outputShape.getRegistryName().toString());
        }

        @Override
        protected RelationaryLootEntry func_212829_b_(JsonObject jsonObject, JsonDeserializationContext jsonDeserializationContext, int weight, int quality, ILootCondition[] conditions, ILootFunction[] functions) {
            Shape inputShape = RelationaryManager.getOrCreateShape(new ResourceLocation(jsonObject.get("inputShape").getAsString()));
            Shape outputShape = RelationaryManager.getOrCreateShape(new ResourceLocation(jsonObject.get("outputShape").getAsString()));
            return new RelationaryLootEntry(inputShape, outputShape, weight, quality, conditions, functions);
        }
    }
}
