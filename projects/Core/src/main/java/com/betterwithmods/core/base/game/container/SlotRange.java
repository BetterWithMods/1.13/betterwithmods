/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.container;

import com.betterwithmods.core.api.base.container.ISlot;
import com.betterwithmods.core.api.base.container.ISlotRange;
import com.google.common.collect.Lists;

import java.util.Collection;
import java.util.Iterator;

public class SlotRange implements ISlotRange {
    private Collection<ISlot> collection;
    private int start;
    private int end;

    /**
     * @param start inclusive beginning id
     * @param end   exclusive end id
     */
    public SlotRange(int start, int end) {
        this.start = start;
        this.end = end;
        this.collection = Lists.newArrayList();
        for (int i = start; i < end; i++) {
            collection.add(new Slot(i));
        }
    }

    @Override
    public Iterator<ISlot> iterator() {
        return collection.iterator();
    }

    @Override
    public boolean contains(ISlot slot) {
        return collection.contains(slot);
    }

    @Override
    public int getStart() {
        return start;
    }

    @Override
    public int getEnd() {
        return end;
    }
}
