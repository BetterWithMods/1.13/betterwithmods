/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.model;

import com.betterwithmods.core.utilties.MiscUtils;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import net.minecraft.resources.IResource;
import net.minecraft.resources.IResourceManager;
import net.minecraft.resources.IResourceManagerReloadListener;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.regex.Pattern;

public class GeometryManager implements IResourceManagerReloadListener {

    public static Map<ResourceLocation, Geometry> geometries = Maps.newHashMap();

    private static final Pattern ENTITY_PATTERN = Pattern.compile("models/entity/(.*).json");
    private Logger logger;

    public GeometryManager(Logger logger) {
        this.logger = logger;
    }

    @Override
    public void onResourceManagerReload(IResourceManager resourceManager) {
        Gson gson = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();
        geometries.clear();
        for (ResourceLocation resourcelocation : resourceManager.getAllResourceLocations("models/entity", (file) -> {
            return file.endsWith(".json") && !file.startsWith("_"); //Forge filter anything beginning with "_" as it's used for metadata.
        })) {
            ResourceLocation registryName = MiscUtils.stripPath(ENTITY_PATTERN, resourcelocation);
            if (registryName == null) {
                logger.error("Failed to parse registryName for {}", resourcelocation);
                continue;
            }
            try (IResource iresource = resourceManager.getResource(resourcelocation)) {
                JsonObject jsonobject = JSONUtils.fromJson(gson, IOUtils.toString(iresource.getInputStream(), StandardCharsets.UTF_8), JsonObject.class);
                if (jsonobject != null) {
                    addGeometry(Geometry.deserialize(registryName, jsonobject));
                } else {
                    logger.error("Couldn't load entity model {} as it's null or empty", registryName);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void addGeometry(Geometry geometry) {
        if (geometries.containsKey(geometry.getRegistryName())) {
            throw new IllegalStateException("Duplicate shape ignored with ID " + geometry.getRegistryName());
        } else {
            geometries.put(geometry.getRegistryName(), geometry);
        }
    }


    public static Geometry getOrCreate(ResourceLocation registryName) {
        return geometries.getOrDefault(registryName, new Geometry(registryName));
    }
}
