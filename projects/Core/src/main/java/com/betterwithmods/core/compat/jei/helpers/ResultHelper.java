/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.compat.jei.helpers;

import com.betterwithmods.core.api.crafting.result.IResult;
import mezz.jei.api.ingredients.IIngredientHelper;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class ResultHelper<T extends IResult<?>> implements IIngredientHelper<T> {

    @Nullable
    @Override
    public T getMatch(@Nonnull Iterable<T> results, @Nonnull T result) {
        for (T r : results) {
            if (r.equals(result)) {
                return result;
            }
        }
        return null;
    }

    @Nonnull
    @Override
    public String getWildcardId(@Nonnull T r) {
        return getUniqueId(r);
    }

    @Nonnull
    @Override
    public String getModId(@Nonnull T r) {
        return r.getId().getNamespace();
    }

    @Nonnull
    @Override
    public String getResourceId(@Nonnull T r) {
        return r.getId().getPath();
    }

    @Nonnull
    @Override
    public T copyIngredient(@Nonnull T r) {
        return (T) r.copy();
    }

    @Nonnull
    @Override
    public String getErrorInfo(@Nullable T r) {
        return ""; //TODO
    }

    @Nonnull
    @Override
    public String getDisplayName(@Nonnull T result) {
        return result.displayText().getFormattedText();
    }

    @Nonnull
    @Override
    public String getUniqueId(T result) {
        return result.getId().toString();
    }
}
