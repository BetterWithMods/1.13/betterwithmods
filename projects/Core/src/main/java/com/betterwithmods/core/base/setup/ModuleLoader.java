/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.setup;

import com.google.common.collect.Maps;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.function.Supplier;

public class ModuleLoader extends SetupLoader {

    private static final Map<String, ModuleLoader> LOADED_MODS = Maps.newHashMap();

    public final Map<String, Supplier<Boolean>> MODULE_CONDITIONS = Maps.newHashMap();

    private final Logger logger;
    public String modid;

    public ModuleLoader(String modid) {
        this.modid = modid;
        this.logger = LogManager.getLogger(modid);
        LOADED_MODS.put(modid, this);
    }

    public void addModule(ModuleBase module) {
        this.addSetup(module);
        module.setLoader(this);
        logger.info("Adding Module {}", module.getName());
    }

    public void addCondition(String name, Supplier<Boolean> condition) {
        MODULE_CONDITIONS.put(name, condition);
    }

    public static boolean getModuleCondition(String modid, String name) {
        ModuleLoader loader = LOADED_MODS.get(modid);
        if(loader != null) {
            return loader.MODULE_CONDITIONS.getOrDefault(name, () -> false).get();
        }
        return false;
    }

    public Logger getLogger() {
        return logger;
    }


}
