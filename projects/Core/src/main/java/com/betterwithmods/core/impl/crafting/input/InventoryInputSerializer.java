/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.impl.crafting.input;

import com.betterwithmods.core.utilties.DeserializeUtils;
import com.google.gson.JsonElement;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.NonNullList;
import net.minecraftforge.items.IItemHandlerModifiable;

import javax.annotation.Nonnull;

public class InventoryInputSerializer extends BaseInputSerializer<IItemHandlerModifiable, InventoryInput<?>> {

    @Nonnull
    @Override
    public InventoryInput<?> read(@Nonnull JsonElement json) {
        return new InventoryInput<>(DeserializeUtils.readIngredients(json.getAsJsonObject()));
    }

    @Nonnull
    @Override
    public InventoryInput<?> read(PacketBuffer buffer) {
        NonNullList<Ingredient> ingredients = NonNullList.create();
        int size = buffer.readInt();
        for (int i = 0; i < size; i++) {
            ingredients.add(Ingredient.read(buffer));
        }
        return new InventoryInput<>(ingredients);
    }

    @Override
    public void write(PacketBuffer buffer, InventoryInput<?> input) {
        buffer.writeInt(input.getIngredients().size());
        for (Ingredient ingredient : input.getIngredients()) {
            ingredient.write(buffer);
        }
    }

    @Override
    public <V extends InventoryInput<?>> void write(V inventoryInput, JsonElement jsonElement) {
        DeserializeUtils.writeIngredients(jsonElement.getAsJsonObject(), inventoryInput.getIngredients(), "inputs");
    }



}
