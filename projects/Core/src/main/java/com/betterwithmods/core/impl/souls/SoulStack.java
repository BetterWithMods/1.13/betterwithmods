/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.impl.souls;

import com.betterwithmods.core.api.souls.ISoulStack;
import com.betterwithmods.core.api.souls.SoulType;
import net.minecraft.nbt.CompoundNBT;

public class SoulStack implements ISoulStack {
    protected SoulType type;
    protected int amount;

    public SoulStack(CompoundNBT tag) {
        deserializeNBT(tag);
    }

    public SoulStack(SoulType type) {
        this(type, 0);
    }

    public SoulStack(SoulType type, int amount) {
        this.type = type;
        this.amount = amount;
    }

    @Override
    public SoulType getType() {
        return type;
    }

    @Override
    public int getAmount() {
        return amount;
    }

    @Override
    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        tag.putString("type", type.getName());
        tag.putInt("amount", amount);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        if(nbt.contains("type")) {
            type = SoulType.fromName(nbt.getString("type"));
        }
        if(nbt.contains("amount")) {
            amount = nbt.getInt("amount");
        }
    }

    @Override
    public ISoulStack copy() {
        return new SoulStack(type, amount);
    }

    public static SoulStack EMPTY = new SoulStack(SoulType.EMPTY, 0);

}
