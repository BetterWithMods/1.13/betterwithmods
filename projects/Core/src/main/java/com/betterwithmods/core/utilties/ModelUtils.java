/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties;

import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.IUnbakedModel;
import net.minecraft.client.renderer.texture.MissingTextureSprite;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.model.ModelLoaderRegistry;
import net.minecraftforge.client.model.data.EmptyModelData;

import java.util.Random;

@OnlyIn(Dist.CLIENT)
public class ModelUtils {

    private static final Minecraft MC = Minecraft.getInstance();

    public static IBakedModel getBakedModel(BlockState state) {
        return MC.getBlockRendererDispatcher().getModelForState(state);
    }

    public static TextureAtlasSprite getParticleSprite(BlockState state) {
        IBakedModel model = getBakedModel(state);
        return model.getParticleTexture();
    }

    public static IUnbakedModel getUnbakedModel(String location) {
        return getUnbakedModel(new ResourceLocation(location));
    }

    public static IUnbakedModel getUnbakedModel(ResourceLocation location) {
        try {
            return ModelLoaderRegistry.getModel(location);
        } catch (Exception e) {
            throw new RuntimeException("No model found at " + location.toString());
        }
    }

    public static TextureAtlasSprite getSprite(ResourceLocation location) {
        return ModelLoader.defaultTextureGetter().apply(location);
    }

    private static final Random RANDOM = new Random();

    public static boolean isMissingTexture(TextureAtlasSprite sprite) {
        return sprite instanceof MissingTextureSprite;
    }

    public static TextureAtlasSprite getSideSprite(BlockState state, Direction facing) {
        return getBakedModel(state).getQuads(state, facing, RANDOM, EmptyModelData.INSTANCE).stream().findFirst().map(BakedQuad::getSprite).orElse(getParticleSprite(state));
    }

    public static TextureAtlasSprite getSprite(ItemStack stack) {
        IBakedModel model = MC.getItemRenderer().getModelWithOverrides(stack);
        if (model != null) {
            TextureAtlasSprite sprite = model.getParticleTexture();
            if(!isMissingTexture(sprite)) {
                return model.getParticleTexture();
            }
        }
        return null;
    }


}
