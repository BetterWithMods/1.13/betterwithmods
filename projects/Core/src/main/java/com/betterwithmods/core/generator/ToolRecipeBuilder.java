/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.generator;

import com.betterwithmods.core.Registrars;
import com.betterwithmods.core.base.game.recipes.ToolRecipeSerializer;
import com.betterwithmods.core.relationary.Relation;
import com.betterwithmods.core.relationary.Shape;
import com.betterwithmods.core.utilties.DeserializeUtils;
import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementRewards;
import net.minecraft.advancements.ICriterionInstance;
import net.minecraft.advancements.IRequirementsStrategy;
import net.minecraft.advancements.criterion.RecipeUnlockedTrigger;
import net.minecraft.data.IFinishedRecipe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import org.apache.commons.lang3.tuple.Triple;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

public class ToolRecipeBuilder {

    private final ItemStack result;
    private Ingredient tool, input;
    private String group;
    private boolean damageTool;
    private NonNullList<ItemStack> additionalDrops = NonNullList.create();
    private List<Triple<Relation, Shape, Integer>> relationDrops = Lists.newArrayList();
    private SoundEvent sound;
    private final Advancement.Builder advancementBuilder = Advancement.Builder.builder();


    public ToolRecipeBuilder(ItemStack result) {
        this.result = result;
    }

    public static ToolRecipeBuilder recipe(ItemStack stack) {
        return new ToolRecipeBuilder(stack);
    }

    public ToolRecipeBuilder group(String group) {
        this.group = group;
        return this;
    }

    public ToolRecipeBuilder tool(Ingredient tool) {
        this.tool = tool;
        return this;
    }

    public ToolRecipeBuilder input(Ingredient input) {
        this.input = input;
        return this;
    }

    public ToolRecipeBuilder addDrops(Collection<ItemStack> drops) {
        this.additionalDrops.addAll(drops);
        return this;
    }

    public ToolRecipeBuilder addDrop(Relation relation, Shape shape, int count) {
        this.relationDrops.add(Triple.of(relation, shape, count));
        return this;
    }

    public ToolRecipeBuilder damageTool() {
        this.damageTool = true;
        return this;
    }

    public ToolRecipeBuilder sound(SoundEvent sound) {
        this.sound = sound;
        return this;
    }

    public ToolRecipeBuilder addCriterion(String name, ICriterionInstance criterionIn) {
        this.advancementBuilder.withCriterion(name, criterionIn);
        return this;
    }

    public void build(Consumer<IFinishedRecipe> consumerIn) {
        this.build(consumerIn, result.getItem().getRegistryName());
    }

    public void build(Consumer<IFinishedRecipe> consumerIn, ResourceLocation id) {
        advancementBuilder.withParentId(new ResourceLocation("recipes/root")).withCriterion("has_the_recipe", new RecipeUnlockedTrigger.Instance(id)).withRewards(AdvancementRewards.Builder.recipe(id)).withRequirementsStrategy(IRequirementsStrategy.OR);
        ResourceLocation advancementId = new ResourceLocation(id.getNamespace(), "recipes/" + this.result.getItem().getGroup().getPath() + "/" + id.getPath());
        consumerIn.accept(new Result(id, this.result, tool, input, this.group == null ? "" : this.group, damageTool, additionalDrops, relationDrops, sound, advancementBuilder, advancementId));
    }


    public class Result implements IFinishedRecipe {

        private final ResourceLocation id;
        private final ItemStack result;
        private final Ingredient tool, input;
        private final String group;
        private final boolean damageTool;
        private final NonNullList<ItemStack> additionalDrops;
        private final List<Triple<Relation, Shape, Integer>> relationDrops;
        private final SoundEvent sound;
        private final Advancement.Builder advancementBuilder;
        private final ResourceLocation advancementId;



        public Result(ResourceLocation id, ItemStack result, Ingredient tool, Ingredient input, String group, boolean damageTool, NonNullList<ItemStack> additionalDrops, List<Triple<Relation, Shape, Integer>> relationDrops, SoundEvent sound, Advancement.Builder advancementBuilder, ResourceLocation advancementId) {
            this.id = id;
            this.result = result;
            this.tool = tool;
            this.input = input;
            this.group = group;
            this.damageTool = damageTool;
            this.additionalDrops = additionalDrops;
            this.relationDrops = relationDrops;
            this.sound = sound;
            this.advancementBuilder = advancementBuilder;
            this.advancementId = advancementId;
        }

        @Override
        public void serialize(@Nonnull JsonObject object) {
            if (!this.group.isEmpty()) {
                object.addProperty("group", this.group);
            }
            object.addProperty("damageTool", damageTool);
            if (this.sound != null) {
                object.addProperty("sound", sound.getRegistryName().toString());
            }
            object.add("tool", this.tool.serialize());
            object.add("input", this.input.serialize());


            JsonObject resultObject = new JsonObject();
            DeserializeUtils.serializeItemStack(result, resultObject);
            object.add("result", resultObject);

            if (!additionalDrops.isEmpty()) {
                JsonArray drops = JSONUtils.getJsonArray(object, "additionalDrops", new JsonArray());
                for (ItemStack drop : additionalDrops) {
                    JsonObject dropObject = new JsonObject();
                    DeserializeUtils.serializeItemStack(drop, dropObject);
                    drops.add(dropObject);
                }
                object.add("additionalDrops", drops);
            }

            if (!relationDrops.isEmpty()) {
                JsonArray drops = JSONUtils.getJsonArray(object, "additionalDrops", new JsonArray());
                for (Triple<Relation, Shape, Integer> triple : relationDrops) {
                    JsonObject o = new JsonObject();
                    o.addProperty("type", ToolRecipeSerializer.DROP_TYPE);
                    o.addProperty("relation", triple.getLeft().getRegistryName().toString());
                    o.addProperty("shape", triple.getMiddle().getRegistryName().toString());
                    o.addProperty("count", triple.getRight());
                    drops.add(o);
                }
                object.add("additionalDrops", drops);
            }
        }

        @Nonnull
        @Override
        public ResourceLocation getID() {
            return id;
        }

        @Override
        public IRecipeSerializer<?> getSerializer() {
            return Registrars.RecipeSerializers.TOOL_RECIPE;
        }

        @Nullable
        @Override
        public JsonObject getAdvancementJson() {
            return advancementBuilder.serialize();
        }

        @Nullable
        @Override
        public ResourceLocation getAdvancementID() {
            return advancementId;
        }
    }
}
