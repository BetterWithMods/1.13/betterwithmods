/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.impl.crafting.multiplex;

import com.betterwithmods.core.References;
import com.betterwithmods.core.api.crafting.input.IRecipeContext;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipe;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipeSerializer;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipeType;
import com.betterwithmods.core.impl.Registries;
import com.betterwithmods.core.network.MultiplexRecipeReloadMessage;
import com.betterwithmods.core.network.NetworkHandler;
import com.betterwithmods.core.utilties.MiscUtils;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import net.minecraft.resources.IResource;
import net.minecraft.resources.IResourceManager;
import net.minecraft.resources.IResourceManagerReloadListener;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.PacketDistributor;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nonnull;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class MultiplexRecipeManager implements IResourceManagerReloadListener {

    private static final Logger LOGGER = LogManager.getLogger(String.format("{} - Multiplex Recipes", References.MODID_MECHANIST));

    private Map<IMultiplexRecipeType<?, ?>, Map<ResourceLocation, IMultiplexRecipe<?, ?>>> recipes = Util.make(Maps.newHashMap(), this::initialize);

    private static Map<MinecraftServer, MultiplexRecipeManager> MANAGERS = Maps.newHashMap();

    private static final Pattern RECIPE_PATTERN = Pattern.compile("multiplex_recipes/(.*).json");

    @OnlyIn(Dist.CLIENT)
    public static MultiplexRecipeManager CLIENT_INSTANCE = new MultiplexRecipeManager(LOGGER);

    private Logger logger;

    public MultiplexRecipeManager(Logger logger) {
        this.logger = logger;
    }

    @Override
    public void onResourceManagerReload(IResourceManager resourceManager) {
        Gson gson = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();
        initialize(recipes);
        Map<ResourceLocation, IMultiplexRecipe<?, ?>> recipesMap = Maps.newHashMap();
        for (ResourceLocation resourcelocation : resourceManager.getAllResourceLocations("multiplex_recipes", (file) -> {
            return file.endsWith(".json") && !file.startsWith("_"); //Forge filter anything beginning with "_" as it's used for metadata.
        })) {
            ResourceLocation registryName = MiscUtils.stripPath(RECIPE_PATTERN, resourcelocation);
            if (registryName == null) {
                logger.error("Failed to parse registryName for {}", resourcelocation);
                continue;
            }
            try (IResource iresource = resourceManager.getResource(resourcelocation)) {
                JsonObject jsonobject = JSONUtils.fromJson(gson, IOUtils.toString(iresource.getInputStream(), StandardCharsets.UTF_8), JsonObject.class);
                if (jsonobject != null) {


                    IMultiplexRecipe<?, ?> recipe = deserializeRecipe(registryName, jsonobject);
                    if (recipe != null) {
                        queue(recipe, recipesMap);
                    }


                } else {
                    logger.error("Couldn't load entity model {} as it's null or empty", registryName);
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }

        for (IMultiplexRecipe<?, ?> recipe : recipesMap.values()) {
            addMultiplexRecipe(recipe);
        }


        MultiplexRecipeReloadMessage message = new MultiplexRecipeReloadMessage(this);
        NetworkHandler.CHANNEL.send(PacketDistributor.ALL.noArg(), message);
    }

    private void queue(@Nonnull IMultiplexRecipe recipe, Map<ResourceLocation, IMultiplexRecipe<?, ?>> map) {
        ResourceLocation id = recipe.getId();
        IMultiplexRecipe<?, ?> existing = map.get(id);
        if (existing != null && existing.getInput().canMerge(recipe.getInput())) {
            existing.getInput().merge(recipe.getInput());
        } else {
            map.put(recipe.getId(), recipe);
        }
    }


    private void addMultiplexRecipe(@Nonnull IMultiplexRecipe recipe) {
        Map<ResourceLocation, IMultiplexRecipe<?, ?>> map = this.recipes.get(recipe.getType());
        if (map.containsKey(recipe.getId())) {
            throw new IllegalStateException("Duplicate multiplex recipe ignored with ID " + recipe.getId());
        } else {
            map.put(recipe.getId(), recipe);
        }
    }

    private IMultiplexRecipe deserializeRecipe(ResourceLocation id, JsonObject object) {
        IMultiplexRecipeSerializer<?> serializer = Registries.getSerializer(Registries.MULTIPLEX_RECIPE_SERIALIZERS, object);
        try {
            return serializer.read(id, object);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    private void initialize(Map<IMultiplexRecipeType<?, ?>, Map<ResourceLocation, IMultiplexRecipe<?, ?>>> map) {
        map.clear();
        for (IMultiplexRecipeType<?, ?> type : Registries.MULTIPLEX_RECIPE_TYPES.getValues()) {
            map.put(type, Maps.newHashMap());
        }
    }

    public <T, R extends IMultiplexRecipe<T, R>> void craft(IMultiplexRecipeType<T, R> type, World world, BlockPos pos, IRecipeContext<T, R> context) {
        getRecipe(type, world, pos, context).ifPresent(r -> r.craft(world, pos, context));
    }


    public <T, R extends IMultiplexRecipe<T, R>> Optional<R> getRecipe(IMultiplexRecipeType<T, R> type, World world, BlockPos pos, IRecipeContext<T, R> context) {
        Map<ResourceLocation, IMultiplexRecipe<?, ?>> map = recipes.get(type);
        try {
            //TODO give recipe matching priority?
            return map.values().stream().flatMap(recipe -> Util.streamOptional(type.matches(recipe, world, pos, context))).max(Comparator.naturalOrder());
        } catch (NullPointerException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    public static Optional<MultiplexRecipeManager> get(MinecraftServer server) {
        return Optional.of(MANAGERS.computeIfAbsent(server, o -> new MultiplexRecipeManager(LOGGER)));
    }


    @SuppressWarnings("unchecked")
    public <T, R extends IMultiplexRecipe<T, R>> Map<ResourceLocation, R> getRecipes(IMultiplexRecipeType<T, R> type) {
        return (Map<ResourceLocation, R>) recipes.getOrDefault(type, Collections.emptyMap());
    }

    public Collection<IMultiplexRecipe<?, ?>> getRecipes() {
        return recipes.values().stream().flatMap(m -> m.values().stream()).collect(Collectors.toSet());
    }


    public void reloadRecipes(Collection<IMultiplexRecipe<?, ?>> recipeList) {
        Map<IMultiplexRecipeType<?, ?>, Map<ResourceLocation, IMultiplexRecipe<?, ?>>> map = Maps.newHashMap();
        recipeList.forEach((recipe) -> {
            Map<ResourceLocation, IMultiplexRecipe<?, ?>> typeMap = map.computeIfAbsent(recipe.getType(), (r) -> Maps.newHashMap());
            IMultiplexRecipe<?, ?> result = typeMap.put(recipe.getId(), recipe);
            if (result != null) {
                throw new IllegalStateException("Duplicate Multiplex Recipe ignored with ID " + recipe.getId());
            }
        });
        this.recipes = ImmutableMap.copyOf(map);
    }

}
