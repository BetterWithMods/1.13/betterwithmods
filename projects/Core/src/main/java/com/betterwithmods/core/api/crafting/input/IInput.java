/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.api.crafting.input;

import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipe;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

public interface IInput<T, R extends IMultiplexRecipe<T, R>> extends Comparable<IInput<T, R>> {

    boolean matches(IWorld world, BlockPos pos, R recipe, IRecipeContext<T, R> context);

    NonNullList<Ingredient> getIngredients();

    default Integer size() {
        return getIngredients().size();
    }

    @Override
    default int compareTo(IInput<T, R> other) {
        return size().compareTo(other.size());
    }

    void consume(World world, BlockPos pos, R recipe, IRecipeContext<T, R> context);

    IInputSerializer<?, ?> getSerializer();

    default boolean canMerge(IInput<?,?> original) { return false; }

    default void merge(IInput<T, R> input) {}
}
