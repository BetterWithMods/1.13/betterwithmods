/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.recipes;


import com.betterwithmods.core.References;
import com.betterwithmods.core.Relationships;
import com.betterwithmods.core.relationary.Relation;
import com.betterwithmods.core.relationary.RelationaryManager;
import com.betterwithmods.core.relationary.Shape;
import com.betterwithmods.core.utilties.DeserializeUtils;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.crafting.ShapedRecipe;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.registries.ForgeRegistries;

public class ToolRecipeSerializer extends net.minecraftforge.registries.ForgeRegistryEntry<IRecipeSerializer<?>> implements IRecipeSerializer<ToolRecipe> {

    @Override
    public ToolRecipe read(ResourceLocation recipeId, JsonObject json) {
        String group = JSONUtils.getString(json, "group", "");
        Ingredient tool = Ingredient.deserialize(json.get("tool"));
        Ingredient input = Ingredient.deserialize(json.get("input"));
        ItemStack result = ShapedRecipe.deserializeItem(JSONUtils.getJsonObject(json, "result"));
        boolean damageTool = JSONUtils.getBoolean(json, "damageTool", false);
        NonNullList<ItemStack> additionalDrops = NonNullList.create();
        if (json.has("additionalDrops")) {
            for (JsonElement dropElement : JSONUtils.getJsonArray(json, "additionalDrops")) {
                ItemStack drop = deserializeAdditionalDrop(dropElement);
                additionalDrops.add(drop);
            }
        }
        String soundName = JSONUtils.getString(json, "sound", "");
        SoundEvent sound = ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation(soundName));

        return new ToolRecipe(recipeId, group, tool, input, result, additionalDrops, sound, damageTool);
    }

    public static final String DROP_TYPE = References.MODID_CORE + ":relation";

    private static ItemStack deserializeAdditionalDrop(JsonElement element) {
        JsonObject object = element.getAsJsonObject();
        if (object.has("type") && object.get("type").getAsString().equals(DROP_TYPE)) {
            Relation relation = Relationships.relation(DeserializeUtils.getRL(object, "relation"));
            Shape shape = Relationships.shape(DeserializeUtils.getRL(object, "shape"));
            int count = object.get("count").getAsInt();
            return relation.first(shape, count);
        } else {
            return ShapedRecipe.deserializeItem(object);
        }
    }

    @Override
    public ToolRecipe read(ResourceLocation recipeId, PacketBuffer buffer) {
        String soundName = buffer.readString(Short.MAX_VALUE);
        String group = buffer.readString(Short.MAX_VALUE);
        Ingredient tool = Ingredient.read(buffer);
        Ingredient input = Ingredient.read(buffer);
        ItemStack result = buffer.readItemStack();
        boolean damageTool = buffer.readBoolean();
        NonNullList<ItemStack> additionalDrops = NonNullList.create();
        int count = buffer.readInt();
        for (int i = 0; i < count; i++) {
            additionalDrops.add(buffer.readItemStack());
        }
        SoundEvent sound = ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation(soundName));
        return new ToolRecipe(recipeId, group, tool, input, result, additionalDrops, sound, damageTool);
    }

    @Override
    public void write(PacketBuffer buffer, ToolRecipe recipe) {
        if (recipe.sound != null) {
            buffer.writeString(recipe.sound.getRegistryName().toString());
        } else {
            buffer.writeString("");
        }
        buffer.writeString(recipe.getGroup());
        recipe.tool.write(buffer);
        recipe.input.write(buffer);
        buffer.writeItemStack(recipe.result);
        buffer.writeBoolean(recipe.damageTool);
        buffer.writeInt(recipe.additionalDrops.size());
        if (!recipe.additionalDrops.isEmpty()) {
            for (ItemStack drop : recipe.additionalDrops) {
                buffer.writeItemStack(drop, true);
            }
        }

    }

}

