/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.impl.crafting.result.count;

import com.betterwithmods.core.Registrars;
import com.betterwithmods.core.api.IPositionedText;
import com.betterwithmods.core.api.ISerializer;
import com.betterwithmods.core.api.crafting.result.count.ICount;
import com.betterwithmods.core.api.crafting.result.count.ICountSerializer;
import com.betterwithmods.core.impl.PositionedText;
import com.google.common.collect.Lists;
import com.google.gson.JsonElement;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.registries.ForgeRegistryEntry;

import java.util.List;

public class Digit implements ICount {
    protected int digit;

    public Digit(int digit) {
        this.digit = MathHelper.clamp(digit, 0, 64);
    }

    @Override
    public Integer get() {
        return digit;
    }

    @Override
    public ICountSerializer<?> getSerializer() {
        return Registrars.CountSerializers.DIGIT;
    }

    public static class DigitSerializer extends ForgeRegistryEntry<ICountSerializer<?>> implements ICountSerializer<Digit>, ISerializer<Digit> {

        @Override
        public Digit read(JsonElement json) {
            return new Digit(json.isJsonPrimitive() ? json.getAsInt() : JSONUtils.getInt(json.getAsJsonObject(), "digit"));
        }

        @Override
        public Digit read(PacketBuffer buffer) {
            return new Digit(buffer.readInt());
        }

        @Override
        public void write(PacketBuffer buffer, Digit count) {
            buffer.writeInt(count.get());
        }

        @Override
        public <V extends Digit> void write(V t, JsonElement jsonElement) {
            jsonElement.getAsJsonObject().addProperty("digit", t.digit);
        }
    }

    @Override
    public boolean equals(ICount count) {
        return count instanceof Digit && ((Digit) count).digit == this.digit;
    }

    @Override
    public List<IPositionedText> getText() {
        List<IPositionedText> text = Lists.newArrayList();
        if (digit > 1) {
            text.add(new PositionedText(new StringTextComponent(Integer.toString(digit)), 14, 9, 1));
        }
        return text;
    }
}
