/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

import java.util.List;
import java.util.Random;
import java.util.function.UnaryOperator;

public class VectorBuilder {

    public static final VectorBuilder ZERO = new VectorBuilder();

    private final Random random = new Random();

    private Vec3d vec;

    private List<UnaryOperator<Vec3d>> operations = Lists.newArrayList();

    public VectorBuilder addOperation(UnaryOperator<Vec3d> operation) {
        this.operations.add(operation);
        return this;
    }

    public VectorBuilder offset(double offset) {
        return this.offset(offset, offset, offset);
    }

    public VectorBuilder offset(Vec3d v) {
        return addOperation(vec -> {
            Preconditions.checkNotNull(vec, "vec");
            return vec.add(v.x, v.y, v.z);
        });
    }

    public VectorBuilder randomOffset(List<BlockPos> positions) {
        return addOperation(vec -> {
            int i = Rand.RANDOM.nextInt(positions.size());
            return vec.add(new Vec3d(positions.get(i)));
        });

    }

    public VectorBuilder offset(double x, double y, double z) {
        return addOperation(vec -> {
            Preconditions.checkNotNull(vec, "vec");
            return vec.add(x, y, z);
        });

    }

    public VectorBuilder offset(Direction facing, double scale) {
        return offset(new Vec3d(facing.getDirectionVec()).scale(scale));
    }


    public VectorBuilder rand(double multiplier) {
        return rand(multiplier, multiplier, multiplier);
    }

    public VectorBuilder rand(double multiplierX, double multiplierY, double multiplierZ) {
        return addOperation(vec -> {
            Preconditions.checkNotNull(vec, "vec");
            return vec.add(random.nextDouble() * multiplierX, random.nextDouble() * multiplierY, random.nextDouble() * multiplierZ);
        });
    }


    public VectorBuilder setGaussian(double multiplierX, double multiplierY, double multiplierZ) {
        return addOperation(vec -> {
            Preconditions.checkNotNull(vec, "vec");
            return vec.add(random.nextGaussian() * multiplierX, random.nextGaussian() * multiplierY, random.nextGaussian() * multiplierZ);
        });
    }

    public VectorBuilder setGaussian(double multiplier) {
        return setGaussian(multiplier, multiplier, multiplier);
    }

    public Vec3d build(BlockPos initial) {
        return build(new Vec3d(initial));
    }

    public Vec3d build(Vec3d initial) {
        this.vec = initial;
        operations.stream().forEachOrdered(c -> this.vec = c.apply(this.vec));
        return this.vec;
    }


}
