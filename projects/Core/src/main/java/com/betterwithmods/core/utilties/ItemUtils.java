/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.tags.Tag;
import net.minecraft.util.Hand;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

import java.util.Optional;

public class ItemUtils {

    public static ItemStack getStack(BlockState state) {
        return getStack(state.getBlock());
    }

    public static ItemStack getStack(IItemProvider provider) {
        return new ItemStack(provider);
    }

    public static Optional<BlockState> getState(ItemStack stack) {
        return getBlock(stack).map(Block::getDefaultState);
    }

    public static boolean isBlock(ItemStack stack) {
        return stack != null && !stack.isEmpty() && isBlock(stack.getItem());
    }

    public static boolean isBlock(Item item) {
        return item instanceof BlockItem;
    }

    public static Optional<Block> getBlock(Item item) {
        if (isBlock(item)) {
            return Optional.of(((BlockItem) item).getBlock());
        }
        return Optional.empty();
    }

    public static Optional<Block> getBlock(ItemStack stack) {
        if (stack != null && !stack.isEmpty()) {
            return getBlock(stack.getItem());
        }
        return Optional.empty();
    }

    public static Optional<Item> getItem(ItemStack stack) {
        if (!stack.isEmpty())
            return Optional.of(stack.getItem());
        return Optional.empty();
    }

    public static StackEjector EJECT_OFFSET = new StackEjector(new VectorBuilder().rand(0.5f).setGaussian(0.05f), new VectorBuilder().setGaussian(0.05f));

    public static StackEjector EJECT_EXACT = new StackEjector(new VectorBuilder(), new VectorBuilder());

    public static void ejectAll(StackEjector ejector, World world, BlockPos pos, Iterable<ItemStack> stacks) {
        for (ItemStack stack : stacks)
            ejector.setStack(stack).ejectStack(world, pos, BlockPos.ZERO);
    }


    public static void ejectStackOffset(World world, BlockPos pos, Iterable<ItemStack> stacks) {
        for (ItemStack stack : stacks)
            ejectStackOffset(world, pos, stack);
    }

    public static void ejectStackOffset(World world, BlockPos pos, ItemStack stack) {
        ejectStackOffset(world, new Vec3d(pos), stack);
    }

    public static void ejectStackOffset(World world, Vec3d vec, ItemStack stack) {
        if (stack.isEmpty())
            return;
        EJECT_OFFSET.setStack(stack).ejectStack(world, vec, Vec3d.ZERO);
    }

    public static void ejectStackExact(World world, BlockPos pos, Iterable<ItemStack> stacks) {
        for (ItemStack stack : stacks)
            ejectStackExact(world, pos, stack);
    }

    public static void ejectStackExact(World world, BlockPos pos, ItemStack stack) {
        ejectStackExact(world, new Vec3d(pos), stack);
    }

    public static void ejectStackExact(World world, Vec3d vec, ItemStack stack) {
        if (stack.isEmpty())
            return;
        EJECT_EXACT.setStack(stack).ejectStack(world, vec, Vec3d.ZERO);
    }

    public static Ingredient fromTag(Tag<? extends IItemProvider> tag) {
        return Ingredient.fromItemListStream(tag.getAllElements().stream().map(IItemProvider::asItem).map(ItemStack::new).map(Ingredient.SingleItemList::new));
    }

    public static void damageItem(ItemStack stack, PlayerEntity player, Hand hand, int amount) {
        stack.damageItem(amount, player, (p) -> p.sendBreakAnimation(hand));
    }

    public static ItemStack withCount(ItemStack stack, int count) {
        ItemStack copy = stack.copy();
        copy.setCount(count);
        return copy;
    }
}
