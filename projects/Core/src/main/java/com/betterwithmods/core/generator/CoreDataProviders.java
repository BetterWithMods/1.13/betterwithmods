/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.generator;

import com.betterwithmods.core.Relationships;
import com.betterwithmods.core.Tags;
import com.betterwithmods.core.base.providers.DataProviders;
import com.betterwithmods.core.relationary.Relation;
import com.betterwithmods.core.relationary.Shape;
import com.google.common.collect.Sets;
import net.minecraft.block.Blocks;
import net.minecraft.data.DataGenerator;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.tags.ItemTags;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;

import java.util.Set;
import java.util.function.Consumer;

public class CoreDataProviders extends DataProviders {
    @Override
    public void onGatherData(GatherDataEvent event) {
        DataGenerator generator = event.getGenerator();
        if (event.includeServer()) {
            generator.addProvider(new BlockTagsProvider(generator));
            generator.addProvider(new ItemTagsProvider(generator));
            generator.addProvider(new Relationary(generator));
        }
    }

    private static class ItemTagsProvider extends net.minecraft.data.ItemTagsProvider {

        public ItemTagsProvider(DataGenerator generatorIn) {
            super(generatorIn);
        }

        @Override
        protected void registerTags() {

            getBuilder(Tags.Items.DUSTS_BLAZE).add(Items.BLAZE_POWDER);
            getBuilder(Tags.Items.DUSTS_GUNPOWDER).add(Items.GUNPOWDER);


            getBuilder(Tags.Items.PORK).add(Items.PORKCHOP, Items.COOKED_PORKCHOP);
            getBuilder(Tags.Items.BEEF).add(Items.BEEF, Items.COOKED_BEEF);
            getBuilder(Tags.Items.MUTTON).add(Items.MUTTON, Items.COOKED_MUTTON);
            getBuilder(Tags.Items.CHICKEN).add(Items.CHICKEN, Items.COOKED_CHICKEN);
            getBuilder(Tags.Items.RABBIT).add(Items.RABBIT, Items.COOKED_RABBIT);
            getBuilder(Tags.Items.FISH).add(Items.TROPICAL_FISH, Items.COD, Items.PUFFERFISH, Items.SALMON, Items.COOKED_SALMON, Items.COOKED_COD);
            getBuilder(Tags.Items.ROTTEN).add(Items.ROTTEN_FLESH);

            getBuilder(Tags.Items.SINKS).add(net.minecraftforge.common.Tags.Items.STONE);
            getBuilder(Tags.Items.FLOATS).add(ItemTags.PLANKS).add(ItemTags.LOGS);
            getBuilder(Tags.Items.EQUILIBRIUM).add(Items.SLIME_BALL);
            getBuilder(Tags.Items.FIRESTARTERS).add(Items.FLINT_AND_STEEL).add(Items.FIRE_CHARGE);
            getBuilder(Tags.Items.SEEDS).add(Items.WHEAT_SEEDS, Items.BEETROOT_SEEDS, Items.MELON_SEEDS, Items.PUMPKIN_SEEDS);
            getBuilder(Tags.Items.UNCOOKED_MEAT).add(Items.BEEF, Items.MUTTON, Items.CHICKEN, Items.PORKCHOP);
            getBuilder(Tags.Items.COOKED_MEAT).add(Items.COOKED_BEEF, Items.COOKED_MUTTON, Items.COOKED_CHICKEN, Items.COOKED_PORKCHOP);
        }
    }

    private static class BlockTagsProvider extends net.minecraft.data.BlockTagsProvider {

        public BlockTagsProvider(DataGenerator generatorIn) {
            super(generatorIn);
        }

        @Override
        protected void registerTags() {
            getBuilder(Tags.Blocks.LOGS).add(Blocks.DARK_OAK_LOG).add(Blocks.OAK_LOG).add(Blocks.ACACIA_LOG).add(Blocks.BIRCH_LOG).add(Blocks.JUNGLE_LOG).add(Blocks.SPRUCE_LOG);
            getBuilder(Tags.Blocks.WOOD).add(Blocks.DARK_OAK_WOOD).add(Blocks.OAK_WOOD).add(Blocks.ACACIA_WOOD).add(Blocks.BIRCH_WOOD).add(Blocks.JUNGLE_WOOD).add(Blocks.SPRUCE_WOOD);
            getBuilder(Tags.Blocks.GRASS_PLANTS).add(Blocks.GRASS, Blocks.FERN, Blocks.LARGE_FERN, Blocks.TALL_GRASS);
            getBuilder(Tags.Blocks.BARK_LOGS).add(Tags.Blocks.LOGS, Tags.Blocks.WOOD);
        }
    }

    private static class Relationary extends RelationaryProviders {

        public Relationary(DataGenerator generatorIn) {
            super(generatorIn);
        }

        @Override
        public void registerRelations(Consumer<Relation> consumer) {
            Set<Relation> relations = Sets.newHashSet();

            relations.add(Relationships.ACACIA_WOOD
                    .addShape(Relationships.LOG, new ItemStack(Blocks.ACACIA_LOG))
                    .addShape(Relationships.PLANK, new ItemStack(Blocks.ACACIA_PLANKS)));
            relations.add(Relationships.BIRCH_WOOD
                    .addShape(Relationships.LOG, new ItemStack(Blocks.BIRCH_LOG))
                    .addShape(Relationships.PLANK, new ItemStack(Blocks.BIRCH_PLANKS)));
            relations.add(Relationships.DARK_OAK_WOOD
                    .addShape(Relationships.LOG, new ItemStack(Blocks.DARK_OAK_LOG))
                    .addShape(Relationships.PLANK, new ItemStack(Blocks.DARK_OAK_PLANKS)));
            relations.add(Relationships.JUNGLE_WOOD
                    .addShape(Relationships.LOG, new ItemStack(Blocks.JUNGLE_LOG))
                    .addShape(Relationships.PLANK, new ItemStack(Blocks.JUNGLE_PLANKS)));
            relations.add(Relationships.OAK_WOOD
                    .addShape(Relationships.LOG, new ItemStack(Blocks.OAK_LOG))
                    .addShape(Relationships.PLANK, new ItemStack(Blocks.OAK_PLANKS)));
            relations.add(Relationships.SPRUCE_WOOD
                    .addShape(Relationships.LOG, new ItemStack(Blocks.SPRUCE_LOG))
                    .addShape(Relationships.PLANK, new ItemStack(Blocks.SPRUCE_PLANKS)));
            relations.add(Relationships.STRIPPED_ACACIA_WOOD
                    .addShape(Relationships.LOG, new ItemStack(Blocks.STRIPPED_ACACIA_LOG))
                    .addShape(Relationships.PLANK, new ItemStack(Blocks.ACACIA_PLANKS)));
            relations.add(Relationships.STRIPPED_BIRCH_WOOD
                    .addShape(Relationships.LOG, new ItemStack(Blocks.STRIPPED_BIRCH_LOG))
                    .addShape(Relationships.PLANK, new ItemStack(Blocks.BIRCH_PLANKS)));
            relations.add(Relationships.STRIPPED_DARK_OAK_WOOD
                    .addShape(Relationships.LOG, new ItemStack(Blocks.STRIPPED_DARK_OAK_LOG))
                    .addShape(Relationships.PLANK, new ItemStack(Blocks.DARK_OAK_PLANKS)));
            relations.add(Relationships.STRIPPED_JUNGLE_WOOD
                    .addShape(Relationships.LOG, new ItemStack(Blocks.STRIPPED_JUNGLE_LOG))
                    .addShape(Relationships.PLANK, new ItemStack(Blocks.JUNGLE_PLANKS)));
            relations.add(Relationships.STRIPPED_OAK_WOOD
                    .addShape(Relationships.LOG, new ItemStack(Blocks.STRIPPED_OAK_LOG))
                    .addShape(Relationships.PLANK, new ItemStack(Blocks.OAK_PLANKS)));
            relations.add(Relationships.STRIPPED_SPRUCE_WOOD
                    .addShape(Relationships.LOG, new ItemStack(Blocks.STRIPPED_SPRUCE_LOG))
                    .addShape(Relationships.PLANK, new ItemStack(Blocks.SPRUCE_PLANKS)));


            relations.forEach(consumer);
        }

        @Override
        public void registerShapes(Consumer<Shape> consumer) {
            Set<Shape> shapes = Sets.newHashSet();
            shapes.add(Relationships.BLOCK);
            shapes.add(Relationships.BARK);
            shapes.add(Relationships.LOG);
            shapes.add(Relationships.PLANK);
            shapes.add(Relationships.SIDING);
            shapes.add(Relationships.MOULDING);
            shapes.add(Relationships.CORNER);
            shapes.add(Relationships.INGOT);
            shapes.add(Relationships.NUGGET);
            shapes.add(Relationships.DUST);


            shapes.forEach(consumer);
        }
    }
}

