/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.generator;

import com.betterwithmods.core.utilties.DeserializeUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.minecraft.advancements.Advancement;
import net.minecraft.data.IFinishedRecipe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;

public class RecipeResult implements IFinishedRecipe {
    private final ResourceLocation id;
    private final ItemStack result;
    private final String group;
    private final List<String> pattern;
    private final Map<Character, Ingredient> key;
    private final Advancement.Builder advancementBuilder;
    private final ResourceLocation advancementId;
    private final IRecipeSerializer<?> serializer;

    public RecipeResult(IRecipeSerializer<?> serializer, ResourceLocation idIn, ItemStack result, String groupIn, List<String> patternIn, Map<Character, Ingredient> keyIn, Advancement.Builder advancementBuilderIn, ResourceLocation advancementIdIn) {
        this.serializer = serializer;
        this.id = idIn;
        this.result = result;
        this.group = groupIn;
        this.pattern = patternIn;
        this.key = keyIn;
        this.advancementBuilder = advancementBuilderIn;
        this.advancementId = advancementIdIn;
    }

    @Override
    public void serialize(JsonObject p_218610_1_) {
        if (!this.group.isEmpty()) {
            p_218610_1_.addProperty("group", this.group);
        }

        JsonArray jsonarray = new JsonArray();

        for (String s : this.pattern) {
            jsonarray.add(s);
        }

        p_218610_1_.add("pattern", jsonarray);
        JsonObject jsonobject = new JsonObject();

        for (Map.Entry<Character, Ingredient> entry : this.key.entrySet()) {
            jsonobject.add(String.valueOf(entry.getKey()), entry.getValue().serialize());
        }

        p_218610_1_.add("key", jsonobject);

        JsonObject jsonobject1 = new JsonObject();
        DeserializeUtils.serializeItemStack(result, jsonobject1);

        p_218610_1_.add("result", jsonobject1);
    }

    public IRecipeSerializer<?> getSerializer() {
        return serializer;
    }

    /**
     * Gets the ID for the recipe.
     */
    public ResourceLocation getID() {
        return this.id;
    }

    /**
     * Gets the JSON for the advancement that unlocks this recipe. Null if there is no advancement.
     */
    @Nullable
    public JsonObject getAdvancementJson() {
        return this.advancementBuilder.serialize();
    }

    /**
     * Gets the ID for the advancement associated with this recipe. Should not be null if {@link #getAdvancementJson}
     * is non-null.
     */
    @Nullable
    public ResourceLocation getAdvancementID() {
        return this.advancementId;
    }
}
