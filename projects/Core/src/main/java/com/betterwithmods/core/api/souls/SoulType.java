/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.api.souls;

import net.minecraft.util.IStringSerializable;

public enum SoulType implements IStringSerializable  {
    ENDER("ender"),
    GILDED("gilded"),
    BURNING("burning"),
    DROWNED("drowned"),
    LOST("lost"),
    VOID("void"),
    EMPTY("empty");

    private String name;

    SoulType(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public static SoulType fromName(String name) {
        for (SoulType type : SoulType.values()) {
            if(type.name.equals(name)) {
                return type;
            }
        }

        return SoulType.EMPTY;
    }
}
