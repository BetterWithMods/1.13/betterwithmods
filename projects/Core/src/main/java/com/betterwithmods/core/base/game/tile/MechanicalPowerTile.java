/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.tile;

import com.betterwithmods.core.api.BWMApi;
import com.betterwithmods.core.api.mech.IMechanicalPower;
import com.betterwithmods.core.api.mech.IPower;
import com.betterwithmods.core.base.config.CoreCommon;
import com.betterwithmods.core.impl.CapabilityMechanicalPower;
import com.betterwithmods.core.impl.Power;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

public abstract class MechanicalPowerTile extends TileBase implements IMechanicalPower {

    private final LazyOptional<IMechanicalPower> powerHandler;

    @Nonnull
    public IPower power = Power.ZERO, previousPower = Power.ZERO;

    public MechanicalPowerTile(TileEntityType<?> tileEntityTypeIn) {
        super(tileEntityTypeIn);
        this.powerHandler = LazyOptional.of(() -> this);
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        if (isMechanicalSide(side) && cap == CapabilityMechanicalPower.MECHANICAL_POWER) {
            return CapabilityMechanicalPower.MECHANICAL_POWER.orEmpty(cap, powerHandler);
        }
        return super.getCapability(cap, side);
    }

    protected boolean isMechanicalSide(Direction facing) {
        return true;
    }

    @Override
    public void onChanged() {
        markDirty();
        if (world != null) {
            calculate();
            postCalculate();
            this.previousPower = this.power.clone();
        }
    }

    @SuppressWarnings("ConstantConditions")
    protected void calculate() {
        IPower previousPower = this.power.clone();

        int previousTorque = previousPower.getTorque();
        int previousDistance = previousPower.getDistance();

        int newTorque = 0;
        int newDistance = 0;

        int sources = 0;

        for (Direction facing : getInputs()) {
            IPower output = getInput(world, pos, facing);
            if (output != null) {
                int distance = output.getDistance();
                int torque = output.getTorque();
                //Overpower
                IPower maxInput = getMaximumInput(facing);
                if (maxInput != null && torque > maxInput.getTorque()) {
                    if (overpower(world, pos))
                        return;
                }

                //Ignore torques less than minimum
                IPower minInput = getMinimumInput();
                if (minInput != null && torque < minInput.getTorque()) {
                    continue;
                }

                if (distance > previousDistance) {
                    newDistance = distance;
                    if (torque >= newTorque) {
                        sources++;
                        newTorque = torque;
                    }
                }
            }
        }


        boolean powered = false;
        if (newDistance > previousDistance) {
            if (newTorque > 0) {
                newDistance = newDistance - 1;

                //Overpower if distance is less than minimum
                IPower minInput = getMinimumInput();
                if (minInput != null && newDistance < minInput.getDistance()) {
                    if (overpower(world, pos))
                        return;
                }
            }

            powered = true;
            //Find all distances less than current value
        } else {
            //Unpowered
            newDistance = 0;
            newTorque = 0;
            powered = false;
            //Find all distances less than the previous value
        }

        if (powered && onPowered(sources)) {
            return;
        }

        Power power = new Power(newTorque, newDistance);
        if (!power.equals(this.power)) {
            this.power = power;

            if (CoreCommon.DEBUGGING.get()) {
                //On change
                if (DEBUG_CURSOR.hasNext()) {
                    Block block = DEBUG_CURSOR.next();
                    world.setBlockState(pos.down(2), block.getDefaultState());
                } else {
                    DEBUG_CURSOR = DEBUG_ELEMENTS.iterator();
                }
//                System.out.println(pos + "," + power);
            }
        }


        Set<BlockPos> toNotify = Sets.newHashSet();

        for (Direction facing : getOutputs()) {
            BlockPos offset = pos.offset(facing);
            IMechanicalPower mech = BWMApi.MECHANICAL_UTILS.getMechanicalPower(world, offset, facing).orElse(null);
            Direction offsetFacing = facing.getOpposite();
            if (mech != null) {
                boolean update = false;
                if (mech.canInputFrom(offsetFacing)) {
                    IPower output = getInput(world, pos, facing);
                    if (output != null) {
                        if ((powered && this.power.getTorque() > output.getTorque())
                                || output.getTorque() > 0 && previousPower.getDistance() > output.getDistance()) {
                            update = true;
                        }
                    } else {
                        toNotify.add(offset);
                    }
                }

                if (update) {
                    toNotify.add(offset);
                }
            }
        }

        for (BlockPos notify : toNotify) {
            world.neighborChanged(notify, getBlockState().getBlock(), pos);
        }
    }

    public boolean onPowered(int sources) {
        if (sources > 1) {
            return this.overpower(world, pos);
        }
        return false;
    }


    @Override
    public boolean canInputFrom(Direction facing) {
        for (Direction f : getInputs()) {
            if (f == facing)
                return true;
        }
        return false;
    }

    @Nonnull
    public abstract Iterable<Direction> getInputs();

    @Nonnull
    public abstract Iterable<Direction> getOutputs();

    private static final String POWER = "power", PREVIOUS_POWER = "previousPower";

    @Nonnull
    @Override
    public CompoundNBT write(CompoundNBT compound) {
        compound.put(POWER, this.power.serializeNBT());
        compound.put(PREVIOUS_POWER, this.previousPower.serializeNBT());
        return super.write(compound);
    }

    @Override
    public void read(CompoundNBT compound) {
        super.read(compound);
        if (compound.contains(POWER)) {
            power.deserializeNBT(compound.getCompound(POWER));
        }
        if (compound.contains(PREVIOUS_POWER)) {
            previousPower.deserializeNBT(compound.getCompound(PREVIOUS_POWER));
        }
    }


    public abstract void postCalculate();


    public LinkedList<Block> DEBUG_ELEMENTS = new LinkedList<>();

    {
        if (CoreCommon.DEBUGGING.get()) {
            DEBUG_ELEMENTS = new LinkedList<>(Lists.newArrayList(
                    Blocks.WHITE_WOOL,
                    Blocks.ORANGE_WOOL,
                    Blocks.MAGENTA_WOOL,
                    Blocks.LIGHT_BLUE_WOOL,
                    Blocks.YELLOW_WOOL,
                    Blocks.LIME_WOOL,
                    Blocks.PINK_WOOL,
                    Blocks.GRAY_WOOL,
                    Blocks.LIGHT_GRAY_WOOL,
                    Blocks.CYAN_WOOL,
                    Blocks.PURPLE_WOOL,
                    Blocks.BLUE_WOOL,
                    Blocks.BROWN_WOOL,
                    Blocks.GREEN_WOOL,
                    Blocks.RED_WOOL,
                    Blocks.BLACK_WOOL
            ));
        }
    }

    public Iterator<Block> DEBUG_CURSOR = DEBUG_ELEMENTS.iterator();
}
