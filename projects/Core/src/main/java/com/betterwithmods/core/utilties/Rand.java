/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties;


import org.apache.commons.lang3.Range;

import java.util.Random;

public class Rand {

    public static Random RANDOM = new Random();

    public static int between(Random r, int min, int max) {
        return r.nextInt(max - min) + min;
    }

    public static int between(int min, int max) {
        return Rand.between(RANDOM, min, max);
    }

    public static float between(Random r, float min, float max) {
        return (r.nextFloat() * max - min) + min;
    }

    public static float between(float min, float max) {
        return between(RANDOM, min, max);
    }

    public static boolean chance(Random r, double goal) {
        return r.nextDouble() <= goal;
    }

    public static boolean chance(double goal) {
        return chance(RANDOM, goal);
    }

    public static void onRandom(Random r, double chance, Runnable runnable) {
        if (chance(r, chance)) {
            runnable.run();
        }
    }

    public static int between(Range<Integer> range) {
        return between(range.getMinimum(), range.getMinimum());
    }

}
