/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.model;


import com.betterwithmods.core.utilties.MiscUtils;
import com.google.gson.JsonObject;
import net.minecraft.util.JSONUtils;

import javax.vecmath.Vector2d;
import javax.vecmath.Vector3d;

public class Cube {

    private final Vector3d origin, size;
    private final Vector2d uv;
    private final boolean mirror;

    private final int inflate;

    public Cube(Vector3d origin, Vector3d size, Vector2d uv, boolean mirror, int inflate) {
        this.origin = origin;
        this.size = size;
        this.uv = uv;
        this.mirror = mirror;
        this.inflate = inflate;
    }

    public Vector3d getOrigin() {
        return origin;
    }

    public Vector3d getSize() {
        return size;
    }

    public Vector2d getUv() {
        return uv;
    }

    public boolean isMirror() {
        return mirror;
    }

    public int getInflate() {
        return inflate;
    }

    public static Cube deserialize(JsonObject object) {

        Vector3d origin = new Vector3d(0, 0, 0);
        if (JSONUtils.hasField(object, "origin")) {
            origin = MiscUtils.parseVec3d(JSONUtils.getJsonArray(object, "origin"));
        }

        Vector3d size = new Vector3d(0, 0, 0);
        if (JSONUtils.hasField(object, "size")) {
            size = MiscUtils.parseVec3d(JSONUtils.getJsonArray(object, "size"));
        }

        Vector2d uv = new Vector2d(0, 0);
        if (JSONUtils.hasField(object, "uv")) {
            uv = MiscUtils.parseVec2d(JSONUtils.getJsonArray(object, "uv"));
        }

        boolean mirror = JSONUtils.getBoolean(object, "mirror", false);
        int inflate = JSONUtils.getInt(object, "inflate", 0);

        return new Cube(origin, size, uv, mirror, inflate);
    }
}
