/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.impl.crafting.result;

import com.betterwithmods.core.Registrars;
import com.betterwithmods.core.api.IPositionedText;
import com.betterwithmods.core.api.crafting.result.IResult;
import com.betterwithmods.core.api.crafting.result.IResultRenderer;
import com.betterwithmods.core.api.crafting.result.IResultSerializer;
import com.betterwithmods.core.impl.crafting.ItemStackBuilder;
import com.betterwithmods.core.impl.crafting.result.count.Digit;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

import javax.annotation.Nullable;
import java.util.List;

public class StackResult extends BaseStackResult {

    protected ItemStackBuilder builder;

    public StackResult(IItemProvider item) {
        this(item, 1);
    }


    public StackResult(IItemProvider item, int count) {
        this(ItemStackBuilder.builder().item(item).count(new Digit(count)));
    }

    public StackResult(ItemStack stack) {
        this(ItemStackBuilder.builder().fromStack(stack));
    }

    public StackResult(ItemStackBuilder builder) {
        this.builder = builder;
    }

    @Override
    public <T> ItemStack get(@Nullable T input) {
        return builder.build();
    }

    public ItemStackBuilder getBuilder() {
        return builder;
    }

    @Override
    public boolean equals(IResult result) {
        if (result instanceof StackResult) {
            return this.builder.equals(((StackResult) result).builder);
        }
        return false;
    }

    @Override
    public IResultSerializer<?> getSerializer() {
        return Registrars.ResultSerializers.STACK;
    }

    @Override
    public StackResult copy() {
        return new StackResult(builder);
    }

    @Override
    public ItemStack displayStack() {
        return builder.displayStack();
    }

    public List<IPositionedText> displayCount() {
        return builder.getCountProvider().getText();
    }

    @Override
    public ResourceLocation getId() {
        return builder.getItem().getRegistryName();
    }

    @Override
    public Class<ItemStack> getType() {
        return ItemStack.class;
    }

    @Override
    public ITextComponent displayText() {
        return displayStack().getTextComponent();
    }

    @Override
    public IResultRenderer<ItemStack> getRenderer() {
        return StackResultRenderer.INSTANCE;
    }
}
