/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.relationary;

import com.betterwithmods.core.network.NetworkHandler;
import com.betterwithmods.core.network.RelationaryReloadMessage;
import com.betterwithmods.core.utilties.MiscUtils;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import net.minecraft.item.ItemStack;
import net.minecraft.resources.IResource;
import net.minecraft.resources.IResourceManager;
import net.minecraft.resources.IResourceManagerReloadListener;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.ModLoader;
import net.minecraftforge.fml.network.PacketDistributor;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nullable;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

public class RelationaryManager implements IResourceManagerReloadListener {

    public static Container<Relation> RELATIONS = new Container<>(Relation::getRegistryName);
    public static Container<Shape> SHAPES = new Container<>(Shape::getRegistryName);

    private final Container<Relation> relations = new Container<>(Relation::getRegistryName);
    private final Container<Shape> shapes = new Container<>(Shape::getRegistryName);

    private static final Pattern SHAPES_PATTERN = Pattern.compile("shapes/(.*).json");
    private static final Pattern RELATIONS_PATTERN = Pattern.compile("relations/(.*).json");

    protected static final Multimap<HashableItemStack, Relation> ITEM_STACK_TO_RELATION = MultimapBuilder.hashKeys().hashSetValues().build();

    private Logger logger;
    private MinecraftServer server;
    private boolean locked;


    public RelationaryManager(MinecraftServer server, Logger logger) {
        this.logger = logger;
        this.server = server;
        MANAGERS.put(server, this);
    }

    public static Relation getOrCreateRelation(ResourceLocation registryName) {
        return RELATIONS.getOrDefault(registryName, new Relation(registryName));
    }

    public static Set<Relation> getRelations(ItemStack stack) {
        HashableItemStack hashed = new HashableItemStack(stack);
        return (Set<Relation>) ITEM_STACK_TO_RELATION.get(hashed);
    }

    public static Shape getOrCreateShape(ResourceLocation registryName) {
        return SHAPES.getOrDefault(registryName, new Shape(registryName, ""));
    }

    public static Set<Shape> getShape(Relation relation, ItemStack stack) {
        return (Set<Shape>) relation.fromStack(stack);
    }

    public static boolean isShape(Relation relation, ItemStack stack, Shape shape) {
        return getShape(relation, stack).contains(shape);
    }

    @Nullable
    public static Relation getRelationWithShape(ItemStack stack, Shape shape) {
        Set<Relation> relations = getRelations(stack);
        for (Relation relation : relations) {
            if (isShape(relation, stack, shape)) {
                return relation;
            }
        }
        return null;
    }

    public void addShape(Shape shape) throws IllegalAccessException {
        if (locked)
            throw new IllegalAccessException("Cannot add to Relationary after locked");
        if (shapes.containsKey(shape.getRegistryName())) {
            throw new IllegalStateException("Duplicate shape ignored with ID " + shape.getRegistryName());
        } else {
            shapes.add(shape);
        }
    }

    public void addRelation(Relation relation) throws IllegalAccessException {
        if (locked)
            throw new IllegalAccessException("Cannot add to Relationary after locked");
        if (relations.containsKey(relation.getRegistryName())) {
            throw new IllegalStateException("Duplicate relation ignored with ID " + relation.getRegistryName());
        } else {
            relations.add(relation);
        }
    }



    @Override
    public void onResourceManagerReload(IResourceManager resourceManager) {
        Gson gson = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();
        relations.clear();
        shapes.clear();
        locked = false;
        for (ResourceLocation resourcelocation : resourceManager.getAllResourceLocations("shapes", (file) -> {
            return file.endsWith(".json") && !file.startsWith("_"); //Forge filter anything beginning with "_" as it's used for metadata.
        })) {
            ResourceLocation registryName = MiscUtils.stripPath(SHAPES_PATTERN, resourcelocation);
            if (registryName == null) {
                logger.error("Failed to parse registryName for {}", resourcelocation);
                continue;
            }
            try (IResource iresource = resourceManager.getResource(resourcelocation)) {
                JsonObject jsonobject = JSONUtils.fromJson(gson, IOUtils.toString(iresource.getInputStream(), StandardCharsets.UTF_8), JsonObject.class);
                if (jsonobject != null) {
                    addShape(Shape.write(registryName, jsonobject));
                } else {
                    logger.error("Couldn't load shape {} as it's null or empty", registryName);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        setShapes(shapes);
        Map<ResourceLocation, Relation> relationsMap = Maps.newHashMap();
        for (ResourceLocation resourcelocation : resourceManager.getAllResourceLocations("relations", (file) -> {
            return file.endsWith(".json") && !file.startsWith("_"); //Forge filter anything beginning with "_" as it's used for metadata.
        })) {
            ResourceLocation registryName = MiscUtils.stripPath(RELATIONS_PATTERN, resourcelocation);
            if (registryName == null) {
                logger.error("Failed to parse registryName for {}", resourcelocation);
                continue;
            }
            try {
                for (IResource iresource : resourceManager.getAllResources(resourcelocation)) {
                    JsonObject jsonobject = JSONUtils.fromJson(gson, IOUtils.toString(iresource.getInputStream(), StandardCharsets.UTF_8), JsonObject.class);
                    if (jsonobject != null) {
                        Relation relation = relationsMap.getOrDefault(registryName, new Relation(registryName));
                        Relation.read(relation, jsonobject);
                        relationsMap.put(registryName, relation);
                    } else {
                        logger.error("Couldn't load relation {} as it's null or empty", registryName);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        ModLoader.get().postEvent(new RelationaryLoadedEvent(this, relationsMap));

        relationsMap.values().forEach(relation -> {
            try {
                addRelation(relation);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });

        locked = true;

        setRelations(relations);


        Relation.VERSION++;
        RelationaryReloadMessage message = new RelationaryReloadMessage(this);
        NetworkHandler.CHANNEL.send(PacketDistributor.ALL.noArg(), message);
    }

    public Collection<Shape> getShapes() {
        return this.shapes.values();
    }

    public Collection<Relation> getRelations() {
        return this.relations.values();
    }

    public static void setRelations(Container<Relation> relations) {
        RELATIONS = relations;
    }

    public static void setShapes(Container<Shape> shapes) {
        SHAPES = shapes;
    }


    private static final Map<MinecraftServer, RelationaryManager> MANAGERS = Maps.newHashMap();

    public static RelationaryManager get(MinecraftServer server) {
        return MANAGERS.get(server);
    }

    public Container<Relation> getRelationContainer() {
        return relations;
    }

    public Container<Shape> getShapeContainer() {
        return shapes;
    }
}

