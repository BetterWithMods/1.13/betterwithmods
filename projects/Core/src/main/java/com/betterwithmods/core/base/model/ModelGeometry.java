/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.model;

import com.google.common.collect.Lists;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.util.ResourceLocation;

import javax.vecmath.Vector2d;
import javax.vecmath.Vector3d;
import java.util.Collection;
import java.util.List;

public class ModelGeometry extends Model {

    private ResourceLocation registryName;

    private final Geometry.Wrapper geometry;

    private List<RendererModel> renderers = Lists.newArrayList();

    public ModelGeometry(ResourceLocation location) {
        this.registryName = location;
        this.geometry = new Geometry.Wrapper(registryName);
        this.geometry.setCallback(this::setup);
        setup();
    }


    public void setup() {
        this.textureWidth = geometry.getTextureWidth();
        this.textureHeight = geometry.getTextureHeight();

        Collection<Bone> bones = this.geometry.getBones();
        this.renderers.clear();

        for (Bone bone : bones) {
            RendererModel renderer = new RendererModel(this, bone.getName());

            Vector3d pivot = bone.getPivot();
            renderer.rotationPointX = (float) pivot.x;
            renderer.rotationPointY = (float) pivot.y;
            renderer.rotationPointZ = (float) pivot.z;
            Vector3d rotation = bone.getRotation();
            renderer.rotateAngleX = (float) -Math.toRadians(rotation.x);
            renderer.rotateAngleY = (float) -Math.toRadians(rotation.y);
            renderer.rotateAngleZ = (float) -Math.toRadians(rotation.z);
            renderer.mirror = bone.isMirror();
            List<Cube> cubes = bone.getCubes();
            for (Cube cube : cubes) {
                Vector3d origin = cube.getOrigin();
                float x = (float) origin.x;
                float y = (float) origin.y;
                float z = (float) origin.z;
                Vector3d size = cube.getSize();
                int w = (int) size.x;
                int h = (int) size.y;
                int d = (int) size.z;
                float inflate = cube.getInflate(); //TODO might need to scale this to a float
                boolean mirror = cube.isMirror();

                Vector2d uv = cube.getUv();

                renderer.setTextureOffset((int) uv.x, (int) uv.y);
                renderer.addBox(x, y, z, w, h, d, inflate, mirror);
            }

            this.renderers.add(renderer);
        }
    }

    public void render(float scale) {
        for (RendererModel renderer : renderers) {
            renderer.render(scale);
        }

    }

    public Geometry getGeometry() {
        return geometry;
    }

}
