/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.impl.crafting;

import com.betterwithmods.core.api.ISerializer;
import com.betterwithmods.core.api.crafting.result.count.ICount;
import com.betterwithmods.core.impl.crafting.result.count.Digit;
import com.betterwithmods.core.relationary.Relation;
import com.betterwithmods.core.relationary.Shape;
import com.betterwithmods.core.utilties.DeserializeUtils;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.JsonUtils;
import net.minecraftforge.registries.ForgeRegistries;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Random;

public class ItemStackBuilder {
    private static final Random random = new Random();

    public Item item;

    public ICount countProvider = new Digit(1);

    public CompoundNBT tag;

    protected ItemStackBuilder() {
    }


    public static ItemStackBuilder builder() {
        return new ItemStackBuilder();
    }

    public ItemStackBuilder fromRelationShape(Relation relation, Shape shape) {
        Collection<ItemStack> stacks = relation.fromShape(shape);
        ItemStack stack = stacks.stream().findFirst().orElse(ItemStack.EMPTY);
        return fromStack(stack);
    }


    public ItemStackBuilder fromStack(ItemStack stack) {
        this.item = stack.getItem();
        this.tag = stack.getTag();
        return this;
    }

    public ItemStackBuilder item(IItemProvider item) {
        this.item = item.asItem();
        return this;
    }

    public ItemStackBuilder count(ICount countProvider) {
        this.countProvider = countProvider;
        return this;
    }

    public ItemStackBuilder tag(CompoundNBT tag) {
        this.tag = tag;
        return this;
    }

    public ItemStack build() {
        ItemStack stack = new ItemStack(item, countProvider.get());
        stack.setTag(tag);
        return stack;
    }

    public static final Serializer SERIALIZER = new Serializer();

    public static class Serializer implements ISerializer<ItemStackBuilder> {

        @Nonnull
        @Override
        public ItemStackBuilder read(@Nonnull JsonElement json) {

            ICount count = DeserializeUtils.readCount(json);
            String itemName = JSONUtils.getString(json.getAsJsonObject(), "item");
            Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(itemName));
            if (item == null)
                throw new JsonSyntaxException("Unknown item '" + itemName + "'");
            CompoundNBT tag = JsonUtils.readNBT(json.getAsJsonObject(), "nbt");
            if (count == null)
                throw new JsonParseException("Invalid count for " + itemName);
            return ItemStackBuilder
                    .builder()
                    .item(item)
                    .tag(tag)
                    .count(count);
        }

        @Nonnull
        @Override
        public ItemStackBuilder read(PacketBuffer buffer) {
            ItemStack stack = buffer.readItemStack();
            Item item = stack.getItem();
            CompoundNBT tag = stack.getTag();
            ICount count = ICount.read(buffer);

            return ItemStackBuilder
                    .builder()
                    .item(item)
                    .tag(tag)
                    .count(count);
        }

        @Override
        public void write(PacketBuffer buffer, ItemStackBuilder builder) {
            ItemStack stack = new ItemStack(builder.item, 1, builder.tag);
            buffer.writeItemStack(stack);
            ICount.write(buffer, builder.countProvider);
        }


    }

    public boolean equals(ItemStackBuilder builder) {
        return this.item.equals(builder.item) &&
                this.countProvider.equals(builder.countProvider) &&
                ((this.tag == null || builder.tag == null) || this.tag.equals(builder.tag));
    }

    public Item getItem() {
        return item;
    }

    @Override
    public String toString() {
        if(tag != null) {
            return String.format("%s:%s:%s", item.getRegistryName(), countProvider.getSerializer().getRegistryName(), tag.toString());
        }
        return String.format("%s:%s", item.getRegistryName(), countProvider.getSerializer().getRegistryName());
    }

    public ItemStack displayStack() {
        return new ItemStack(item, 1, tag);
    }

    public ICount getCountProvider() {
        return countProvider;
    }

    public CompoundNBT getTag() {
        return tag;
    }
}
