/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.ai.goal.GoalSelector;
import net.minecraft.entity.ai.goal.PrioritizedGoal;
import net.minecraft.tags.Tag;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.World;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class EntityUtils {

    public static void doWaterSplashEffect(Entity entity) {
        ReflectionHelper.invokeMethod(Entity.class, entity, "doWaterSplashEffect");
    }

    public static <T extends Entity> List<T> getEntitesInShape(World world, BlockPos pos, VoxelShape shape, Class<T> entityClazz, Predicate<T> filter) {
        return shape.toBoundingBoxList().stream().flatMap((box) ->
                world.getEntitiesWithinAABB(entityClazz,
                        box.offset(pos.getX(), pos.getY(), pos.getZ()),
                        filter).stream())
                .collect(Collectors.toList());
    }

    public static Set<PrioritizedGoal> getGoals(GoalSelector selector) {
        return (Set<PrioritizedGoal>) ReflectionHelper.getValue(GoalSelector.class, selector, "goals");
    }

    public static void clear(GoalSelector selector) {
        getGoals(selector).clear();
    }

    public static <T extends Goal> Optional<T> findGoals(Class<T> aiClazz, GoalSelector selector) {
        for(PrioritizedGoal goal: getGoals(selector)) {
            Goal inner = goal.getGoal();
            if(aiClazz.isAssignableFrom(inner.getClass())) {
                return Optional.of( (T) inner);
            }
        }
        return Optional.empty();
    }


    public static <T extends Entity> boolean filter(T e, Tag<EntityType<?>> tag) {
        return tag.contains(e.getType());
    }
}

