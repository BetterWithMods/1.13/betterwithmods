/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.impl.souls;

import com.betterwithmods.core.api.souls.ISoulStack;
import com.betterwithmods.core.api.souls.ISoulStorage;
import com.betterwithmods.core.api.souls.SoulType;
import net.minecraft.nbt.CompoundNBT;

public class SoulStorage implements ISoulStorage {

    private ISoulStack soulStack = SoulStack.EMPTY;
    private int capacity;

    public SoulStorage(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public boolean insert(ISoulStack soulStack, boolean simulate) {
        if (getSoulType() == SoulType.EMPTY) {
            if(!simulate) {
                this.setSoulType(soulStack.getType());
            }
        }
        //New Type
        if (getSoulType() == soulStack.getType()) {
            if(!simulate) {
                this.soulStack.grow(soulStack.getAmount());
                this.soulStack.setAmount(Math.min(getCapacity(), this.soulStack.getAmount()));
            }
            return true;
        }

        return false;
    }

    @Override
    public ISoulStack extract(SoulType type, int amount, boolean simulate) {
        //TODO
        return SoulStack.EMPTY;
    }

    @Override
    public void setSoulType(SoulType type) {
        this.soulStack = new SoulStack(type);
    }

    @Override
    public void setAmount(int amount) {
        this.soulStack.setAmount(amount);
    }

    @Override
    public SoulType getSoulType() {
        return this.soulStack.getType();
    }

    @Override
    public int getAmount() {
        return this.soulStack.getAmount();
    }

    @Override
    public int getCapacity() {
        return capacity;
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        tag.putInt("capacity", this.capacity);
        tag.put("soulStack", this.soulStack.serializeNBT());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT tag) {
        this.setCapacity(tag.getInt("capacity"));
        if (tag.contains("soulStack")) {
            this.soulStack = new SoulStack(tag.getCompound("soulStack"));
        }
    }


}
