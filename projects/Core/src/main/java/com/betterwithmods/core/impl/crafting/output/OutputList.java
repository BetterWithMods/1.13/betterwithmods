/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.impl.crafting.output;

import com.betterwithmods.core.Registrars;
import com.betterwithmods.core.api.crafting.output.IOutputSerializer;
import com.betterwithmods.core.api.crafting.result.IResult;
import com.google.common.collect.Lists;

import java.util.Collection;
import java.util.List;

public class OutputList extends BaseOutput {

    private List<IResult> results;

    public OutputList(List<IResult> results) {
        this.results = results;
    }

    public OutputList() {
        this(Lists.newArrayList());
    }

    @Override
    public Collection<IResult> getAllResults() {
        return results;
    }

    @Override
    public IOutputSerializer<?> getSerializer() {
        return Registrars.OutputSerializers.LIST;
    }

    @Override
    public boolean isDynamic() {
        return results.stream().anyMatch(IResult::isDynamic);
    }

    @Override
    public <T> List<IResult<T>> getDisplayResults(Class<T> type) {
        return super.getDisplayResults(type);
    }

    @Override
    public void addResult(IResult result) {
        results.add(result);
    }
}
