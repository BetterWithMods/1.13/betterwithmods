/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.goals;

import net.minecraft.entity.MobEntity;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.item.Item;
import net.minecraft.tags.Tag;
import net.minecraft.util.math.AxisAlignedBB;

import java.util.List;

public class FeedTargetGoal extends Goal {

    private final MobEntity entity;
    private final PickupFeedGoal pickupTask;
    private final Tag<Item> feedTag;
    private ItemEntity feedItemEntity;
    private final float speed;


    public FeedTargetGoal(MobEntity entity, Tag<Item> feedTag, PickupFeedGoal pickupTask, float speed) {
        this.entity = entity;
        this.feedTag = feedTag;
        this.speed = speed;
        this.pickupTask = pickupTask;
    }

    @Override
    public boolean shouldContinueExecuting() {
        return false ;
    }

    @Override
    public void startExecuting() {
        this.feedItemEntity = null;

        if(pickupTask.feedCooldown <= 0) {
            AxisAlignedBB searchArea = new AxisAlignedBB(entity.getPosition()).grow(10);
            List<ItemEntity> foundItems = entity.world.getEntitiesWithinAABB(ItemEntity.class, searchArea);

            ItemEntity feedToEat = foundItems.stream()
                    .filter(entityItem -> feedTag.contains(entityItem.getItem().getItem()))
                    .findFirst()
                    .orElse(null);

            if (feedToEat != null) {
                this.feedItemEntity = feedToEat;
                this.entity.getNavigator().tryMoveToXYZ(feedToEat.posX, feedToEat.posY, feedToEat.posZ, speed);
            }
        }
    }

    @Override
    public void resetTask() {
        this.feedItemEntity = null;
    }

    @Override
    public boolean shouldExecute() {
        return true;
    }
}
