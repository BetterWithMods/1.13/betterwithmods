/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.network;

import com.betterwithmods.core.References;
import com.betterwithmods.core.base.setup.ISetup;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.event.lifecycle.FMLLoadCompleteEvent;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;

public class NetworkHandler implements ISetup {

    public static SimpleChannel CHANNEL = NetworkRegistry.ChannelBuilder
            .named(new ResourceLocation(References.MODID_CORE, "network"))
            .clientAcceptedVersions(References.PROTOCOL_VERSION::equals)
            .serverAcceptedVersions(References.PROTOCOL_VERSION::equals)
            .networkProtocolVersion(() -> References.PROTOCOL_VERSION)
            .simpleChannel();

    @Override
    public void onLoadComplete(FMLLoadCompleteEvent event) {
        int messageNumber = 0;

        CHANNEL.messageBuilder(RelationaryReloadMessage.class, messageNumber++)
                .encoder(RelationaryReloadMessage::encode)
                .decoder(RelationaryReloadMessage::decode)
                .consumer(RelationaryReloadMessage::handle)
                .add();

        CHANNEL.messageBuilder(MultiplexRecipeReloadMessage.class, messageNumber++)
                .encoder(MultiplexRecipeReloadMessage::encode)
                .decoder(MultiplexRecipeReloadMessage::decode)
                .consumer(MultiplexRecipeReloadMessage::handle)
                .add();
    }


}
