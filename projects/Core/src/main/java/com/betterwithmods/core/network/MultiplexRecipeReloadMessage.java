/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.network;

import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipe;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipeSerializer;
import com.betterwithmods.core.impl.Registries;
import com.betterwithmods.core.impl.crafting.multiplex.MultiplexRecipeManager;
import com.google.common.collect.Lists;
import net.minecraft.network.PacketBuffer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.fml.server.ServerLifecycleHooks;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;

public class MultiplexRecipeReloadMessage {

    @Nonnull
    private final List<IMultiplexRecipe<?, ?>> recipes;

    public MultiplexRecipeReloadMessage() {
        this(ServerLifecycleHooks.getCurrentServer());
    }

    public MultiplexRecipeReloadMessage(MinecraftServer server) {
        this(MultiplexRecipeManager.get(server).orElseThrow(() -> new IllegalStateException("No MultiplexRecipeManager Found")));
    }

    public MultiplexRecipeReloadMessage(MultiplexRecipeManager manager) {
        this(manager.getRecipes());
    }

    public MultiplexRecipeReloadMessage(Collection<IMultiplexRecipe<?, ?>> recipes) {
        this.recipes = Lists.newArrayList(recipes);
    }

    public static MultiplexRecipeReloadMessage decode(PacketBuffer buffer) {
        int recipeCount = buffer.readVarInt();
        List<IMultiplexRecipe<?, ?>> recipes = Lists.newArrayList();
        for (int i = 0; i < recipeCount; i++) {
            recipes.add(decodeRecipe(buffer));
        }
        return new MultiplexRecipeReloadMessage(recipes);
    }

    public void encode(PacketBuffer buffer) {
        buffer.writeVarInt(recipes.size());
        for (IMultiplexRecipe<?, ?> recipe : recipes) {
            encodeRecipe(buffer, recipe);
        }
    }

    public <R extends IMultiplexRecipe<?, ?>> void encodeRecipe(PacketBuffer buffer, R recipe) {
        IMultiplexRecipeSerializer<R> serializer = (IMultiplexRecipeSerializer<R>) recipe.getSerializer();
        buffer.writeResourceLocation(serializer.getRegistryName());
        buffer.writeResourceLocation(recipe.getId());
        serializer.write(buffer, recipe);
    }

    private static IMultiplexRecipe<?, ?> decodeRecipe(PacketBuffer buffer) {
        ResourceLocation serializeId = buffer.readResourceLocation();
        ResourceLocation id = buffer.readResourceLocation();
//        System.out.println("Reading Serializer:" + serializeId);
//        System.out.println("Reading  Recipe:" + id);
        return Registries.get(Registries.MULTIPLEX_RECIPE_SERIALIZERS, serializeId)
                .map(serializer -> (serializer.read(id, buffer)))
                .orElseThrow(() -> new IllegalStateException("Recipe:" + id + " was not able to be deserialized by: " + serializeId));
    }

    public void handle(Supplier<NetworkEvent.Context> context) {
        context.get().enqueueWork(() -> MultiplexRecipeManager.CLIENT_INSTANCE.reloadRecipes(recipes));
        context.get().setPacketHandled(true);
    }

}
