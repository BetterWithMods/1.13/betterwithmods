/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.container;

import com.betterwithmods.core.References;
import com.betterwithmods.core.api.base.container.ISlotRange;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIntArray;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.SlotItemHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;

public abstract class InventoryContainer extends BaseContainer {

    private List<SlotTransformation> transformations = Lists.newArrayList();
    private Map<ResourceLocation, ISlotRange> slotRanges = Maps.newHashMap();

    protected IIntArray data;

    @Nullable
    protected IItemHandlerModifiable inventory;

    public InventoryContainer(@Nullable ContainerType<?> type, int windowId, PlayerEntity player, ItemStackHandler inventory, @Nullable IIntArray data) {
        super(type, windowId, player);
        this.inventory = inventory;
        this.data = data;
        if (this.data != null) {
            trackIntArray(this.data);
        }

    }

    private static final int SLOT_WIDTH = 18;

    protected void addSlots(ResourceLocation rangeName, IItemHandler inventory, int slots, int rows, int startIndex, int startX, int startY) {
        addSlots(rangeName, inventory, slots, rows, startIndex, startX, startY, SlotItemHandler::new);
    }

    protected void addSlots(ResourceLocation rangeName, IInventory inventory, int slots, int rows, int startIndex, int startX, int startY, SlotSupplier<IInventory> supplier) {
        addSlotSupplier(rangeName, inventory, slots, rows, startIndex, startX, startY, supplier);
    }

    protected void addSlots(ResourceLocation rangeName, IItemHandler inventory, int slots, int rows, int startIndex, int startX, int startY, SlotSupplier<IItemHandler> supplier) {
        addSlotSupplier(rangeName, inventory, slots, rows, startIndex, startX, startY, supplier);
    }

    protected <T> void addSlotSupplier(ResourceLocation rangeName, @Nullable T inventory, int slots, int rows, int startIndex, int startX, int startY, SlotSupplier<T> supplier) {
        if (inventory != null) {
            int start = inventorySlots.size();
            int columns = slots / rows;
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    int index = j + i * columns + startIndex;
                    int x = startX + j * SLOT_WIDTH;
                    int y = startY + i * SLOT_WIDTH;
                    this.addSlot(supplier.create(inventory, index, x, y));
                }
            }
            addSlotRange(rangeName, start, start + slots);
        }
    }


    protected void addPlayerInventory(int startIndex, int startX, int startY, int hotbarOffsetY) {
        PlayerInventory inventory = player.inventory;
        int start = inventorySlots.size();
        addSlots(PLAYER_HOTBAR, inventory, 9, 1, 0, startX, (startY + hotbarOffsetY), Slot::new);
        addSlots(PLAYER_INVENTORY, inventory, 27, 3, 9, startX, startY, Slot::new);
        addSlotRange(PLAYER, start, start + 36);
    }

    protected void addSlotRange(ResourceLocation id, SlotRange range) {
        slotRanges.put(id, range);
    }

    protected void addSlotRange(ResourceLocation id, int min, int max) {
        addSlotRange(id, new SlotRange(min, max));
    }

    @Override
    public boolean mergeItemStack(ItemStack stack, int startIndex, int endIndex, boolean reverseDirection) {
        return super.mergeItemStack(stack, startIndex, endIndex, reverseDirection);
    }

    @Override
    public boolean canInteractWith(PlayerEntity playerIn) {
        return true;
    }

    @Nonnull
    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
        if (transformations.isEmpty())
            return ItemStack.EMPTY;
        ItemStack stack = ItemStack.EMPTY;
        Slot slot = inventorySlots.get(index);
        if (slot != null && slot.getHasStack()) {
            ItemStack slotStack = slot.getStack();
            stack = slotStack.copy();
            for (SlotTransformation transformation : transformations) {
                ItemStack result = transformation.transferStackRange(this, slotStack, index);
                if (result.isEmpty())
                    return result;
            }
            if (slotStack.isEmpty())
                slot.putStack(ItemStack.EMPTY);
            else {
                slot.onSlotChanged();
            }
            slot.onTake(player, slotStack);
        }
        return stack;
    }

    public ISlotRange getRange(ResourceLocation range) {
        return slotRanges.get(range);
    }

    public void addSlotTransformation(SlotTransformation transformation) {
        transformations.add(transformation);
    }

    public void onChanged(IItemHandler handler) {}

    public static final ResourceLocation CONTAINER = new ResourceLocation(References.MODID_CORE, "container");
    public static final ResourceLocation PLAYER = new ResourceLocation(References.MODID_CORE, "player");
    public static final ResourceLocation PLAYER_HOTBAR = new ResourceLocation(References.MODID_CORE, "player_hotbar");
    public static final ResourceLocation PLAYER_INVENTORY = new ResourceLocation(References.MODID_CORE, "player_inventory");
}
