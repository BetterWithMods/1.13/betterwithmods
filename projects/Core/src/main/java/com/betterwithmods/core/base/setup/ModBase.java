/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.setup;

import com.betterwithmods.core.base.setup.proxy.IProxy;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLLoadCompleteEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

import java.util.function.Supplier;

public abstract class ModBase extends ModuleLoader {

    private final String modid, name;

    protected IProxy proxy;

    public IEventBus bus() {
        return FMLJavaModLoadingContext.get().getModEventBus();
    }

    public ModBase(String modid, String name) {
        super(modid);
        this.name = name;
        this.modid = modid;

        bus().addListener(this::onConfigure);
        bus().addListener(this::onCommonSetup);
        bus().addListener(this::onClientSetup);
        bus().addListener(this::onLoadComplete);
        bus().addListener(this::onServerStarting);
        bus().addListener(this::onGatherData);
    }

    @Override
    public void onCommonSetup(FMLCommonSetupEvent event) {
        getLogger().info("common setup");
        super.onCommonSetup(event);
    }

    @Override
    public void onClientSetup(FMLClientSetupEvent event) {
        getLogger().info("client setup");
        super.onClientSetup(event);
    }

    @Override
    public void onLoadComplete(FMLLoadCompleteEvent event) {
        getLogger().info("load complete");
        super.onLoadComplete(event);
    }

    @Override
    public void onServerStarting(FMLServerStartingEvent event) {
        super.onServerStarting(event);
    }

    private Supplier<Supplier<IProxy>> create(Supplier<Supplier<IProxy>> wrap) {
        return () -> () -> {
            IProxy p = wrap.get().get();
            p.setLogger(getLogger());
            return p;
        };
    }

    public void addProxy(Supplier<Supplier<IProxy>> client, Supplier<Supplier<IProxy>> server) {
        this.proxy = DistExecutor.runForDist(create(client), create(server));
        addSetup(proxy);
    }

}
