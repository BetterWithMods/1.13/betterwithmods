/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.api.crafting.result;

import com.betterwithmods.core.api.IPositionedText;
import com.google.common.collect.Lists;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nullable;
import java.util.List;

public interface IResult<T> {

    Class<T> getType();

    <S> T get(@Nullable S input);

    default boolean equals(IResult result) {
        return false;
    }

    default IResultSerializer<?> getSerializer() {
        return null;
    }

    default IResult copy() {
        return null;
    }

    @OnlyIn(Dist.CLIENT)
    default List<IPositionedText> displayCount() {
        return Lists.newArrayList();
    }

    default List<ITextComponent> tooltip() {
        return Lists.newArrayList();
    }

    default boolean isDynamic() {
        return false;
    }

    default ResourceLocation getId() {
        return new ResourceLocation("");
    }

    ItemStack displayStack();

    ITextComponent displayText();

    IResultRenderer<T> getRenderer();
}
