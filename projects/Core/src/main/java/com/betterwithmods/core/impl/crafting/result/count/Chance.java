/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.impl.crafting.result.count;

import com.betterwithmods.core.Registrars;
import com.betterwithmods.core.api.IPositionedText;
import com.betterwithmods.core.api.ISerializer;
import com.betterwithmods.core.api.crafting.result.count.ICount;
import com.betterwithmods.core.api.crafting.result.count.ICountSerializer;
import com.betterwithmods.core.impl.PositionedText;
import com.betterwithmods.core.utilties.DeserializeUtils;
import com.betterwithmods.core.utilties.Rand;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.registries.ForgeRegistryEntry;

import javax.annotation.Nonnull;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

public class Chance implements ICount {

    protected final double chance;
    @Nonnull
    private final ICount count;

    public Chance(double chance, ICount count) {
        this.chance = MathHelper.clamp(chance, 0, 100);
        this.count = count;
    }


    @Override
    public Integer get() {
        if (Rand.chance(chance)) {
            return count.get();
        }
        return 0;
    }


    @Nonnull
    public ICount getParent() {
        return count;
    }

    @Override
    public ICountSerializer<?> getSerializer() {
        return Registrars.CountSerializers.CHANCE;
    }

    public static class ChanceSerializer extends ForgeRegistryEntry<ICountSerializer<?>> implements ICountSerializer<Chance>, ISerializer<Chance> {

        private static final Digit FALLBACK = new Digit(1);

        @Override
        public Chance read(JsonElement json) {
            float chance = JSONUtils.getFloat(json.getAsJsonObject(), "chance");
            ICount count = DeserializeUtils.readCount(json);
            if (count == null) {
                count = FALLBACK;
            }
            return new Chance(chance, count);
        }

        @Override
        public Chance read(PacketBuffer buffer) {
            double chance = buffer.readDouble();
            ICount count = ICount.read(buffer);
            return new Chance(chance, count);
        }

        @Override
        public void write(PacketBuffer buffer, Chance count) {
            buffer.writeDouble(count.chance);
            ICount parent = count.getParent();
            ICount.write(buffer, parent);
        }

        @Override
        public <V extends Chance> void write(V chance, JsonElement jsonElement) {
            JsonObject json = jsonElement.getAsJsonObject();
            json.addProperty("chance", chance.chance);
            JsonObject jsonCount = new JsonObject();
            DeserializeUtils.writeCount(chance, jsonCount);
            json.add("count", jsonCount);
        }
    }

    @Override
    public boolean equals(ICount count) {
        if (count instanceof Chance) {
            Chance c = ((Chance) count);
            return c.getParent().equals(this.getParent()) && c.chance == this.chance;
        }
        return false;
    }

    private static final NumberFormat FORMAT = new DecimalFormat("##.###%");

    @OnlyIn(Dist.CLIENT)
    @Override
    public List<IPositionedText> getText() {
        List<IPositionedText> list = getParent().getText();
        list.add(new PositionedText(new StringTextComponent(FORMAT.format(chance)), 0, 0, 0.8));
        return list;
    }
}
