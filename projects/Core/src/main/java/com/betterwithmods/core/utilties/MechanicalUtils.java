/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties;

import com.betterwithmods.core.api.BWMApi;
import com.betterwithmods.core.api.mech.IMechanicalPower;
import com.betterwithmods.core.api.mech.IMechanicalUtils;
import com.betterwithmods.core.base.game.tile.MechanicalPowerTile;
import com.betterwithmods.core.impl.CapabilityMechanicalPower;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.common.util.LazyOptional;

import javax.annotation.Nonnull;
import java.util.function.BiConsumer;

public class MechanicalUtils implements IMechanicalUtils {

    @Nonnull
    @Override
    public LazyOptional<IMechanicalPower> getMechanicalPower(@Nonnull IWorld world, @Nonnull BlockPos pos, @Nonnull Direction facing) {
        TileEntity tile = world.getTileEntity(pos);
        if (tile != null) {
            return tile.getCapability(CapabilityMechanicalPower.MECHANICAL_POWER, facing);
        }
        return LazyOptional.empty();
    }

    @Override
    public void forEachAdjacent(@Nonnull IWorld world, @Nonnull BlockPos pos, Iterable<Direction> faces, @Nonnull BiConsumer<Direction, IMechanicalPower> onEach) {
        for (Direction facing : faces) {
            BlockPos offset = pos.offset(facing);
            Direction outputFace = facing.getOpposite();
            LazyOptional<IMechanicalPower> optionalMech = MechanicalUtils.get(world, offset, outputFace);
            if (optionalMech.isPresent()) {
                IMechanicalPower mech = optionalMech.orElse(null);
                onEach.accept(outputFace, mech);
            }
        }
    }

    public static LazyOptional<IMechanicalPower> get(IWorld world, BlockPos pos, Direction facing) {
        return BWMApi.MECHANICAL_UTILS.getMechanicalPower(world,pos,facing);
    }

    public static void forEach(@Nonnull IWorld world, @Nonnull BlockPos pos, Iterable<Direction> faces, @Nonnull BiConsumer<Direction, IMechanicalPower> onEach) {
        BWMApi.MECHANICAL_UTILS.forEachAdjacent(world,pos,faces,onEach);
    }

    public static void debug(World world, BlockPos pos, PlayerEntity player, Hand hand) {

        TileEntity tile = world.getTileEntity(pos);
        if (hand == Hand.MAIN_HAND && !world.isRemote && tile instanceof MechanicalPowerTile) {
            player.sendStatusMessage(new StringTextComponent(((MechanicalPowerTile) tile).power.toString()), false);
        }
    }
}
