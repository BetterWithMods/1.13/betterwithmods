/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.ingredient;

import com.betterwithmods.core.Core;
import com.betterwithmods.core.References;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.IIngredientSerializer;

import javax.annotation.Nullable;
import java.util.stream.Stream;

public class AnyIngredient extends Ingredient {
    public static final AnyIngredient ANYTHING = new AnyIngredient();

    protected AnyIngredient() {
        super(Stream.empty());
    }

    public static class Serializer implements IIngredientSerializer<AnyIngredient> {
        public static ResourceLocation NAME = new ResourceLocation(References.MODID_CORE, "any_ingredient");

        @Override
        public AnyIngredient parse(PacketBuffer buffer) {
            return new AnyIngredient();
        }

        @Override
        public AnyIngredient parse(JsonObject json) {
            return new AnyIngredient();
        }

        @Override
        public void write(PacketBuffer buffer, AnyIngredient ingredient) {
        }


    }

    @Override
    public JsonElement serialize() {
        JsonObject jsonobject = new JsonObject();
        jsonobject.addProperty("type", AnyIngredient.Serializer.NAME.toString());
        return jsonobject;
    }

    @Override
    public boolean test(@Nullable ItemStack stack) {
        return true;
    }

    @Override
    public IIngredientSerializer<? extends Ingredient> getSerializer() {
        return Core.ANY_INGREDIENT;
    }
}
