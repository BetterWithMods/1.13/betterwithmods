/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.loottables.conditions;

import com.betterwithmods.core.References;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.loot.LootContext;
import net.minecraft.world.storage.loot.LootParameters;
import net.minecraft.world.storage.loot.conditions.ILootCondition;
import net.minecraftforge.common.ToolType;

public class MatchToolType implements ILootCondition {
    private final ToolType toolType;

    public MatchToolType(ToolType toolType) {
        this.toolType = toolType;
    }

    @Override
    public boolean test(LootContext lootContext) {
        ItemStack stack = lootContext.get(LootParameters.TOOL);
        return stack != null && stack.getToolTypes().contains(toolType);
    }

    public static IBuilder builder(ToolType type) {
        return () -> new MatchToolType(type);
    }

    public static class Serializer extends AbstractSerializer<MatchToolType> {
        public Serializer() {
            super(new ResourceLocation(References.MODID_CORE, "match_tooltype"), MatchToolType.class);
        }

        @Override
        public void serialize(JsonObject jsonObject, MatchToolType matchToolType, JsonSerializationContext jsonSerializationContext) {
            jsonObject.addProperty("tooltype", matchToolType.toolType.getName());
        }

        @Override
        public MatchToolType deserialize(JsonObject jsonObject, JsonDeserializationContext jsonDeserializationContext) {
            ToolType toolType = ToolType.get(jsonObject.get("tooltype").getAsString());
            return new MatchToolType(toolType);
        }
    }
}
