/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.impl;

import com.betterwithmods.core.api.mech.IPower;
import net.minecraft.nbt.CompoundNBT;

import javax.annotation.Nonnull;

public class Power implements IPower {

    public static final Power ZERO = new Power(0,0);
    public static final Power ONE_TORQUE = new Power(1,0);


    private static final String TORQUE = "torque", DISTANCE = "distance";


    private int torque, distance;

    public Power(IPower torque) {
        this(torque.getTorque(), torque.getDistance());
    }

    public Power() {
        this(0,0);
    }

    public Power(int torque, int distance) {
        this.torque =  torque;
        this.distance = distance;
    }

    @Override
    public int getTorque() {
        return torque;
    }

    @Override
    public int getDistance() {
        return distance;
    }

    @Nonnull
    public Power setTorque(int torque) {
        return new Power(torque, distance);
    }
    @Nonnull
    public Power setDistance(int distance) {
        return new Power(torque, distance);
    }

    @Override
    public IPower clone() {
        return new Power(getTorque(), getDistance());
    }

    @Override
    public String toString() {
        return String.format("%d %d", torque, distance);
    }

    @Override
    public Power incrementDistance(int amount) {
        return new Power(getTorque(), this.getDistance() + amount);
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Power) {
            return ((Power) o).getTorque() == getTorque() && ((Power) o).getDistance() == getDistance();
        }
        return false;
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        tag.putInt(TORQUE, torque);
        tag.putInt(DISTANCE, distance);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        torque = nbt.getInt(TORQUE);
        distance = nbt.getInt(DISTANCE);
    }
}

