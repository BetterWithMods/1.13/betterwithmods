/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.ingredient;

import com.betterwithmods.core.Core;
import com.betterwithmods.core.References;
import com.betterwithmods.core.relationary.Relation;
import com.betterwithmods.core.relationary.RelationaryManager;
import com.betterwithmods.core.relationary.Shape;
import com.betterwithmods.core.utilties.DeserializeUtils;
import com.google.common.collect.Lists;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.IIngredientSerializer;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.stream.Stream;

public class ShapeIngredient extends Ingredient {

    private Ingredient parent;
    private Shape fromShape, toShape;

    private List<ItemStack> list = Lists.newArrayList();
    private ItemStack[] matchingStacks;

    public ShapeIngredient(IItemProvider provider, Shape fromShape, Shape toShape) {
        this(Ingredient.fromItems(provider), fromShape, toShape);
    }

    public ShapeIngredient(Ingredient parent, Shape fromShape, Shape toShape) {
        super(Stream.of());
        this.parent = parent;
        this.fromShape = fromShape;
        this.toShape = toShape;
    }

    @Override
    public boolean test(@Nullable ItemStack input) {
        if(input != null && !input.isEmpty()) {
            determineMatchingStacks();
            for(ItemStack stack: matchingStacks) {
                //TODO extract comparison
                if(stack.getItem() == input.getItem() && stack.getDamage() == input.getDamage() && stack.areShareTagsEqual(input)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void determineMatchingStacks() {
        if (list.isEmpty()) {
            for (ItemStack stack : parent.getMatchingStacks()) {
                Relation relation = RelationaryManager.getOrCreateRelation(stack.getItem().getRegistryName());
                if (RelationaryManager.isShape(relation, stack, fromShape)) {
                    list.addAll(relation.getStacks(toShape));
                }
            }
            matchingStacks = new ItemStack[list.size()];
            list.toArray(matchingStacks);
        }
    }
    @Override
    public ItemStack[] getMatchingStacks() {
        determineMatchingStacks();
        return matchingStacks;
    }

    public static class Serializer implements IIngredientSerializer<ShapeIngredient> {
        public static ResourceLocation NAME = new ResourceLocation(References.MODID_CORE, "shape_ingredient");

        @Override
        public ShapeIngredient parse(PacketBuffer buffer) {
            Ingredient parent = Ingredient.read(buffer);
            Shape fromShape = RelationaryManager.getOrCreateShape(buffer.readResourceLocation());
            Shape toShape = RelationaryManager.getOrCreateShape(buffer.readResourceLocation());
            return new ShapeIngredient(parent, fromShape, toShape);
        }

        @Override
        public ShapeIngredient parse(JsonObject json) {
            Ingredient parent = Ingredient.deserialize(json.get("parent"));
            Shape fromShape = RelationaryManager.getOrCreateShape(DeserializeUtils.getRL(json, "fromShape"));
            Shape toShape = RelationaryManager.getOrCreateShape(DeserializeUtils.getRL(json, "toShape"));
            return new ShapeIngredient(parent, fromShape, toShape);
        }

        @Override
        public void write(PacketBuffer buffer, ShapeIngredient ingredient) {
            ingredient.parent.write(buffer);
            buffer.writeResourceLocation(ingredient.fromShape.getRegistryName());
            buffer.writeResourceLocation(ingredient.toShape.getRegistryName());
        }
    }

    @Nonnull
    @Override
    public IIngredientSerializer<? extends Ingredient> getSerializer() {
        return Core.SHAPE_INGREDIENT;
    }

    @Override
    public JsonElement serialize() {
        JsonObject jsonobject = new JsonObject();
        jsonobject.addProperty("type", ShapeIngredient.Serializer.NAME.toString());
        jsonobject.add("parent", this.parent.serialize());
        jsonobject.addProperty("fromShape", this.fromShape.getRegistryName().toString());
        jsonobject.addProperty("toShape", this.toShape.getRegistryName().toString());
        return jsonobject;
    }
}
