/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core;

import com.betterwithmods.core.api.crafting.input.IInputSerializer;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipeSerializer;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipeType;
import com.betterwithmods.core.api.crafting.output.IOutputSerializer;
import com.betterwithmods.core.api.crafting.result.IResultSerializer;
import com.betterwithmods.core.api.crafting.result.count.ICountSerializer;
import com.betterwithmods.core.base.config.CoreClient;
import com.betterwithmods.core.base.config.CoreCommon;
import com.betterwithmods.core.base.game.events.PlayerLoggingInEvent;
import com.betterwithmods.core.base.game.ingredient.*;
import com.betterwithmods.core.base.game.loottables.RelationaryLootEntry;
import com.betterwithmods.core.base.game.loottables.conditions.ConstantCondition;
import com.betterwithmods.core.base.game.loottables.conditions.MatchToolType;
import com.betterwithmods.core.base.game.loottables.conditions.ModuleLootCondition;
import com.betterwithmods.core.base.setup.ModBase;
import com.betterwithmods.core.base.setup.ModConfiguration;
import com.betterwithmods.core.generator.CoreDataProviders;
import com.betterwithmods.core.impl.CapabilityMechanicalPower;
import com.betterwithmods.core.impl.CapabilitySoulHolder;
import com.betterwithmods.core.impl.crafting.multiplex.MultiplexRecipeManager;
import com.betterwithmods.core.modules.FixStructureBlocks;
import com.betterwithmods.core.modules.TileTickHandler;
import com.betterwithmods.core.network.MultiplexRecipeReloadMessage;
import com.betterwithmods.core.network.NetworkHandler;
import com.betterwithmods.core.network.RelationaryReloadMessage;
import com.betterwithmods.core.proxy.ClientProxy;
import com.betterwithmods.core.proxy.ServerProxy;
import com.betterwithmods.core.relationary.RelationaryManager;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.resources.IReloadableResourceManager;
import net.minecraft.resources.IResourceManagerReloadListener;
import net.minecraft.resources.SimpleReloadableResourceManager;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.loot.LootEntryManager;
import net.minecraft.world.storage.loot.conditions.LootConditionManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.common.crafting.IIngredientSerializer;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.server.FMLServerAboutToStartEvent;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.registries.IForgeRegistryEntry;
import net.minecraftforge.registries.RegistryBuilder;
import org.apache.logging.log4j.Logger;

@Mod(References.MODID_CORE)
public class Core extends ModBase {

    public static IIngredientSerializer<ToolIngredient> TOOL_INGREDIENT = CraftingHelper.register(ToolIngredient.Serializer.NAME, new ToolIngredient.Serializer());
    public static IIngredientSerializer<StackIngredient> STACK_INGREDIENT = CraftingHelper.register(StackIngredient.Serializer.NAME, new StackIngredient.Serializer());
    public static IIngredientSerializer<RelationIngredient> RELATION_INGREDIENT = CraftingHelper.register(RelationIngredient.Serializer.NAME, new RelationIngredient.Serializer());
    public static IIngredientSerializer<ShapeIngredient> SHAPE_INGREDIENT = CraftingHelper.register(ShapeIngredient.Serializer.NAME, new ShapeIngredient.Serializer());
    public static IIngredientSerializer<AnyIngredient> ANY_INGREDIENT = CraftingHelper.register(AnyIngredient.Serializer.NAME, new AnyIngredient.Serializer());
    public static IIngredientSerializer<StackSizeIngredient> STACKSIZE_INGREDIENT = CraftingHelper.register(StackSizeIngredient.Serializer.NAME, new StackSizeIngredient.Serializer());

    static {
        LootConditionManager.registerCondition(new MatchToolType.Serializer());
        LootConditionManager.registerCondition(new ModuleLootCondition.Serializer());
        LootConditionManager.registerCondition(new ConstantCondition.BooleanSerializer());
        LootEntryManager.func_216194_a(new RelationaryLootEntry.Serializer());
    }

    public static Logger LOGGER;

    public Core() {
        super(References.MODID_CORE, References.NAME_CORE);
        LOGGER = getLogger();
        bus().addListener(this::newRegistries);
        bus().addListener(this::onBlockColors);
        new Registrars(References.MODID_CORE, getLogger());
        addSetup(new NetworkHandler());
        addSetup(new CoreDataProviders());
        addProxy(() -> ClientProxy::new, () -> ServerProxy::new);
        addModule(new TileTickHandler());
        addModule(new FixStructureBlocks());
        MinecraftForge.EVENT_BUS.register(this);
        new ModConfiguration<>(CoreClient::new, CoreCommon::new);
    }

    @Override
    public void onCommonSetup(FMLCommonSetupEvent event) {
        super.onCommonSetup(event);
        CapabilityMechanicalPower.register();
        CapabilitySoulHolder.register();
    }


    public static void add(IReloadableResourceManager manager, int i, IResourceManagerReloadListener listener) {
        if (manager instanceof SimpleReloadableResourceManager) {
            SimpleReloadableResourceManager m = (SimpleReloadableResourceManager) manager;
            m.reloadListeners.add(i, listener);
            m.initTaskQueue.add(i, listener);
        } else {
            throw new IllegalStateException("Not a SimpleReloadableResourceManager");
        }

    }

    @SubscribeEvent
    public void serverAboutToStart(FMLServerAboutToStartEvent event) {
        MinecraftServer server = event.getServer();
        add(server.getResourceManager(), 1, new RelationaryManager(server, getLogger()));
        server.getResourceManager().addReloadListener(MultiplexRecipeManager.get(server).orElseThrow(() -> new IllegalStateException("What how is this null?")));
    }

    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void onPlayerLogin(PlayerEvent.PlayerLoggedInEvent event) {
        ServerPlayerEntity playerIn = (ServerPlayerEntity) event.getPlayer();
        RelationaryReloadMessage reloadMessage = new RelationaryReloadMessage();
        NetworkHandler.CHANNEL.send(PacketDistributor.PLAYER.with(() -> playerIn), reloadMessage);
        playerIn.getRecipeBook().init(playerIn);
    }

    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void onPlayerLoggingIn(PlayerLoggingInEvent event) {
        ServerPlayerEntity playerIn = (ServerPlayerEntity) event.getPlayer();
        MultiplexRecipeReloadMessage message = new MultiplexRecipeReloadMessage();
        NetworkHandler.CHANNEL.send(PacketDistributor.PLAYER.with(() -> playerIn), message);
    }


    @Override
    public void newRegistries(RegistryEvent.NewRegistry event) {
        makeRegistry(new ResourceLocation(References.MODID_CORE, "multiplex_recipe_serializers"), IMultiplexRecipeSerializer.class).create();
        makeRegistry(new ResourceLocation(References.MODID_CORE, "multiplex_recipe_types"), IMultiplexRecipeType.class).create();
        makeRegistry(new ResourceLocation(References.MODID_CORE, "result_serializers"), IResultSerializer.class).create();
        makeRegistry(new ResourceLocation(References.MODID_CORE, "input_serializers"), IInputSerializer.class).create();
        makeRegistry(new ResourceLocation(References.MODID_CORE, "count_serializers"), ICountSerializer.class).create();
        makeRegistry(new ResourceLocation(References.MODID_CORE, "output_serializers"), IOutputSerializer.class).create();
    }

    private static <T extends IForgeRegistryEntry<T>> RegistryBuilder<T> makeRegistry(ResourceLocation name, Class<T> type) {
        return new RegistryBuilder<T>().setName(name).setType(type).setMaxID(Integer.MAX_VALUE - 1).disableSaving().allowModification();
    }

}

