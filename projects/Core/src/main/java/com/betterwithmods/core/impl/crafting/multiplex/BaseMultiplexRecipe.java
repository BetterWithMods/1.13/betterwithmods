/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.impl.crafting.multiplex;

import com.betterwithmods.core.api.crafting.input.IInput;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipe;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipeType;
import com.betterwithmods.core.api.crafting.output.IOutput;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nonnull;

public abstract class BaseMultiplexRecipe<T, R extends BaseMultiplexRecipe<T, R>> implements IMultiplexRecipe<T, R> {

    private IInput<T, R> inputs;
    protected IOutput outputs;
    private ResourceLocation id;
    private IMultiplexRecipeType<?, ?> type;

    public BaseMultiplexRecipe(IMultiplexRecipeType<?, ?> type, ResourceLocation id, IInput<T, R> inputs, IOutput outputs) {
        this.type = type;
        this.inputs = inputs;
        this.outputs = outputs;
        this.id = id;
    }

    @Override
    public IInput<T, R> getInput() {
        return inputs;
    }

    @Override
    public ResourceLocation getId() {
        return id;
    }

    @Override
    public IMultiplexRecipeType<?, ?> getType() {
        return type;
    }

    @Nonnull
    @Override
    public IOutput getOutput() {
        return outputs;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof IMultiplexRecipe) {
            return ((IMultiplexRecipe) o).getId().equals(this.getId());
        }
        return super.equals(o);
    }


}
