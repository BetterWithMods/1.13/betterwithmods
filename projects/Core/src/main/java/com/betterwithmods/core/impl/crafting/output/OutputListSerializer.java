/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.impl.crafting.output;

import com.betterwithmods.core.utilties.DeserializeUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.network.PacketBuffer;

public class OutputListSerializer extends BaseOutputSerializer<OutputList> {

    @Override
    public OutputList read(JsonElement json) {
        JsonObject object = json.getAsJsonObject();
        JsonArray results = object.getAsJsonArray("results");
        return new OutputList(DeserializeUtils.getResults(results));
    }

    @Override
    public OutputList read(PacketBuffer buffer) {
        return new OutputList(DeserializeUtils.readResults(buffer));
    }

    @Override
    public void write(PacketBuffer buffer, OutputList list) {
        DeserializeUtils.writeResults(buffer, list.getAllResults());
    }

    @Override
    public <V extends OutputList> void write(V list, JsonElement jsonElement) {
        JsonObject o = jsonElement.getAsJsonObject();
        DeserializeUtils.writeResults(list.getAllResults(), o);
    }
}
