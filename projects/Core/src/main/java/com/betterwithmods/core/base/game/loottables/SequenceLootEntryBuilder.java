/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.loottables;

import net.minecraft.world.storage.loot.LootEntry;
import net.minecraft.world.storage.loot.SequenceLootEntry;
import net.minecraft.world.storage.loot.conditions.ILootCondition;

public class SequenceLootEntryBuilder extends LootEntry.Builder<SequenceLootEntryBuilder> {
    private final LootEntry[] entries;
    private final ILootCondition[] conditions;

    public SequenceLootEntryBuilder(LootEntry.Builder<?>[] builders, ILootCondition.IBuilder[] conditions) {
        this.entries = new LootEntry[builders.length];
        for (int i = 0; i < builders.length; i++) {
            LootEntry.Builder<?> builder = builders[i];
            entries[i] = builder.build();
        }

        this.conditions = new ILootCondition[conditions.length];
        for (int i = 0; i < conditions.length; i++) {
            ILootCondition.IBuilder builder = conditions[i];
            this.conditions[i] = builder.build();
        }

    }


    @Override
    protected SequenceLootEntryBuilder func_212845_d_() {
        return this;
    }

    @Override
    public LootEntry build() {
        return new SequenceLootEntry(entries, conditions);
    }
}
