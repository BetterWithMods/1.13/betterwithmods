/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.impl.crafting.result;

import com.betterwithmods.core.Relationships;
import com.betterwithmods.core.api.crafting.result.count.ICount;
import com.betterwithmods.core.relationary.Relation;
import com.betterwithmods.core.relationary.Shape;
import com.betterwithmods.core.utilties.DeserializeUtils;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nonnull;

public class RelationshipResultSerializer extends BaseResultSerializer<RelationshipResult> {

    private static final Logger LOGGER = LogManager.getLogger();

    @Override
    public RelationshipResult read(JsonElement json) {
        ICount count = DeserializeUtils.readCount(json);

        if (count != null) {
            JsonObject object = json.getAsJsonObject();
            Relation relation = Relationships.relation(new ResourceLocation(JSONUtils.getString(object, "relation", "")));
            Shape fromShape = Relationships.shape(new ResourceLocation(JSONUtils.getString(object, "fromShape", "")));
            Shape toShape = Relationships.shape(new ResourceLocation(JSONUtils.getString(object, "toShape", "")));
            return new RelationshipResult(relation, fromShape, toShape, count);
        }
        throw new JsonParseException("Recipe Result Error");
    }

    @Nonnull
    @Override
    public RelationshipResult read(PacketBuffer buffer) {
        ResourceLocation relationId = buffer.readResourceLocation();
        ResourceLocation fromShapeId = buffer.readResourceLocation();
        ResourceLocation toShapeId = buffer.readResourceLocation();
        LOGGER.debug("{} - {}", relationId, fromShapeId);
        Relation relation = Relationships.relation(relationId);
        Shape fromShape = Relationships.shape(fromShapeId);
        Shape toShape = Relationships.shape(toShapeId);
        ICount count = ICount.read(buffer);

        return new RelationshipResult(relation, fromShape, toShape, count);
    }

    @Override
    public void write(PacketBuffer buffer, RelationshipResult relationshipResult) {
        buffer.writeResourceLocation(relationshipResult.getRelation().getRegistryName());
        buffer.writeResourceLocation(relationshipResult.getFromShape().getRegistryName());
        buffer.writeResourceLocation(relationshipResult.getToShape().getRegistryName());
        ICount.write(buffer, relationshipResult.getCount());
    }

    @Override
    public <V extends RelationshipResult> void write(V result, JsonElement jsonElement) {
        JsonObject object = jsonElement.getAsJsonObject();
        object.addProperty("relation", result.getRelation().getRegistryName().toString());
        object.addProperty("fromShape", result.getFromShape().getRegistryName().toString());
        object.addProperty("toShape", result.getToShape().getRegistryName().toString());
        JsonObject count = new JsonObject();
        DeserializeUtils.writeCount(result.getCount(), count);
        object.add("count", count);
    }
}
