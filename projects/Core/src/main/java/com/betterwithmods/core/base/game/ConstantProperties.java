/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game;

import com.betterwithmods.core.base.game.item.ItemProperties;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.item.Item;

public class ConstantProperties {

    public static final Item.Properties BASE = new Item.Properties();

    public static final ItemProperties NETHERCOAL = new ItemProperties().burnTime(3200);

    public static final Item.Properties COMBAT = new Item.Properties();

    public static final Block.Properties PLANT_PROPERTIES = Block.Properties.create(Material.PLANTS).doesNotBlockMovement().tickRandomly().hardnessAndResistance(0.0F).sound(SoundType.PLANT);

    public static final Block.Properties LAMP = Block.Properties.create(Material.REDSTONE_LIGHT).lightValue(15).hardnessAndResistance(0.3F).sound(SoundType.GLASS);

    public static final Block.Properties REDSTONE_DEVICE = Block.Properties.create(Material.ROCK).hardnessAndResistance(0.3f).sound(SoundType.STONE);

    public static final Block.Properties CAULDRON = Block.Properties.create(Material.ROCK).hardnessAndResistance(0.3f).sound(SoundType.METAL);

    public static final Block.Properties CRUCIBLE = Block.Properties.create(Material.ROCK).hardnessAndResistance(0.3f).sound(SoundType.GLASS);

    public static final Block.Properties PLANTER = Block.Properties.create(Material.ROCK, MaterialColor.ADOBE).hardnessAndResistance(1.25F, 4.2F);

    public static final Block.Properties DIRT = Block.Properties.create(Material.EARTH, MaterialColor.DIRT).hardnessAndResistance(0.5F).sound(SoundType.GROUND);

    public static final Block.Properties GRASS = Block.Properties.create(Material.ORGANIC).hardnessAndResistance(0.5F).sound(SoundType.PLANT);

    public static final Block.Properties MINING_CHARGE = Block.Properties.create(Material.TNT).hardnessAndResistance(0.0F).sound(SoundType.PLANT);

    public static final Block.Properties ROPE = Block.Properties.create(Material.WOOL).hardnessAndResistance(0.0F).sound(SoundType.PLANT);

    public static final Block.Properties ANCHOR = Block.Properties.create(Material.ROCK).hardnessAndResistance(0.3F).sound(SoundType.STONE);

    public static final Block.Properties WOOD = Block.Properties.create(Material.WOOD, MaterialColor.WOOD).hardnessAndResistance(2.0F, 3.0F).sound(SoundType.WOOD);

    public static final Block.Properties ROCK = Block.Properties.create(Material.ROCK).hardnessAndResistance(1.5F, 6.0F);

    public static final Block.Properties WOOL = Block.Properties.create(Material.WOOL, MaterialColor.SNOW).hardnessAndResistance(0.8F).sound(SoundType.CLOTH);

}
