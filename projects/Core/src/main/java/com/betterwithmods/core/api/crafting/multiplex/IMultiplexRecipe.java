/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.api.crafting.multiplex;

import com.betterwithmods.core.api.crafting.input.IInput;
import com.betterwithmods.core.api.crafting.input.IRecipeContext;
import com.betterwithmods.core.api.crafting.output.IOutput;
import com.google.common.collect.Lists;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

import javax.annotation.Nonnull;
import java.util.Collection;

public interface IMultiplexRecipe<T, R extends IMultiplexRecipe<T, R>> extends Comparable<R> {

    @Nonnull
    IInput<T, R> getInput();

    @Nonnull
    IOutput getOutput();

    @Nonnull
    IMultiplexRecipeSerializer<?> getSerializer();

    @Nonnull
    ResourceLocation getId();

    @Nonnull
    IMultiplexRecipeType<?, ?> getType();

    @SuppressWarnings("unchecked")
    default boolean matches(IWorld world, BlockPos pos, IRecipeContext context) {
        return getInput().matches(world, pos, self(), context);
    }

    default void craft(World world, BlockPos pos, IRecipeContext<T, R> context) {
        if (context.canCraft(self(), world, pos)) {
            onCraft(world, pos, context);
            consume(world, pos, context);
        }
    }

    default void consume(World world, BlockPos pos, IRecipeContext context) {
        getInput().consume(world, pos, self(), context);
        context.onCrafted();
    }

    void onCraft(World world, BlockPos pos, IRecipeContext<T, R> context);

    @Override
    default int compareTo(R other) {
        return getInput().compareTo(other.getInput());
    }

    @Nonnull
    default NonNullList<Ingredient> getInputs() {
        return getInput().getIngredients();
    }

    @Nonnull
    default Collection<R> getDisplayRecipes() { return Lists.newArrayList(self()); }

    default R self() {
        return (R) this;
    }

}
