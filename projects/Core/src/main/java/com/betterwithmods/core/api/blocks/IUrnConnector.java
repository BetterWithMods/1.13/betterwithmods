package com.betterwithmods.core.api.blocks;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldReader;

public interface IUrnConnector {
    boolean canConnect(IWorldReader worldReader, BlockPos pos);
}
