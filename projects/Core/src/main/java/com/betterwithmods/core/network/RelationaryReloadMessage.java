/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.network;

import com.betterwithmods.core.relationary.Container;
import com.betterwithmods.core.relationary.Relation;
import com.betterwithmods.core.relationary.RelationaryManager;
import com.betterwithmods.core.relationary.Shape;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import org.apache.logging.log4j.LogManager;

import java.util.Collection;
import java.util.function.Supplier;

public class RelationaryReloadMessage {

    private Container<Shape> shapes;
    private Container<Relation> relations;

    public RelationaryReloadMessage() {
        this(RelationaryManager.get(ServerLifecycleHooks.getCurrentServer()));
    }

    public RelationaryReloadMessage(RelationaryManager manager) {
        this(manager.getShapeContainer(), manager.getRelationContainer());
    }

    public RelationaryReloadMessage(Container<Shape> shapes, Container<Relation> relations) {
        this.shapes = shapes;
        this.relations = relations;
    }

    public static RelationaryReloadMessage decode(PacketBuffer buffer) {
        int shapesCount = buffer.readInt();
        Container<Shape> shapes = new Container<>(Shape::getRegistryName);
        for (int i = 0; i < shapesCount; i++) {
            shapes.add(Shape.read(buffer));
        }
        Container<Relation> relations = new Container<>(Relation::getRegistryName);
        int relationCount = buffer.readInt();
        for (int i = 0; i < relationCount; i++) {
            relations.add(Relation.read(buffer));
        }
        LogManager.getLogger().info("Received {} shapes and {} relations", shapesCount, relationCount);
        return new RelationaryReloadMessage(shapes, relations);
    }

    public void encode(PacketBuffer buffer) {
        Collection<Shape> shapes = this.shapes.values();
        buffer.writeInt(shapes.size());
        for (Shape shape : shapes) {
            shape.write(buffer);
        }

        Collection<Relation> relations = this.relations.values();
        buffer.writeInt(relations.size());
        for (Relation relation : relations) {
            relation.write(buffer);
        }
        LogManager.getLogger().info("Sending {} shapes and {} relations", shapes.size(), relations.size());
    }


    public void handle(Supplier<NetworkEvent.Context> context) {
        context.get().enqueueWork(() -> {
            RelationaryManager.setShapes(shapes);
            RelationaryManager.setRelations(relations);
            LogManager.getLogger().info("Successfully Loaded {} shapes and {} relations", shapes.values().size(), relations.values().size());
        });
        context.get().setPacketHandled(true);
    }

}
