/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.tile;

import com.betterwithmods.core.api.BWMApi;
import com.betterwithmods.core.api.mech.IMechanicalPower;
import com.betterwithmods.core.api.mech.IPower;
import com.betterwithmods.core.impl.Power;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Set;

public class GeneratorTile extends MechanicalPowerTile implements IMechanicalPower, ITickableTileEntity {


    public Power powerSource = null;

    public GeneratorTile(TileEntityType<?> tileEntityTypeIn) {
        super(tileEntityTypeIn);
    }

    @Nonnull
    @Override
    public Iterable<Direction> getInputs() {
        return Lists.newArrayList();
    }

    @Nonnull
    @Override
    public Iterable<Direction> getOutputs() {
        return Lists.newArrayList();
    }

    @Override
    protected void calculate() {
        if(this.powerSource != null) {
            this.power = powerSource;

            Set<BlockPos> toNotify = Sets.newHashSet();

            for (Direction facing : getOutputs()) {
                BlockPos offset = pos.offset(facing);
                IMechanicalPower mech = BWMApi.MECHANICAL_UTILS.getMechanicalPower(world,offset,facing.getOpposite()).orElse(null);
                if(mech != null && mech.canInputFrom(facing.getOpposite())) {
                    toNotify.add(offset);
                }
            }

            for (BlockPos notify : toNotify) {
                world.neighborChanged(notify, getBlockState().getBlock(), pos);
            }
        }
    }

    public void postCalculate() { }

    @Nullable
    @Override
    public IPower getOutput(@Nonnull IWorld world, @Nonnull BlockPos pos, @Nonnull Direction facing) {
        return power;
    }

    @Nullable
    @Override
    public IPower getMaximumInput(@Nonnull Direction facing) {
        return null;
    }

    @Nullable
    @Override
    public IPower getMinimumInput() {
        return null;
    }

    @Override
    public boolean overpower(World world, BlockPos pos) { return false; }

    @Override
    public void tick() {
        calculatePowerSource();

        if(powerSource != null) {
            if(!power.equals(this.powerSource)) {
                onChanged();
            }
        }
    }


    public void calculatePowerSource() { }

    @Override
    public boolean canInputFrom(Direction facing) {
        return false;
    }
}
