/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties.inventory;

import com.betterwithmods.core.base.game.ingredient.IStackIngredient;
import com.betterwithmods.core.utilties.StackEjector;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class InventoryUtils {

    public static int getStackCount(Collection<Ingredient> ingredients, ItemStack stack, int defaultCount) {
        return ingredients.stream().filter(i -> i.test(stack)).findFirst().map(i -> getStackCount(i, stack, defaultCount)).orElse(0);
    }

    public static int getStackCount(Ingredient ingredient, ItemStack stack, int defaultCount) {
        return ingredient instanceof IStackIngredient ? ((IStackIngredient) ingredient).getCount(stack) : defaultCount;
    }

    public static boolean consumeFromInventory(IItemHandler handler, Ingredient ingredient, boolean extract, NonNullList<ItemStack> containers) {
        final Function<ItemStack, Integer> count = stack -> getStackCount(ingredient, stack, 1);
        Optional<ItemStack> stack = findInInventory(handler, ingredient, count, extract);
        stack.map(ForgeHooks::getContainerItem).filter(s -> !s.isEmpty()).ifPresent(containers::add);
        return stack.map(s -> s.getCount() >= count.apply(s)).get();
    }

    public static Optional<ItemStack> findInInventory(IItemHandler handler, Predicate<ItemStack> filter, boolean extract) {
        return findInInventory(handler, filter, s -> 64, extract);
    }

    public static Optional<ItemStack> findInInventory(IItemHandler handler, Predicate<ItemStack> filter, Function<ItemStack, Integer> count, boolean extract) {
        return findSlot(handler, slot -> filter.test(slot.getStack())).map(slot -> {
            if (extract) {
                return slot.extract(count.apply(slot.getStack()));
            }
            return slot.getStack();
        });
    }

    public static Optional<ItemSlot> findSlot(@Nonnull IItemHandler handler, Predicate<ItemSlot> filter) {
        return stream(handler).filter(s -> !s.isEmpty()).filter(filter).findFirst();
    }

    public static Iterable<ItemSlot> iterable(Iterator<ItemSlot> iterator) {
        return new ItemHandlerIterable(iterator);
    }

    public static Iterable<ItemSlot> iterable(@Nonnull IItemHandler handler) {
        return new ItemHandlerIterable(handler);
    }


    public static Iterable<ItemSlot> iterable(@Nonnull IItemHandler handler, int start) {
        return new ItemHandlerIterable(new ItemHandlerIterator(handler, start));
    }

    public static Iterator<ItemSlot> iterator(@Nonnull IItemHandler handler) {
        return new ItemHandlerIterator(handler);
    }

    public static Iterator<ItemSlot> iterator(@Nonnull IItemHandler handler, int start) {
        return new ItemHandlerIterator(handler, start);
    }

    public static Stream<ItemSlot> stream(IItemHandler handler) {
        return StreamSupport.stream(iterable(handler).spliterator(), false);
    }

    public static IItemHandlerModifiable getInventory(IWorld world, BlockPos pos) {
        TileEntity tile = world.getTileEntity(pos);
        if (tile != null) {
            IItemHandler handler = getInventory(tile, null);
            if (handler instanceof IItemHandlerModifiable) {
                return (IItemHandlerModifiable) handler;
            }
        }
        return null;
    }

    public static IItemHandler getInventory(ICapabilityProvider provider, Direction dir) {
        return provider.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, dir).orElse(null);
    }

    public static boolean isChanged(ItemStack input, ItemStack result) {
        return !input.isItemEqual(result) && input.getCount() != result.getCount();
    }

    public static void addToInventoryStackedOrDrop(IItemHandler handler, NonNullList<ItemStack> stacks, World world, BlockPos pos, StackEjector ejector) {
        for (ItemStack stack : stacks)
            addToInventoryStackedOrDrop(handler, stack, world, pos, ejector);
    }

    public static void addToInventoryStackedOrDrop(IItemHandler handler, ItemStack stack, World world, BlockPos pos, StackEjector ejector) {
        if (handler == null || stack.isEmpty())
            return;

        if (!stack.isStackable()) {
            addToInventoryOrDrop(handler, stack, world, pos, ejector);
        }

        for (ItemSlot slot : iterable(handler)) {
            if (slot.canStack(stack)) {
                ItemStack returned = handler.insertItem(slot.getSlot(), stack, false);
                if (returned.isEmpty()) {
                    return;
                }
            }
        }

        if (!stack.isEmpty()) {
            for (ItemSlot slot : iterable(handler)) {
                if (slot.isEmpty()) {
                    ItemStack returned = handler.insertItem(slot.getSlot(), stack, false);
                    if (returned.isEmpty()) {
                        return;
                    }
                }
            }
        }

        if (!stack.isEmpty()) {
            ejector.setStack(stack).ejectStack(world, pos, BlockPos.ZERO);
        }
    }

    public static void addToInventoryOrDrop(IItemHandler handler, NonNullList<ItemStack> stacks, World world, BlockPos pos, StackEjector ejector) {
        for (ItemStack s : stacks) {
            addToInventoryOrDrop(handler, s, world, pos, ejector);
        }
    }

    public static void addToInventoryOrDrop(IItemHandler handler, ItemStack stack, World world, BlockPos pos, StackEjector ejector) {
        addToInventory(handler, stack, returned -> {
            if (!returned.isEmpty()) ejector.setStack(returned.copy()).ejectStack(world, pos, BlockPos.ZERO);
        });
    }

    public static ItemStack addToInventory(IItemHandler handler, ItemStack stack) {
        for (ItemSlot slot : iterable(handler)) {
            ItemStack returned = handler.insertItem(slot.getSlot(), stack, false);
            if (returned.isEmpty()) {
                return returned;
            }
        }
        return stack;
    }

    public static ItemStack addToInventory(IItemHandler handler, ItemStack stack, Consumer<ItemStack> ifFull) {
        for (ItemSlot slot : iterable(handler)) {
            ItemStack returned = handler.insertItem(slot.getSlot(), stack, false);
            if (returned.isEmpty()) {
                return returned;
            }
        }
        ifFull.accept(stack);
        return stack;
    }

    public static boolean isFull(IItemHandler handler) {
        return stream(handler).allMatch(ItemSlot::isFull);
    }

    public static boolean isEmpty(IItemHandler handler) {
        return stream(handler).allMatch(ItemSlot::isEmpty);
    }

    public static boolean addEntityItem(IItemHandler inventory, ItemEntity entityItem) {
        ItemStack stack = entityItem.getItem().copy();
        return addEntityItem(inventory, entityItem, stack);
    }

    public static boolean addEntityItem(IItemHandler inventory, ItemEntity entityItem, ItemStack stack) {

        if (!InventoryUtils.isFull(inventory)) {
            ItemStack returned = InventoryUtils.addToInventory(inventory, stack);
            if (returned.isEmpty())
                entityItem.remove();
            else
                entityItem.setItem(returned);
            return true;
        }
        return false;
    }


    public static void drop(IItemHandler handler, World world, BlockPos pos, StackEjector ejector) {
        for (ItemSlot slot : iterable(handler)) {
            ejector.setStack(slot.getStack()).ejectStack(world, pos, BlockPos.ZERO);
        }
    }
}
