/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.arcanium.content.items;

import com.betterwithmods.arcanium.Registrars;
import com.betterwithmods.core.base.game.item.BaseItem;
import com.betterwithmods.core.base.game.item.ItemProperties;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Rarity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistries;

import javax.annotation.Nullable;
import java.util.List;

public class ArcaneScrollItem extends BaseItem {


    public ArcaneScrollItem(ItemProperties properties) {
        super(properties);

    }

    public static ItemStack withEnchantment(Enchantment enchantment) {
        ItemStack stack = new ItemStack(Registrars.Items.ARCANE_SCROLL);
        CompoundNBT enchantTag = new CompoundNBT();
        enchantTag.putString("enchant", enchantment.getRegistryName().toString());
        stack.setTag(enchantTag);
        return stack;
    }

    public static Enchantment getEnchantment(ItemStack scroll) {
        ResourceLocation enchantmentName = new ResourceLocation(scroll.getTag().getString("enchant"));
        return ForgeRegistries.ENCHANTMENTS.getValue(enchantmentName);
    }


    @Override
    public void fillItemGroup(ItemGroup group, NonNullList<ItemStack> items) {
        if(group == Registrars.ITEM_GROUP) {
            ForgeRegistries.ENCHANTMENTS.forEach(enchantment -> items.add(ArcaneScrollItem.withEnchantment(enchantment)));

        }
    }

    @Override
    public boolean hasEffect(ItemStack stack) {
        return true;
    }

    /**
     * Return an item rarity from EnumRarity
     */
    @Override
    public Rarity getRarity(ItemStack stack) {
        return Rarity.UNCOMMON;
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World world, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        Enchantment enchantment = ArcaneScrollItem.getEnchantment(stack);

        if (enchantment != null) {
            tooltip.add(new TranslationTextComponent(enchantment.getName()));
        }
    }


}
