/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.arcanium.content.void_portal;

import com.betterwithmods.core.base.game.block.BlockBase;
import com.betterwithmods.core.utilties.EnvironmentUtils;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

import javax.annotation.Nonnull;

public class VoidProxyBlock extends BlockBase {
    public VoidProxyBlock(Properties properties) {
        super(properties);
        setRenderLayer(BlockRenderLayer.CUTOUT);
    }

    @SuppressWarnings("deprecation")
    @Nonnull
    @Override
    public VoxelShape getShape(BlockState state, IBlockReader world, BlockPos pos, ISelectionContext context) {
        return VoxelShapes.empty();
    }


    public static boolean canProxy(World world, BlockPos pos, BlockState originalState) {
        //Ignore all tiles entities, might come back to this later.
        if (world.getTileEntity(pos) == null) {
            if (!originalState.isAir(world, pos)) {
                //Ignore blocks with properties for now, basically only normal solids
                return true;
            }
        }
        return false;
    }

    public static boolean setProxyState(Block proxy, World world, BlockPos pos, int time) {
        BlockState originalState = world.getBlockState(pos);
        if (canProxy(world, pos, originalState)) {
            world.setBlockState(pos, proxy.getDefaultState());
            TileEntity tile = world.getTileEntity(pos);
            if (tile instanceof VoidProxyTile) {
                VoidProxyTile voidTile = ((VoidProxyTile) tile);
                voidTile.setParentState(originalState);
                voidTile.setTime(time);
                EnvironmentUtils.forEachParticle(world, pos, ParticleTypes.PORTAL, 10, EnvironmentUtils.RANDOM_PARTICLE, EnvironmentUtils.SPEED);
                return true;
            }
        }
        return false;
    }

    @Override
    public BlockRenderType getRenderType(BlockState state) {
        return BlockRenderType.INVISIBLE;
    }
}
