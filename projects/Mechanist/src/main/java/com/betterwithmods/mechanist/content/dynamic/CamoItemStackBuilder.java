/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.dynamic;

import com.betterwithmods.core.api.crafting.result.count.ICount;
import com.betterwithmods.core.impl.crafting.ItemStackBuilder;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.util.IItemProvider;

public class CamoItemStackBuilder extends ItemStackBuilder {

    private BlockState parent;

    public static CamoItemStackBuilder builder() {
        return new CamoItemStackBuilder();
    }


    @Override
    public CamoItemStackBuilder item(IItemProvider item) {
        return (CamoItemStackBuilder) super.item(item);
    }

    @Override
    public CamoItemStackBuilder count(ICount countProvider) {
        return (CamoItemStackBuilder) super.count(countProvider);
    }

    public CamoItemStackBuilder parent(BlockState parent) {
        this.parent = parent;
        return (CamoItemStackBuilder) this.tag(createTag());
    }

    private CompoundNBT createTag() {
        CompoundNBT tag = new CompoundNBT();
        CompoundNBT blockEntityTag = new CompoundNBT();
        blockEntityTag.put("parent", NBTUtil.writeBlockState(parent));
        tag.put("BlockEntityTag", blockEntityTag);
        return tag;
    }

    public BlockState getParent() {
        return parent;
    }
}
