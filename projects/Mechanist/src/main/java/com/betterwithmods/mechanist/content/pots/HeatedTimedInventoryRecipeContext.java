/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.pots;

import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipe;
import com.betterwithmods.core.base.game.recipes.multiplex.ITimedRecipe;
import com.betterwithmods.core.base.game.recipes.multiplex.inventory.TimedInventoryRecipeContext;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.items.IItemHandlerModifiable;

import java.util.concurrent.atomic.AtomicInteger;

public class HeatedTimedInventoryRecipeContext<R extends IMultiplexRecipe<IItemHandlerModifiable, R> & ITimedRecipe & IHeatedRecipe> extends TimedInventoryRecipeContext<R> {
    private AtomicInteger heatLevel;

    public HeatedTimedInventoryRecipeContext(AtomicInteger recipeTime, AtomicInteger heatLevel) {
        super(recipeTime);
        this.heatLevel = heatLevel;
    }

    @Override
    public boolean canCraft(R recipe, World world, BlockPos pos) {
        return this.heatLevel.get() >= recipe.getRecipeHeat() && super.canCraft(recipe, world, pos);
    }
}
