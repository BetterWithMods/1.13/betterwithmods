/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.saw;

import com.betterwithmods.core.api.mech.IPower;
import com.betterwithmods.core.base.game.tile.MechanicalPowerTile;
import com.betterwithmods.core.impl.crafting.multiplex.MultiplexRecipeManager;
import com.betterwithmods.core.utilties.DirectionUtils;
import com.betterwithmods.core.utilties.EnvironmentUtils;
import com.betterwithmods.core.utilties.Rand;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.function.IntSupplier;

public class SawTile extends MechanicalPowerTile implements ITickableTileEntity {

    private static final int PARTICLE_EVENT_OFF = 100, PARTICLE_EVENT_ON = 101;

    private static final IntSupplier MAX_DELAY = () -> 10 + Rand.between(0, 6);
    private int delay;

    public SawTile() {
        super(Registrars.Tiles.SAW);
    }

    @Override
    public void tick() {
        if (world != null) {
            if (power.getTorque() > 0) {
                if (delay <= 0) {
                    BlockPos offset = pos.offset(getFacing());
                    MultiplexRecipeManager.get(world.getServer()).ifPresent(m -> m.getRecipe(Registrars.MultiplexRecipeTypes.SAW, world, offset, IWorld::getBlockState).ifPresent(r -> r.craft(world, offset, IWorld::getBlockState)));
                    delay = MAX_DELAY.getAsInt();
                }
                delay--;
            }
        }
    }

    @Override
    public boolean receiveClientEvent(int id, int type) {
        if (id == PARTICLE_EVENT_OFF) {
            return true;
        } else if (id == PARTICLE_EVENT_ON) {
            return true;
        }
        return false;
    }

    public Direction getFacing() {
        return getBlockState().get(SawBlock.FACING);
    }

    @Nonnull
    @Override
    public Iterable<Direction> getInputs() {
        return DirectionUtils.excluding(getFacing());
    }

    @Nonnull
    @Override
    public Iterable<Direction> getOutputs() {
        return Collections.emptyList();
    }



    @Override
    public void postCalculate() {
        BlockState state = world.getBlockState(pos);
        if (state.getBlock() instanceof SawBlock) {
            boolean powered = power.getTorque() > 0;
            world.setBlockState(pos, state.with(SawBlock.POWERED, powered));

            boolean nowPowered = power.getTorque() > previousPower.getTorque();
            boolean nowUnpowered = power.getTorque() < previousPower.getTorque();

            if(nowPowered)
                EnvironmentUtils.playSound(world, pos, Registrars.Sounds.SAW_CUT, SoundCategory.BLOCKS, SawBlock.VOLUME.build(), SawBlock.PITCH_ON.build());
            if(nowUnpowered)
                EnvironmentUtils.playSound(world, pos, Registrars.Sounds.SAW_CUT, SoundCategory.BLOCKS, SawBlock.VOLUME.build(), SawBlock.PITCH_OFF.build());
        }
    }

    @Nullable
    @Override
    public IPower getOutput(@Nonnull IWorld world, @Nonnull BlockPos pos, @Nonnull Direction facing) {
        return null;
    }

    @Override
    public boolean overpower(World world, BlockPos pos) {
        return false;
    }

    private static final String DELAY = "delay";

    @Nonnull
    @Override
    public CompoundNBT write(CompoundNBT compound) {
        compound.putInt(DELAY, delay);
        return super.write(compound);
    }

    @Override
    public void read(CompoundNBT compound) {
        if (compound.contains(DELAY)) {
            delay = compound.getInt(DELAY);
        }
        super.read(compound);
    }


}
