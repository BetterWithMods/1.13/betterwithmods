/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.dynamic.block.furniture;

import com.betterwithmods.mechanist.content.dynamic.block.DynamicWaterloggableBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;

import javax.annotation.Nullable;

public class TableBlock extends DynamicWaterloggableBlock {

    private static BooleanProperty TABLE_LEG = BooleanProperty.create("table_leg");

    public TableBlock(Properties properties) {
        super(properties);
        setDefaultState(getDefaultState().with(TABLE_LEG, true));

        setRenderLayer(BlockRenderLayer.CUTOUT);
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(TABLE_LEG);
    }

    @Override
    public boolean canBeConnectedTo(BlockState state, IBlockReader world, BlockPos pos, Direction facing) {
        return world.getBlockState(pos.offset(facing)).getBlock() instanceof TableBlock;
    }

    private boolean isConnected(BlockState state, IWorld world, BlockPos pos, Direction facing) {
        Direction opp = facing.getOpposite();
        BlockState state1 = world.getBlockState(pos.offset(facing));
        BlockState state2 = world.getBlockState(pos.offset(opp));
        return state1.getBlock().canBeConnectedTo(state, world, pos.offset(facing), opp) && state2.getBlock().canBeConnectedTo(state, world, pos.offset(opp), facing);
    }


    private boolean isConnected(BlockState state, IWorld world, BlockPos pos) {
        return isConnected(state, world, pos, Direction.NORTH) || isConnected(state, world, pos, Direction.WEST);
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        BlockState state = context.getWorld().getBlockState(context.getPos());
        return getDefaultState()
                .with(WATERLOGGED, shouldWaterlog(context))
                .with(TABLE_LEG, !isConnected(state, context.getWorld(), context.getPos()));
    }

    @Override
    public BlockState updatePostPlacement(BlockState stateIn, Direction facing, BlockState facingState, IWorld world, BlockPos currentPos, BlockPos facingPos) {
        BlockState superState = super.updatePostPlacement(stateIn, facing, facingState, world, currentPos, facingPos);
        return superState.with(TABLE_LEG, !isConnected(stateIn, world, currentPos));
    }

    private static VoxelShape TABLE_LEGS, TABLE;

    static {
        TABLE = Block.makeCuboidShape(0, 14, 0, 16, 16, 16);
        VoxelShape leg = Block.makeCuboidShape(6, 0, 6, 10, 14, 10);
        TABLE_LEGS = VoxelShapes.combineAndSimplify(TABLE, leg, IBooleanFunction.OR);
    }


    @Override
    public VoxelShape getShape(BlockState state, IBlockReader world, BlockPos pos, ISelectionContext context) {
        return state.get(TABLE_LEG) ? TABLE_LEGS :TABLE;
    }

//    @Override
//    public boolean onBlockActivated(IBlockState state, World world, BlockPos pos, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
//
//        TileEntity tiles = world.getTileEntity(pos);
//        if(tiles instanceof CamoflageTile) {
//            System.out.println(((CamoflageTile) tiles).getParentState());
//
//        }
//
//
//        return super.onBlockActivated(state, world, pos, player, hand, side, hitX, hitY, hitZ);
//    }
}
