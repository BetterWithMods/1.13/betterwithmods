/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.filtered_hopper.client;

import com.betterwithmods.core.References;
import com.betterwithmods.core.base.game.client.baking.ModelFactory;
import com.betterwithmods.core.base.game.client.baking.ModelInfo;
import com.betterwithmods.core.utilties.ItemUtils;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemOverrideList;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.data.ModelProperty;

import javax.annotation.Nonnull;
import java.util.Map;

public class FilteredHopperModelFactory extends ModelFactory<ItemStack> {

    private static final FactoryItemOverrideList<ItemStack> OVERRIDE_LIST = new FactoryItemOverrideList<>();

    public static final ModelProperty<ItemStack> FILTER = new ModelProperty<>();

    private IBakedModel sourceModel;

    public FilteredHopperModelFactory(IBakedModel sourceModel) {
        //TODO particles
        super(FILTER, new ResourceLocation("minecraft:block/stone"));
        this.sourceModel = sourceModel;
    }

    @Override
    public IBakedModel bake(@Nonnull ModelInfo<ItemStack> info) {
        return new FilteredHopperModel(sourceModel, info);
    }

    @Override
    public ModelInfo<ItemStack> fromItemStack(ItemStack stack) {
        BlockState state = ItemUtils.getBlock(stack).orElse(Blocks.AIR).getDefaultState();
        return new ModelInfo<>(state, d -> ItemStack.EMPTY, true);
    }

    public ItemOverrideList getOverrides() {
        return OVERRIDE_LIST;
    }

    private static final ResourceLocation FILTERED_HOPPER = new ResourceLocation(References.MODID_MECHANIST, "filtered_hopper");

    public static void registerModel(Map<ResourceLocation, IBakedModel> registry) {
        for(ResourceLocation model: registry.keySet()) {
            if(model.getNamespace().equals(FILTERED_HOPPER.getNamespace()) && model.getPath().equals(FILTERED_HOPPER.getPath())) {
                registry.put(model, new FilteredHopperModelFactory(registry.get(model)));
            }
        }
    }
}
