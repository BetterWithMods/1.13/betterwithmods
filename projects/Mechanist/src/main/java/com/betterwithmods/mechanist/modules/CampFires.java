/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.modules;

import com.betterwithmods.core.References;
import com.betterwithmods.core.base.game.ConstantIngredients;
import com.betterwithmods.core.base.setup.ModuleBase;
import com.betterwithmods.core.impl.tiles.TileTickEvent;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.CampfireBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.INBT;
import net.minecraft.tileentity.AbstractFurnaceTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class CampFires extends ModuleBase {
    public CampFires() {
        super("campfires");
    }

    @Override
    public void configs(ForgeConfigSpec.Builder builder) {

    }

    @Override
    public void commonSetup(FMLCommonSetupEvent event) {
        CampFire.registerCapability();
    }

    @Override
    public String getDescription() {
        return "Campfires now require fuels to stay lit.";
    }

    @SubscribeEvent
    public void onPlaced(BlockEvent.EntityPlaceEvent event) {
        BlockState placed = event.getPlacedBlock();
        if (placed.getBlock() == Blocks.CAMPFIRE && event.getBlockSnapshot().getReplacedBlock().getMaterial().isReplaceable()) {
            IWorld world = event.getWorld();
            BlockPos pos = event.getPos();
            world.setBlockState(pos, placed.with(CampfireBlock.LIT, false), 3);
        }
    }


    @SubscribeEvent
    public void onClickedItem(PlayerInteractEvent.RightClickItem event) {
        ItemStack stack = event.getItemStack();
        if (event.getWorld().isRemote) {
            if(stack.getItem() == Blocks.CAMPFIRE.asItem()) {
                event.setResult(Event.Result.DENY);
                event.setCanceled(true);
                event.getEntityPlayer().swingArm(event.getHand());
            }
        }
    }
    @SubscribeEvent
    public void onClicked(PlayerInteractEvent.RightClickBlock event) {
        ItemStack stack = event.getItemStack();
        World world = event.getWorld();
        BlockPos pos = event.getPos();
        TileEntity tile = world.getTileEntity(pos);
        if (tile != null && tile.getType() == TileEntityType.CAMPFIRE) {
            CampFire cap = tile.getCapability(CampFire.CAMPFIRE).orElse(null);
            if (cap != null) {
                if (ConstantIngredients.FIRESTARTER.test(stack)) {
                    cap.increment(100);
                } else {
                    int burntime = getBurnTime(stack);
                    if (burntime != -1) {
                        cap.increment(burntime);
                        stack.shrink(1);
                    }
                }
            }
        }



    }

    public static int getBurnTime(ItemStack stack) {
        int ret = stack.getBurnTime();
        return net.minecraftforge.event.ForgeEventFactory.getItemBurnTime(stack, ret == -1 ? AbstractFurnaceTileEntity.getBurnTimes().getOrDefault(stack.getItem(), 0) : ret);
    }


    @SubscribeEvent
    public void onTileTick(TileTickEvent.Pre event) {
        TileEntity tile = event.getTile();
        World world = tile.getWorld();
        if (tile.getType() == TileEntityType.CAMPFIRE) {


            CampFire cap = tile.getCapability(CampFire.CAMPFIRE).orElse(null);
            if (cap != null) {
                boolean lit = false;
                if (cap.getFuel() > 0) {
                    lit = true;
                }
                BlockState state = world.getBlockState(tile.getPos());
                if (state.has(CampfireBlock.LIT)) {
                    if (state.get(CampfireBlock.LIT) != lit) {
                        world.setBlockState(tile.getPos(), state.with(CampfireBlock.LIT, lit));
                    }
                }

                cap.decrement(1);
            }
        }
    }


    @SubscribeEvent
    public void attachCapability(AttachCapabilitiesEvent<TileEntity> event) {
        TileEntity tile = event.getObject();
        if (tile.getType() == TileEntityType.CAMPFIRE) {
            event.addCapability(CAPABILITY, new CampFire());
        }
    }

    public static final ResourceLocation CAPABILITY = new ResourceLocation(References.MODID_MECHANIST, "campfire");

    public static class CampFire implements ICapabilityProvider {
        private int fuel;

        public void increment(int add) {
            this.fuel += add;
        }

        public int getFuel() {
            return fuel;
        }

        public void decrement(int subtract) {
            if (this.fuel > 0)
                this.fuel -= subtract;

        }

        @CapabilityInject(CampFire.class)
        public static Capability<CampFire> CAMPFIRE = null;

        public static void registerCapability() {
            CapabilityManager.INSTANCE.register(CampFire.class, new Capability.IStorage<CampFire>() {

                @Nullable
                @Override
                public INBT writeNBT(Capability<CampFire> capability, CampFire instance, Direction side) {
                    return null;
                }

                @Override
                public void readNBT(Capability<CampFire> capability, CampFire instance, Direction side, INBT nbt) {

                }
            }, CampFire::new);
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            if (cap == CAMPFIRE) {
                return CAMPFIRE.orEmpty(cap, LazyOptional.of(() -> this));
            }
            return LazyOptional.empty();
        }
    }


}
