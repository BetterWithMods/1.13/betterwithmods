/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.mechanical.generators.tile;

import com.betterwithmods.core.Tags;
import com.betterwithmods.core.impl.Power;
import com.betterwithmods.core.utilties.WorldUtils;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.content.mechanical.generators.block.BaseAxleGeneratorBlock;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.IFluidState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.shapes.VoxelShape;

import javax.annotation.Nonnull;

public class WaterwheelTile extends BaseAxleGeneratorTile {

    public static final int FLUID_VECTOR_THRESHOLD = 3;

    private int waterDirection;

    public WaterwheelTile() {
        super(Registrars.Tiles.WATERWHEEL);
    }

    public boolean isValidFluid(Fluid fluid) {
        return Tags.Fluids.WATERWHEEL.contains(fluid);
    }


    @Override
    protected void validateGenerator() {
        super.validateGenerator();

        BaseAxleGeneratorBlock block = getBlock();
        VoxelShape shape = block.getAreaShape(getBlockState());
        if (!shape.isEmpty()) {
            Iterable<BlockPos> positions = WorldUtils.getAllBlocks(shape.getBoundingBox().offset(pos));

            Vec3d sum = new Vec3d(0, 0, 0);
            for (BlockPos position : positions) {
                //Ignore the axle
                if (!position.equals(pos)) {
                    IFluidState fluidState = world.getFluidState(position);
                    Fluid fluid = fluidState.getFluid();
                    if (isValidFluid(fluid)) {
                        Vec3d flow = fluid.getFlow(world, position, fluidState);
                        sum = sum.add(flow);
                        //                        System.out.println("flow:" + flow + "," + position);
                    }
                }
            }
//            System.out.println(sum + "," + normal);
            waterDirection = (int) Math.pow(Math.ceil(Math.max(sum.x, sum.z)), 0);
            valid &= (Math.abs(sum.x) > FLUID_VECTOR_THRESHOLD || Math.abs(sum.z) > FLUID_VECTOR_THRESHOLD);
        }
    }

    @Override
    public void calculatePowerSource() {
        int torque = 0;

        if (isValid()) {
            torque = 1;
        }
        this.powerSource = new Power(torque, 4);

        super.calculatePowerSource();
    }


    @Override
    public double calcPreviousRotation() {
        return waterDirection * super.calcPreviousRotation();
    }

    @Override
    public double calcRotationIncrement() {
        return waterDirection * super.calcRotationIncrement();
    }

    @Override
    public AxisAlignedBB getRenderBoundingBox() {
        return new AxisAlignedBB(pos, pos.add(1, 1, 1)).expand(2, 2, 2);
    }

    private static final String WATER_DIRECTION = "waterDirection";

    @Nonnull
    @Override
    public CompoundNBT write(CompoundNBT compound) {
        compound.putInt(WATER_DIRECTION, waterDirection);
        return super.write(compound);
    }

    @Override
    public void read(CompoundNBT compound) {
        if (compound.contains(WATER_DIRECTION))
            waterDirection = compound.getInt(WATER_DIRECTION);
        super.read(compound);
    }
}
