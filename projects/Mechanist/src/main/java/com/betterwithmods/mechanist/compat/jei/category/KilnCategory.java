/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.compat.jei.category;

import com.betterwithmods.core.References;
import com.betterwithmods.core.api.crafting.result.IResult;
import com.betterwithmods.core.compat.jei.CoreJEI;
import com.betterwithmods.core.compat.jei.IconDrawable;
import com.betterwithmods.core.compat.jei.category.AbstractBlockStateMultiplexCategory;
import com.betterwithmods.mechanist.compat.jei.MechanistJEI;
import com.betterwithmods.mechanist.content.kiln.KilnRecipe;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.ingredient.IGuiIngredientGroup;
import mezz.jei.api.gui.ingredient.IGuiItemStackGroup;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.block.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.stream.Collectors;

public class KilnCategory extends AbstractBlockStateMultiplexCategory<KilnRecipe> {


    public KilnCategory(IGuiHelper guiHelper, String type, int heat, ResourceLocation uid) {
        super(guiHelper,
                guiHelper.createDrawable(MechanistJEI.Constants.KILN_BACKGROUND, 0, 0, 129, 47),
                String.format("jei.%s.%s", References.MODID_MECHANIST, type),
                IconDrawable.builder(guiHelper, new ItemStack(Blocks.BRICKS), MechanistJEI.Constants.KILN_BACKGROUND, 130, (heat - 1) * 14, 14, 14)
                , uid);
    }

    @Nonnull
    @Override
    public Class<? extends KilnRecipe> getRecipeClass() {
        return KilnRecipe.class;
    }

    @Override
    public void setRecipe(@Nonnull IRecipeLayout layout, @Nonnull KilnRecipe recipe, @Nonnull IIngredients ingredients) {
        IGuiItemStackGroup guiItemStacks = layout.getItemStacks();
        IGuiIngredientGroup<IResult<ItemStack>> guiResults = layout.getIngredientsGroup(CoreJEI.STACK_RESULT);
        createSlot(guiItemStacks, true,  0, 15, 15);
        guiItemStacks.set(ingredients);
        createSlotsHorizontal(guiResults, false, 3, 3, 75, 16);
        guiResults.set(ingredients);

        List<List<IResult<ItemStack>>> resultList = ingredients.getOutputs(CoreJEI.STACK_RESULT);
        if (resultList.size() > 3) {
            List<List<IResult<ItemStack>>> subList = resultList.subList(2, resultList.size());
            List<IResult<ItemStack>> results = subList.stream().flatMap(List::stream).collect(Collectors.toList());
            guiResults.set(3, results);
        }

    }
}
