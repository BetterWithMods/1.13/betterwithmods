/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.urn;

import com.betterwithmods.core.api.blocks.IUrnConnector;
import com.betterwithmods.core.base.game.block.BlockBase;
import com.betterwithmods.core.utilties.VoxelUtils;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;

import javax.annotation.Nullable;

public class UrnBlock extends BlockBase {

    public static final DirectionProperty POSITION = DirectionProperty.create("position", Direction.UP, Direction.DOWN);

    public UrnBlock(Properties properties) {
        super(properties);
        setDefaultState(getDefaultState().with(POSITION, Direction.DOWN));
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(POSITION);
    }

    public static VoxelShape[] SHAPES =
            new VoxelShape[]{
                    VoxelUtils.or(
                            Block.makeCuboidShape(6, 0, 6, 10, 11, 10),
                            Block.makeCuboidShape(5, 1, 5, 11, 8, 11),
                            Block.makeCuboidShape(5, 9, 5, 11, 10, 11)
                    ),
                    VoxelUtils.or(
                            Block.makeCuboidShape(6, 5, 6, 10, 16, 10),
                            Block.makeCuboidShape(5, 14, 5, 11, 15, 11),
                            Block.makeCuboidShape(5, 6, 5, 11, 13, 11)
                    )
            };

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        Direction dir = state.get(POSITION);

        return SHAPES[dir.getIndex()];
    }


    private boolean canConnect(IWorld world, BlockPos pos) {
        BlockPos abovePos = pos.up();
        BlockState above = world.getBlockState(abovePos);
        Block block = above.getBlock();
        if (block instanceof IUrnConnector) {
            return ((IUrnConnector) block).canConnect(world, pos);
        }
        return false;
    }

    @Override
    public BlockState updatePostPlacement(BlockState stateIn, Direction facing, BlockState facingState, IWorld worldIn, BlockPos currentPos, BlockPos facingPos) {
        return stateIn.with(POSITION, canConnect(worldIn, currentPos) ? Direction.UP : Direction.DOWN);
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return getDefaultState().with(POSITION, canConnect(context.getWorld(), context.getPos()) ? Direction.UP : Direction.DOWN);
    }
}
