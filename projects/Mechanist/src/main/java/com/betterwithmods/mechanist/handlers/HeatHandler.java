/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.handlers;

import com.betterwithmods.core.Tags;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import org.apache.commons.lang3.tuple.Pair;

import java.util.concurrent.atomic.AtomicInteger;

public class HeatHandler {

    public static final Integer NORMAL_HEAT = 1, STOKED_HEAT = 2;

    public static Integer getHeat(IBlockReader world, BlockPos pos) {
        return getHeat(world.getBlockState(pos));
    }

    public static Integer getHeat(BlockState state) {
        if (Tags.Blocks.NORMAL_FIRE.contains(state.getBlock()))
            return NORMAL_HEAT;
        if (Tags.Blocks.STOKED_FIRE.contains(state.getBlock()))
            return STOKED_HEAT;
        return 0;
    }


    /**
     * @param world - world of the TileEntity
     * @param machine - position of the machine above the heat sources
     * @return Pair of Integers, left is the heat level of the block below the machine; right is the the total amount of the same level heat sources in the 3x3 below the machine.
     */
    public static Pair<Integer, Integer> calculateHeat(IBlockReader world, BlockPos machine) {
        BlockPos below = machine.down();
        int heat = HeatHandler.getHeat(world, below);
        int total = 0;
        if(heat > 0) {
            total = 1;
            for (int x = -1; x <= 1; x++) {
                for (int z = -1; z <= 1; z++) {
                    if(x != 0 && z != 0) {
                        BlockPos pos = new BlockPos(below).add(x, 0, z);
                        int i = HeatHandler.getHeat(world, pos);
                        if(i == heat) {
                            total++;
                        }
                    }
                }
            }
        }
        return Pair.of(heat, total);

    }

    private static final int HEAT_UPDATE_DELAY = 20;
    public static  void calculateHeatLevel(IWorld world, BlockPos pos, AtomicInteger calcHeatDelay, AtomicInteger heat, AtomicInteger speedMultiplier) {
        if (calcHeatDelay.get() <= 0) {
            Pair<Integer, Integer> value = calculateHeat(world, pos);
            heat.set(value.getLeft());
            speedMultiplier.set(value.getRight());
            calcHeatDelay.set(HEAT_UPDATE_DELAY);
        }
        calcHeatDelay.set(Math.max(0, calcHeatDelay.decrementAndGet()));
    }

}
