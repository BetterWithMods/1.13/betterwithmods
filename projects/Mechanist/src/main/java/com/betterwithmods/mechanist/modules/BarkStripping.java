/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.modules;

import com.betterwithmods.core.base.game.ConstantIngredients;
import com.betterwithmods.core.base.setup.ModuleBase;
import com.betterwithmods.core.utilties.StackEjector;
import com.betterwithmods.core.utilties.VectorBuilder;
import com.betterwithmods.mechanist.content.dynamic.block.BarkBlock;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class BarkStripping extends ModuleBase {

    public BarkStripping() {
        super("bark_stripping");
    }

    @Override
    public void configs(ForgeConfigSpec.Builder builder) {
    }

    private static VectorBuilder POS = new VectorBuilder().setGaussian(0.25).offset(0.5);
    private static final StackEjector EJECTOR = new StackEjector(POS, VectorBuilder.ZERO);

    @SubscribeEvent
    public void onBlockClicked(PlayerInteractEvent.RightClickBlock event) {
        ItemStack stack = event.getItemStack();
        if (ConstantIngredients.AXE.test(stack)) {
            World world = event.getWorld();
            BlockPos pos = event.getPos();
            BlockState state = world.getBlockState(pos);
            ItemStack wood = BarkBlock.fromParent(state);
            if (!wood.isEmpty()) {
                EJECTOR.setStack(wood).ejectStack(world, pos, BlockPos.ZERO);
            }
        }
    }

    @Override
    public String getDescription() {
        return "Drop Bark when stripping logs.";
    }
}
