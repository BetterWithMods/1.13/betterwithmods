/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.mechanical.joint;

import com.betterwithmods.core.api.mech.IPower;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.content.mechanical.MechanicalJointTile;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class ClutchboxTile extends MechanicalJointTile {
    public ClutchboxTile() {
        super(Registrars.Tiles.CLUTCHBOX);
    }

    @Nullable
    @Override
    public IPower getOutput(@Nonnull IWorld world, @Nonnull BlockPos pos, @Nonnull Direction facing) {
        if (isLit()) {
            return null;
        }
        return super.getOutput(world,pos,facing);
    }


    @Override
    public void breakBlock(World world, BlockPos pos) {
        BlockState state = world.getBlockState(pos);
        if (state.getBlock() instanceof ClutchboxBlock) {
            ClutchboxBrokenBlock broken = (ClutchboxBrokenBlock) Registrars.Blocks.CLUTCHBOX_BROKEN;
            BlockState brokenState = broken.getDefaultState().with(BaseJointBlock.FACING, state.get(BaseJointBlock.FACING));
            world.setBlockState(pos, broken.getConnections(brokenState, world, pos));
        }
    }

    @Override
    public void tick() {
        if(isLit()) {
            overpowerTime = -1;
            return;
        }
        super.tick();
    }

    private boolean isLit() {
        BlockState state = world.getBlockState(pos);
        Block block = state.getBlock();
        if (block instanceof ClutchboxBlock) {
            return state.get(ClutchboxBlock.LIT);
        }
        return false;
    }
}
