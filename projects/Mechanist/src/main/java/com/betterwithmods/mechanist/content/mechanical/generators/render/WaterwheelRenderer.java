/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.mechanical.generators.render;

import com.betterwithmods.core.References;
import com.betterwithmods.core.base.game.client.tile.GeometryTileRenderer;
import com.betterwithmods.mechanist.content.mechanical.generators.tile.WaterwheelTile;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;

public class WaterwheelRenderer extends GeometryTileRenderer<WaterwheelTile> {
    private static ResourceLocation WATERWHEEL = new ResourceLocation(References.MODID_MECHANIST, "waterwheel");

    private static ResourceLocation TEXTURE = new ResourceLocation(References.MODID_MECHANIST, "textures/entity/waterwheel/waterwheel.png");

    @Override
    public void renderModel(WaterwheelTile tile, double x, double y, double z, float partialTicks, int destroyStage) {
        Direction.Axis axis = tile.getAxis();
        GlStateManager.rotatef(axis == Direction.Axis.X ? 90 : 0, 0, 1, 0);
        bindTexture(TEXTURE);

        GlStateManager.color3f(1, 1, 1);
        double rotation = (tile.getCurrentRotation() + (partialTicks * tile.getPreviousRotation()));
        GlStateManager.rotatef(-(float) rotation, 0, 0, 1);
        get(WATERWHEEL).render(0.0625F);
    }
}
