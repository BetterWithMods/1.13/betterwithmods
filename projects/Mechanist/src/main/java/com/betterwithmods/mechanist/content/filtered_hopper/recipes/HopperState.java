/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.filtered_hopper.recipes;

import com.betterwithmods.core.api.souls.ISoulStorage;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandlerModifiable;

public final class HopperState {

    private ItemStack filter;
    private ItemEntity itemEntity;
    private IItemHandlerModifiable inventory;
    private ISoulStorage soulStorage;

    public HopperState(ItemStack filter, ItemEntity itemEntity, IItemHandlerModifiable inventory, ISoulStorage soulStorage) {
        this.filter = filter;
        this.itemEntity = itemEntity;
        this.inventory = inventory;
        this.soulStorage = soulStorage;
    }

    public ItemStack getFilter() {
        return filter;
    }

    public ItemEntity getItemEntity() {
        return itemEntity;
    }

    public IItemHandlerModifiable getInventory() {
        return inventory;
    }

    public ISoulStorage getSoulStorage() {
        return soulStorage;
    }
}
