/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.dynamic.block;

import com.betterwithmods.core.Tags;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.content.dynamic.CamoflageTile;
import com.betterwithmods.mechanist.content.dynamic.block.mini.SidingBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.state.EnumProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.Tag;
import net.minecraft.util.Direction;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;

public class BarkBlock extends SidingBlock {

    public static final EnumProperty<Direction.Axis> AXIS = BlockStateProperties.AXIS;

    public BarkBlock(Properties properties) {
        super(properties);
    }

    private static final VoxelShape[] SHAPES = new VoxelShape[]{
            Block.makeCuboidShape(0, 14, 0, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 16, 2, 16),
            Block.makeCuboidShape(0, 0, 14, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 16, 16, 2),
            Block.makeCuboidShape(14, 0, 0, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 2, 16, 16),
    };

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader world, BlockPos pos, ISelectionContext context) {
        int i = state.get(FACING).getIndex();
        return SHAPES[i];
    }

    private static final Tag<Block> BARK_LOGS = Tags.Blocks.BARK_LOGS;

    @Override
    public void fillItemGroup(ItemGroup group, NonNullList<ItemStack> items) {
        for (Block block : BARK_LOGS.getAllElements()) {
            ItemStack bark = CamoflageTile.fromParent(this, block.getDefaultState());
            items.add(bark);
        }
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(AXIS);
    }

    public static ItemStack fromParent(BlockState state, int count) {
        return fromParent(state.getBlock(), count);
    }

    public static ItemStack fromParent(BlockState state) {
        return fromParent(state, 1);
    }

    public static ItemStack fromParent(Block block) {
        return fromParent(block, 1);
    }

    public static ItemStack fromParent(Block block, int count) {
        if (BARK_LOGS.contains(block)) {
            return CamoflageTile.fromParent(Registrars.Blocks.BARK, block.getDefaultState(), count);
        }
        return ItemStack.EMPTY;
    }

}
