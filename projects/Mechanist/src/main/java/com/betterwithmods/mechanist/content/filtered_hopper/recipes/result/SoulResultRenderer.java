package com.betterwithmods.mechanist.content.filtered_hopper.recipes.result;

import com.betterwithmods.core.api.crafting.result.IResult;
import com.betterwithmods.core.api.crafting.result.IResultRenderer;
import com.betterwithmods.core.impl.souls.SoulStack;
import net.minecraft.client.gui.FontRenderer;

import javax.annotation.Nullable;

public class SoulResultRenderer implements IResultRenderer<SoulStack> {
    public static final SoulResultRenderer INSTANCE = new SoulResultRenderer();

    @Override
    public void render(int x, int y, @Nullable IResult<SoulStack> result, FontRenderer font) {

    }
}
