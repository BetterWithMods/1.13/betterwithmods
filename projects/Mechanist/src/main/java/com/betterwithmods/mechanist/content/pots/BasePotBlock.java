/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.pots;

import com.betterwithmods.core.base.game.BlockStateProperties;
import com.betterwithmods.core.base.game.block.BlockBase;
import com.betterwithmods.core.utilties.VoxelUtils;
import com.google.common.collect.Maps;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

import java.util.Map;

public class BasePotBlock extends BlockBase {
    private static final DirectionProperty FACING = BlockStateProperties.FACING_EXCEPT_DOWN;

    public BasePotBlock(Properties properties) {
        super(properties);
        setDefaultState(getDefaultState().with(FACING, Direction.UP));
    }

    private BlockState withFacing(BlockState state, Direction facing) {
        if (facing == Direction.DOWN)
            facing = Direction.UP;
        return state.with(FACING, facing);
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(FACING);
    }

    public void tilt(World world, BlockPos pos, Direction facing) {
        BlockState state = world.getBlockState(pos);
        world.setBlockState(pos, withFacing(state, facing));
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader world, BlockPos pos, ISelectionContext context) {
        Direction facing = state.get(FACING);
        return SHAPES.getOrDefault(facing, VoxelShapes.fullCube());
    }


    private static final Map<Direction, VoxelShape> SHAPES = Maps.newEnumMap(Direction.class);

    static {
        {
            //UP
            VoxelShape INSIDE = Block.makeCuboidShape(3.0D, 2.0D, 3.0D, 13.0D, 16.0D, 13.0D);
            VoxelShape WALLS_A = Block.makeCuboidShape(1.0D, 2, 1.0D, 15.0D, 16.0D, 15.0D);
            VoxelShape WALLS_B = Block.makeCuboidShape(0, 2, 0, 16, 14, 16);
            VoxelShape WALLS_BOTTOM = Block.makeCuboidShape(2.0D, 0, 2.0D, 14.0D, 2, 14.0D);
            VoxelShape WALLS = VoxelShapes.combineAndSimplify(VoxelUtils.or(WALLS_A, WALLS_B, WALLS_BOTTOM), INSIDE, IBooleanFunction.ONLY_FIRST);
            SHAPES.put(Direction.UP, WALLS);
        }
        {
            //SOUTH
            VoxelShape INSIDE = Block.makeCuboidShape(3.0D, 3.0, 2.0D, 13.0D, 13.0, 16.0D);
            VoxelShape WALLS_A = Block.makeCuboidShape(1.0D, 1, 2, 15.0D, 15D, 16D);
            VoxelShape WALLS_B = Block.makeCuboidShape(0, 0, 2, 16, 16, 14);
            VoxelShape WALLS_BOTTOM = Block.makeCuboidShape(2, 2, 0, 14.0D, 14, 2);
            VoxelShape WALLS = VoxelShapes.combineAndSimplify(VoxelUtils.or(WALLS_A, WALLS_B, WALLS_BOTTOM), INSIDE, IBooleanFunction.ONLY_FIRST);
            SHAPES.put(Direction.SOUTH, WALLS);
        }
        {
            //NORTH
            VoxelShape INSIDE = Block.makeCuboidShape(3.0D, 3.0, 0, 13.0D, 13.0, 14.0D);
            VoxelShape WALLS_A = Block.makeCuboidShape(1.0D, 1, 0, 15.0D, 15D, 14);
            VoxelShape WALLS_B = Block.makeCuboidShape(0, 0, 2, 16, 16, 14);
            VoxelShape WALLS_BOTTOM = Block.makeCuboidShape(2, 2, 14, 14.0D, 14, 16);
            VoxelShape WALLS = VoxelShapes.combineAndSimplify(VoxelUtils.or(WALLS_A, WALLS_B, WALLS_BOTTOM), INSIDE, IBooleanFunction.ONLY_FIRST);
            SHAPES.put(Direction.NORTH, WALLS);
        }
        {
            //EAST
            VoxelShape INSIDE = Block.makeCuboidShape(2, 3.0, 3.0D, 16, 13.0, 13);
            VoxelShape WALLS_A = Block.makeCuboidShape(2, 1, 1, 16, 15D, 15);
            VoxelShape WALLS_B = Block.makeCuboidShape(2, 0, 0, 14, 16, 16);
            VoxelShape WALLS_BOTTOM = Block.makeCuboidShape(0, 2, 2, 2, 14, 14);
            VoxelShape WALLS = VoxelShapes.combineAndSimplify(VoxelUtils.or(WALLS_A, WALLS_B, WALLS_BOTTOM), INSIDE, IBooleanFunction.ONLY_FIRST);
            SHAPES.put(Direction.EAST, WALLS);
        }
        {
            //WEST
            VoxelShape INSIDE = Block.makeCuboidShape(0, 3.0, 3.0D, 14, 13.0, 13);
            VoxelShape WALLS_A = Block.makeCuboidShape(0, 1, 1, 14, 15D, 15);
            VoxelShape WALLS_B = Block.makeCuboidShape(2, 0, 0, 14, 16, 16);
            VoxelShape WALLS_BOTTOM = Block.makeCuboidShape(14, 2, 2, 16, 14, 14);
            VoxelShape WALLS = VoxelShapes.combineAndSimplify(VoxelUtils.or(WALLS_A, WALLS_B, WALLS_BOTTOM), INSIDE, IBooleanFunction.ONLY_FIRST);
            SHAPES.put(Direction.WEST, WALLS);
        }
    }

    @Override
    public boolean onBlockActivated(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if (!world.isRemote()) {
            NetworkHooks.openGui((ServerPlayerEntity) player, getContainerProvider(world, pos), pos);
        }
        return true;
    }


}

