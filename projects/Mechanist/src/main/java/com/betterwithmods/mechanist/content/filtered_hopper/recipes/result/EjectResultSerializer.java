/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.filtered_hopper.recipes.result;

import com.betterwithmods.core.api.crafting.result.count.ICount;
import com.betterwithmods.core.impl.crafting.ItemStackBuilder;
import com.betterwithmods.core.impl.crafting.result.BaseResultSerializer;
import com.betterwithmods.core.utilties.DeserializeUtils;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import net.minecraft.item.Item;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.JsonUtils;
import net.minecraftforge.registries.ForgeRegistries;

public class EjectResultSerializer extends BaseResultSerializer<EjectResult> {



    @Override
    public EjectResult read(JsonElement json) {
        JsonObject object = json.getAsJsonObject();
        ICount count = DeserializeUtils.readCount(json);
        if (count != null) {
            String itemName = JSONUtils.getString(object, "item");
            Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(itemName));
            if (item == null)
                throw new JsonSyntaxException("Unknown item '" + itemName + "'");
            CompoundNBT tag = JsonUtils.readNBT(object, "nbt");
            ItemStackBuilder builder = ItemStackBuilder
                    .builder()
                    .item(item)
                    .tag(tag)
                    .count(count);

            return new EjectResult(builder);
        }



        throw new JsonParseException("Recipe Result Error");
    }

    @Override
    public EjectResult read(PacketBuffer buffer) {
        ItemStackBuilder builder = ItemStackBuilder.SERIALIZER.read(buffer);
        return new EjectResult(builder);
    }

    @Override
    public void write(PacketBuffer buffer, EjectResult recipeResult) {
        ItemStackBuilder.SERIALIZER.write(buffer, recipeResult.getBuilder());
    }

    @Override
    public <V extends EjectResult> void write(V stackResult, JsonElement jsonElement) {
        JsonObject result = jsonElement.getAsJsonObject();
        DeserializeUtils.writeItemStackBuilder(stackResult.getBuilder(), result);
    }


}
