/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.soulforge;

import com.betterwithmods.core.base.game.block.BlockBaseWaterlogged;
import com.betterwithmods.core.utilties.VoxelUtils;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

import javax.annotation.Nullable;

public class SoulforgeBlock extends BlockBaseWaterlogged {

    public static final DirectionProperty HORIZONTAL_FACING = BlockStateProperties.HORIZONTAL_FACING;

    public SoulforgeBlock(Properties properties) {
        super(properties);
        setDefaultState(getDefaultState().with(HORIZONTAL_FACING, Direction.NORTH));
    }

    @Override
    public boolean onBlockActivated(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if (!world.isRemote()) {
            NetworkHooks.openGui((ServerPlayerEntity) player, getContainerProvider(world, pos), pos);
        }
        return true;
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(HORIZONTAL_FACING);
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return getDefaultState().with(HORIZONTAL_FACING, context.getPlacementHorizontalFacing());
    }

    private static final  VoxelShape[] SHAPES = new VoxelShape[]{
            VoxelUtils.or(
                    Block.makeCuboidShape(4,0,0,12,2,16),
                    Block.makeCuboidShape(6,2,6,10,9,10),
                    Block.makeCuboidShape(5,9,5,11,12,11),
                    Block.makeCuboidShape(5,12,4,11,16,12),
                    Block.makeCuboidShape(5,15,12,11,16,16),
                    Block.makeCuboidShape(6,13,2,10,16,4),
                    Block.makeCuboidShape(7,14,0,9,16,2)
            ),
            VoxelUtils.or(
                    Block.makeCuboidShape(0, 0, 4, 16, 2, 12),
                    Block.makeCuboidShape(6, 2, 6, 10, 9, 10),
                    Block.makeCuboidShape(5, 9, 5, 11, 12, 11),
                    Block.makeCuboidShape(4, 12, 5, 12, 16, 11),
                    Block.makeCuboidShape(0, 15, 5, 4, 16, 11),
                    Block.makeCuboidShape(12, 13, 6, 14, 16, 10),
                    Block.makeCuboidShape(14, 14, 7, 16, 16, 9)
            ),
            VoxelUtils.or(
                    Block.makeCuboidShape(12,0,0,4,2,16),
                    Block.makeCuboidShape(10,2,6,6,9,10),
                    Block.makeCuboidShape(11,9,5,5,12,11),
                    Block.makeCuboidShape(11,12,4,5,16,12),
                    Block.makeCuboidShape(11,15,0,5,16,4),
                    Block.makeCuboidShape(10,13,12,6,16,14),
                    Block.makeCuboidShape(9,14,14,7,16,16)
            ),
            VoxelUtils.or(
                    Block.makeCuboidShape(0,0,4,16,2,12),
                    Block.makeCuboidShape(6,2,6,10,9,10),
                    Block.makeCuboidShape(5,9,5,11,12,11),
                    Block.makeCuboidShape(4,12,5,12,16,11),
                    Block.makeCuboidShape(12,15,5,16,16,11),
                    Block.makeCuboidShape(2,13,6,4,16,10),
                    Block.makeCuboidShape(0,14,7,2,16,9)
            ),
    };

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPES[state.get(HORIZONTAL_FACING).getHorizontalIndex()];
    }

}
