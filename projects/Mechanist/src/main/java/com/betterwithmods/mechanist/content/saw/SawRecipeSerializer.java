/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.saw;

import com.betterwithmods.core.api.crafting.output.IOutput;
import com.betterwithmods.core.base.game.recipes.multiplex.state.StateMultiplexRecipeSerializer;
import com.betterwithmods.core.impl.crafting.input.BlockStateInput;
import com.betterwithmods.core.utilties.DeserializeUtils;
import com.google.gson.JsonObject;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nonnull;

public class SawRecipeSerializer extends StateMultiplexRecipeSerializer<SawRecipe> {
    @Nonnull
    @Override
    public SawRecipe read(@Nonnull ResourceLocation id, @Nonnull JsonObject json) {
        BlockStateInput<SawRecipe> input = DeserializeUtils.readIInput(JSONUtils.getJsonObject(json, "input"));
        JsonObject resultObject = JSONUtils.getJsonObject(json, "result");
        IOutput outputs = DeserializeUtils.readIOutput(resultObject);
        return new SawRecipe(id, input, outputs);
    }

    @Nonnull
    @Override
    public SawRecipe read(@Nonnull ResourceLocation id, @Nonnull PacketBuffer buffer) {
        BlockStateInput<SawRecipe> input = DeserializeUtils.readIInput(buffer);
        IOutput outputs = DeserializeUtils.readIOutput(buffer);
        return new SawRecipe(id, input, outputs);
    }

    @Override
    public void write(@Nonnull PacketBuffer buffer, @Nonnull SawRecipe recipe) {
        DeserializeUtils.writeIInput(buffer, recipe.getInput());
        DeserializeUtils.writeIOutput(buffer, recipe.getOutput());
    }
}
