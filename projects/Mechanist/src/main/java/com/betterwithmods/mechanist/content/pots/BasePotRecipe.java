/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.pots;

import com.betterwithmods.core.api.crafting.input.IInput;
import com.betterwithmods.core.api.crafting.input.IRecipeContext;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipeSerializer;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipeType;
import com.betterwithmods.core.api.crafting.output.IOutput;
import com.betterwithmods.core.base.game.recipes.multiplex.inventory.TimedInventoryMultiplexRecipe;
import com.betterwithmods.core.utilties.StackEjector;
import com.betterwithmods.core.utilties.VectorBuilder;
import com.betterwithmods.core.utilties.inventory.InventoryUtils;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.items.IItemHandlerModifiable;

public class BasePotRecipe<T extends BasePotRecipe<T>> extends TimedInventoryMultiplexRecipe<T> implements IHeatedRecipe {

    private Integer heatLevel;

    public BasePotRecipe(IMultiplexRecipeType<?, ?> type, ResourceLocation id, IInput<IItemHandlerModifiable, T> inputs, IOutput outputs, Integer recipeTime, Integer heatLevel) {
        super(type, id, inputs, outputs, recipeTime);
        this.heatLevel = Math.max(heatLevel,0);
    }

    private static StackEjector EJECT_OFFSET = new StackEjector(new VectorBuilder().rand(0.5f).offset(0.25f).offset(0, 1, 1), new VectorBuilder().setGaussian(0.05f));

    @Override
    public void onCraft(World world, BlockPos pos, IRecipeContext<IItemHandlerModifiable, T> context) {
        IItemHandlerModifiable inventory = context.getState(world, pos);
        InventoryUtils.addToInventoryStackedOrDrop(inventory, outputs.getOutputs(inventory, ItemStack.class), world, pos, EJECT_OFFSET);
    }

    @Override
    public Integer getRecipeHeat() {
        return heatLevel;
    }

    @Override
    public IMultiplexRecipeSerializer<?> getSerializer() {
        return null;
    }
}
