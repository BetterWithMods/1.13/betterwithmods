/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.modules;

import com.betterwithmods.core.base.setup.ModuleBase;
import com.betterwithmods.core.utilties.DirectionUtils;
import com.betterwithmods.core.utilties.ReflectionHelper;
import com.google.common.collect.ImmutableMap;
import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.fluid.FlowingFluid;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.Fluids;
import net.minecraft.fluid.IFluidState;
import net.minecraft.item.BucketItem;
import net.minecraft.item.ItemStack;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.stats.Stats;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.event.entity.player.FillBucketEvent;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import javax.annotation.Nullable;

public class StrategicWater extends ModuleBase {

    public StrategicWater() {
        super("strategic_water");
    }

    @Override
    public void configs(ForgeConfigSpec.Builder builder) {
    }


    private static final ImmutableMap<Block, IBucketPickupHandler> BUCKET_HANDLER_OVERRIDES =
            ImmutableMap.<Block, IBucketPickupHandler>builder()
                    .put(Blocks.WATER, new BucketHandlerWrapper(Blocks.WATER))
                    .build();

    private static final int BUCKET_COOLDOWN = 20;

    @SubscribeEvent
    public void fillBucket(FillBucketEvent event) {
        if (event.getResult() == Event.Result.DENY)
            return;

        BlockRayTraceResult target = (BlockRayTraceResult) event.getTarget();
        ItemStack current = event.getEmptyBucket(); //Not necessarily empty, just what the event calls initial bucket
        World world = event.getWorld();
        PlayerEntity player = event.getEntityPlayer();
        if (player.isCreative())
            return;

        BucketItem bucket = null;
        if (current.getItem() instanceof BucketItem) {
            bucket = (BucketItem) current.getItem();
        }

        if (bucket == null) {
            return;
        }
        Fluid containedBlock = bucket.containedBlock;

        if (player.getCooldownTracker().hasCooldown(bucket))
            return;

        if (target != null && target.getType() == RayTraceResult.Type.BLOCK) {
            BlockPos blockpos = target.getPos();
            if (world.isBlockModifiable(player, blockpos) && player.canPlayerEdit(blockpos, target.getFace(), current)) {
                if (containedBlock == Fluids.EMPTY) {
                    BlockState state = world.getBlockState(blockpos);
                    ItemStack result = tryPickupLiquid(bucket, world, blockpos, state, player, current);
                    if (result == null) {
                        BlockPos offset = blockpos.offset(target.getFace());
                        BlockState offsetState = world.getBlockState(offset);
                        result = tryPickupLiquid(bucket, world, offset, offsetState, player, current);
                    }
                    if (result != null) {
                        event.setResult(Event.Result.ALLOW);
                        event.setFilledBucket(result);
                        player.getCooldownTracker().setCooldown(result.getItem(), BUCKET_COOLDOWN);
                    }
                } else {
                    BlockState blockstate = world.getBlockState(blockpos);
                    BlockPos blockpos1 = blockstate.getBlock() instanceof ILiquidContainer && bucket.containedBlock == Fluids.WATER ? blockpos : target.getPos().offset(target.getFace());

                    if (tryPlaceContainedLiquid(bucket, player, world, blockpos1, target)) {
                        bucket.onLiquidPlaced(world, current, blockpos1);
                        if (player instanceof ServerPlayerEntity) {
                            CriteriaTriggers.PLACED_BLOCK.trigger((ServerPlayerEntity) player, blockpos1, current);
                        }
                        ItemStack result = bucket.emptyBucket(current, player);
                        player.addStat(Stats.ITEM_USED.get(bucket));
                        event.setResult(Event.Result.ALLOW);
                        event.setFilledBucket(result);
                        player.getCooldownTracker().setCooldown(result.getItem(), BUCKET_COOLDOWN);
                    }
                }
            }

        }

    }


    public static ItemStack tryPickupLiquid(BucketItem bucket, World world, BlockPos blockpos, BlockState state, PlayerEntity player, ItemStack current) {
        if (state.getBlock() instanceof IBucketPickupHandler) {
            IBucketPickupHandler handler = ((IBucketPickupHandler) state.getBlock());
            Fluid fluid = BUCKET_HANDLER_OVERRIDES.getOrDefault(state.getBlock(), handler).pickupFluid(world, blockpos, state);

            if (fluid != Fluids.EMPTY) {
                player.addStat(Stats.ITEM_USED.get(bucket));
                player.playSound(fluid.isIn(FluidTags.LAVA) ? SoundEvents.ITEM_BUCKET_FILL_LAVA : SoundEvents.ITEM_BUCKET_FILL, 1.0F, 1.0F);
                ItemStack result = bucket.fillBucket(current, player, fluid.getFilledBucket());
                if (!world.isRemote) {
                    CriteriaTriggers.FILLED_BUCKET.trigger((ServerPlayerEntity) player, new ItemStack(fluid.getFilledBucket()));
                }
                return result;
            }
        }
        return null;
    }

    public static boolean tryPlaceContainedLiquid(BucketItem bucket, @Nullable PlayerEntity player, World world, BlockPos posIn, @Nullable BlockRayTraceResult target) {
        Fluid containedBlock = bucket.containedBlock;
        if (!(containedBlock instanceof FlowingFluid)) {
            return false;
        } else {
            BlockState iblockstate = world.getBlockState(posIn);
            Material material = iblockstate.getMaterial();
            boolean notSolid = !material.isSolid();
            boolean replaceable = material.isReplaceable();
            if (world.isAirBlock(posIn) || notSolid || replaceable || iblockstate.getBlock() instanceof ILiquidContainer && ((ILiquidContainer) iblockstate.getBlock()).canContainFluid(world, posIn, iblockstate, containedBlock)) {
                IFluidState fluidState = ((FlowingFluid) containedBlock).getFlowingFluidState(4, false);
                if (world.dimension.doesWaterVaporize() && containedBlock.isIn(FluidTags.WATER)) {
                    int i = posIn.getX();
                    int j = posIn.getY();
                    int k = posIn.getZ();
                    world.playSound(player, posIn, SoundEvents.BLOCK_FIRE_EXTINGUISH, SoundCategory.BLOCKS, 0.5F, 2.6F + (world.rand.nextFloat() - world.rand.nextFloat()) * 0.8F);

                    for (int l = 0; l < 8; ++l) {
                        world.addParticle(ParticleTypes.LARGE_SMOKE, (double) i + Math.random(), (double) j + Math.random(), (double) k + Math.random(), 0.0D, 0.0D, 0.0D);
                    }
                } else if (iblockstate.getBlock() instanceof ILiquidContainer) {
                    if (((ILiquidContainer) iblockstate.getBlock()).receiveFluid(world, posIn, iblockstate, fluidState)) {
                        bucket.playEmptySound(player, world, posIn);
                    }
                } else {
                    if (!world.isRemote && (notSolid || replaceable) && !material.isLiquid()) {
                        world.destroyBlock(posIn, true);
                    }
                    bucket.playEmptySound(player, world, posIn);
                    world.setBlockState(posIn, fluidState.getBlockState(), 11);
                    for (Direction facing : DirectionUtils.HORIZONTAL_SET) {
                        BlockPos pos = posIn.offset(facing);
                        BlockState blockState = world.getBlockState(pos);
                        IFluidState fluidState1 = world.getFluidState(pos);
                        if (blockState.getMaterial().isReplaceable() && fluidState1.isEmpty()) {
                            world.setBlockState(pos, fluidState.getBlockState(), 11);
                        }
                    }
                }

                return true;
            } else {
                return target != null && tryPlaceContainedLiquid(bucket, player, world, target.getPos().offset(target.getFace()), null);
            }
        }
    }

    @Override
    public String getDescription() {
        return "Buckets no longer can place water sources, making them a commodity";
    }


    public static class BucketHandlerWrapper implements IBucketPickupHandler {
        public Block block;

        public BucketHandlerWrapper(Block block) {
            this.block = block;
        }

        @Override
        public Fluid pickupFluid(IWorld world, BlockPos pos, BlockState state) {
            if (block instanceof FlowingFluidBlock) {
                Fluid fluid = (Fluid) ReflectionHelper.getValue(FlowingFluidBlock.class, block, "fluid");
                return fluid;
            }
            return null;
        }
    }
}
