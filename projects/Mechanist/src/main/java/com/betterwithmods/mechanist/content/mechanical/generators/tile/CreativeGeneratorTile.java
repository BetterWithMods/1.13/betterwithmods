/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.mechanical.generators.tile;

import com.betterwithmods.core.api.mech.IPower;
import com.betterwithmods.core.base.game.tile.GeneratorTile;
import com.betterwithmods.core.impl.Power;
import com.betterwithmods.core.utilties.DirectionUtils;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class CreativeGeneratorTile extends GeneratorTile {

    public static final Power GENERATOR = new Power(1, 4);

    public CreativeGeneratorTile() {
        super(Registrars.Tiles.CREATIVE_GENERATOR);
        this.powerSource = null;
    }

    @Nonnull
    @Override
    public Iterable<Direction> getOutputs() {
        return DirectionUtils.FACING_SET;
    }

    @Nullable
    @Override
    public IPower getOutput(@Nonnull IWorld world, @Nonnull BlockPos pos, @Nonnull Direction facing) {
        if(facing == Direction.UP)
            return new Power(10, 2);
        return GENERATOR;
    }

    @Nullable
    @Override
    public IPower getMaximumInput(@Nonnull Direction facing) {
        return null;
    }

    @Nullable
    @Override
    public IPower getMinimumInput() {
        return null;
    }

    @Override
    public boolean overpower(World world, BlockPos pos) { /* NOOP */ return false; }

    @Override
    public void calculatePowerSource() { }


    @Override
    public void postCalculate() { }

    @Override
    protected void calculate() { /*NOOP*/ }

}
