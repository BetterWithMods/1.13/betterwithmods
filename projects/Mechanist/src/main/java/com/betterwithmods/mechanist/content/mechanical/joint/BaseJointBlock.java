/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.mechanical.joint;

import com.betterwithmods.core.base.config.CoreCommon;
import com.betterwithmods.core.base.game.block.BlockBase;
import com.betterwithmods.core.utilties.MechanicalUtils;
import com.betterwithmods.mechanist.content.mechanical.BaseMechanicalAxisBlock;
import com.betterwithmods.mechanist.handlers.BlockRotationHandler;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.List;

public class BaseJointBlock extends BlockBase {

    public static final DirectionProperty FACING = BlockStateProperties.FACING;
    private static final BooleanProperty UP = BlockStateProperties.UP;
    private static final BooleanProperty DOWN = BlockStateProperties.DOWN;
    private static final BooleanProperty NORTH = BlockStateProperties.NORTH;
    private static final BooleanProperty EAST = BlockStateProperties.EAST;
    private static final BooleanProperty SOUTH = BlockStateProperties.SOUTH;
    private static final BooleanProperty WEST = BlockStateProperties.WEST;


    public BaseJointBlock(Properties properties) {
        super(properties);
        setDefaultState(getDefaultState()
                .with(FACING, Direction.UP)
                .with(UP, false)
                .with(DOWN, false)
                .with(NORTH, false)
                .with(EAST, false)
                .with(SOUTH, false)
                .with(WEST, false)
        );
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(FACING, UP, DOWN, NORTH, EAST, SOUTH, WEST);
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        IWorld world = context.getWorld();
        BlockPos pos = context.getPos();
        BlockState stateIn = getDefaultState().with(FACING, context.getFace());
        return getConnections(stateIn, world, pos);
    }


    @Override
    public BlockState updatePostPlacement(BlockState stateIn, Direction facing, BlockState facingState, IWorld world, BlockPos pos, BlockPos facingPos) {
        return getConnections(stateIn, world, pos);
    }

    public BlockState getConnections(BlockState stateIn, IWorld world, BlockPos pos) {
        return stateIn
                .with(UP, isProperAxle(stateIn, world, pos, Direction.UP))
                .with(DOWN, isProperAxle(stateIn, world, pos, Direction.DOWN))
                .with(NORTH, isProperAxle(stateIn, world, pos, Direction.NORTH))
                .with(EAST, isProperAxle(stateIn, world, pos, Direction.EAST))
                .with(SOUTH, isProperAxle(stateIn, world, pos, Direction.SOUTH))
                .with(WEST, isProperAxle(stateIn, world, pos, Direction.WEST));
    }


    private boolean isProperAxle(BlockState state, IWorld world, BlockPos pos, Direction facing) {

        //Don't put an output on the input face
        if (state.getBlock() == this && state.get(FACING) == facing)
            return false;

        Direction.Axis axis = facing.getAxis();
        BlockState offsetState = world.getBlockState(pos.offset(facing));
        Block block = offsetState.getBlock();
        if (block instanceof BaseMechanicalAxisBlock) {
            Direction.Axis axleAxis = ((BaseMechanicalAxisBlock) block).getAxis(offsetState);
            return axis == axleAxis;
        }
        return false;
    }


    @Override
    public boolean onBlockActivated(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockRayTraceResult hit) {
        if (CoreCommon.DEBUGGING.get()) {
            MechanicalUtils.debug(world, pos, player, hand);
        }
        return BlockRotationHandler.cycleRotation(player, world, pos, state, this::onRotate);
    }

    public void onRotate(World world, BlockPos pos, BlockState state) {
        world.setBlockState(pos, state.cycle(FACING));
        BlockState newState = world.getBlockState(pos);
        world.notifyBlockUpdate(pos, state, newState, 3);
        world.func_217393_a(pos, state, newState);
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable IBlockReader world, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        super.addInformation(stack, world, tooltip, flagIn);
    }


}
