/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.dynamite;

import com.betterwithmods.core.utilties.EnvironmentUtils;
import com.betterwithmods.core.utilties.ItemUtils;
import com.betterwithmods.core.utilties.WorldUtils;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.block.Blocks;
import net.minecraft.entity.*;
import net.minecraft.entity.projectile.ProjectileHelper;
import net.minecraft.entity.projectile.ProjectileItemEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.*;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

public class DynamiteEntity extends ProjectileItemEntity {
    private static final byte FUSE_UPDATE_TAG = 100;
    private static final DataParameter<Integer> FUSE = EntityDataManager.createKey(DynamiteEntity.class, DataSerializers.VARINT);

    private int fuse;
    private Entity ignoreEntity;
    private int ignoreTime;

    public DynamiteEntity(EntityType<? extends DynamiteEntity> type, World world) {
        super(type, world);
    }

    public DynamiteEntity(World worldIn, LivingEntity throwerIn) {
        super(Registrars.Entities.DYNAMITE, throwerIn, worldIn);
    }

    public DynamiteEntity(World worldIn, double x, double y, double z) {
        super(Registrars.Entities.DYNAMITE, x, y, z, worldIn);
    }

    @Override
    protected void registerData() {
        super.registerData();
        this.getDataManager().register(FUSE, 80);
    }

    @Override
    protected Item func_213885_i() {
        return Registrars.Items.DYNAMITE;
    }

    @Override
    protected void onImpact(RayTraceResult result) {

    }

    @Override
    public void writeAdditional(CompoundNBT nbt) {
        nbt.putShort("Fuse", (short) this.getFuse());
        super.writeAdditional(nbt);
    }

    @Override
    public void readAdditional(CompoundNBT nbt) {
        this.setFuse(nbt.getShort("Fuse"));
        super.readAdditional(nbt);
    }

    protected float getEyeHeight(Pose p_213316_1_, EntitySize p_213316_2_) {
        return 0.0F;
    }

    public void setFuse(int fuseIn) {
        this.dataManager.set(FUSE, fuseIn);
        this.fuse = fuseIn;
    }

    public void notifyDataManagerChange(DataParameter<?> key) {
        if (FUSE.equals(key)) {
            this.fuse = this.getFuseDataManager();
        }
    }

    public int getFuseDataManager() {
        return this.dataManager.get(FUSE);
    }

    public int getFuse() {
        return this.fuse;
    }

    @Override
    public IPacket<?> createSpawnPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    public void explode() {
        float intensity = 2F;
        world.createExplosion(null, this.posX, this.posY, this.posZ, intensity, Explosion.Mode.BREAK);
        //TODO redneck fishing
    }

    @Override
    public void tick() {
        this.lastTickPosX = this.posX;
        this.lastTickPosY = this.posY;
        this.lastTickPosZ = this.posZ;
        super.baseTick();
        if (this.throwableShake > 0) {
            --this.throwableShake;
        }

        AxisAlignedBB axisalignedbb = this.getBoundingBox().expand(this.getMotion()).grow(1.0D);

        for(Entity entity : this.world.getEntitiesInAABBexcluding(this, axisalignedbb, (p_213881_0_) -> !p_213881_0_.isSpectator() && p_213881_0_.canBeCollidedWith())) {
            if (entity == this.ignoreEntity) {
                ++this.ignoreTime;
                break;
            }

            if (this.owner != null && this.ticksExisted < 2 && this.ignoreEntity == null) {
                this.ignoreEntity = entity;
                this.ignoreTime = 3;
                break;
            }
        }

        RayTraceResult raytraceresult = ProjectileHelper.func_221267_a(this, axisalignedbb, (p_213880_1_) -> !p_213880_1_.isSpectator() && p_213880_1_.canBeCollidedWith() && p_213880_1_ != this.ignoreEntity, RayTraceContext.BlockMode.OUTLINE, true);
        if (this.ignoreEntity != null && this.ignoreTime-- <= 0) {
            this.ignoreEntity = null;
        }

        if (raytraceresult.getType() != RayTraceResult.Type.MISS) {
            if (raytraceresult.getType() == RayTraceResult.Type.BLOCK && this.world.getBlockState(((BlockRayTraceResult)raytraceresult).getPos()).getBlock() == Blocks.NETHER_PORTAL) {
                this.setPortal(((BlockRayTraceResult)raytraceresult).getPos());
            } else if (!net.minecraftforge.event.ForgeEventFactory.onProjectileImpact(this, raytraceresult)){
                this.onImpact(raytraceresult);
            }
        }


        Vec3d vec3d = this.getMotion();
        if (!onGround && !this.hasNoGravity()) {
            setMotion(getMotion().subtract(0,getGravityVelocity(),0));
        }
        if(onGround || collidedVertically) {
            setMotion(getMotion().mul(0.5, -0.5, 0.5));
        }

        this.move(MoverType.SELF, vec3d);

        float f = MathHelper.sqrt(getDistanceSq(vec3d));
        this.rotationYaw = (float)(MathHelper.atan2(vec3d.x, vec3d.z) * (double)(180F / (float)Math.PI));

        for(this.rotationPitch = (float)(MathHelper.atan2(vec3d.y, (double)f) * (double)(180F / (float)Math.PI)); this.rotationPitch - this.prevRotationPitch < -180.0F; this.prevRotationPitch -= 360.0F) {
            ;
        }

        while(this.rotationPitch - this.prevRotationPitch >= 180.0F) {
            this.prevRotationPitch += 360.0F;
        }

        while(this.rotationYaw - this.prevRotationYaw < -180.0F) {
            this.prevRotationYaw -= 360.0F;
        }

        while(this.rotationYaw - this.prevRotationYaw >= 180.0F) {
            this.prevRotationYaw += 360.0F;
        }

        this.rotationPitch = MathHelper.lerp(0.2F, this.prevRotationPitch, this.rotationPitch);
        this.rotationYaw = MathHelper.lerp(0.2F, this.prevRotationYaw, this.rotationYaw);
        float f1;
        if (this.isInWater()) {
            for(int i = 0; i < 4; ++i) {
                this.world.addParticle(ParticleTypes.BUBBLE, this.posX - vec3d.x * 0.25D, this.posY - vec3d.y * 0.25D, this.posZ - vec3d.z * 0.25D, vec3d.x, vec3d.y, vec3d.z);
            }

            f1 = 0.8F;
        } else {
            f1 = 0.99F;
        }


        //Convert back to an item if unlit
        if(getFuse() <= -1) {
            if(!world.isRemote) {
                if(onGround) {
                    ItemUtils.ejectStackOffset(world, WorldUtils.fromEntity(this), new ItemStack(Registrars.Items.DYNAMITE));
                    remove();
                }
            }
        }

        if (getFuse() >= 0) {
            if (!world.isRemote) {
                //Set fuse value on client for particles
                world.setEntityState(this, FUSE_UPDATE_TAG);
                //Play sounds every second
                if (getFuse() % 20 == 0) {
                    world.playSound(null, getPosition(), SoundEvents.ENTITY_TNT_PRIMED, SoundCategory.NEUTRAL, 1.0F, 1.0F);
                }
            }

            float smokeOffset = 0.25F;
            EnvironmentUtils.addParticle(world, ParticleTypes.SMOKE, this.getPositionVec().subtract(getMotion().scale(smokeOffset)), getMotion());

            if (getFuse() <= 1) {
                if (!this.getEntityWorld().isRemote) {
                    explode();
                }
                this.remove();
            }
            fuse--;
        }

    }

}
