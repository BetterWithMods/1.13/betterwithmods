/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.compat.jei.category;

import com.betterwithmods.core.References;
import com.betterwithmods.core.api.crafting.result.IResult;
import com.betterwithmods.core.compat.jei.CoreJEI;
import com.betterwithmods.core.compat.jei.category.AbstractBlockStateMultiplexCategory;
import com.betterwithmods.core.utilties.ItemUtils;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.compat.jei.MechanistJEI;
import com.betterwithmods.mechanist.content.turntable.TurntableRecipe;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Lists;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawableAnimated;
import mezz.jei.api.gui.ingredient.IGuiIngredientGroup;
import mezz.jei.api.gui.ingredient.IGuiItemStackGroup;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.item.ItemStack;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class TurntableCategory extends AbstractBlockStateMultiplexCategory<TurntableRecipe> {
    private final Cache<Integer, IDrawableAnimated> ANIMATED_CACHE = CacheBuilder.newBuilder().build();

    public TurntableCategory(IGuiHelper guiHelper) {
        super(guiHelper, guiHelper.createDrawable(MechanistJEI.Constants.TURNTABLE_BACKGROUND, 0, 0, 127, 36),
                String.format("jei.%s.turntable", References.MODID_MECHANIST),
                guiHelper.createDrawableIngredient(new ItemStack(Registrars.Blocks.TURNTABLE)),
                MechanistJEI.Constants.TURNTABLE_CATEGORY);
    }

    @Nonnull
    @Override
    public Class<? extends TurntableRecipe> getRecipeClass() {
        return TurntableRecipe.class;
    }

    @Override
    public void setIngredients(TurntableRecipe recipe, IIngredients ingredients) {
        super.setIngredients(recipe, ingredients);
        ItemStack replace = ItemUtils.getStack(recipe.getRecipeStage());
        if(!replace.isEmpty()) {
            ingredients.setOutputs(VanillaTypes.ITEM, Lists.newArrayList(replace));
        }
    }

    @Override
    public void draw(TurntableRecipe recipe, double mouseX, double mouseY) {
        try {
            int time = recipe.getRecipeTime() * 10;

            IDrawableAnimated animation = ANIMATED_CACHE.get(recipe.getRecipeTime(),
                    () -> this.guiHelper.drawableBuilder(MechanistJEI.Constants.TURNTABLE_BACKGROUND, 0, 37, 24, 17).buildAnimated(time, IDrawableAnimated.StartDirection.LEFT, false));
            animation.draw(23, 18);
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setRecipe(@Nonnull IRecipeLayout layout, @Nonnull TurntableRecipe recipe, @Nonnull IIngredients ingredients) {
        IGuiItemStackGroup guiItemStacks = layout.getItemStacks();

        createSlot(guiItemStacks, true, 0, 1, 1);
        if (!ingredients.getOutputs(VanillaTypes.ITEM).isEmpty()) {
            createSlot(guiItemStacks, false,  1, 52, 1);
        }
        guiItemStacks.set(ingredients);



        if (!ingredients.getOutputs(CoreJEI.STACK_RESULT).isEmpty()) {
            IGuiIngredientGroup<IResult<ItemStack>> guiResults = layout.getIngredientsGroup(CoreJEI.STACK_RESULT);
            createSlotsHorizontal(guiResults, false, 3, 2, 73, 18);
            guiResults.set(ingredients);
            List<List<IResult<ItemStack>>> resultList = ingredients.getOutputs(CoreJEI.STACK_RESULT);
            if (resultList.size() > 3) {
                List<List<IResult<ItemStack>>> subList = resultList.subList(2, resultList.size());
                List<IResult<ItemStack>> results = subList.stream().flatMap(List::stream).collect(Collectors.toList());
                guiResults.set(3, results);
            }
        }
    }
}
