/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.filtered_hopper;

import com.betterwithmods.core.References;
import com.betterwithmods.core.api.souls.ISoulStorage;
import com.betterwithmods.core.impl.souls.SoulStorage;
import com.betterwithmods.core.utilties.DirectionUtils;
import com.betterwithmods.core.utilties.ItemUtils;
import com.betterwithmods.core.utilties.Timer;
import com.betterwithmods.core.utilties.inventory.InventoryUtils;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.content.filtered_hopper.client.FilteredHopperModelFactory;
import com.betterwithmods.mechanist.content.filtered_hopper.recipes.HopperItemCollector;
import com.betterwithmods.mechanist.content.mechanical.MechanicalInventoryTile;
import com.betterwithmods.mechanist.content.pots.BasePotBlock;
import com.betterwithmods.mechanist.content.saw.SawBlock;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.IIntArray;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.client.model.data.IModelData;
import net.minecraftforge.client.model.data.ModelDataMap;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class FilteredHopperTile extends MechanicalInventoryTile implements ITickableTileEntity, INamedContainerProvider {
    private static final VoxelShape COLLECTION_AREA = BasePotBlock.makeCuboidShape(0, 4, 0, 16, 24, 15);

    private final Timer delay = new Timer(0);
    private final ItemStackHandler filterInventory;

    private ISoulStorage soulStorage = new SoulStorage(8);

    private final HopperItemCollector collector = new HopperItemCollector(COLLECTION_AREA, optionalInventory);

    public FilteredHopperTile() {
        super(Registrars.Tiles.FILTERED_HOPPER);
        filterInventory = new ItemStackHandler(1) {
            @Override
            protected int getStackLimit(int slot, @Nonnull ItemStack stack) {
                return 1;
            }
        };
    }

    @Nonnull
    @Override
    public ItemStackHandler createInventory() {
        return new ItemStackHandler(18);
    }

    @Nonnull
    @Override
    public Iterable<Direction> getInputs() {
        return DirectionUtils.HORIZONTAL_SET;
    }

    @Override
    public void postCalculate() {
        BlockState state = world.getBlockState(pos);
        world.setBlockState(pos, state.with(SawBlock.POWERED, this.power.getTorque() > 0));
    }

    @Override
    public boolean overpower(World world, BlockPos pos) {
        return false;
    }

    @Override
    public void tick() {
        if (delay.ready()) {
            updateFilterState();
            collector.tick(world, pos);
            delay.setTime(0);
        }
        delay.incrementTime(1);
    }


    @Override
    public void onRemoved() {
        super.onRemoved();
        InventoryUtils.drop(filterInventory, world,pos, ItemUtils.EJECT_OFFSET);
    }

    public static final String DELAY = "delay", FILTER_INVENTORY = "filterInventory", SOULS = "souls";

    @Nonnull
    @Override
    public CompoundNBT write(CompoundNBT compound) {
        compound.put(DELAY, delay.serializeNBT());
        compound.put(FILTER_INVENTORY, filterInventory.serializeNBT());
        compound.put(SOULS, soulStorage.serializeNBT());
        return super.write(compound);
    }

    @Override
    public void read(CompoundNBT compound) {
        if (compound.contains(DELAY)) {
            delay.deserializeNBT(compound.getCompound(DELAY));
        }
        if (compound.contains(FILTER_INVENTORY)) {
            filterInventory.deserializeNBT(compound.getCompound(FILTER_INVENTORY));
        }
        if(compound.contains(SOULS)) {
            soulStorage.deserializeNBT(compound.getCompound(SOULS));
        }
        super.read(compound);
    }

    public ItemStackHandler getFilterInventory() {
        return filterInventory;
    }

    public ItemStack getFilter() {
        return getFilterInventory().getStackInSlot(0);
    }

    public ISoulStorage getSoulStorage() {
        return soulStorage;
    }

    @Nonnull
    @Override
    public ITextComponent getDisplayName() {
        return new TranslationTextComponent(String.format("container.%s.filtered_hopper", References.MODID_MECHANIST));
    }

    @Nullable
    @Override
    public Container createMenu(int windowId, @Nonnull PlayerInventory playerInventory, @Nonnull PlayerEntity playerEntity) {
        return new FilteredHopperContainer(windowId, playerEntity, getInventory(), getFilterInventory(), data);
    }

    private void updateFilterState() {
        BlockState state = world.getBlockState(pos);
        boolean filtered = state.get(FilteredHopperBlock.FILTERED);
        ItemStack stack = getFilter();
        boolean hasFilter = !stack.isEmpty();
        if (filtered != hasFilter) {
            world.setBlockState(pos, state.with(FilteredHopperBlock.FILTERED, hasFilter));
        }
        update();
        updateClient();
    }

    @Nonnull
    @Override
    public IModelData getModelData() {
        ModelDataMap.Builder builder = new ModelDataMap.Builder();
        builder.withInitial(FilteredHopperModelFactory.FILTER, getFilter());
        return builder.build();
    }

    public final IIntArray data = new IIntArray() {

        @Override
        public int get(int index) {
            switch (index) {
                case 0:
                    return FilteredHopperTile.this.power.getTorque() > 0 ? 1 : 0;
                default:
                    return -1;
            }

        }

        @Override
        public void set(int index, int value) {
        }

        @Override
        public int size() {
            return 1;
        }
    };
}
