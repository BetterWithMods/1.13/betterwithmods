/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.filtered_hopper.recipes;

import com.betterwithmods.core.api.crafting.input.IInput;
import com.betterwithmods.core.api.crafting.input.IRecipeContext;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipeSerializer;
import com.betterwithmods.core.api.crafting.output.IOutput;
import com.betterwithmods.core.impl.crafting.multiplex.BaseMultiplexRecipe;
import com.betterwithmods.core.impl.souls.SoulStack;
import com.betterwithmods.core.utilties.ItemUtils;
import com.betterwithmods.core.utilties.StackEjector;
import com.betterwithmods.core.utilties.inventory.InventoryUtils;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Collections;

public class HopperRecipe extends BaseMultiplexRecipe<HopperState, HopperRecipe> {

    public HopperRecipe(ResourceLocation id, IInput<HopperState, HopperRecipe> inputs, IOutput outputs) {
        super(Registrars.MultiplexRecipeTypes.FILTERED_HOPPER, id, inputs, outputs);
    }

    @Nonnull
    @Override
    public IMultiplexRecipeSerializer<?> getSerializer() {
        return Registrars.MultiplexRecipeSerializers.FILTERED_HOPPER;
    }

    @Override
    public void onCraft(World world, BlockPos pos, IRecipeContext<HopperState, HopperRecipe> context) {

        if (outputs != null) {
            HopperState state = context.getState(world, pos);
            for (StackEjector result : outputs.getResults(StackEjector.class, state)) {
                result.ejectStack(world, new Vec3d(pos.up()).add(0.5,0,0.5), Vec3d.ZERO);
            }
            for (ItemStack result : outputs.getResults(ItemStack.class, state)) {
                InventoryUtils.addToInventoryStackedOrDrop(state.getInventory(), result, world, pos, ItemUtils.EJECT_OFFSET);
            }
            for (SoulStack result : outputs.getResults(SoulStack.class, state)) {
                state.getSoulStorage().insert(result.copy(), false);
            }
        }
    }

    @Override
    public HopperInput getInput() {
        return (HopperInput) super.getInput();
    }

    @Nonnull
    @Override
    public Collection<HopperRecipe> getDisplayRecipes() {
        if(this.outputs == null || this.outputs.getAllResults().isEmpty()) {
            return Collections.emptySet();
        }
        return Collections.singleton(this);
    }
}
