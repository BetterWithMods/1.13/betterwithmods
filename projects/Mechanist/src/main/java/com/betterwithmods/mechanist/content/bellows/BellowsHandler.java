/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.bellows;

import com.betterwithmods.core.Tags;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.blocks.StokedFireBlock;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.FireBlock;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.util.Constants;

import java.util.Collection;
import java.util.List;

public class BellowsHandler {


    public static Iterable<BlockPos> getArea(BlockPos pos, Direction direction, int distance) {
        BlockPos offset = pos.offset(direction);
        BlockPos left = offset.offset(direction.rotateY());
        BlockPos right = offset.offset(direction.rotateYCCW());
        Collection<Iterable<BlockPos>> collection = Lists.newArrayList();
        List<BlockPos> positions = Lists.newArrayList(offset, left, right);
        for (BlockPos p : positions) {
            collection.add(BlockPos.getAllInBoxMutable(p, p.offset(direction, distance)));
        }
        return Iterables.concat(collection);
    }

    public static void onCompressed(World world, BlockPos pos, BlockState state) {
        Direction dir = state.get(BellowsBlock.FACING);
        for (BlockPos p : getArea(pos, dir, 2)) {
            BlockState s = world.getBlockState(p);
            stokedFlames(world, p, s);
            //TODO item blowing
        }
    }

    public static void stokedFlames(World world, BlockPos p, BlockState s) {
        BlockPos below = p.down();
        if(world.getBlockState(below).isIn(Tags.Blocks.SUPPORTED_STOKED_FIRE)) {
            if (s.getBlock().equals(Blocks.FIRE)) {
                world.setBlockState(p, ((StokedFireBlock) Registrars.Blocks.STOKED_FIRE).getStateForPlacement(world, p));
            } else if (s.getBlock().equals(Registrars.Blocks.STOKED_FIRE)) {
                world.setBlockState(p, s.with(StokedFireBlock.AGE, 0));
            }
        } else {
            if (s.getBlock() instanceof FireBlock) {
                world.removeBlock(p, false);
                world.playEvent(Constants.WorldEvents.FIRE_EXTINGUISH_SOUND, p, Block.getStateId(s));
            }
        }
    }
}
