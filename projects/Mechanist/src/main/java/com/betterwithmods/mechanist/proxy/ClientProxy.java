/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.proxy;

import com.betterwithmods.core.base.setup.proxy.BaseClientProxy;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.content.dispenser.AdvancedDispenserScreen;
import com.betterwithmods.mechanist.content.dynamic.CamoFactory;
import com.betterwithmods.mechanist.content.dynamic.CamoflageBlock;
import com.betterwithmods.mechanist.content.dynamite.DynamicRenderer;
import com.betterwithmods.mechanist.content.dynamite.DynamiteEntity;
import com.betterwithmods.mechanist.content.filtered_hopper.FilteredHopperScreen;
import com.betterwithmods.mechanist.content.filtered_hopper.client.FilteredHopperModelFactory;
import com.betterwithmods.mechanist.content.mechanical.generators.render.HorizontalWindmillRenderer;
import com.betterwithmods.mechanist.content.mechanical.generators.render.VerticalWindmillRenderer;
import com.betterwithmods.mechanist.content.mechanical.generators.render.WaterwheelRenderer;
import com.betterwithmods.mechanist.content.mechanical.generators.tile.HorizontalWindmillTile;
import com.betterwithmods.mechanist.content.mechanical.generators.tile.VerticalWindmillTile;
import com.betterwithmods.mechanist.content.mechanical.generators.tile.WaterwheelTile;
import com.betterwithmods.mechanist.content.millstone.MillstoneScreen;
import com.betterwithmods.mechanist.content.mining_charge.MiningChargeEntity;
import com.betterwithmods.mechanist.content.mining_charge.MiningChargeRenderer;
import com.betterwithmods.mechanist.content.pots.cauldron.CauldronScreen;
import com.betterwithmods.mechanist.content.pots.crucible.CrucibleScreen;
import com.betterwithmods.mechanist.content.soulforge.SoulforgeScreen;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScreenManager;
import net.minecraft.client.particle.CampfireParticle;
import net.minecraft.client.renderer.color.BlockColors;
import net.minecraft.client.renderer.color.ItemColors;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.item.BlockItem;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.GrassColors;
import net.minecraft.world.biome.BiomeColors;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.ColorHandlerEvent;
import net.minecraftforge.client.event.ModelBakeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@OnlyIn(Dist.CLIENT)
public class ClientProxy extends BaseClientProxy {


    public ClientProxy() {}

    @Override
    public void onCommonSetup(FMLCommonSetupEvent event) {
        DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> Minecraft.getInstance().particles.registerFactory(Registrars.Particles.CAULDRON_STEAM, CampfireParticle.CozySmokeFactory::new));
    }

    @Override
    public void onClientSetup(FMLClientSetupEvent event) {
        getLogger().info("Client Proxy!");

        ScreenManager.registerFactory(Registrars.Containers.CAULDRON, CauldronScreen::new);
        ScreenManager.registerFactory(Registrars.Containers.CRUCIBLE, CrucibleScreen::new);
        ScreenManager.registerFactory(Registrars.Containers.MILLSTONE, MillstoneScreen::new);
        ScreenManager.registerFactory(Registrars.Containers.ADVANCED_DISPENSER, AdvancedDispenserScreen::new);
        ScreenManager.registerFactory(Registrars.Containers.SOULFORGE, SoulforgeScreen::new);
        ScreenManager.registerFactory(Registrars.Containers.FILTERED_HOPPER, FilteredHopperScreen::new);

        RenderingRegistry.registerEntityRenderingHandler(MiningChargeEntity.class, MiningChargeRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(DynamiteEntity.class, DynamicRenderer::new);

        ClientRegistry.bindTileEntitySpecialRenderer(HorizontalWindmillTile.class, new HorizontalWindmillRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(VerticalWindmillTile.class, new VerticalWindmillRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(WaterwheelTile.class, new WaterwheelRenderer());
    }

    public void onBlockColors(ColorHandlerEvent.Block event) {
        BlockColors colors = event.getBlockColors();
        colors.register((state, world, pos, tintIndex) -> world != null && pos != null ? BiomeColors.getGrassColor(world, pos) : GrassColors.get(0.5D, 1.0D),
                Registrars.Blocks.GRASS_SLAB,
                Registrars.Blocks.GRASS_PLANTER,
                Registrars.Blocks.VINE_TRAP
        );
        colors.register((state, world, pos, tintIndex) -> world != null && pos != null ? BiomeColors.getWaterColor(world, pos) : -1,
                Registrars.Blocks.WATER_PLANTER);

        colors.register((state, world, pos, tintIndex) -> world != null && pos != null ? BiomeColors.getWaterColor(world, pos) : -1,
                Registrars.Blocks.WATER_PLANTER);

    }

    public void onItemColors(ColorHandlerEvent.Item event) {
        ItemColors itemColors = event.getItemColors();
        BlockColors blockColors = event.getBlockColors();

        itemColors.register((stack, tintIndex) -> {
                    BlockState iblockstate = ((BlockItem) stack.getItem()).getBlock().getDefaultState();
                    return blockColors.getColor(iblockstate, null, null, tintIndex);
                },
                Registrars.Blocks.GRASS_PLANTER,
                Registrars.Blocks.GRASS_SLAB,
                Registrars.Blocks.VINE_TRAP
        );

        itemColors.register((stack, tintIndex) -> 4159204, Registrars.Blocks.WATER_PLANTER);
    }

    @Override
    public void onModelBake(ModelBakeEvent event) {
        Map<ResourceLocation, IBakedModel> registry = event.getModelRegistry();
        Set<ResourceLocation> names = CamoflageBlock.getCamoBlocks().stream().map(Block::getRegistryName).collect(Collectors.toSet());
        for (ResourceLocation location : registry.keySet()) {
            ResourceLocation block = new ResourceLocation(location.getNamespace(), location.getPath());
            if (names.contains(block)) {
                IBakedModel originalModel = registry.get(location);
                registry.put(location, new CamoFactory(originalModel));
            }
        }
        FilteredHopperModelFactory.registerModel(registry);

    }

}
