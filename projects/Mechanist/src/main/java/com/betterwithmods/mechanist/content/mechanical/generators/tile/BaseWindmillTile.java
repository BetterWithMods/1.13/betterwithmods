/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.mechanical.generators.tile;

import com.betterwithmods.core.utilties.BannerUtils;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntityType;

import javax.annotation.Nonnull;

public class BaseWindmillTile extends BaseAxleGeneratorTile {

    private BannerUtils.BannerData[] sailBanners;
    private int sails;
    private int sail_cursor;


    public BaseWindmillTile(TileEntityType<?> tileEntityTypeIn, int sails) {
        super(tileEntityTypeIn);
        this.sailBanners = new BannerUtils.BannerData[sails];
        this.sails = sails;
    }

    public static final String SAIL_CURSOR = "sail_cursor";

    @Nonnull
    @Override
    public CompoundNBT write(CompoundNBT compound) {
        BannerUtils.writeArray(sailBanners, compound);
        compound.putInt(SAIL_CURSOR, sail_cursor);
        return super.write(compound);
    }

    @Override
    public void read(CompoundNBT compound) {
        BannerUtils.readArray(sailBanners, compound);
        if(compound.contains(SAIL_CURSOR)) {
            sail_cursor = compound.getInt(SAIL_CURSOR);
        }
        super.read(compound);
    }

    public BannerUtils.BannerData getBanner(int sail) {
        return sailBanners[sail];
    }

    public int getSailCount() {
        return sails;
    }

    public void setBanner(BannerUtils.BannerData data) {
        this.sailBanners[this.sail_cursor] = data;
        this.sail_cursor = (this.sail_cursor + 1) % this.sails;
    }
}
