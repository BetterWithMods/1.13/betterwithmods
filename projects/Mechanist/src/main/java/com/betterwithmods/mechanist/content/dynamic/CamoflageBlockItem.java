/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.dynamic;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;

public class CamoflageBlockItem extends BlockItem {
    public CamoflageBlockItem(Block blockIn, Properties builder) {
        super(blockIn, builder);
    }

    @Override
    public ITextComponent getDisplayName(ItemStack stack) {
        if (!stack.isEmpty()) {
            CamoflageBlock camo = (CamoflageBlock) getBlock();
            BlockState parent = camo.getParent(stack);
            if (parent != null) {
                Block parentBlock = parent.getBlock();
                ItemStack parentStack = new ItemStack(parentBlock);
                ITextComponent prefix = parentBlock.asItem().getDisplayName(parentStack);
                return prefix.appendText(" ").appendSibling(super.getDisplayName(stack));
            }
        }
        return super.getDisplayName(stack);
    }

//    @Override
//    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer playerIn, EnumHand handIn) {
//        ItemStack stack = playerIn.getHeldItem(handIn);
//        Mechanist.LOGGER.info("Item {}", stack.getTag());
//
//        return super.onItemRightClick(world, playerIn, handIn);
//    }
}
