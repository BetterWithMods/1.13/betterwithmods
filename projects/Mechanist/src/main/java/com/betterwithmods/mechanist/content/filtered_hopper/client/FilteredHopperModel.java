/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.filtered_hopper.client;

import com.betterwithmods.core.base.game.client.baking.InfoBakedModel;
import com.betterwithmods.core.base.game.client.baking.ModelInfo;
import com.betterwithmods.core.utilties.ItemUtils;
import com.betterwithmods.core.utilties.ModelUtils;
import com.google.common.collect.Lists;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.model.*;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Direction;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.client.model.data.IModelData;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Random;

public class FilteredHopperModel extends InfoBakedModel<ItemStack> {

    private IBakedModel original;

    public FilteredHopperModel(IBakedModel original, ModelInfo<ItemStack> info) {
        super(info);
        this.original = original;
    }

    @Nonnull
    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, @Nonnull Random rand, @Nonnull IModelData extraData) {
        List<BakedQuad> originalQuads = original.getQuads(state, side, rand, extraData);
        if (state != null) {
            List<BakedQuad> newQuads = Lists.newArrayList();
            for (BakedQuad quad : originalQuads) {
                if (shouldReplaceQuad(quad, extraData)) {
                    BakedQuad replacedQuad = replaceQuad(quad, extraData, state);
                    if(replacedQuad != null) {
                        newQuads.add(replacedQuad);
                    }
                } else {
                    newQuads.add(quad);
                }
            }
            return newQuads;
        }
        return originalQuads;
    }


    @Override
    public TextureAtlasSprite getParticleTexture() {
        return original.getParticleTexture();
    }

    @Override
    public ItemCameraTransforms getItemCameraTransforms() {
        return original.getItemCameraTransforms();
    }

    @Override
    public ItemOverrideList getOverrides() {
        return ItemOverrideList.EMPTY;
    }

    public boolean shouldReplaceQuad(@Nonnull BakedQuad quad, @Nonnull IModelData data) {
        return ModelUtils.isMissingTexture(quad.getSprite());
    }

    public BakedQuad replaceQuad(@Nonnull BakedQuad quad, @Nonnull IModelData data, BlockState state) {
        ItemStack filter = getInfo().apply(data);
        if(ItemUtils.isBlock(filter)) {
            return ItemUtils.getState(filter)
                    .map(filterState -> {
                        BlockRenderLayer layer = filterState.getBlock().getRenderLayer();
                        if (MinecraftForgeClient.getRenderLayer() == layer)
                            return ModelUtils.getSideSprite(filterState, Direction.NORTH);
                        return null;
                    })
                    .map(s -> (BakedQuad) new BakedQuadRetextured(quad, s))
                    .orElse(null);
        } else {
            TextureAtlasSprite sprite = ModelUtils.getSprite(filter);
            if(sprite != null) {
                if (MinecraftForgeClient.getRenderLayer() == BlockRenderLayer.CUTOUT_MIPPED)
                    return new BakedQuadRetextured(quad,sprite);
            }
        }
        return null;
    }



}
