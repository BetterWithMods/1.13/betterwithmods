/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.saw;

import com.betterwithmods.core.api.crafting.input.IInput;
import com.betterwithmods.core.api.crafting.input.IRecipeContext;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipeSerializer;
import com.betterwithmods.core.api.crafting.output.IOutput;
import com.betterwithmods.core.base.game.recipes.multiplex.state.StateMultiplexRecipe;
import com.betterwithmods.core.compat.jei.DisplayStackResult;
import com.betterwithmods.core.impl.crafting.input.BlockStateInput;
import com.betterwithmods.core.impl.crafting.output.OutputList;
import com.betterwithmods.core.utilties.EnvironmentUtils;
import com.betterwithmods.core.utilties.ItemUtils;
import com.betterwithmods.core.utilties.MiscUtils;
import com.betterwithmods.mechanist.Mechanist;
import com.betterwithmods.mechanist.Registrars;
import com.google.common.collect.Lists;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Collection;
import java.util.List;

public class SawRecipe extends StateMultiplexRecipe<SawRecipe> {

    public SawRecipe(ResourceLocation id, IInput<BlockState, SawRecipe> inputs, IOutput outputs) {
        super(Registrars.MultiplexRecipeTypes.SAW, id, inputs, outputs);
    }

    @Override
    public IMultiplexRecipeSerializer<?> getSerializer() {
        return Registrars.MultiplexRecipeSerializers.SAW;
    }

    @Override
    public void onCraft(World world, BlockPos pos, IRecipeContext<BlockState, SawRecipe> context) {
        EnvironmentUtils.playSound(world, pos, Registrars.Sounds.SAW_CUT, SoundCategory.BLOCKS, SawBlock.VOLUME.build(), SawBlock.PITCH_ON.build());
        ItemUtils.ejectStackOffset(world, pos, outputs.getOutputs(context.getState(world, pos), ItemStack.class));
    }

    @Override
    public Collection<SawRecipe> getDisplayRecipes() {

        if (outputs.isDynamic()) {
            List<SawRecipe> recipes = Lists.newArrayList();
            Collection<Ingredient> ingredients = getInput().getIngredients();
            for (Ingredient ingredient : ingredients) {
                for (ItemStack stack : ingredient.getMatchingStacks()) {
                    ItemUtils.getState(stack).ifPresent(s -> {
                                NonNullList<ItemStack> o = outputs.getOutputs(s, ItemStack.class);
                                if (!o.isEmpty()) {
                                    OutputList list = new OutputList(o.stream().map(DisplayStackResult::new).collect(MiscUtils.nonnulllist()));
                                    recipes.add(new SawRecipe(getId(), new BlockStateInput<>(stack), list));
                                } else {
                                    Mechanist.LOGGER.info("SawRecipe: {} for input {} has no outputs", getId(), stack.toString());
                                }
                            }
                    );
                }
            }
            return recipes;
        }
        return Lists.newArrayList(this);
    }
}
