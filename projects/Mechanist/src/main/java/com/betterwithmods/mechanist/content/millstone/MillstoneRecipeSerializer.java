/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.millstone;

import com.betterwithmods.core.api.crafting.input.IInput;
import com.betterwithmods.core.api.crafting.output.IOutput;
import com.betterwithmods.core.impl.Registries;
import com.betterwithmods.core.impl.crafting.multiplex.BaseMultiplexRecipeSerializer;
import com.betterwithmods.core.utilties.DeserializeUtils;
import com.google.gson.JsonObject;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.registries.ForgeRegistries;

public class MillstoneRecipeSerializer extends BaseMultiplexRecipeSerializer<MillstoneRecipe> {

    @Override
    public MillstoneRecipe read(ResourceLocation id, JsonObject json) {
        int recipeTime = JSONUtils.getInt(json, "recipeTime", 0);
        IInput<IItemHandlerModifiable, MillstoneRecipe> input = DeserializeUtils.readIInput(JSONUtils.getJsonObject(json, "input"));
        if (input.size() > 3) {
            throw new IllegalStateException("Millstone Recipes can only have 3 inputs:" + id);
        }
        JsonObject resultObject = JSONUtils.getJsonObject(json, "result");
        IOutput outputs = DeserializeUtils.readIOutput(resultObject);
        SoundEvent sound = Registries.get(ForgeRegistries.SOUND_EVENTS, new ResourceLocation(JSONUtils.getString(json, "sound", ""))).orElse(null);
        return new MillstoneRecipe(id, input, outputs, recipeTime, sound);
    }


    @Override
    public MillstoneRecipe read(ResourceLocation id, PacketBuffer buffer) {
        int recipeTime = buffer.readInt();
        IInput<IItemHandlerModifiable, MillstoneRecipe> input = DeserializeUtils.readIInput(buffer);
        IOutput outputs = DeserializeUtils.readIOutput(buffer);
        SoundEvent sound = null;
        if (buffer.readBoolean()) {
            sound = Registries.get(ForgeRegistries.SOUND_EVENTS, buffer.readResourceLocation()).orElse(null);
        }
        return new MillstoneRecipe(id, input, outputs, recipeTime, sound);
    }

    @Override
    public void write(PacketBuffer buffer, MillstoneRecipe recipe) {
        buffer.writeInt(recipe.getRecipeTime());
        DeserializeUtils.writeIInput(buffer, recipe.getInput());
        DeserializeUtils.writeIOutput(buffer, recipe.getOutput());
        boolean sounds = recipe.getSound() != null;
        buffer.writeBoolean(sounds);
        if (sounds) {
            buffer.writeResourceLocation(recipe.getSound().getName());
        }
    }


}
