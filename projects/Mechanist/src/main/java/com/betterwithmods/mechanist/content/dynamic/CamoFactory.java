/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.dynamic;

import com.betterwithmods.core.base.game.client.baking.InfoBakedModel;
import com.betterwithmods.core.base.game.client.baking.ModelFactory;
import com.betterwithmods.core.base.game.client.baking.ModelInfo;
import com.betterwithmods.core.utilties.ItemUtils;
import com.betterwithmods.core.utilties.ModelUtils;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.renderer.model.*;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.data.IModelData;
import net.minecraftforge.client.model.data.ModelProperty;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Random;

public class CamoFactory extends ModelFactory<BlockState> {
    private static final FactoryItemOverrideList<BlockState> OVERRIDE_LIST = new FactoryItemOverrideList<>();

    public static final ModelProperty<BlockState> PARENT_STATE = new ModelProperty<>();

    private IBakedModel sourceModel;

    public CamoFactory(IBakedModel sourceModel) {
        //TODO particles
        super(PARENT_STATE, new ResourceLocation("minecraft:block/stone"));
        this.sourceModel = sourceModel;
    }

    @Override
    public IBakedModel bake(@Nonnull ModelInfo<BlockState> info) {
        return new BakedCamo(sourceModel, info);
    }

    @Override
    public ModelInfo<BlockState> fromItemStack(ItemStack stack) {
        BlockState state = ItemUtils.getBlock(stack).orElse(Blocks.AIR).getDefaultState();
        CompoundNBT tag = stack.getChildTag("BlockEntityTag");
        if (tag != null && tag.contains("parent")) {
            CompoundNBT parentTag = tag.getCompound("parent");
            BlockState parentState = NBTUtil.readBlockState(parentTag);
            return new ModelInfo<>(state, d -> parentState, true);
        }
        return new ModelInfo<>(state, d -> Blocks.AIR.getDefaultState(), true);
    }

    @Override
    public ItemOverrideList getOverrides() {
        return OVERRIDE_LIST;
    }

    @Override
    public ItemCameraTransforms getItemCameraTransforms() {
        return sourceModel.getItemCameraTransforms();
    }

    private class BakedCamo extends InfoBakedModel<BlockState> {

        private final IBakedModel sourceModel;

        private BakedCamo(IBakedModel sourceModel, ModelInfo<BlockState> info) {
            super(info);
            this.sourceModel = sourceModel;
        }

        @Nonnull
        @Override
        public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, @Nonnull Random rand, @Nonnull IModelData extraData) {
            List<BakedQuad> sourceQuads = sourceModel.getQuads(state, side, rand, extraData);
            BlockState parent = getInfo().apply(extraData);
            //Render base model with parent is missing
            if (parent == null) {
                return ImmutableList.of();
            }

            List<BakedQuad> newQuads = Lists.newArrayList();
            for (BakedQuad quads : sourceQuads) {
                TextureAtlasSprite sprite = ModelUtils.getSideSprite(parent, quads.getFace());
                BakedQuadRetextured newQuad = new BakedQuadRetextured(quads, sprite);
                newQuads.add(newQuad);
            }
            return newQuads;
        }

        @Override
        public TextureAtlasSprite getParticleTexture() {
            return sourceModel.getParticleTexture();
        }

        @Override
        public ItemCameraTransforms getItemCameraTransforms() {
            return sourceModel.getItemCameraTransforms();
        }

        @Override
        public ItemOverrideList getOverrides() {
            return ItemOverrideList.EMPTY;
        }
    }

}
