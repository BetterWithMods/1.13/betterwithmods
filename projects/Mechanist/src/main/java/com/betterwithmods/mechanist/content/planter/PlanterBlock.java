/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.planter;

import com.betterwithmods.core.Tags;
import com.betterwithmods.core.base.game.block.BlockBase;
import com.betterwithmods.core.utilties.EntityUtils;
import com.betterwithmods.core.utilties.EnvironmentUtils;
import com.betterwithmods.core.utilties.PlayerUtils;
import com.betterwithmods.core.utilties.VoxelUtils;
import com.google.common.collect.Maps;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.PlantType;

import java.util.Map;

public class PlanterBlock extends BlockBase {

    private static final Map<PlanterType, PlanterBlock> MAP = Maps.newHashMap();
    private final PlanterType type;

    public PlanterBlock(PlanterType type, Properties properties) {
        super(properties);
        this.type = type;
        MAP.put(type, this);
        setRenderLayer(BlockRenderLayer.CUTOUT);

    }

    public PlanterType getType() {
        return this.type;
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader world, BlockPos pos, ISelectionContext context) {
        PlanterType type = getType();
        return type.getShape();
    }

    @Override
    public boolean canSustainPlant(BlockState state, IBlockReader world, BlockPos pos, Direction facing, IPlantable plantable) {
        PlantType type = plantable.getPlantType(world, pos.offset(facing));
        PlanterType planterType = getType();
        for (PlantType availableType : planterType.getPlantTypes()) {
            if (availableType == type)
                return true;
        }
        return false;
    }

    @Override
    public boolean isFertile(BlockState state, IBlockReader world, BlockPos pos) {
        PlanterType type = getType();
        return type == PlanterType.FARMLAND || type == PlanterType.FERTILIZED_FARMLAND;
    }

    public boolean setType(BlockState state, World world, BlockPos pos, PlanterType type) {
        Block block = state.getBlock();
        if (block instanceof PlanterBlock) {
            PlanterType currentType = ((PlanterBlock) block).getType();
            if (currentType != type) {
                return world.setBlockState(pos, MAP.get(type).getDefaultState());
            }
        }
        return false;
    }

    @Override
    public boolean onBlockActivated(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockRayTraceResult hit) {
        PlanterType currentType = getType();
        if (currentType != null) {
            return currentType.onBlockActivated(state, world, pos, player, hand);
        }
        return false;
    }


    @Override
    public Material getMaterial(BlockState state) {
        if (type == PlanterType.WATER) {
            return Material.ICE;
        }
        return super.getMaterial(state);
    }

    @Override
    public void onEntityCollision(BlockState state, World world, BlockPos pos, Entity entity) {
        if (type == PlanterType.WATER) {
            if (entity instanceof PlayerEntity) {

                if(PlayerUtils.isHolding((PlayerEntity) entity, Tags.Items.SOAP)) {
                    EnvironmentUtils.forEachParticle(world, pos.up(), ParticleTypes.BUBBLE_COLUMN_UP, 40, EnvironmentUtils.RANDOM_PARTICLE, EnvironmentUtils.SPEED);
                }
                EntityUtils.doWaterSplashEffect(entity);
            }
        }
    }


    public static final VoxelShape INSIDE = Block.makeCuboidShape(2, 2, 2, 14, 16, 14);
    public static final VoxelShape PLANTER;

    static {
        VoxelShape planter_a = Block.makeCuboidShape(2, 0, 2, 14, 16.0D, 14.0D);
        VoxelShape planter_b = Block.makeCuboidShape(1, 2, 1, 15, 15, 15);
        VoxelShape planter_c = Block.makeCuboidShape(0, 15, 0, 16, 16, 16);
        PLANTER = VoxelUtils.or(planter_a, planter_b, planter_c);
    }
}
