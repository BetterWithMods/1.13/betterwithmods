/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.turntable;

import com.betterwithmods.core.base.game.block.BlockBase;
import com.betterwithmods.core.utilties.EnvironmentUtils;
import com.betterwithmods.core.utilties.PlayerUtils;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.EnumProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;

public class TurntableBlock extends BlockBase {
    private static final BooleanProperty POWERED = BlockStateProperties.POWERED;
    public static final EnumProperty<TickRate> TICK_RATE = EnumProperty.create("tickrate", TickRate.class);

    public TurntableBlock(Block.Properties properties) {
        super(properties);
        setDefaultState(getDefaultState().with(POWERED, false).with(TICK_RATE, TickRate.ONE));
        setRenderLayer(BlockRenderLayer.CUTOUT);
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(POWERED).add(TICK_RATE);
    }

    @Override
    public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if(handIn == Hand.MAIN_HAND) {
            if (PlayerUtils.emptyHands(player) && player.isSneaking()) {
                if (state.has(TICK_RATE)) {
                    worldIn.setBlockState(pos, state.with(TICK_RATE, state.get(TICK_RATE).next()));
                    EnvironmentUtils.playSound(worldIn, pos, SoundEvents.BLOCK_WOODEN_BUTTON_CLICK_ON, SoundCategory.BLOCKS, 0.3F, 0.7F);
                    return true;
                }
            }
        }
        return super.onBlockActivated(state, worldIn, pos, player, handIn, hit);
    }

    public enum TickRate implements IStringSerializable {
        ONE(10),
        TWO(20),
        THREE(40),
        FOUR(80);

        private int ticks;
        private static final TickRate[] VALUES = values();

        TickRate(int ticks) {
            this.ticks = ticks;
        }

        public int getTicks() {
            return ticks;
        }

        public TickRate next() {
            return VALUES[(ordinal() + 1) % 4];
        }

        @Override
        public String getName() {
            return name().toLowerCase();
        }
    }
}
