/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.filtered_hopper.recipes;

import com.betterwithmods.core.api.crafting.input.IInput;
import com.betterwithmods.core.api.crafting.input.IInputSerializer;
import com.betterwithmods.core.api.crafting.input.IRecipeContext;
import com.betterwithmods.core.utilties.EnvironmentUtils;
import com.betterwithmods.core.utilties.FloatBuilder;
import com.betterwithmods.core.utilties.inventory.InventoryUtils;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class HopperInput implements IInput<HopperState, HopperRecipe> {
    /**
     * Predicate for indicating the required filter slot. Null indicates that an empty filter is required.
     */
    @Nullable
    private Ingredient filter;

    private Ingredient input;

    private Action action;

    public HopperInput(Ingredient filter, Ingredient input, Action action) {
        this.filter = filter;
        this.input = input;
        this.action = action;
    }

    private boolean isValidFilter(ItemStack filterStack) {
        if (this.filter == null && filterStack.isEmpty()) {
            return true;
        } else if (this.filter != null && this.filter.test(filterStack)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean matches(IWorld world, BlockPos pos, HopperRecipe recipe, IRecipeContext<HopperState, HopperRecipe> context) {
        HopperState state = context.getState(world, pos);
        //Matches current filter
        if (isValidFilter(state.getFilter())) {
            ItemStack input = state.getItemEntity().getItem();
            //Item entity is a valid input
            return getIngredients().stream().anyMatch(ingredient -> ingredient.test(input));
        }
        return false;

    }

    @Override
    public NonNullList<Ingredient> getIngredients() {
        NonNullList<Ingredient> inputs = NonNullList.create();
        inputs.add(input);
        if(filter != null) {
            inputs.add(filter);
        }
        return inputs;
    }

    public static final FloatBuilder PITCH = FloatBuilder.builder(1.25f).addRandom(0.1f);


    @Override
    public void consume(World world, BlockPos pos, HopperRecipe recipe, IRecipeContext<HopperState, HopperRecipe> context) {
        HopperState state = context.getState(world, pos);
        ItemEntity entity = state.getItemEntity();

        HopperInput input = recipe.getInput();
        Action action = input.getAction();

        ItemStack stack = entity.getItem();
        if (action == Action.PERMIT) {
            int count = InventoryUtils.getStackCount(getIngredients(), stack, stack.getCount());
            ItemStack addition = stack.split(count);

            if (InventoryUtils.addEntityItem(state.getInventory(), entity, addition)) {
                EnvironmentUtils.playSound(world, pos, SoundEvents.ENTITY_ITEM_PICKUP, SoundCategory.BLOCKS, 0.75f, PITCH.build());
            }
        } else if (action == Action.CONSUME) {
            int count = InventoryUtils.getStackCount(getIngredients(), stack, 1);
            stack.shrink(count);
            EnvironmentUtils.playSound(world, pos, SoundEvents.ENTITY_ITEM_PICKUP, SoundCategory.BLOCKS, 0.75f, PITCH.build());//TODO sound?
        }
    }

    @Override
    public IInputSerializer<?, ?> getSerializer() {
        return Registrars.InputSerializers.FILTERED_HOPPER;
    }

    public Ingredient getFilter() {
        return filter;
    }

    public Ingredient getInput() {
        return input;
    }

    public Action getAction() {
        return action;
    }

    public enum Action implements IStringSerializable {
        CONSUME,
        PERMIT,
        DENY;

        @Override
        public String getName() {
            return name().toLowerCase();
        }

        public static Action getAction(String action) {
            return Action.valueOf(action.toUpperCase());
        }
    }
}
