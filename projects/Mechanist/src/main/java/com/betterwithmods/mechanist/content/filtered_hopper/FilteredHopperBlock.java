/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.filtered_hopper;

import com.betterwithmods.core.api.blocks.IUrnConnector;
import com.betterwithmods.core.base.game.block.BlockBase;
import com.betterwithmods.core.utilties.VoxelUtils;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

public class FilteredHopperBlock extends BlockBase implements IUrnConnector {

    public static final BooleanProperty POWERED = BlockStateProperties.POWERED;
    public static final BooleanProperty FILTERED = BooleanProperty.create("filtered");

    public FilteredHopperBlock(Properties properties) {
        super(properties);
        setDefaultState(getDefaultState().with(POWERED, false).with(FILTERED, false));
    }

    @Override
    public boolean onBlockActivated(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if (!world.isRemote()) {
            NetworkHooks.openGui((ServerPlayerEntity) player, getContainerProvider(world, pos), pos);
        }
        return true;
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(POWERED, FILTERED);
    }

    private static final VoxelShape
            SHAPE = VoxelUtils.or(
                    Block.makeCuboidShape(5, 0, 5, 11, 4, 11),
                    Block.makeCuboidShape(0, 4, 0, 16, 5, 16),
                    Block.makeCuboidShape(0, 5, 0, 1, 16, 16),
                    Block.makeCuboidShape(15, 5, 0, 16, 16, 16),
                    Block.makeCuboidShape(1, 5, 15, 15, 16, 16),
                    Block.makeCuboidShape(1, 5, 0, 15, 16, 1)
            ),
            FILTERED_SHAPE = VoxelUtils.or(
                    Block.makeCuboidShape(5, 0, 5, 11, 4, 11),
                    Block.makeCuboidShape(0, 4, 0, 16, 16, 16)
            );


    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return state.get(FILTERED) ? FILTERED_SHAPE : SHAPE;
    }

    @Override
    public boolean canRenderInLayer(BlockState state, BlockRenderLayer layer) {
        return true;
    }

    @Override
    public boolean canConnect(IWorldReader worldReader, BlockPos pos) {
        return true;
    }
}
