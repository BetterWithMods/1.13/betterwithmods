/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.mechanical.generators.render;

import com.betterwithmods.core.References;
import com.betterwithmods.core.utilties.BannerUtils;
import com.betterwithmods.mechanist.content.mechanical.generators.tile.VerticalWindmillTile;
import net.minecraft.util.ResourceLocation;

import javax.vecmath.Vector3f;

public class VerticalWindmillRenderer extends BaseWindmillRenderer<VerticalWindmillTile> {

    private static ResourceLocation STRUTS = new ResourceLocation(References.MODID_MECHANIST, "vertical_windmill_struts");
    private static ResourceLocation SAILS = new ResourceLocation(References.MODID_MECHANIST, "vertical_windmill_sails");

    private static ResourceLocation STRUTS_TEXTURE = new ResourceLocation(References.MODID_MECHANIST, "textures/entity/vertical_windmill.png");

    private static ResourceLocation SAILS_TEXTURE = new ResourceLocation(References.MODID_MECHANIST, "textures/entity/vertical_windmill_sails.png");

    @Override
    public void renderSail(VerticalWindmillTile tile, int sail) {
        BannerUtils.BannerData banner = tile.getBanner(sail);
        if (banner != null) {
            bindTexture(banner.getTexture(WINDMILLS));
        } else {
            bindTexture(SAILS_TEXTURE);
        }
        get(SAILS).render(0.0625f);
    }

    @Override
    public void renderStructure() {
        bindTexture(STRUTS_TEXTURE);
        get(STRUTS).render(0.0625F);
    }

    @Override
    public Vector3f getRotationAxes() {
        return new Vector3f(0, 1, 0);
    }
}
