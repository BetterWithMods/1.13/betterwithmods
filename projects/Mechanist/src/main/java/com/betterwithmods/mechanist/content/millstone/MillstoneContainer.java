/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.millstone;

import com.betterwithmods.core.base.game.container.InventoryContainer;
import com.betterwithmods.core.base.game.container.SlotTransformation;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.IIntArray;
import net.minecraft.util.IntArray;
import net.minecraftforge.items.ItemStackHandler;

public class MillstoneContainer extends InventoryContainer {

    public MillstoneContainer(int id, PlayerEntity player) {
        this(id, player, new ItemStackHandler(3), new IntArray(3));

    }

    public MillstoneContainer(int id, PlayerEntity player, ItemStackHandler tile, IIntArray data) {
        super(Registrars.Containers.MILLSTONE, id, player, tile, data);
        this.data = data;
        addSlots(CONTAINER, inventory, 3, 1, 0, 62, 43);
        addPlayerInventory(0, 8, 76, 58);
        addSlotTransformation(new SlotTransformation(PLAYER, CONTAINER));
        addSlotTransformation(new SlotTransformation(CONTAINER,PLAYER));
    }

    public boolean isPowered() {
        return this.data.get(2) > 0;
    }

    public double recipeProgress() {
        double current = this.data.get(0);
        double max = this.data.get(1);
        return max != 0 && current != 0 ? current / max : 0.0;
    }

}
