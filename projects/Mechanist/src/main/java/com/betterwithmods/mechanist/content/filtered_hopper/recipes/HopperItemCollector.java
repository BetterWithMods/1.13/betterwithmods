/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.filtered_hopper.recipes;

import com.betterwithmods.core.impl.crafting.multiplex.MultiplexRecipeManager;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.handlers.ItemCollector;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.World;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;

public class HopperItemCollector extends ItemCollector {
    public HopperItemCollector(VoxelShape collectionArea, LazyOptional<? extends IItemHandler> inventory) {
        super(collectionArea, inventory);
    }

    @Override
    public void pickupItem(World world, BlockPos pos, ItemEntity entity, IItemHandler inventory) {
        HopperContext context = new HopperContext(entity);

        MultiplexRecipeManager.get(world.getServer())
                .ifPresent( m -> m.craft(Registrars.MultiplexRecipeTypes.FILTERED_HOPPER, world, pos, context));
    }
}
