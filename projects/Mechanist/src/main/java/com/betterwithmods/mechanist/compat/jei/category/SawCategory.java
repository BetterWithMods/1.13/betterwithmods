/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.compat.jei.category;

import com.betterwithmods.core.References;
import com.betterwithmods.core.api.crafting.result.IResult;
import com.betterwithmods.core.compat.jei.CoreJEI;
import com.betterwithmods.core.compat.jei.category.AbstractBlockStateMultiplexCategory;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.compat.jei.MechanistJEI;
import com.betterwithmods.mechanist.content.saw.SawRecipe;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.ingredient.IGuiIngredientGroup;
import mezz.jei.api.gui.ingredient.IGuiItemStackGroup;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.item.ItemStack;

import javax.annotation.Nonnull;

public class SawCategory extends AbstractBlockStateMultiplexCategory<SawRecipe> {

    public SawCategory(IGuiHelper guiHelper) {
        super(guiHelper, guiHelper.createDrawable(MechanistJEI.Constants.SAW_BACKGROUND, 0, 0, 108, 35),
                String.format("jei.%s.saw", References.MODID_MECHANIST),
                guiHelper.createDrawableIngredient(new ItemStack(Registrars.Blocks.SAW)),
                MechanistJEI.Constants.SAW_CATEGORY);
    }

    @Nonnull
    @Override
    public Class<? extends SawRecipe> getRecipeClass() {
        return SawRecipe.class;
    }

    @Override
    public void setRecipe(@Nonnull IRecipeLayout layout, @Nonnull SawRecipe recipe, @Nonnull IIngredients ingredients) {
        IGuiItemStackGroup guiItemStacks = layout.getItemStacks();
        IGuiIngredientGroup<IResult<ItemStack>> guiResults = layout.getIngredientsGroup(CoreJEI.STACK_RESULT);

        createSlot(guiItemStacks, true, 0, 1,1);
        createSlotsHorizontal(guiResults, false, 3, 3, 54, 10);

        guiItemStacks.set(ingredients);
        guiResults.set(ingredients);


    }
}
