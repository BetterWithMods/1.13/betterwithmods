/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.turntable;

import com.betterwithmods.core.base.game.recipes.multiplex.TimedRecipeContext;
import com.betterwithmods.core.utilties.EnvironmentUtils;
import com.betterwithmods.core.utilties.FloatBuilder;
import net.minecraft.block.BlockState;
import net.minecraft.particles.BlockParticleData;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

import java.util.concurrent.atomic.AtomicInteger;

public class TurntableRecipeContext extends TimedRecipeContext<BlockState, TurntableRecipe> {

    public TurntableRecipeContext(AtomicInteger recipeTime) {
        super(recipeTime);
    }

    @Override
    public BlockState getState(IWorld world, BlockPos pos) {
        return world.getBlockState(pos);
    }
    public static final FloatBuilder PITCH = FloatBuilder.builder(0.8f).addRandom(0.1f);
    @Override
    public boolean canCraft(TurntableRecipe recipe, World world, BlockPos pos) {
        BlockState state = world.getBlockState(pos);
        EnvironmentUtils.playSound(world, pos, state.getSoundType().getBreakSound(), SoundCategory.BLOCKS, 0.5f, PITCH.build());
        double y = state.getShape(world,pos).getBoundingBox().maxY;
        ((ServerWorld)world).spawnParticle(new BlockParticleData(ParticleTypes.BLOCK, state), pos.getX() + 0.5, pos.getY() + y, pos.getZ() + 0.5, 30, 0.0D, 0.0D, 0.0D, (double)0.15F);
        return super.canCraft(recipe, world, pos);
    }
}
