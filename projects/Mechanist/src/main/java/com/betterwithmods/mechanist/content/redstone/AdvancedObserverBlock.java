/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.redstone;

import net.minecraft.block.BlockState;
import net.minecraft.block.ObserverBlock;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

public class AdvancedObserverBlock extends ObserverBlock {

    public AdvancedObserverBlock(Properties builder) {
        super(builder);
    }

    @Override
    public BlockState updatePostPlacement(BlockState stateIn, Direction facing, BlockState facingState, IWorld world, BlockPos currentPos, BlockPos facingPos) {
        if (stateIn.get(FACING) != facing && !stateIn.get(POWERED)) {
            this.startSignal(world, currentPos);
        }
        return stateIn;
    }

    private void startSignal(IWorld world, BlockPos pos) {
        if (!world.isRemote() && !world.getPendingBlockTicks().isTickScheduled(pos, this)) {
            world.getPendingBlockTicks().scheduleTick(pos, this, 2);
        }
    }

    @Override
    protected void updateNeighborsInFront(World world, BlockPos pos, BlockState state) {
        Direction front = state.get(FACING);
        for (Direction facing : Direction.values()) {
            if (facing != front) {
                BlockPos blockpos = pos.offset(facing.getOpposite());
                world.neighborChanged(blockpos, this, pos);
                world.notifyNeighborsOfStateExcept(blockpos, this, front);
            }
        }
    }


    @Override
    public int getWeakPower(BlockState state, IBlockReader blockAccess, BlockPos pos, Direction side) {
        return state.get(POWERED) && state.get(FACING) == side.getOpposite() ? 15 : 0;
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        Direction facing = context.getNearestLookingDirection().getOpposite();
        return this.getDefaultState().with(FACING, facing);
    }

}
