/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.detector;

import com.betterwithmods.core.api.BWMApi;
import com.betterwithmods.core.base.game.tile.TileBase;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class DetectorTile extends TileBase implements ITickableTileEntity {

    private int delay = 0;

    public DetectorTile() {
        super(Registrars.Tiles.DETECTOR);
    }

    @Override
    public void tick() {
        World world = getWorld();
        if (world != null && !world.isRemote()) {
            if (delay <= 0) {
                BlockState state = world.getBlockState(pos);
                if (state.has(DetectorBlock.FACING)) {
                    Direction facing = state.get(DetectorBlock.FACING);
                    BlockPos pos = getPos();
                    BlockPos offset = pos.offset(facing);
                    boolean detected = BWMApi.DETECTION_HANDLER_REGISTRY.detect(facing, world, pos, offset);
                    DetectorBlock.setLit(world, pos, state, detected);
                }
                delay = 4;
            }
            delay = Math.max(0, delay - 1);
        }

    }

    private static final String DELAY = "delay";

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        compound.putInt(DELAY, delay);
        return super.write(compound);
    }

    @Override
    public void read(CompoundNBT compound) {
        if (compound.contains(DELAY)) {
            delay = compound.getInt(DELAY);
        }
        super.read(compound);
    }


}
