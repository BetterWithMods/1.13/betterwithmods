/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.planter;

import com.betterwithmods.core.base.game.block.BlockBase;
import com.betterwithmods.core.utilties.VoxelUtils;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;

public class UnfiredPlanterBlock extends BlockBase {

    public UnfiredPlanterBlock(Properties properties) {
        super(properties);
    }

    public static final VoxelShape INSIDE = Block.makeCuboidShape(2, 2, 2, 14, 16, 14);
    public static final VoxelShape PLANTER;

    static {
        VoxelShape planter_a = Block.makeCuboidShape(2, 0, 2, 14, 16.0D, 14.0D);
        VoxelShape planter_b = Block.makeCuboidShape(1, 2, 1, 15, 15, 15);
        VoxelShape planter_c = Block.makeCuboidShape(0, 15, 0, 16, 16, 16);
        PLANTER = VoxelShapes.combineAndSimplify(VoxelUtils.or(planter_a, planter_b, planter_c), INSIDE, IBooleanFunction.ONLY_FIRST);
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return PLANTER;
    }
}
