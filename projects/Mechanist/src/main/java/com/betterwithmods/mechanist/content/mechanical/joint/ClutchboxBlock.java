/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.mechanical.joint;

import com.betterwithmods.core.utilties.WorldUtils;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class ClutchboxBlock extends MechanicalJointBlock {

    public static final BooleanProperty LIT = BlockStateProperties.LIT;

    public ClutchboxBlock(Properties properties) {
        super(properties);
        setDefaultState(getDefaultState().with(LIT, false));
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(LIT);
    }

    private boolean isCurrentlyValid(World world, BlockPos pos) {
        boolean lit = isLit(world, pos);
        boolean powered = WorldUtils.isPowered(world, pos);
        return lit == powered;
    }

    private boolean isLit(IBlockReader world, BlockPos pos) {
        BlockState state = world.getBlockState(pos);
        if(state.getBlock() == this) {
            return state.get(LIT);
        }
        return false;
    }

    @Override
    public void onChanged(World world, BlockPos pos, BlockState state) {
        if (!world.isRemote) {
            if (!isCurrentlyValid(world, pos)) {
                boolean lit = isLit(world, pos);
                world.setBlockState(pos, state.with(LIT, !lit), 2);
            }
        }
        super.onChanged(world, pos, state);
    }

    public int tickRate(IWorldReader world) {
        return 4;
    }


    @Override
    public boolean canConnectRedstone(BlockState state, IBlockReader world, BlockPos pos, @Nullable Direction side) {
        return side != state.get(FACING);
    }

}
