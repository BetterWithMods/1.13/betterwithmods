/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.filtered_hopper.recipes.result;

import com.betterwithmods.core.api.IPositionedText;
import com.betterwithmods.core.api.crafting.result.IResult;
import com.betterwithmods.core.api.crafting.result.IResultRenderer;
import com.betterwithmods.core.api.crafting.result.IResultSerializer;
import com.betterwithmods.core.impl.crafting.ItemStackBuilder;
import com.betterwithmods.core.impl.crafting.result.EjectorResultRenderer;
import com.betterwithmods.core.impl.crafting.result.count.Digit;
import com.betterwithmods.core.utilties.ItemUtils;
import com.betterwithmods.core.utilties.StackEjector;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

import javax.annotation.Nullable;
import java.util.List;

public class EjectResult implements IResult<StackEjector> {

    protected StackEjector ejector;
    protected ItemStackBuilder builder;

    public EjectResult(IItemProvider item) {
        this(item, 1);
    }

    public EjectResult(IItemProvider item, int count) {
        this(ItemStackBuilder.builder().item(item).count(new Digit(count)));
    }

    public EjectResult(ItemStack stack) {
        this(ItemStackBuilder.builder().fromStack(stack));
    }

    public EjectResult(ItemStackBuilder builder) {
        this(builder, ItemUtils.EJECT_OFFSET);
    }

    public EjectResult(ItemStackBuilder builder, StackEjector ejector) {
        this.ejector = ejector;
        this.builder = builder;
    }

    @Override
    public IResultSerializer<?> getSerializer() {
        return Registrars.ResultSerializers.EJECT;
    }

    @Override
    public <T> StackEjector get(@Nullable T input) {
        return ejector.setStack(builder.build());
    }

    public ItemStackBuilder getBuilder() {
        return builder;
    }

    @Override
    public boolean equals(IResult result) {
        if (result instanceof EjectResult) {
            return this.builder.equals(((EjectResult) result).builder);
        }
        return false;
    }

    @Override
    public EjectResult copy() {
        return new EjectResult(builder, ejector);
    }

    @Override
    public ItemStack displayStack() {
        return builder.displayStack();
    }

    @Override
    public ITextComponent displayText() {
        return displayStack().getTextComponent();
    }

    public List<IPositionedText> displayCount() {
        return builder.getCountProvider().getText();
    }

    @Override
    public ResourceLocation getId() {
        return builder.getItem().getRegistryName();
    }

    @Override
    public Class<StackEjector> getType() {
        return StackEjector.class;
    }


    @Override
    public IResultRenderer<StackEjector> getRenderer() {
        return EjectorResultRenderer.INSTANCE;
    }
}
