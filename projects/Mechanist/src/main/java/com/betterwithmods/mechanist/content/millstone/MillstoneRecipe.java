/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.millstone;

import com.betterwithmods.core.api.crafting.input.IInput;
import com.betterwithmods.core.api.crafting.input.IRecipeContext;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipeSerializer;
import com.betterwithmods.core.api.crafting.output.IOutput;
import com.betterwithmods.core.base.game.recipes.multiplex.inventory.TimedInventoryMultiplexRecipe;
import com.betterwithmods.core.utilties.EnvironmentUtils;
import com.betterwithmods.core.utilties.ItemUtils;
import com.betterwithmods.core.utilties.StackEjector;
import com.betterwithmods.core.utilties.VectorBuilder;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.items.IItemHandlerModifiable;

public class MillstoneRecipe extends TimedInventoryMultiplexRecipe<MillstoneRecipe> {

    private SoundEvent sound;

    public MillstoneRecipe(ResourceLocation id, IInput<IItemHandlerModifiable, MillstoneRecipe> inputs, IOutput outputs, Integer recipeTime, SoundEvent sound) {
        super(Registrars.MultiplexRecipeTypes.MILLSTONE, id, inputs, outputs, recipeTime);
        this.sound = sound;
    }

    public static StackEjector EJECTOR = new StackEjector(
            new VectorBuilder(),
            new VectorBuilder().setGaussian(0.05f));

    @Override
    public IMultiplexRecipeSerializer<?> getSerializer() {
        return Registrars.MultiplexRecipeSerializers.MILLSTONE;
    }

    @Override
    public void craft(World world, BlockPos pos, IRecipeContext<IItemHandlerModifiable, MillstoneRecipe> context) {
        //Play sounds
        if(this.getSound() != null) {
            EnvironmentUtils.playRandomSound(world, pos, getSound(), SoundCategory.BLOCKS, MillstoneTile.VOLUME.build(), MillstoneTile.PITCH.build(), 0.05d);
        }

        if (context.canCraft(this, world, pos)) {
            BlockPos position = offset(world, pos);
            onCraft(world, position, context);
            consume(world, pos, context);
        }

    }

    @Override
    public void onCraft(World world, BlockPos pos, IRecipeContext<IItemHandlerModifiable, MillstoneRecipe> context) {
        ItemUtils.ejectAll(EJECTOR, world, pos, outputs.getOutputs(context.getState(world,pos), ItemStack.class));
    }

    private BlockPos offset(World world, BlockPos pos) {
        float angle = (float) Math.toRadians((world.rand.nextInt(9) * 45));
        double x = MathHelper.sin(angle);
        double z = MathHelper.cos(angle);
        BlockPos newPos = pos.add(new BlockPos(x, 0.5, z));
        if (world.getBlockState(newPos).getMaterial().isReplaceable()) {
            return newPos;
        }
        return offset(world, pos);
    }


    public SoundEvent getSound() {
        return sound;
    }
}
