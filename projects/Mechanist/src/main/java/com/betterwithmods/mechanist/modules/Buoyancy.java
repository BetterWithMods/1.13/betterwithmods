/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.modules;

import com.betterwithmods.core.Tags;
import com.betterwithmods.core.base.game.events.ItemEntityFloatEvent;
import com.betterwithmods.core.base.setup.ModuleBase;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.OptionalInt;

public class Buoyancy extends ModuleBase {


    private static final int FLOATING = 1, EQUILIBRIUM = 0, SINKS = -1;

    private ForgeConfigSpec.IntValue defaultBuoyancy;

    public Buoyancy() {
        super("buoyancy");
    }


    @SubscribeEvent
    public void onItemFloat(ItemEntityFloatEvent event) {
        updateBuoy(event.getItemEntity());
        event.setCanceled(true);
    }

    @Override
    public void configs(ForgeConfigSpec.Builder builder) {
        this.defaultBuoyancy = builder.comment("Default buoyancy: sinks=-1, equilibrium=0, floating=1. Only applies for items that are not one of the 3 tags.")
                .translation("config.betterwithmods_mechanist.defaultBuoyancy")
                .defineInRange("defaultBuoyancy", SINKS, SINKS, FLOATING);
    }

    @Override
    public String getDescription() {
        return "";
    }

    private OptionalInt getBuoyancy(ItemStack stack) {
        Item item = stack.getItem();
        if (item.isIn(Tags.Items.FLOATS)) {
            return OptionalInt.of(FLOATING);
        } else if (item.isIn(Tags.Items.EQUILIBRIUM)) {
            return OptionalInt.of(EQUILIBRIUM);
        } else if (item.isIn(Tags.Items.SINKS)) {
            return OptionalInt.of(SINKS);
        }
        return OptionalInt.of(defaultBuoyancy.get());
    }

    private void updateBuoy(ItemEntity entity) {
        OptionalInt buoyancy = getBuoyancy(entity.getItem());
        if (buoyancy.isPresent()) {
            Vec3d motion = entity.getMotion();
            double scale = buoyancy.getAsInt();
            Vec3d vec = new Vec3d(0, (Math.abs(motion.getY()) < 0.06) ? scale * 5.0E-4F : 0, 0);
            entity.setMotion(motion.add(vec).mul(0.99F, 1, 0.99F));
        }
    }


}
