/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.rope;

import com.betterwithmods.mechanist.Registrars;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;

public class RopeHandler {

    public static BlockPos findLowestRope(IWorldReader world, BlockPos pos) {
        BlockPos down = pos.down();
        Block below = world.getBlockState(down).getBlock();
        if (below == Registrars.Blocks.ROPE) {
            return findLowestRope(world, down);
        } else {
            return pos;
        }
    }

    public static boolean placeLowestRope(World world, BlockPos pos, PlayerEntity player, ItemStack stack) {

        BlockState state = world.getBlockState(pos);

        BlockPos lowest = findLowestRope(world, pos).down();
        if (world.getBlockState(lowest).getMaterial().isReplaceable() && state.getBlock().isValidPosition(state, world, lowest)) {
            world.setBlockState(lowest, Registrars.Blocks.ROPE.getDefaultState());
            world.playSound(null, lowest, SoundType.PLANT.getPlaceSound(), SoundCategory.BLOCKS, 1, 1);
            if (player != null && !player.isCreative())
                stack.shrink(1);
            return true;
        }
        return false;
    }

}
