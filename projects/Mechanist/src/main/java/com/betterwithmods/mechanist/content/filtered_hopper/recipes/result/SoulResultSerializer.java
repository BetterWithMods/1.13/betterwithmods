/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.filtered_hopper.recipes.result;

import com.betterwithmods.core.api.souls.ISoulStack;
import com.betterwithmods.core.api.souls.SoulType;
import com.betterwithmods.core.impl.crafting.result.BaseResultSerializer;
import com.betterwithmods.core.impl.souls.SoulStack;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;

public class SoulResultSerializer extends BaseResultSerializer<SoulResult> {

    @Override
    public SoulResult read(JsonElement json) {
        JsonObject object = json.getAsJsonObject();
//        ICount count = DeserializeUtils.readCount(json);
//        if (count != null) {
        SoulType soulType = SoulType.fromName(JSONUtils.getString(object, "soulType"));
        int count = JSONUtils.getInt(object, "count");
        return new SoulResult(new SoulStack(soulType, count));
//        }
    }

    @Override
    public SoulResult read(PacketBuffer buffer) {
        SoulType type = SoulType.fromName(buffer.readString());
        int amount = buffer.readInt();
        return new SoulResult(type, amount);
    }

    @Override
    public void write(PacketBuffer buffer, SoulResult soulResult) {
        ISoulStack soulStack = soulResult.getStack();
        buffer.writeString(soulStack.getType().getName());
        buffer.writeInt(soulStack.getAmount());
    }

    @Override
    public <V extends SoulResult> void write(V v, JsonElement jsonElement) {
        ISoulStack stack = v.getStack();
        JsonObject object = jsonElement.getAsJsonObject();
        object.addProperty("soulType", stack.getType().getName());
        object.addProperty("count", stack.getAmount());
    }
}
