/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.kiln;

import com.betterwithmods.core.impl.crafting.multiplex.MultiplexRecipeManager;
import com.betterwithmods.core.utilties.MiscUtils;
import com.betterwithmods.core.utilties.Timer;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.content.dynamic.CamoflageTile;
import com.betterwithmods.mechanist.handlers.HeatHandler;
import com.betterwithmods.mechanist.handlers.KilnStructureHandler;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.util.math.BlockPos;

import java.util.concurrent.atomic.AtomicInteger;

public class KilnTile extends CamoflageTile implements ITickableTileEntity {

    private final AtomicInteger heat = new AtomicInteger();
    private final Timer timer = new Timer();
    private final AtomicInteger speedMultiplier = new AtomicInteger();
    private final AtomicInteger calcHeatDelay = new AtomicInteger();

    private static final int CHECK_DELAY = 100; //recheck every 5 seconds

    private int formCheckDelay;

    public KilnTile() {
        super(Registrars.Tiles.KILN);
    }

    @Override
    public void tick() {
        if(world != null && !world.isRemote()) {
            if (formCheckDelay <= 0) {
                if (!KilnStructureHandler.isValidKiln(world, pos, pos.down())) {
                    world.setBlockState(pos, getParentState());
                    return;
                }
                formCheckDelay = CHECK_DELAY;
            }
            formCheckDelay = Math.max(0, formCheckDelay - 1);
            HeatHandler.calculateHeatLevel(world, pos, calcHeatDelay, heat, speedMultiplier);
            if(heat.get() > 0) {
                craftRecipes();
            }
        }
    }

    private void noRecipe() {
        BlockPos craftPos = pos.up();
        timer.reset();
        world.sendBlockBreakProgress(0, craftPos, -1);
    }

    public void craftRecipes() {
        BlockPos craftPos = pos.up();
        KilnRecipeContext context = new KilnRecipeContext(timer.getTime(), heat);
        MultiplexRecipeManager.get(world.getServer())
                .ifPresent(m -> MiscUtils.ifPresentOrElse(m.getRecipe(Registrars.MultiplexRecipeTypes.KILN, world, craftPos, context),
                        r -> {
                            timer.update(r);
                            r.craft(world, craftPos, context);
                            int i = Math.max(speedMultiplier.get() / 2, 1);
                            timer.incrementTime(i);
                        },
                        this::noRecipe
                )
                );
    }


    private static final String SPEED_MULTIPLIER = "SPEED_MULTIPLIER";
    private static final String TIMER = "timer";
    private static final String FORM_CHECK_DELAY = "formCheckDelay";

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        compound.putInt(FORM_CHECK_DELAY, formCheckDelay);
        compound.putInt(SPEED_MULTIPLIER, speedMultiplier.get());
        compound.put(TIMER, timer.serializeNBT());
        return super.write(compound);
    }

    @Override
    public void read(CompoundNBT compound) {
        if (compound.contains(FORM_CHECK_DELAY)) {
            formCheckDelay = compound.getInt(FORM_CHECK_DELAY);
        }
        timer.deserializeNBT(compound.getCompound(TIMER));
        if (compound.contains(SPEED_MULTIPLIER)) {
            speedMultiplier.set(compound.getInt(SPEED_MULTIPLIER));
        }
        super.read(compound);
    }
}
