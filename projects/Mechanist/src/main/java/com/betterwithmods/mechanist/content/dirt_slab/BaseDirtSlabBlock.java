/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.dirt_slab;

import com.betterwithmods.core.base.game.ConstantIngredients;
import com.betterwithmods.core.base.game.block.BlockBottomSlab;
import com.betterwithmods.core.utilties.ItemUtils;
import com.betterwithmods.core.utilties.PlayerUtils;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Hand;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.common.ToolType;


public class BaseDirtSlabBlock extends BlockBottomSlab {

    public BaseDirtSlabBlock(Properties builder) {
        super(builder, Blocks.DIRT.getDefaultState());
        setHarvestTool(ToolType.SHOVEL);
    }

    @Override
    public boolean isSameBlock(Block block) {
        return block instanceof BaseDirtSlabBlock;
    }

    @Override
    public boolean isSameItem(IItemProvider provider) {
        return ItemUtils.getBlock(provider.asItem()).orElse(Blocks.AIR) instanceof BaseDirtSlabBlock;
    }

    @Override
    public boolean onBlockActivated(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockRayTraceResult hit) {
        ItemStack stack = PlayerUtils.getHeld(player, hand);
        if (ConstantIngredients.SHOVEL.test(stack)) {
            if(state.getBlock() != Registrars.Blocks.GRASS_PATH_SLAB) {
                ItemUtils.damageItem(stack, player, hand, 1);
                world.playSound(player, pos, SoundEvents.ITEM_SHOVEL_FLATTEN, SoundCategory.BLOCKS, 1.0F, 1.0F);
                world.setBlockState(pos, Registrars.Blocks.GRASS_PATH_SLAB.getDefaultState());
                return true;
            }
        }
        return false;
    }


}

