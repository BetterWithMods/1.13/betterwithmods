/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.filtered_hopper.recipes;

import com.betterwithmods.core.api.crafting.input.IRecipeContext;
import com.betterwithmods.mechanist.content.filtered_hopper.FilteredHopperTile;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;

public class HopperContext implements IRecipeContext<HopperState, HopperRecipe> {


    private ItemEntity itemEntity;

    public HopperContext(ItemEntity itemEntity) {
        this.itemEntity = itemEntity;
    }

    private FilteredHopperTile getHopper(IWorld world, BlockPos pos) {
        TileEntity tileEntity = world.getTileEntity(pos);
        if (tileEntity instanceof FilteredHopperTile)
            return (FilteredHopperTile) tileEntity;
        return null;
    }


    @Override
    public HopperState getState(IWorld world, BlockPos pos) {
        FilteredHopperTile hopper = getHopper(world, pos);
        if (hopper != null) {
            return new HopperState(hopper.getFilter(), itemEntity, hopper.getInventory(), hopper.getSoulStorage());
        }
        return null;
    }
}
