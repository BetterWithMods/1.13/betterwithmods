/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.kiln;

import com.betterwithmods.core.utilties.ItemUtils;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.content.dynamic.CamoflageBlock;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;

import javax.annotation.Nullable;

public class KilnBlock extends CamoflageBlock {
    public KilnBlock(Properties properties) {
        super(properties);
    }

    public static boolean place(IWorld world, BlockPos pos, BlockState parent) {
        if(world.setBlockState(pos, Registrars.Blocks.KILN.getDefaultState(), 3)) {
            KilnTile tile = (KilnTile) world.getTileEntity(pos);
            tile.setParentState(parent);
            return true;
        }
        return false;
    }

    @Override
    public ItemStack getPickBlock(BlockState state, @Nullable RayTraceResult target, IBlockReader world, BlockPos pos, PlayerEntity player) {
        KilnTile tile = (KilnTile) world.getTileEntity(pos);
        return ItemUtils.getStack(tile.getParentState());
    }

    @Override
    public boolean isCamoTile() {
        return false;
    }
}
