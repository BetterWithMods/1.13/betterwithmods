/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.pots;

import com.betterwithmods.core.Tags;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipeType;
import com.betterwithmods.core.api.mech.IMechanicalPower;
import com.betterwithmods.core.api.mech.IPower;
import com.betterwithmods.core.base.game.tile.InventoryTile;
import com.betterwithmods.core.impl.CapabilityMechanicalPower;
import com.betterwithmods.core.impl.Power;
import com.betterwithmods.core.impl.crafting.multiplex.MultiplexRecipeManager;
import com.betterwithmods.core.utilties.*;
import com.betterwithmods.core.utilties.inventory.InventoryUtils;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.handlers.HeatHandler;
import com.betterwithmods.mechanist.handlers.ItemCollector;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.IIntArray;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class BasePotTile<T extends BasePotRecipe<T>> extends InventoryTile<ItemStackHandler> implements IMechanicalPower, INamedContainerProvider, ITickableTileEntity {

    private static final VoxelShape COLLECTION_AREA = BasePotBlock.makeCuboidShape(0, 2, 0, 16, 24, 15);

    private final Timer timer = new Timer();
    private final AtomicInteger heat = new AtomicInteger();
    private final AtomicInteger speedMultiplier = new AtomicInteger();
    private final AtomicInteger calcHeatDelay = new AtomicInteger();
    private final ItemCollector collector = new ItemCollector(COLLECTION_AREA, optionalInventory);

    @Nonnull
    private Direction tilt = Direction.UP;

    private byte stage;
    private int delay;

    public BasePotTile(TileEntityType<?> tileEntityTypeIn) {
        super(tileEntityTypeIn);
    }

    @Nonnull
    @Override
    public ItemStackHandler createInventory() {
        return new ItemStackHandler(27);
    }

    public abstract IMultiplexRecipeType<IItemHandlerModifiable, T> getRecipeType();


    private static final VectorBuilder
            POSITION = new VectorBuilder().offset(0.5, 0, 0.5).rand(0.4, 1, 0.4),
            PARTICLE_SPEED = new VectorBuilder().rand(0, 0.05, 0);
    private static final int PARTICLE_EVENT = 100;

    @Override
    public boolean receiveClientEvent(int id, int param) {
        if (id == PARTICLE_EVENT) {
            spawnParticles(param);
        }
        return false;
    }

    private void spawnParticles(int heat) {
        if (heat > 0 && Rand.chance(0.2)) {
            EnvironmentUtils.forEachParticle(world, pos.up(), Registrars.Particles.CAULDRON_STEAM, heat, POSITION, PARTICLE_SPEED);
        }
    }

    public void craftRecipes() {
        HeatedTimedInventoryRecipeContext<T> context = new HeatedTimedInventoryRecipeContext<>(timer.getTime(), heat);
        MultiplexRecipeManager.get(world.getServer())
                .ifPresent(m -> MiscUtils.ifPresentOrElse(m.getRecipe(getRecipeType(), world, pos, context),
                        r -> {
                            timer.update(r);
                            r.craft(world, pos, context);
                            timer.incrementTime(speedMultiplier.get() / 2);
                        },
                        timer::reset
                ));
    }

    @Override
    public void tick() {

        if (delay <= 0) {
            if (tilt.getAxis().isVertical()) {
                HeatHandler.calculateHeatLevel(world, pos, calcHeatDelay, heat, speedMultiplier);
                collector.tick(world, pos);
                if (heat.get() > 0) {
                    world.addBlockEvent(pos, getBlockState().getBlock(), PARTICLE_EVENT, heat.get());
                    craftRecipes();
                }
                delay = 0;
            } else {
                switch (stage) {
                    case 1: {
                        //Wait for animation before spilling
                        //TODO animation
                        delay = 20 * 2;
                        stage = 2;
                        break;
                    }
                    case 2: {
                        //Spill items
                        delay = 20;
                        spillContents();
                        break;
                    }
                }
            }
        }
        delay = Math.max(0, delay - 1);
    }


    public void spillContents() {
        VectorBuilder pos = new VectorBuilder().offset(tilt, 0.75).offset(0.5, 0.25, 0.5);
        VectorBuilder motion = new VectorBuilder().offset(tilt, 0.075);
        StackEjector ejector = new StackEjector(pos, motion);

        Ingredient blacklist = Ingredient.fromTag(Tags.Items.TIPPING_BLACKLIST);

        Optional<ItemStack> optionalItemStack = InventoryUtils.findInInventory(getInventory(), blacklist.negate(), s -> 8, true);

        optionalItemStack.ifPresent(stack ->
                ejector.setStack(stack).ejectStack(world, getPos(), new BlockPos(0, 0, 0)));
    }


    @Override
    public void onChanged() {
        markDirty();

        int sources = 0;
        Direction side = null;
        for (Direction facing : DirectionUtils.HORIZONTAL_SET) {
            IPower input = getInput(world, pos, facing);

            if (input != null) {

                int torque = input.getTorque();
                IPower minInput = getMinimumInput();
                if (minInput != null && torque < minInput.getTorque()) {
                    continue;
                }

                if (sources > 0) {
                    MechanicalUtils.get(world, pos.offset(facing), facing.getOpposite()).ifPresent(m -> m.overpower(world, pos.offset(facing)));
                    continue;
                }
                side = facing;

                sources++;
            }
        }
        if (side != null) {
            tilt = side.rotateY();
            stage = 1;
        } else {
            tilt = Direction.UP;
        }

        markDirty();
        if (getBlockState().getBlock() instanceof BasePotBlock) {
            BasePotBlock pot = (BasePotBlock) getBlockState().getBlock();
            pot.tilt(world, pos, tilt);
        }
    }


    @Nullable
    @Override
    public IPower getOutput(@Nonnull IWorld world, @Nonnull BlockPos pos, @Nonnull Direction facing) {
        return null;
    }

    @Nullable
    @Override
    public IPower getMaximumInput(@Nonnull Direction facing) {
        return null;
    }

    @Nullable
    @Override
    public IPower getMinimumInput() {
        return Power.ONE_TORQUE;
    }

    @Override
    public boolean canInputFrom(Direction facing) {
        return facing.getAxis().isHorizontal();
    }

    @Override
    public boolean overpower(World world, BlockPos pos) {
        return false;
    }

    private static final String TILT = "tilt", STAGE = "stage", DELAY = "delay", CALC_HEAT_DELAY = "calcHeatDelay";
    private static final String SPEED_MULTIPLIER = "SPEED_MULTIPLIER";
    public static final String TIMER = "timer";


    @Nonnull
    @Override
    public CompoundNBT write(CompoundNBT compound) {
        DirectionUtils.write(compound, TILT, tilt);
        compound.putByte(STAGE, stage);
        compound.putInt(DELAY, delay);
        compound.putInt(CALC_HEAT_DELAY, calcHeatDelay.get());
        compound.putInt(SPEED_MULTIPLIER, speedMultiplier.get());
        compound.put(TIMER, timer.serializeNBT());
        return super.write(compound);
    }

    @Override
    public void read(CompoundNBT compound) {
        super.read(compound);
        Direction facing = DirectionUtils.read(compound, TILT);
        if (facing != null) {
            tilt = facing;
        }
        if (compound.contains(STAGE)) {
            stage = compound.getByte(STAGE);
        }
        if (compound.contains(CALC_HEAT_DELAY)) {
            calcHeatDelay.set(compound.getInt(CALC_HEAT_DELAY));
        }
        if (compound.contains(DELAY)) {
            delay = compound.getInt(DELAY);
        }
        timer.deserializeNBT(compound.getCompound(TIMER));
        if (compound.contains(SPEED_MULTIPLIER)) {
            speedMultiplier.set(compound.getInt(SPEED_MULTIPLIER));
        }
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        if (cap == CapabilityMechanicalPower.MECHANICAL_POWER) {
            return CapabilityMechanicalPower.MECHANICAL_POWER.orEmpty(cap, LazyOptional.of(() -> this));
        }
        return super.getCapability(cap, side);
    }

    public final IIntArray data = new IIntArray() {

        @Override
        public int get(int index) {
            switch (index) {
                case 0:
                    return BasePotTile.this.timer.getTime().get();
                case 1:
                    return BasePotTile.this.timer.getGoalTime().get();
                case 2:
                    return BasePotTile.this.heat.get();
                case 3:
                    return BasePotTile.this.speedMultiplier.get();
                default:
                    return -1;
            }

        }

        @Override
        public void set(int index, int value) {
        }

        @Override
        public int size() {
            return 4;
        }
    };
}
