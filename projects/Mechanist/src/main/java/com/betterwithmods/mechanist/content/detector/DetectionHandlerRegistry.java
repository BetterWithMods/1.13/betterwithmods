/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.detector;

import com.betterwithmods.core.api.blocks.detector.IDetectionHandler;
import com.betterwithmods.core.api.blocks.detector.IDetectionRegistry;
import com.betterwithmods.core.impl.blocks.detector.EntityDetectionHandler;
import com.betterwithmods.core.impl.blocks.detector.RainDetectionHandler;
import com.betterwithmods.core.impl.blocks.detector.StateDetectionHandler;
import com.google.common.collect.Lists;
import net.minecraft.block.CactusBlock;
import net.minecraft.block.SugarCaneBlock;
import net.minecraft.block.VineBlock;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Collection;
import java.util.List;

public class DetectionHandlerRegistry implements IDetectionRegistry {

    public static List<IDetectionHandler> HANDLERS = Lists.newArrayList();

    public DetectionHandlerRegistry() {
        add(new StateDetectionHandler(state -> state.getMaterial().isSolid()));
        add(new StateDetectionHandler(state -> state.getBlock() instanceof SugarCaneBlock));
        add(new StateDetectionHandler(state -> state.getBlock() instanceof VineBlock));
        add(new StateDetectionHandler(state -> state.getBlock() instanceof CactusBlock));
        add(new EntityDetectionHandler());
        add(new RainDetectionHandler());
        add(new CropDetectionHandler());

    }

    @Override
    public boolean detect(Direction facing, World world, BlockPos detector, BlockPos offset) {
        for (IDetectionHandler handler : HANDLERS) {
            if (handler.detect(facing, world, detector, offset))
                return true;
        }
        return false;
    }

    @Override
    public void add(IDetectionHandler handler) {
        HANDLERS.add(handler);
    }

    @Override
    public Collection<IDetectionHandler> getRegistry() {
        return HANDLERS;
    }


}
