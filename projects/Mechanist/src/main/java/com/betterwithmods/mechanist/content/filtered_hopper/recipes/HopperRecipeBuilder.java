/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.filtered_hopper.recipes;

import com.betterwithmods.core.api.crafting.output.IOutput;
import com.betterwithmods.core.api.crafting.result.IResult;
import com.betterwithmods.core.generator.multiplex.FinishedMultiplexRecipe;
import com.betterwithmods.core.generator.multiplex.IFinishedMultiplexRecipe;
import com.betterwithmods.core.impl.crafting.output.OutputList;
import com.betterwithmods.core.utilties.DeserializeUtils;
import com.betterwithmods.mechanist.Registrars;
import com.google.common.collect.Lists;
import com.google.gson.JsonObject;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;

import java.util.function.Consumer;

public class HopperRecipeBuilder {

    protected ResourceLocation id;
    protected Ingredient filter;
    protected Ingredient input;
    protected IOutput outputs = new OutputList();
    protected HopperInput.Action action = HopperInput.Action.PERMIT;

    public HopperRecipeBuilder() {
    }

    public static HopperRecipeBuilder builder() {
        return new HopperRecipeBuilder();
    }

    public HopperRecipeBuilder id(ResourceLocation id) {
        this.id = id;
        return this;
    }

    public HopperRecipeBuilder filter(Ingredient filter) {
        this.filter = filter;
        return this;
    }

    public HopperRecipeBuilder inputs(Ingredient input) {
        this.input = input;
        return this;
    }

    public HopperRecipeBuilder outputs(IOutput outputs) {
        this.outputs = outputs;
        return this;
    }

    public HopperRecipeBuilder outputs(IResult... results) {
        return this.outputs(new OutputList(Lists.newArrayList(results)));
    }

    public HopperRecipeBuilder noOutput() {
        this.outputs = null;
        return this;
    }

    public HopperRecipeBuilder action(HopperInput.Action action) {
        this.action = action;
        return this;
    }

    public void build(Consumer<IFinishedMultiplexRecipe> consumer) {
        consumer.accept(new HopperRecipeBuilder.Result(id, filter, input, action, outputs));
    }


    private class Result extends FinishedMultiplexRecipe {
        private HopperInput inputs;

        public Result(ResourceLocation id, Ingredient filter, Ingredient input, HopperInput.Action action, IOutput outputs) {
            super(Registrars.MultiplexRecipeSerializers.FILTERED_HOPPER, id, outputs);
            this.inputs = new HopperInput(filter, input, action);
        }

        @Override
        public void write(JsonObject jsonObject) {
            DeserializeUtils.writeInput(this.inputs, jsonObject);
            DeserializeUtils.writeOutput(this.outputs, jsonObject);
        }
    }

}
