/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.modules.dynamicblocks;

import com.betterwithmods.core.Relationships;
import com.betterwithmods.core.base.game.block.MaterialKey;
import com.betterwithmods.core.base.setup.ModuleBase;
import com.betterwithmods.core.relationary.Relation;
import com.betterwithmods.core.relationary.RelationaryLoadedEvent;
import com.betterwithmods.core.relationary.Shape;
import com.betterwithmods.core.utilties.ItemUtils;
import com.betterwithmods.mechanist.content.dynamic.CamoflageTile;
import com.google.common.collect.Lists;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.event.lifecycle.FMLLoadCompleteEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

import java.util.List;
import java.util.Map;

public class DynamicBlocks extends ModuleBase {

    private ForgeConfigSpec.ConfigValue<List<? extends String>> variants;

    public DynamicBlocks() {
        super("dynamic_blocks");
    }

    @Override
    public void configs(ForgeConfigSpec.Builder builder) {
        this.variants = builder.comment("The list of sources from which dynamic block will get variants. Ex. tag:minecraft:planks or block:minecraft:cobblestone")
                .translation("config.betterwithmods_mechanist.variants")
                .defineList("variants",
                        Lists.newArrayList("tag:minecraft:planks", "tag:forge:stone"),
                        o -> true
                );
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::onRelationsLoad);
    }

    @Override
    public void loadComplete(FMLLoadCompleteEvent event) {
        VariantsHandler.loadVariantProviders(variants.get());
        VariantsHandler.onLoadComplete(event);
    }


    public void onRelationsLoad(RelationaryLoadedEvent event) {
        for(Block block: VariantsHandler.DYNAMIC_BLOCKS_TABLE.values()) {
            VariantsHandler.getChildren(block);
        }

        Map<ResourceLocation, Relation> map = event.getLoadingMap();
        for (BlockState state : VariantsHandler.getAllStates()) {
            MaterialKey material = MaterialKey.get(state.getMaterial());
            ItemStack parentStack = ItemUtils.getStack(state);
            //Relation by the name of the parent block
            ResourceLocation registryName = state.getBlock().getRegistryName();
            Relation relation = map.getOrDefault(registryName, new Relation(registryName));
            relation.addShape(Relationships.BLOCK, parentStack);
            Map<Shape, Block> blocks = VariantsHandler.DYNAMIC_BLOCKS_TABLE.row(material);
            for (Map.Entry<Shape, Block> entry : blocks.entrySet()) {
                Shape shape = entry.getKey();
                Block block = entry.getValue();
                relation.addShape(shape, CamoflageTile.builder(block, state).build());
            }
            map.put(registryName, relation);
        }
    }


    @Override
    public void serverStarting(FMLServerStartingEvent event) {
        super.serverStarting(event);
    }

    public static void fillVariants(Block block, NonNullList<ItemStack> items) {
        items.addAll(VariantsHandler.getChildren(block));
    }

    @Override
    public String getDescription() {
        return "Dynamically add various building block to the game";
    }


}
