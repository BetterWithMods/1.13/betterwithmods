/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.generator;

import com.betterwithmods.core.References;
import com.betterwithmods.core.Relationships;
import com.betterwithmods.core.Tags;
import com.betterwithmods.core.api.souls.SoulType;
import com.betterwithmods.core.base.game.ingredient.*;
import com.betterwithmods.core.generator.multiplex.IFinishedMultiplexRecipe;
import com.betterwithmods.core.generator.multiplex.MultiplexRecipeProvider;
import com.betterwithmods.core.impl.crafting.ItemStackBuilder;
import com.betterwithmods.core.impl.crafting.result.RelationshipResult;
import com.betterwithmods.core.impl.crafting.result.StackResult;
import com.betterwithmods.core.impl.crafting.result.count.Digit;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.content.filtered_hopper.recipes.HopperInput;
import com.betterwithmods.mechanist.content.filtered_hopper.recipes.HopperRecipeBuilder;
import com.betterwithmods.mechanist.content.filtered_hopper.recipes.result.EjectResult;
import com.betterwithmods.mechanist.content.filtered_hopper.recipes.result.SoulResult;
import com.betterwithmods.mechanist.content.kiln.KilnRecipeBuilder;
import com.betterwithmods.mechanist.content.millstone.MillstoneRecipeBuilder;
import com.betterwithmods.mechanist.content.pots.cauldron.CauldronRecipeBuilder;
import com.betterwithmods.mechanist.content.pots.crucible.CrucibleRecipeBuilder;
import com.betterwithmods.mechanist.content.saw.SawRecipeBuilder;
import com.betterwithmods.mechanist.content.turntable.TurntableRecipeBuilder;
import com.betterwithmods.mechanist.handlers.HeatHandler;
import net.minecraft.block.Blocks;
import net.minecraft.data.DataGenerator;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.ResourceLocation;

import java.util.function.Consumer;

public class MultiplexRecipes extends MultiplexRecipeProvider {
    public MultiplexRecipes(DataGenerator generatorIn) {
        super(generatorIn);
    }

    @Override
    public void registerRecipes(Consumer<IFinishedMultiplexRecipe> consumer) {
        Ingredient HELLFIRE_DUST = Ingredient.fromTag(Tags.Items.DUST_HELLFIRE);
        Ingredient HELLFIRE_DUST_8 = StackIngredient.from(new IngredientStack(HELLFIRE_DUST, 8));
        Ingredient coal = Ingredient.fromItems(Items.COAL);
        Ingredient meat = StackIngredient.from(
                new IngredientStack(Tags.Items.PORK, 1),
                new IngredientStack(Tags.Items.BEEF, 4),
                new IngredientStack(Tags.Items.MUTTON, 4),
                new IngredientStack(Tags.Items.CHICKEN, 6),
                new IngredientStack(Tags.Items.RABBIT, 6),
                new IngredientStack(Tags.Items.ROTTEN, 10)
        );

        Ingredient cord = Ingredient.fromItems(Registrars.Items.HEMP_FIBER, Items.STRING);
        Ingredient DUSTS_GLOWSTONE = new TagIngredient(net.minecraftforge.common.Tags.Items.DUSTS_GLOWSTONE);
        Ingredient DUSTS_REDSTONE = new TagIngredient(net.minecraftforge.common.Tags.Items.DUSTS_REDSTONE);
        Ingredient DUSTS_BLAZE = new TagIngredient(Tags.Items.DUSTS_BLAZE);
        Ingredient DUSTS_GUNPOWDER = new TagIngredient(Tags.Items.DUSTS_GUNPOWDER);

        Ingredient hemp_leaf = Ingredient.fromItems(Registrars.Items.HEMP_LEAF);
        ItemStackBuilder hemp_fiber = ItemStackBuilder.builder().item(Registrars.Items.HEMP_FIBER).count(new Digit(3));

        Ingredient wood = StackIngredient.from(
                new IngredientStack(new TagIngredient(ItemTags.LOGS), 1)
        );

        Ingredient leather = StackIngredient.from(
                new IngredientStack(Items.LEATHER, 1),
                new IngredientStack(Registrars.Items.TANNED_LEATHER, 1),
                new IngredientStack(Registrars.Items.SCOURED_LEATHER, 1),
                new IngredientStack(Registrars.Items.LEATHER_SLICE, 2),
                new IngredientStack(Registrars.Items.TANNED_LEATHER_SLICE, 2),
                new IngredientStack(Registrars.Items.SCOURED_LEATHER_SLICE, 2),
                new IngredientStack(Registrars.Items.TANNED_LEATHER_BELT, 2),
                new IngredientStack(Registrars.Items.TANNED_LEATHER_STRAP, 8)
        );

        Ingredient tannin = StackIngredient.from(
                new IngredientStack(new ShapeIngredient(Blocks.ACACIA_LOG, Relationships.LOG, Relationships.BARK), 8),
                new IngredientStack(new ShapeIngredient(Blocks.DARK_OAK_LOG, Relationships.LOG, Relationships.BARK), 8),
                new IngredientStack(new ShapeIngredient(Blocks.JUNGLE_LOG, Relationships.LOG, Relationships.BARK), 8),
                new IngredientStack(new ShapeIngredient(Blocks.OAK_LOG, Relationships.LOG, Relationships.BARK), 5),
                new IngredientStack(new ShapeIngredient(Blocks.SPRUCE_LOG, Relationships.LOG, Relationships.BARK), 3),
                new IngredientStack(new ShapeIngredient(Blocks.BIRCH_LOG, Relationships.LOG, Relationships.BARK), 2),
                new IngredientStack(new ShapeIngredient(Blocks.BIRCH_LOG, Relationships.LOG, Relationships.BARK), 2)
        );

        MillstoneRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "millstone/hemp_leaf")
                .inputs(hemp_leaf)
                .outputs(new StackResult(hemp_fiber))
                .recipeTime(220)
                .build(consumer);

        MillstoneRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "millstone/coal_dust")
                .inputs(coal)
                .outputs(new StackResult(Registrars.Items.COAL_DUST))
                .recipeTime(220)
                .build(consumer);

        MillstoneRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "millstone/scoured_leather")
                .inputs(Ingredient.fromItems(Items.LEATHER))
                .outputs(new StackResult(Registrars.Items.SCOURED_LEATHER))
                .recipeTime(220)
                .build(consumer);


        MillstoneRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "millstone/scoured_leather_slice")
                .inputs(Ingredient.fromItems(Registrars.Items.LEATHER_SLICE))
                .outputs(new StackResult(Registrars.Items.SCOURED_LEATHER_SLICE))
                .recipeTime(220)
                .build(consumer);

        MillstoneRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "millstone/netherrack_dust")
                .inputs(Ingredient.fromItems(Blocks.NETHERRACK))
                .outputs(new StackResult(Registrars.Items.NETHERRACK_DUST))
                .recipeTime(220)
                .build(consumer);


        CauldronRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "cauldron/tanned_leather")
                .inputs(tannin)
                .input(Registrars.Items.SCOURED_LEATHER)
                .outputs(new StackResult(Registrars.Items.TANNED_LEATHER))

                .recipeTime(200)
                .heatLevel(HeatHandler.NORMAL_HEAT)
                .build(consumer);

        CauldronRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "cauldron/tanned_leather_slice")
                .inputs(tannin)
                .input(StackIngredient.from(Registrars.Items.SCOURED_LEATHER_SLICE, 2))
                .outputs(new StackResult(Registrars.Items.TANNED_LEATHER_SLICE, 2))
                .recipeTime(200)
                .heatLevel(HeatHandler.NORMAL_HEAT)
                .build(consumer);


        CauldronRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "cauldron/beef")
                .inputs(Ingredient.fromItems(Items.BEEF))
                .outputs(new StackResult(Items.COOKED_BEEF))
                .recipeTime(200)
                .heatLevel(HeatHandler.NORMAL_HEAT)
                .build(consumer);


        CauldronRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "cauldron/filament")
                .inputs(cord, DUSTS_GLOWSTONE, DUSTS_REDSTONE)
                .outputs(new StackResult(Registrars.Items.FILAMENT))
                .recipeTime(200)
                .heatLevel(HeatHandler.NORMAL_HEAT)
                .build(consumer);

        CauldronRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "cauldron/element")
                .inputs(cord, DUSTS_BLAZE, DUSTS_REDSTONE)
                .outputs(new StackResult(Registrars.Items.ELEMENT))
                .recipeTime(200)
                .heatLevel(HeatHandler.NORMAL_HEAT)
                .build(consumer);

        CauldronRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "cauldron/fuse")
                .inputs(cord, DUSTS_GUNPOWDER)
                .outputs(new StackResult(Registrars.Items.FUSE))
                .recipeTime(200)
                .heatLevel(HeatHandler.NORMAL_HEAT)
                .build(consumer);

        //TODO recipe explosions
        CauldronRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "cauldron/concentrated_hellfire")
                .input(HELLFIRE_DUST_8)
                .outputs(new StackResult(Registrars.Items.CONCENTRATED_HELLFIRE))
                .recipeTime(200)
                .heatLevel(HeatHandler.NORMAL_HEAT)
                .build(consumer);

        CauldronRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "cauldron/blasting_oil")
                .inputs(HELLFIRE_DUST, Ingredient.fromItems(Registrars.Items.TALLOW))
                .outputs(new StackResult(Registrars.Items.BLASTING_OIL))
                .recipeTime(200)
                .heatLevel(HeatHandler.NORMAL_HEAT)
                .build(consumer);

        CauldronRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "cauldron/stoked/tallow")
                .inputs(meat)
                .outputs(new StackResult(Registrars.Items.TALLOW))
                .recipeTime(200)
                .heatLevel(HeatHandler.STOKED_HEAT)
                .build(consumer);

        CauldronRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "cauldron/stoked/soap")
                .inputs(Ingredient.fromItems(Registrars.Items.TALLOW), Ingredient.fromItems(Registrars.Items.POTASH))
                .outputs(new StackResult(Registrars.Items.SOAP))
                .recipeTime(200)
                .heatLevel(HeatHandler.STOKED_HEAT)
                .build(consumer);


        CauldronRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "cauldron/stoked/glue")
                .inputs(leather)
                .outputs(new StackResult(Registrars.Items.GLUE))
                .recipeTime(200)
                .heatLevel(HeatHandler.STOKED_HEAT)
                .build(consumer);
        //TODO leather armors -> glue

        CauldronRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "cauldron/stoked/bow")
                .input(Items.BOW)
                .outputs(
                        new StackResult(Items.STICK),
                        new StackResult(Items.STRING)
                )
                .recipeTime(200)
                .heatLevel(HeatHandler.STOKED_HEAT)
                .build(consumer);

        CauldronRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "cauldron/stoked/potash")
                .input(wood)
                .outputs(new StackResult(Registrars.Items.POTASH))
                .recipeTime(200)
                .heatLevel(HeatHandler.STOKED_HEAT)
                .build(consumer);

        CrucibleRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "crucible/stoked/ender_slag")
                .input(Registrars.Items.ENDER_SLAG)
                .outputs(
                        new StackResult(Registrars.Items.SOUL_FLUX),
                        new StackResult(Registrars.Items.BRIMSTONE)
                )
                .recipeTime(200)
                .heatLevel(HeatHandler.STOKED_HEAT)
                .build(consumer);


        CrucibleRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "crucible/stoked/stone")
                .input(Blocks.COBBLESTONE)
                .outputs(new StackResult(Blocks.STONE))
                .recipeTime(200)
                .heatLevel(HeatHandler.STOKED_HEAT)
                .build(consumer);

        //Let's see if this works just for the sake of it, I don't want to keep it
        SawRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "saw/logs")
                .input(ItemTags.LOGS)
                .outputs(
                        new RelationshipResult(Relationships.OAK_WOOD, Relationships.LOG, Relationships.PLANK, new Digit(4)),
                        new RelationshipResult(Relationships.OAK_WOOD, Relationships.LOG, Relationships.DUST, new Digit(1)),
                        new RelationshipResult(Relationships.OAK_WOOD, Relationships.LOG, Relationships.BARK, new Digit(1))
                )
                .build(consumer);

        SawRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "saw/planks_siding")
                .input(ItemTags.PLANKS)
                .outputs(
                        new RelationshipResult(Relationships.relation(Blocks.OAK_PLANKS.getRegistryName()), Relationships.BLOCK, Relationships.SIDING, new Digit(2))
                )
                .build(consumer);

        KilnRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "kiln/crucible")
                .input(Registrars.Blocks.UNFIRED_CRUCIBLE)
                .outputs(new StackResult(Registrars.Blocks.CRUCIBLE))
                .recipeTime(200)
                .heatLevel(HeatHandler.STOKED_HEAT)
                .build(consumer);

        KilnRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "kiln/planter")
                .input(Registrars.Blocks.UNFIRED_PLANTER)
                .outputs(new StackResult(Registrars.Blocks.EMPTY_PLANTER))
                .recipeTime(200)
                .heatLevel(HeatHandler.STOKED_HEAT)
                .build(consumer);

        KilnRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "kiln/ore/iron_ore")
                .input(Blocks.IRON_ORE)
                .outputs(new StackResult(Items.IRON_NUGGET))
                .recipeTime(200)
                .heatLevel(HeatHandler.STOKED_HEAT)
                .build(consumer);

        KilnRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "kiln/terracotta")
                .input(Blocks.CLAY)
                .outputs(new StackResult(Blocks.TERRACOTTA))
                .recipeTime(200)
                .heatLevel(HeatHandler.NORMAL_HEAT)
                .build(consumer);

        KilnRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "kiln/end_stone")
                .input(Blocks.END_STONE)
                .outputs(new StackResult(Registrars.Blocks.WHITE_COBBLESTONE),new StackResult(Registrars.Items.ENDER_SLAG))
                .recipeTime(200)
                .heatLevel(HeatHandler.STOKED_HEAT)
                .build(consumer);

        KilnRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "kiln/white_stone")
                .input(Registrars.Blocks.WHITE_COBBLESTONE)
                .outputs(new StackResult(Registrars.Blocks.WHITE_STONE))
                .recipeTime(200)
                .heatLevel(HeatHandler.NORMAL_HEAT)
                .build(consumer);

        KilnRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "kiln/baking/flour")
                .input(Registrars.Blocks.FLOUR)
                .outputs(new StackResult(Items.BREAD))
                .recipeTime(200)
                .heatLevel(HeatHandler.NORMAL_HEAT)
                .build(consumer);

        KilnRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "kiln/baking/cookies")
                .input(Registrars.Blocks.RAW_COOKIE)
                .outputs(new StackResult(Items.COOKIE, 8))
                .recipeTime(200)
                .heatLevel(HeatHandler.NORMAL_HEAT)
                .build(consumer);

        KilnRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "kiln/baking/cake")
                .input(Registrars.Blocks.RAW_CAKE)
                .outputs(new StackResult(Items.CAKE))
                .recipeTime(200)
                .heatLevel(HeatHandler.NORMAL_HEAT)
                .build(consumer);

        TurntableRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "turntable/clay")
                .input(Blocks.CLAY)
                .outputs(new StackResult(Items.CLAY_BALL))
                .replace(Registrars.Blocks.UNFIRED_CRUCIBLE)
                .recipeTime(8)
                .build(consumer);

        TurntableRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "turntable/unfired_crucible")
                .input(Registrars.Blocks.UNFIRED_CRUCIBLE)
                .outputs(new StackResult(Items.CLAY_BALL))
                .replace(Registrars.Blocks.UNFIRED_PLANTER)
                .recipeTime(8)
                .build(consumer);

        TurntableRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "turntable/unfired_planter")
                .input(Registrars.Blocks.UNFIRED_PLANTER)
                .outputs(new StackResult(Items.CLAY_BALL))
                .replace(Registrars.Blocks.UNFIRED_VASE)
                .recipeTime(8)
                .build(consumer);

        TurntableRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "turntable/unfired_vase")
                .input(Registrars.Blocks.UNFIRED_VASE)
                .replace(Registrars.Blocks.UNFIRED_URN)
                .recipeTime(8)
                .build(consumer);

        TurntableRecipeBuilder.builder()
                .id(References.MODID_MECHANIST, "turntable/unfired_urn")
                .input(Registrars.Blocks.UNFIRED_URN)
                .outputs(new StackResult(Items.CLAY_BALL))
                .replace(Blocks.AIR)
                .recipeTime(8)
                .build(consumer);

        HopperRecipeBuilder.builder()
                .id(new ResourceLocation(References.MODID_MECHANIST,"filtered_hopper/anything"))
                .filter(null)
                .action(HopperInput.Action.PERMIT)
                .inputs(AnyIngredient.ANYTHING)
                .noOutput()
                .build(consumer);

        HopperRecipeBuilder.builder()
                .id(new ResourceLocation(References.MODID_MECHANIST,"filtered_hopper/iron_bars"))
                .filter(Ingredient.fromItems(Blocks.IRON_BARS))
                .action(HopperInput.Action.PERMIT)
                .inputs(new StackSizeIngredient(1, StackSizeIngredient.Condition.GREATER))
                .noOutput()
                .build(consumer);

        HopperRecipeBuilder.builder()
                .id(new ResourceLocation(References.MODID_MECHANIST,"filtered_hopper/concentrated_hellfire"))
                .filter(Ingredient.fromItems(Blocks.SOUL_SAND))
                .action(HopperInput.Action.CONSUME)
                .inputs(Ingredient.fromItems(Registrars.Items.NETHERRACK_DUST))
                .outputs(
                        new EjectResult(Registrars.Items.HELLFIRE_DUST),
                        new SoulResult(SoulType.LOST, 1)
                )
                .build(consumer);

        HopperRecipeBuilder.builder()
                .id(new ResourceLocation(References.MODID_MECHANIST,"filtered_hopper/gravel"))
                .filter(Ingredient.fromItems(Blocks.LADDER))
                .action(HopperInput.Action.CONSUME)
                .inputs(Ingredient.fromItems(Blocks.GRAVEL))
                .outputs(
                        new EjectResult(Items.FLINT),
                        new StackResult(Blocks.SAND)
                )
                .build(consumer);

    }

}

