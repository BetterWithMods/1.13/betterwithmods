/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.rope;

import com.betterwithmods.core.api.blocks.IRopeConnector;
import com.betterwithmods.core.base.game.block.BlockBase;
import com.betterwithmods.core.utilties.PlayerUtils;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class AnchorBlock extends BlockBase implements IRopeConnector {

    private static final DirectionProperty FACING = BlockStateProperties.FACING;
    private static final BooleanProperty CONNECTED = BooleanProperty.create("connected");

    private static final VoxelShape ROPE_BOTTOM = Block.makeCuboidShape(7, 0, 7, 9, 6, 9);
    private static final VoxelShape ROPE_TOP = Block.makeCuboidShape(7, 10, 7, 9, 16, 9);


    public AnchorBlock(Properties properties) {
        super(properties);
        setDefaultState(getDefaultState().with(FACING, Direction.UP).with(CONNECTED, false));

        setRenderLayer(BlockRenderLayer.CUTOUT);
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(FACING);
        builder.add(CONNECTED);
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return getDefaultState().with(FACING, context.getFace());
    }

    private boolean isConnector(IWorld world, BlockPos pos, Direction facing) {
        return world.getBlockState(pos.offset(facing)).getBlock() instanceof IRopeConnector;
    }

    @Override
    public BlockState updatePostPlacement(BlockState state, Direction facing, BlockState facingState, IWorld world, BlockPos currentPos, BlockPos facingPos) {
        boolean connected;

        if (state.get(FACING) == Direction.UP) {
            connected = isConnector(world, currentPos, Direction.UP);
        } else {
            connected = isConnector(world, currentPos, Direction.DOWN);
        }

        if (connected) {
            return state.with(CONNECTED, true);
        }
        return state.with(CONNECTED, false);
    }

    @Override
    public boolean canConnect(IWorldReader worldReader, BlockPos pos) {
        BlockState state = worldReader.getBlockState(pos);
        if (state.getBlock() == this)
            return state.get(FACING) != Direction.UP;
        return false;
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader world, BlockPos pos, ISelectionContext context) {
        Direction facing = state.get(FACING);
        boolean connected = state.get(CONNECTED);
        int i = facing.getIndex();
        return connected ? SHAPES_ROPE[i] : SHAPES[i];
    }

    @Override
    public VoxelShape getCollisionShape(BlockState state, IBlockReader p_220071_2_, BlockPos p_220071_3_, ISelectionContext p_220071_4_) {
        Direction facing = state.get(FACING);
        return SLAB_SHAPES[facing.getIndex()];
    }

    @Override
    public boolean onBlockActivated(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockRayTraceResult hit) {
        if (hand != Hand.MAIN_HAND)
            return false;

        ItemStack held = player.getHeldItem(hand);

        if (held.isEmpty()) {
            if (player.isSneaking()) {
                BlockPos lowest = RopeHandler.findLowestRope(world, pos);
                BlockState lowestState = world.getBlockState(lowest);
                //FIXME exact matching blocks :P
                if (lowestState.getBlock() == Registrars.Blocks.ROPE) {
                    world.playEvent(2001, lowest, Block.getStateId(lowestState));
                    world.removeBlock(lowest, false);
                    PlayerUtils.givePlayer(player, new ItemStack(Registrars.Blocks.ROPE));
                    return true;
                }
            }
        } else if (held.getItem() == Registrars.Blocks.ROPE.asItem()) {
            return RopeHandler.placeLowestRope(world, pos, player, held);
        }
        return false;
    }

    @Override
    public boolean isLadder(BlockState state, IWorldReader world, BlockPos pos, LivingEntity entity) {
        return state.get(FACING) != Direction.UP;
    }


    private static VoxelShape[] SHAPES_ROPE = new VoxelShape[6], SHAPES = new VoxelShape[6];

    private static VoxelShape[] ANCHOR_SHAPES = new VoxelShape[]{
            Block.makeCuboidShape(6, 6, 6, 10, 10, 10),
            Block.makeCuboidShape(6, 6, 6, 10, 10, 10),
            Block.makeCuboidShape(6, 6, 6, 10, 10, 10),
            Block.makeCuboidShape(6, 6, 6, 10, 10, 10),
            Block.makeCuboidShape(6, 6, 6, 10, 10, 10),
            Block.makeCuboidShape(6, 6, 6, 10, 10, 10),
    };

    private static VoxelShape[] SLAB_SHAPES = new VoxelShape[]{
            Block.makeCuboidShape(0, 10, 0, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 16, 6, 16),
            Block.makeCuboidShape(0, 0, 10, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 16, 16, 6),
            Block.makeCuboidShape(10, 0, 0, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 6, 16, 16),
    };

    static {
        for (int i = 0; i < SHAPES_ROPE.length; i++) {
            VoxelShape main = VoxelShapes.combineAndSimplify(SLAB_SHAPES[i], ANCHOR_SHAPES[i], IBooleanFunction.OR);
            SHAPES[i] = main;
            if(i == 1) {
                SHAPES_ROPE[i] = VoxelShapes.combineAndSimplify(main, ROPE_TOP, IBooleanFunction.OR);
            } else {
                SHAPES_ROPE[i] = VoxelShapes.combineAndSimplify(main, ROPE_BOTTOM, IBooleanFunction.OR);
            }
        }
    }
}
