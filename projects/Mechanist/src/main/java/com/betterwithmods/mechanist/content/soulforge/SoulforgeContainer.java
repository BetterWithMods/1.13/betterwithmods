/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.soulforge;

import com.betterwithmods.core.References;
import com.betterwithmods.core.base.game.ListenableItemStackHandler;
import com.betterwithmods.core.base.game.container.InventoryContainer;
import com.betterwithmods.core.base.game.container.SlotTransformation;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.ICraftingRecipe;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.network.play.server.SSetSlotPacket;
import net.minecraft.util.IIntArray;
import net.minecraft.util.IWorldPosCallable;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nullable;
import java.util.Optional;

public class SoulforgeContainer extends InventoryContainer {
    public static final ResourceLocation RESULT = new ResourceLocation(References.MODID_MECHANIST, "result");

    private IItemHandlerModifiable resultInventory;
    private final IWorldPosCallable callable;

    public SoulforgeContainer(int id, PlayerEntity player) {
        this(id, player, new ListenableItemStackHandler(16), new ItemStackHandler(1), IWorldPosCallable.DUMMY, null);
    }

    public SoulforgeContainer(int windowId, PlayerEntity player, ListenableItemStackHandler inventory, IItemHandlerModifiable resultInventory, IWorldPosCallable handler, @Nullable IIntArray data) {
        super(Registrars.Containers.SOULFORGE, windowId, player, inventory, data);
        this.resultInventory = resultInventory;
        this.callable = handler;
        inventory.setCallback(this::onChanged);
        addSlot(new SoulforgedResultSlot(this, inventory, resultInventory, 0, 123, 43, player));
        addSlotRange(RESULT, 0, 1);
        addSlots(CONTAINER, inventory, 16, 4, 0, 12, 17);
        addPlayerInventory(0, 8, 102, 58);
        addSlotTransformation(new SlotTransformation(PLAYER, CONTAINER));
        addSlotTransformation(new SlotTransformation(CONTAINER, PLAYER));
        addSlotTransformation(new SlotTransformation(RESULT, PLAYER, false) {
            @Override
            public void onPreTransfer(Slot slot, ItemStack start) {
                callable.consume((world, pos) -> {
                    start.getItem().onCreated(start, world, player);
                });
            }
            @Override
            public void onPostTransfer(Slot slot, ItemStack start, ItemStack end) {
                slot.onSlotChange(start, end);
                slot.onTake(player, end);
            }
        });
        onChanged(inventory);
    }

    @Override
    public void onChanged(IItemHandler handler) {
        super.onChanged(handler);
        callable.consume((world, pos) -> findRecipe(windowId, world, player, inventory, resultInventory));
    }

    private static ICraftingRecipe previousRecipe;

    private void findRecipe(int windowId, World world, PlayerEntity player, IItemHandlerModifiable inventory, IItemHandlerModifiable resultInventory) {
        if (!world.isRemote) {
            CraftingInventory inv = new SoulforgeCraftingInventory(this, 4, 4, inventory);
            ServerPlayerEntity serverplayerentity = (ServerPlayerEntity) player;
            ItemStack itemstack = ItemStack.EMPTY;
            if (previousRecipe != null && previousRecipe.matches(inv, world)) {
                itemstack = previousRecipe.getCraftingResult(inv);
            } else {
                Optional<ICraftingRecipe> optional = SoulforgeCrafting.getRecipe(world, inv, SoulforgeCrafting.SOULFORGE, IRecipeType.CRAFTING);
                if (optional.isPresent()) {
                    ICraftingRecipe recipe = optional.get();
                    itemstack = recipe.getCraftingResult(inv);
                    previousRecipe = recipe;
                }
            }
            resultInventory.setStackInSlot(0, itemstack);
            serverplayerentity.connection.sendPacket(new SSetSlotPacket(windowId, 0, itemstack));
        }
    }


}
