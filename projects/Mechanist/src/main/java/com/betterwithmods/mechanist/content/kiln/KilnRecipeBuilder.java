/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.kiln;

import com.betterwithmods.core.api.crafting.output.IOutput;
import com.betterwithmods.core.api.crafting.result.IResult;
import com.betterwithmods.core.generator.multiplex.FinishedMultiplexRecipe;
import com.betterwithmods.core.generator.multiplex.IFinishedMultiplexRecipe;
import com.betterwithmods.core.impl.crafting.input.BlockStateInput;
import com.betterwithmods.core.impl.crafting.output.OutputList;
import com.betterwithmods.core.utilties.DeserializeUtils;
import com.betterwithmods.mechanist.Registrars;
import com.google.common.collect.Lists;
import com.google.gson.JsonObject;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.ResourceLocation;

import java.util.function.Consumer;

public class KilnRecipeBuilder {

    protected ResourceLocation id;
    protected Ingredient input;
    protected IOutput outputs;
    protected int recipeTime;
    protected int heatLevel;

    public KilnRecipeBuilder() {
    }

    public static KilnRecipeBuilder builder() {
        return new KilnRecipeBuilder();
    }

    public KilnRecipeBuilder id(ResourceLocation id) {
        this.id = id;
        return this;
    }

    public KilnRecipeBuilder id(String modid, String name) {
        return id(new ResourceLocation(modid, name));
    }

    public KilnRecipeBuilder recipeTime(int recipeTime) {
        this.recipeTime = recipeTime;
        return this;
    }

    public KilnRecipeBuilder input(Ingredient input) {
        this.input = input;
        return this;
    }

    public KilnRecipeBuilder input(IItemProvider provider) {
        return this.input(Ingredient.fromItems(provider));
    }


    public KilnRecipeBuilder outputs(IOutput outputs) {
        this.outputs = outputs;
        return this;
    }

    public KilnRecipeBuilder outputs(IResult... outputs) {
        this.outputs(new OutputList(Lists.newArrayList(outputs)));
        return this;
    }

    public KilnRecipeBuilder heatLevel(int heatLevel) {
        this.heatLevel = heatLevel;
        return this;
    }

    public void build(Consumer<IFinishedMultiplexRecipe> consumer) {
        consumer.accept(new Result(id, input, outputs, recipeTime, heatLevel));
    }


    private class Result extends FinishedMultiplexRecipe {

        private BlockStateInput<KilnRecipe> input;
        private int recipeTime;
        private int heatLevel;

        public Result(ResourceLocation id, Ingredient input, IOutput outputs, int recipeTime, int heatLevel) {
            super(Registrars.MultiplexRecipeSerializers.KILN, id, outputs);
            this.input = new BlockStateInput<>(input);
            this.heatLevel = heatLevel;
            this.recipeTime = recipeTime;
        }

        @Override
        public void write(JsonObject jsonObject) {
            DeserializeUtils.writeInput(this.input, jsonObject);
            DeserializeUtils.writeOutput(this.outputs, jsonObject);
            jsonObject.addProperty("recipeTime", recipeTime);
            jsonObject.addProperty("heatLevel", heatLevel);
        }

    }

}
