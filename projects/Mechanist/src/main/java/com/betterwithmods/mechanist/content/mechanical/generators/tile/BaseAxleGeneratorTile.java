/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.mechanical.generators.tile;

import com.betterwithmods.core.api.mech.IPower;
import com.betterwithmods.core.base.game.tile.GeneratorTile;
import com.betterwithmods.core.utilties.DirectionUtils;
import com.betterwithmods.mechanist.content.mechanical.BaseMechanicalAxisBlock;
import com.betterwithmods.mechanist.content.mechanical.generators.block.BaseAxleGeneratorBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public abstract class BaseAxleGeneratorTile extends GeneratorTile {

    private static final int VALIDATE_DELAY_TICKS = 20;

    private double currentRotation, previousRotation;
    private int validateDelay;

    protected boolean valid;

    public BaseAxleGeneratorTile(TileEntityType<?> tileEntityTypeIn) {
        super(tileEntityTypeIn);
    }

    public Direction.Axis getAxis() {
        BlockState state = world.getBlockState(pos);
        if (state.getBlock() instanceof BaseMechanicalAxisBlock)
            return state.get(BaseMechanicalAxisBlock.AXIS);
        return null;
    }

    private boolean onAxis(Direction facing) {
        return facing != null && facing.getAxis().equals(getAxis());
    }

    public BaseAxleGeneratorBlock getBlock() {
        BlockState state = world.getBlockState(pos);
        Block block = state.getBlock();
        return (BaseAxleGeneratorBlock) block;
    }

    protected void validateGenerator() {
        BlockState state = world.getBlockState(pos);
        BaseAxleGeneratorBlock block = getBlock();
        valid = block.isValid(world, pos, state);
        markDirty();
    }

    public boolean isValid() {
        if (validateDelay < 0) {
            validateGenerator();
            validateDelay = VALIDATE_DELAY_TICKS;
        }
        validateDelay--;

        return valid;
    }

    @Nonnull
    @Override
    public Iterable<Direction> getOutputs() {
        return DirectionUtils.fromAxis(getAxis());
    }


    @Override
    public void postCalculate() {
        BlockState state = world.getBlockState(pos);
        if (state.getBlock() instanceof BaseMechanicalAxisBlock) {
            world.setBlockState(pos, state.with(BaseMechanicalAxisBlock.POWERED, power.getTorque() > 0));
        }
    }

    @Nullable
    @Override
    public IPower getOutput(@Nonnull IWorld world, @Nonnull BlockPos pos, @Nonnull Direction facing) {
        if (onAxis(facing)) {
            return super.getOutput(world, pos, facing);
        }
        return null;
    }

    public static final String
            CURRENT_ROTATION = "currentRotation",
            PREVIOUS_ROTATION = "previousRotation",
            VALIDATE_DELAY = "validateDelay",
            VALID = "valid";

    @Nonnull
    @Override
    public CompoundNBT write(CompoundNBT compound) {
        compound.putDouble(CURRENT_ROTATION, currentRotation);
        compound.putDouble(PREVIOUS_ROTATION, previousRotation);
        compound.putInt(VALIDATE_DELAY, validateDelay);
        compound.putBoolean(VALID, valid);
        return super.write(compound);
    }

    @Override
    public void read(CompoundNBT compound) {
        if (compound.contains(CURRENT_ROTATION))
            currentRotation = compound.getDouble(CURRENT_ROTATION);

        if (compound.contains(PREVIOUS_ROTATION))
            previousRotation = compound.getDouble(PREVIOUS_ROTATION);

        if (compound.contains(VALIDATE_DELAY))
            validateDelay = compound.getInt(VALIDATE_DELAY);

        if (compound.contains(VALID))
            valid = compound.getBoolean(VALID);

        super.read(compound);
    }

    private final double runningSpeed = 0.4d;

    @Override
    public void calculatePowerSource() {
        super.calculatePowerSource();
        this.calculateRotation();
    }

    public void calculateRotation() {
        this.previousRotation = calcPreviousRotation();
        this.currentRotation = (this.currentRotation + calcRotationIncrement()) % 360d;
    }

    public double getCurrentRotation() {
        return currentRotation;
    }

    public double getPreviousRotation() {
        return previousRotation;
    }

    public double calcRotationIncrement() {
        return this.power.getTorque() * this.power.getTorque() * runningSpeed;
    }

    public double calcPreviousRotation() {
        return this.power.getTorque() * runningSpeed;
    }

    @OnlyIn(Dist.CLIENT)
    public double getMaxRenderDistanceSquared() {
        return 65536.0D;
    }
}
