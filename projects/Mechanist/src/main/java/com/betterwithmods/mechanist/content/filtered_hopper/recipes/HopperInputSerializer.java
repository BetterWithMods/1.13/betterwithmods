/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.filtered_hopper.recipes;

import com.betterwithmods.core.impl.crafting.input.BaseInputSerializer;
import com.betterwithmods.core.utilties.DeserializeUtils;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;

public class HopperInputSerializer extends BaseInputSerializer<HopperState, HopperInput> {

    @Override
    public HopperInput read(JsonElement json) {
        JsonObject object = json.getAsJsonObject();
        JsonElement filterElement = object.get("filter");
        Ingredient filter = null;
        if (!DeserializeUtils.isEmpty(filterElement)) {
            filter = Ingredient.deserialize(filterElement);
        }
        Ingredient input = Ingredient.deserialize(object.get("inputs"));
        HopperInput.Action action = HopperInput.Action.getAction(JSONUtils.getString(object, "action"));
        return new HopperInput(filter, input, action);
    }

    @Override
    public HopperInput read(PacketBuffer buffer) {
        boolean filterNull = buffer.readBoolean();
        Ingredient filter = null;
        if (!filterNull) {
            filter = Ingredient.read(buffer);
        }
        Ingredient input = Ingredient.read(buffer);
        HopperInput.Action action = HopperInput.Action.getAction(buffer.readString());
        return new HopperInput(filter, input, action);
    }

    @Override
    public void write(PacketBuffer buffer, HopperInput input) {
        Ingredient filter = input.getFilter();
        boolean filterNull = filter == null;
        buffer.writeBoolean(filterNull);
        if (!filterNull) {
            filter.write(buffer);
        }
        input.getInput().write(buffer);
        buffer.writeString(input.getAction().getName());
    }

    @Override
    public <V extends HopperInput> void write(V input, JsonElement jsonElement) {
        JsonObject object = jsonElement.getAsJsonObject();
        Ingredient filter = input.getFilter();
        if (filter != null) {
            object.add("filter", filter.serialize());
        } else {
            object.add("filter", new JsonObject());
        }
        object.add("inputs", input.getInput().serialize());
        String action = input.getAction().getName();
        object.addProperty("action", action);
    }
}
