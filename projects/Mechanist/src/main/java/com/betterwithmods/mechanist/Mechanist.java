/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist;

import com.betterwithmods.core.References;
import com.betterwithmods.core.api.BWMApi;
import com.betterwithmods.core.base.setup.ModBase;
import com.betterwithmods.core.base.setup.ModConfiguration;
import com.betterwithmods.core.utilties.MechanicalUtils;
import com.betterwithmods.mechanist.config.Client;
import com.betterwithmods.mechanist.config.Common;
import com.betterwithmods.mechanist.content.detector.DetectionHandlerRegistry;
import com.betterwithmods.mechanist.content.soulforge.SoulforgeCrafting;
import com.betterwithmods.mechanist.generator.MechanistDataProviders;
import com.betterwithmods.mechanist.handlers.KilnStructureHandler;
import com.betterwithmods.mechanist.modules.*;
import com.betterwithmods.mechanist.modules.dynamicblocks.DynamicBlocks;
import com.betterwithmods.mechanist.proxy.ClientProxy;
import com.betterwithmods.mechanist.proxy.ServerProxy;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.Logger;

@Mod(References.MODID_MECHANIST)
public class Mechanist extends ModBase {
    public static Logger LOGGER;

    static {
        BWMApi.MECHANICAL_UTILS = new MechanicalUtils();
        BWMApi.DETECTION_HANDLER_REGISTRY = new DetectionHandlerRegistry();
    }

    public Mechanist() {
        super(References.MODID_MECHANIST, References.NAME_MECHANIST);
        LOGGER = this.getLogger();
        DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> {
            bus().addListener(this::onBlockColors);
            bus().addListener(this::onItemColors);
            bus().addListener(this::onModelBake);
        });
        new Registrars(References.MODID_MECHANIST, getLogger());

        addSetup(new MechanistDataProviders());
        addSetup(new KilnStructureHandler());
        addSetup(new SoulforgeCrafting());

        addProxy(() -> ClientProxy::new, () -> ServerProxy::new);

        addModule(new MechanicalPower());
        addModule(new RedstoneLamp());
        addModule(new DynamicBlocks());
        addModule(new BarkStripping());
        addModule(new StrategicWater());
        addModule(new CampFires());
        addModule(new Buoyancy());

        new ModConfiguration<>(Client::new, Common::new);
    }

}
