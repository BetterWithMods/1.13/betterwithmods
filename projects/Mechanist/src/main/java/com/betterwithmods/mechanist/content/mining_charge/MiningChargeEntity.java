/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.mining_charge;

import com.betterwithmods.mechanist.Registrars;
import net.minecraft.entity.*;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.PacketBuffer;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraftforge.fml.network.NetworkHooks;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class MiningChargeEntity extends Entity implements IEntityAdditionalSpawnData {

    private static final DataParameter<Integer> FUSE  = EntityDataManager.createKey(MiningChargeEntity.class, DataSerializers.VARINT);

    @Nullable
    private LivingEntity tntPlacedBy;

    private int fuse;
    private Direction facing = Direction.UP;

    public MiningChargeEntity(EntityType<? extends MiningChargeEntity> type, World world) {
        super(type, world);
        this.preventEntitySpawning = true;
    }


    public MiningChargeEntity(World world, BlockPos pos, Direction facing, @Nullable LivingEntity igniter) {
        this(Registrars.Entities.MINING_CHARGE, world);

        Vec3d p = new Vec3d(pos).add(0.5,0,0.5);
        this.setPosition(p.getX(), p.getY(), p.getZ());
        this.setMotion(Vec3d.ZERO);
        this.setFuse(80);
        this.setNoGravity(true);
        this.prevPosX = p.getX();
        this.prevPosY = p.getY();
        this.prevPosZ = p.getZ();
        this.tntPlacedBy = igniter;
        this.facing = facing;
    }


    protected void registerData() {
        this.dataManager.register(FUSE, 80);
    }

    protected boolean canTriggerWalking() {
        return false;
    }

    public boolean canBeCollidedWith() {
        return !this.isAlive();
    }

    public void tick() {
        this.prevPosX = this.posX;
        this.prevPosY = this.posY;
        this.prevPosZ = this.posZ;
        if (!this.hasNoGravity()) {
            this.setMotion(this.getMotion().add(0.0D, -0.04D, 0.0D));
        }

        this.move(MoverType.SELF, this.getMotion());
        this.setMotion(this.getMotion().scale(0.98D));
        if (this.onGround) {
            this.setMotion(this.getMotion().mul(0.7D, -0.5D, 0.7D));
        }
        --this.fuse;
        if (this.fuse <= 0) {
            this.remove();
            if (!this.world.isRemote) {
                this.explode();
            }
        } else {
            this.handleWaterMovement();
            this.world.addParticle(ParticleTypes.SMOKE, this.posX, this.posY + 0.5D, this.posZ, 0.0D, 0.0D, 0.0D);
        }

    }

    private void explode() {
        MiningChargeHandler handler = new MiningChargeHandler();
        handler.onExplode(world, getPosition(), facing);
    }

    protected void writeAdditional(CompoundNBT tag) {
        tag.putShort("Fuse", (short) this.getFuse());
        tag.putShort("Facing", (short) this.getFacing().getIndex());
    }

    protected void readAdditional(CompoundNBT tag) {
        this.setFuse(tag.getShort("Fuse"));
        this.setFacing(Direction.byIndex(tag.getShort("Facing")));
    }

    @Override
    protected float getEyeHeight(Pose p_213316_1_, EntitySize p_213316_2_) {
        return 0.0f;
    }

    @Nonnull
    public IPacket<?> createSpawnPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }


    public void setFacing(Direction facing) {
        this.facing = facing;
    }

    public void setFuse(int fuse) {
        this.dataManager.set(FUSE, fuse);
        this.fuse = fuse;
    }

    public Direction getFacing() {
        return this.facing;
    }

    @Override
    public void notifyDataManagerChange(DataParameter<?> parameter) {
        if (FUSE.equals(parameter)) {
            this.fuse = this.dataManager.get(FUSE);
        }
    }

    public int getFuse() {
        return this.fuse;
    }


    @Override
    public void writeSpawnData(PacketBuffer buffer) {
        buffer.writeEnumValue(getFacing());
    }

    @Override
    public void readSpawnData(PacketBuffer additionalData) {
        setFacing(additionalData.readEnumValue(Direction.class));
    }
}

