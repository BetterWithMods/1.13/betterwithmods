/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.mechanical.generators.block;

import com.betterwithmods.core.utilties.WorldUtils;
import com.betterwithmods.mechanist.content.mechanical.BaseMechanicalAxisBlock;
import com.betterwithmods.mechanist.content.mechanical.axle.AxleBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IWorldReader;

import javax.annotation.Nullable;
import java.util.Arrays;

public class BaseAxleGeneratorBlock extends BaseMechanicalAxisBlock {

    public BaseAxleGeneratorBlock(Properties properties) {
        super(properties);
    }

    public boolean allowAxis(Direction.Axis axis) {
        return false;
    }

    public boolean isValid(IWorldReader world, BlockPos pos, BlockState currentState) {
        return isAreaClear(world, pos, currentState);
    }

    @Override
    public boolean isValidPosition(BlockState state, IWorldReader world, BlockPos pos) {
        BlockState currentState = world.getBlockState(pos);
        Block block = currentState.getBlock();
        if (block instanceof AxleBlock) {
            Direction.Axis axis = currentState.get(BaseMechanicalAxisBlock.AXIS);
            return allowAxis(axis) && isValid(world, pos, currentState);
        }
        return false;
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        BlockState state = context.getWorld().getBlockState(context.getPos());
        if (state.has(AXIS)) {
            return getDefaultState()
                    .with(WATERLOGGED, shouldWaterlog(context))
                    .with(AXIS, state.get(AXIS));
        }
        return Blocks.AIR.getDefaultState();
    }


    public VoxelShape[] shapes() {
        VoxelShape[] a = new VoxelShape[3];
        Arrays.fill(a, VoxelShapes.fullCube());
        return a;
    }


    public VoxelShape getAreaShape(BlockState state) {
        if (state.getBlock() instanceof BaseMechanicalAxisBlock) {
            Direction.Axis axis = state.get(AXIS);
            return shapes()[axis.ordinal()];
        }
        return VoxelShapes.fullCube();
    }

    public boolean isAreaClear(IWorldReader world, BlockPos pos, BlockState currentState) {
        VoxelShape shape = getAreaShape(currentState);
        Direction.Axis axis = currentState.get(AXIS);
        if(shape.isEmpty())
            return false;
        AxisAlignedBB bounds = shape.getBoundingBox().offset(pos);
        Iterable<BlockPos> positions = WorldUtils.getAllBlocks(bounds);
        return WorldUtils.applyAll(world, positions, (w, p) -> (WorldUtils.onAxis(pos, p, axis)) || w.getBlockState(p).getMaterial().isReplaceable());
    }

}
