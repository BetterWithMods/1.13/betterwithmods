package com.betterwithmods.mechanist.compat.jei.category;

import com.betterwithmods.core.References;
import com.betterwithmods.core.api.crafting.result.IResult;
import com.betterwithmods.core.compat.jei.CoreJEI;
import com.betterwithmods.core.compat.jei.category.AbstractMultiplexCategory;
import com.betterwithmods.core.impl.souls.SoulStack;
import com.betterwithmods.core.utilties.StackEjector;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.compat.jei.MechanistJEI;
import com.betterwithmods.mechanist.content.filtered_hopper.recipes.HopperRecipe;
import com.betterwithmods.mechanist.content.filtered_hopper.recipes.HopperState;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IGuiIngredientGroup;
import mezz.jei.api.gui.ingredient.IGuiItemStackGroup;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.item.ItemStack;

import java.util.List;

public class FilteredHopperCategory extends AbstractMultiplexCategory<HopperState, HopperRecipe> {

    public FilteredHopperCategory(IGuiHelper guiHelper) {
        super(guiHelper,
                guiHelper.createDrawable(MechanistJEI.Constants.FILTERED_HOPPER_BACKGROUND, 0, 0, 166, 60),
                String.format("jei.%s.%s", References.MODID_MECHANIST, MechanistJEI.Constants.FILTERED_HOPPER_CATEGORY),
                guiHelper.createDrawableIngredient(new ItemStack(Registrars.Blocks.FILTERED_HOPPER)),
                MechanistJEI.Constants.FILTERED_HOPPER_CATEGORY);

    }

    @Override
    public Class<? extends HopperRecipe> getRecipeClass() {
        return HopperRecipe.class;
    }


    private void drawSlot(int x, int y, int w, int h, int u, int v) {
        IDrawable slot = guiHelper.createDrawable(MechanistJEI.Constants.FILTERED_HOPPER_BACKGROUND, u, v, w, h);
        slot.draw(x, y);
    }


    @Override
    public void draw(HopperRecipe recipe, double mouseX, double mouseY) {
        List<IResult<ItemStack>> stackResults = getOutputs(recipe.getOutput(), ItemStack.class);
        if (!stackResults.isEmpty()) {
            //ItemStacks

            int count = stackResults.size();
            int startX = 109, startY = 22;
            int columns = Math.min(count, 3);
            int rows = Math.min(2, Math.max(1,count / columns));
            for (int j = 0; j < rows; j++) {
                for (int i = 0; i < columns; i++) {
                    int x = startX + i * SLOT_WIDTH;
                    int y = startY + j * SLOT_WIDTH;
                    drawSlot(x, y, SLOT_WIDTH, SLOT_WIDTH, 167, 0);
                }
            }

        }


    }

    @Override
    public void setIngredients(HopperRecipe recipe, IIngredients ingredients) {
        ingredients.setInputIngredients(recipe.getInputs());
        setOutputs(ingredients, recipe.getOutput(), CoreJEI.STACK_RESULT, ItemStack.class);
        setOutputs(ingredients, recipe.getOutput(), MechanistJEI.EJECT_RESULT, StackEjector.class);
        setOutputs(ingredients, recipe.getOutput(), MechanistJEI.SOUL_RESULT, SoulStack.class);
    }

    @Override
    public void setRecipe(IRecipeLayout layout, HopperRecipe recipe, IIngredients ingredients) {
        IGuiItemStackGroup guiItemStacks = layout.getItemStacks();
        IGuiIngredientGroup<IResult<StackEjector>> guiEjectResults = layout.getIngredientsGroup(MechanistJEI.EJECT_RESULT);
        IGuiIngredientGroup<IResult<SoulStack>> guiSoulResults = layout.getIngredientsGroup(MechanistJEI.SOUL_RESULT);
        IGuiIngredientGroup<IResult<ItemStack>> guiStackResults = layout.getIngredientsGroup(CoreJEI.STACK_RESULT);

        //Filter
        createSlot(guiItemStacks, true, 0, 34, 5);
        //Inputs
        createSlot(guiItemStacks, true, 1, 8, 22);

        createSlot(guiEjectResults, false, 1, 87, 6);

        createSlotsHorizontal(guiStackResults, false, 3, 2, 110, 23);

        guiItemStacks.set(ingredients);
        guiStackResults.set(ingredients);
        guiEjectResults.set(ingredients);
        guiSoulResults.set(ingredients);


    }
}
