/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.soulforge;

import com.betterwithmods.core.base.setup.ISetup;
import com.betterwithmods.mechanist.content.soulforge.recipes.ISoulforgeRecipe;
import com.google.common.collect.Lists;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.ICraftingRecipe;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.item.crafting.RecipeManager;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;

import java.util.List;
import java.util.Optional;

public class SoulforgeCrafting implements ISetup {
    public static final IRecipeType<ISoulforgeRecipe> SOULFORGE = IRecipeType.register("soulforge");

    @SafeVarargs
    public static List<ICraftingRecipe> getRecipes(World world, CraftingInventory inventory, IRecipeType<? extends ICraftingRecipe>... types) {
        RecipeManager manager = world.getRecipeManager();
        List<ICraftingRecipe> recipes = Lists.newArrayList();
        for (IRecipeType<? extends ICraftingRecipe> type : types) {
            recipes.addAll(manager.getRecipes(type, inventory, world));
        }
        return recipes;
    }

    @SafeVarargs
    public static Optional<ICraftingRecipe> getRecipe(World world, CraftingInventory inventory, IRecipeType<? extends ICraftingRecipe>... types) {
        return getRecipes(world, inventory, types).stream().findFirst();
    }

    @SafeVarargs
    public static NonNullList<ItemStack> getRemainingItems(World world, CraftingInventory inventory, IRecipeType<? extends ICraftingRecipe>... types) {
        Optional<ICraftingRecipe> optional = getRecipe(world, inventory, types);
        if (optional.isPresent()) {
            return optional.map(r -> r.getRemainingItems(inventory)).orElse(NonNullList.create());
        } else {
            NonNullList<ItemStack> nonnulllist = NonNullList.withSize(inventory.getSizeInventory(), ItemStack.EMPTY);
            for(int i = 0; i < nonnulllist.size(); ++i) {
                nonnulllist.set(i, inventory.getStackInSlot(i));
            }
            return nonnulllist;
        }
    }
}
