/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.generator;

import com.betterwithmods.core.References;
import com.betterwithmods.core.Relationships;
import com.betterwithmods.core.Tags;
import com.betterwithmods.core.base.game.ConstantIngredients;
import com.betterwithmods.core.base.game.ingredient.ShapeIngredient;
import com.betterwithmods.core.base.game.ingredient.TagIngredient;
import com.betterwithmods.core.base.game.ingredient.ToolIngredient;
import com.betterwithmods.core.base.game.loottables.RelationaryLootEntry;
import com.betterwithmods.core.base.game.loottables.conditions.ConstantCondition;
import com.betterwithmods.core.base.game.loottables.conditions.MatchToolType;
import com.betterwithmods.core.base.game.loottables.conditions.ModuleLootCondition;
import com.betterwithmods.core.base.providers.DataProviders;
import com.betterwithmods.core.generator.LootTableProvider;
import com.betterwithmods.core.generator.RecipeProvider;
import com.betterwithmods.core.generator.*;
import com.betterwithmods.core.relationary.Relation;
import com.betterwithmods.core.relationary.Shape;
import com.betterwithmods.core.utilties.DyeUtils;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.content.dynamic.CamoflageBlock;
import com.betterwithmods.mechanist.content.dynamic.CamoflageTile;
import com.betterwithmods.mechanist.content.soulforge.recipes.SoulforgeShapedRecipeBuilder;
import com.google.common.collect.Sets;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.data.*;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.DyeColor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.Tag;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.storage.loot.ConstantRange;
import net.minecraft.world.storage.loot.ItemLootEntry;
import net.minecraft.world.storage.loot.LootParameterSets;
import net.minecraft.world.storage.loot.LootTable;
import net.minecraft.world.storage.loot.conditions.ILootCondition;
import net.minecraft.world.storage.loot.conditions.RandomChance;
import net.minecraft.world.storage.loot.conditions.SurvivesExplosion;
import net.minecraft.world.storage.loot.functions.CopyNbt;
import net.minecraft.world.storage.loot.functions.SetCount;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.common.crafting.ConditionalRecipe;
import net.minecraftforge.common.crafting.conditions.ICondition;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;

import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

public class MechanistDataProviders extends DataProviders {

    @Override
    public void onGatherData(GatherDataEvent event) {
        DataGenerator generator = event.getGenerator();
        if (event.includeServer()) {
            generator.addProvider(new BlockTagsProvider(generator));
            generator.addProvider(new ItemTagsProvider(generator));
            generator.addProvider(new FluidTagsProvider(generator) {
                @Override
                protected void registerTags() {
                    getBuilder(Tags.Fluids.WATERWHEEL).add(Fluids.WATER);
                }
            });
            generator.addProvider(new LoottableProvider(generator));
            generator.addProvider(new Relationary(generator));
            generator.addProvider(new Recipes(generator));
            generator.addProvider(new MultiplexRecipes(generator));
        }
    }


    private static class BlockTagsProvider extends net.minecraft.data.BlockTagsProvider {

        private BlockTagsProvider(DataGenerator generatorIn) {
            super(generatorIn);
        }

        @Override
        protected void registerTags() {

            getBuilder(Tags.Blocks.CHOPPING_BLOCKS).add(Registrars.Blocks.CHOPPING_BLOCK);
            getBuilder(Tags.Blocks.HANDCRANKABLE).add(Registrars.Blocks.MILLSTONE);
            getBuilder(Tags.Blocks.SUPPORTED_STOKED_FIRE).add(Registrars.Blocks.HIBACHI);
            getBuilder(Tags.Blocks.NORMAL_FIRE).add(Blocks.FIRE);
            getBuilder(Tags.Blocks.STOKED_FIRE).add(Registrars.Blocks.STOKED_FIRE);
            getBuilder(Tags.Blocks.KILN_BLOCKS).add(Blocks.BRICKS, Blocks.BRICK_STAIRS, Blocks.BRICK_SLAB);
            Tag.Builder<Block> t = getBuilder(Tags.Blocks.REDSTONE_LAMP);
            for (Block block : Registrars.Blocks.REDSTONE_LAMPS.values())
                t.add(block);

        }
    }


    private static class ItemTagsProvider extends net.minecraft.data.ItemTagsProvider {

        private ItemTagsProvider(DataGenerator generatorIn) {
            super(generatorIn);
        }

        @Override
        protected void registerTags() {
            getBuilder(Tags.Items.SEEDS).add(Registrars.Items.HEMP_SEED);
            getBuilder(Tags.Items.SOAP).add(Registrars.Items.SOAP);
            getBuilder(Tags.Items.ADHESIVE).add(Items.SLIME_BALL, Registrars.Items.GLUE);
            getBuilder(Tags.Items.FIBER_HEMP).add(Registrars.Items.HEMP_FIBER);
            getBuilder(Tags.Items.TIPPING_BLACKLIST).add(Items.BRICK);

            getBuilder(Tags.Items.INGOT_SOULFORGED_STEEL).add(Registrars.Items.SOULFORGED_STEEL_INGOT);
            getBuilder(Tags.Items.NUGGETS_SOULFORGED_STEEL).add(Registrars.Items.SOULFORGED_STEEL_NUGGET);
            getBuilder(Tags.Items.INGOT_DIAMOND).add(Registrars.Items.DIAMOND_INGOT);
            getBuilder(Tags.Items.DUST_COAL).add(Registrars.Items.COAL_DUST);
            getBuilder(Tags.Items.DUST_SULFUR).add(Registrars.Items.BRIMSTONE);
            getBuilder(Tags.Items.DUST_SALTPETER).add(Registrars.Items.NITER);
            getBuilder(Tags.Items.DUST_WOOD).add(Registrars.Items.SAW_DUST);
            getBuilder(Tags.Items.DUST_HELLFIRE).add(Registrars.Items.HELLFIRE_DUST);
            getBuilder(Tags.Items.GEAR_WOOD).add(Registrars.Items.WOODEN_GEAR);

            getBuilder(Tags.Items.TANNED_LEATHER).add(Registrars.Items.TANNED_LEATHER, Registrars.Items.TANNED_LEATHER_SLICE);

            copy(Tags.Blocks.REDSTONE_LAMP, Tags.Items.REDSTONE_LAMP);
        }

    }


    private static class LoottableProvider extends LootTableProvider {

        public LoottableProvider(DataGenerator gen) {
            super(gen);
        }

        @Override
        public void registerTables() {
            add(MechanistBlockLootTables::new, LootParameterSets.BLOCK);
        }

    }

    private static class MechanistBlockLootTables extends BlockLootTables {
        private static final ILootCondition.IBuilder AXE = MatchToolType.builder(ToolType.AXE);
        private static final ILootCondition.IBuilder NO_AXE = AXE.inverted();
        private static final ILootCondition.IBuilder LOG = ModuleLootCondition.builder("log_shattering", AXE, ConstantCondition.booleanBuilder(true));
        private static final ILootCondition.IBuilder OTHER_DROPS = ModuleLootCondition.builder("log_shattering", NO_AXE, ConstantCondition.booleanBuilder(false));

        public MechanistBlockLootTables() {
            super(Registrars.BLOCKS.getEntries());
        }

        public void registerCamoBlock(Block block) {
            registerLootTable(block,
                    LootTable.builder()
                            .addLootPool(pool("main")
                                    .acceptCondition(SurvivesExplosion.builder())
                                    .rolls(ConstantRange.of(1))
                                    .addEntry(ItemLootEntry.builder(block)
                                            .acceptFunction(CopyNbt.func_215881_a(CopyNbt.Source.BLOCK_ENTITY)
                                                    .func_216056_a("parent", "BlockEntityTag.parent"))))
            );
        }

        public void registerLogs(Block block) {
            registerLootTable(block,
                    LootTable.builder()
                            .addLootPool(pool("axe")
                                    .acceptCondition(SurvivesExplosion.builder())
                                    .acceptCondition(LOG)
                                    .rolls(ConstantRange.of(1))
                                    .addEntry(ItemLootEntry.builder(block)))
                            .addLootPool(pool("planks")
                                    .acceptCondition(SurvivesExplosion.builder())
                                    .acceptCondition(OTHER_DROPS)
                                    .rolls(ConstantRange.of(1))
                                    .addEntry(RelationaryLootEntry.create(Relationships.LOG, Relationships.PLANK).acceptFunction(SetCount.func_215932_a(ConstantRange.of(3))))
                            )
                            .addLootPool(pool("bark")
                                    .acceptCondition(SurvivesExplosion.builder())
                                    .acceptCondition(OTHER_DROPS)
                                    .rolls(ConstantRange.of(1))
                                    .addEntry(RelationaryLootEntry.create(Relationships.LOG, Relationships.BARK).acceptFunction(SetCount.func_215932_a(ConstantRange.of(1))))
                            )
                            .addLootPool(pool("sawdust")
                                    .acceptCondition(SurvivesExplosion.builder())
                                    .acceptCondition(OTHER_DROPS)
                                    .rolls(ConstantRange.of(1))
                                    .addEntry(RelationaryLootEntry.create(Relationships.LOG, Relationships.DUST).acceptFunction(SetCount.func_215932_a(ConstantRange.of(1))))
                            )
            );
        }

        @Override
        public void register() {
            Set<Block> blocks = Sets.newHashSet(
                    Registrars.Blocks.HIBACHI,
                    Registrars.Blocks.ADVANCED_OBSERVER,
                    Registrars.Blocks.CAULDRON,
                    Registrars.Blocks.CRUCIBLE,
                    Registrars.Blocks.ANCHOR,
                    Registrars.Blocks.ROPE,
                    Registrars.Blocks.MINING_CHARGE,
                    Registrars.Blocks.DIRT_SLAB,
                    Registrars.Blocks.VINE_TRAP,
                    Registrars.Blocks.AXLE,
                    Registrars.Blocks.CLUTCHBOX,
                    Registrars.Blocks.GEARBOX,
                    Registrars.Blocks.CLUTCHBOX_BROKEN,
                    Registrars.Blocks.GEARBOX_BROKEN,
                    Registrars.Blocks.MILLSTONE,
                    Registrars.Blocks.CREATIVE_GENERATOR,
                    Registrars.Blocks.HORIZONTAL_WINDMILL,
                    Registrars.Blocks.VERTICAL_WINDMILL,
                    Registrars.Blocks.WATERWHEEL,
                    Registrars.Blocks.HANDCRANK,
                    Registrars.Blocks.PADDING_BLOCK,
                    Registrars.Blocks.DETECTOR,
                    Registrars.Blocks.ADVANCED_DISPENSER,
                    Registrars.Blocks.SAW,
                    Registrars.Blocks.CHOPPING_BLOCK,
                    Registrars.Blocks.SOUL_URN,
                    Registrars.Blocks.URN,
                    Registrars.Blocks.UNFIRED_URN,
                    Registrars.Blocks.BELLOWS,
                    Registrars.Blocks.TURNTABLE,
                    Registrars.Blocks.UNFIRED_CRUCIBLE,
                    Registrars.Blocks.UNFIRED_PLANTER,
                    Registrars.Blocks.UNFIRED_VASE,
                    Registrars.Blocks.UNFIRED_URN,
                    Registrars.Blocks.UNFIRED_CLAY_BRICK,
                    Registrars.Blocks.UNFIRED_NETHERSLUDGE_BRICK,
                    Registrars.Blocks.FLOUR,
                    Registrars.Blocks.RAW_CAKE,
                    Registrars.Blocks.RAW_COOKIE,
                    Registrars.Blocks.WHITE_STONE,
                    Registrars.Blocks.WHITE_COBBLESTONE,
                    Registrars.Blocks.SOULFORGE,
                    Registrars.Blocks.FILTERED_HOPPER

            );
            blocks.addAll(Registrars.Blocks.REDSTONE_LAMPS.values());
            blocks.addAll(Registrars.Blocks.PLANTERS);
            blocks.forEach(this::registerBasicBlock);

            registerBasicBlock(Registrars.Blocks.GRASS_SLAB, Registrars.Blocks.DIRT_SLAB);
            registerBasicBlock(Registrars.Blocks.GRASS_PATH_SLAB, Registrars.Blocks.DIRT_SLAB);


            registerLootTable(Registrars.Blocks.HEMP_PLANT_BOTTOM,
                    LootTable.builder()
                            .addLootPool(pool("main")
                                    .acceptCondition(SurvivesExplosion.builder())
                                    .rolls(ConstantRange.of(1))
                                    .addEntry(ItemLootEntry.builder(Registrars.Items.HEMP_SEED).acceptCondition(RandomChance.builder(0.1f))))
            );


            registerLootTable(Registrars.Blocks.HEMP_PLANT_TOP,
                    LootTable.builder()
                            .addLootPool(pool("main")
                                    .acceptCondition(SurvivesExplosion.builder())
                                    .rolls(ConstantRange.of(1))
                                    .addEntry(ItemLootEntry.builder(Registrars.Items.HEMP_LEAF)))
                            .addLootPool(pool("seeds")
                                    .acceptCondition(SurvivesExplosion.builder())
                                    .rolls(ConstantRange.of(1))
                                    .addEntry(ItemLootEntry.builder(Registrars.Items.HEMP_SEED).acceptCondition(RandomChance.builder(0.1f))))
            );

            for (Block block : CamoflageBlock.getAll()) {
                registerCamoBlock(block);
            }

            registerLogs(Blocks.OAK_LOG);
            registerLogs(Blocks.BIRCH_LOG);
            registerLogs(Blocks.SPRUCE_LOG);
            registerLogs(Blocks.DARK_OAK_LOG);
            registerLogs(Blocks.JUNGLE_LOG);
            registerLogs(Blocks.ACACIA_LOG);

            registerLogs(Blocks.STRIPPED_OAK_LOG);
            registerLogs(Blocks.STRIPPED_BIRCH_LOG);
            registerLogs(Blocks.STRIPPED_SPRUCE_LOG);
            registerLogs(Blocks.STRIPPED_DARK_OAK_LOG);
            registerLogs(Blocks.STRIPPED_JUNGLE_LOG);
            registerLogs(Blocks.STRIPPED_ACACIA_LOG);
        }
    }

    private static class Relationary extends RelationaryProviders {

        public Relationary(DataGenerator generatorIn) {
            super(generatorIn);
        }

        private static final ItemStack bark(Block parent) {
            return CamoflageTile.fromParent(Registrars.Blocks.BARK, parent.getDefaultState(), 1);
        }

        @Override
        public void registerRelations(Consumer<Relation> consumer) {
            Set<Relation> relations = Sets.newHashSet();


            relations.add(Relationships.ACACIA_WOOD
                    .addShape(Relationships.DUST, new ItemStack(Registrars.Items.SAW_DUST))
                    .addShape(Relationships.BARK, bark(Blocks.ACACIA_LOG)));

            relations.add(Relationships.BIRCH_WOOD
                    .addShape(Relationships.DUST, new ItemStack(Registrars.Items.SAW_DUST))
                    .addShape(Relationships.BARK, bark(Blocks.BIRCH_LOG)));

            relations.add(Relationships.DARK_OAK_WOOD
                    .addShape(Relationships.DUST, new ItemStack(Registrars.Items.SAW_DUST))
                    .addShape(Relationships.BARK, bark(Blocks.DARK_OAK_WOOD)));

            relations.add(Relationships.JUNGLE_WOOD
                    .addShape(Relationships.DUST, new ItemStack(Registrars.Items.SAW_DUST))
                    .addShape(Relationships.BARK, bark(Blocks.JUNGLE_LOG)));

            relations.add(Relationships.OAK_WOOD
                    .addShape(Relationships.DUST, new ItemStack(Registrars.Items.SAW_DUST))
                    .addShape(Relationships.BARK, bark(Blocks.OAK_LOG)));

            relations.add(Relationships.SPRUCE_WOOD
                    .addShape(Relationships.DUST, new ItemStack(Registrars.Items.SAW_DUST))
                    .addShape(Relationships.BARK, bark(Blocks.SPRUCE_LOG)));

            relations.add(Relationships.STRIPPED_ACACIA_WOOD
                    .addShape(Relationships.DUST, new ItemStack(Registrars.Items.SAW_DUST)));

            relations.add(Relationships.STRIPPED_BIRCH_WOOD
                    .addShape(Relationships.DUST, new ItemStack(Registrars.Items.SAW_DUST)));

            relations.add(Relationships.STRIPPED_DARK_OAK_WOOD
                    .addShape(Relationships.DUST, new ItemStack(Registrars.Items.SAW_DUST)));

            relations.add(Relationships.STRIPPED_JUNGLE_WOOD
                    .addShape(Relationships.DUST, new ItemStack(Registrars.Items.SAW_DUST)));

            relations.add(Relationships.STRIPPED_OAK_WOOD
                    .addShape(Relationships.DUST, new ItemStack(Registrars.Items.SAW_DUST)));

            relations.add(Relationships.STRIPPED_SPRUCE_WOOD
                    .addShape(Relationships.DUST, new ItemStack(Registrars.Items.SAW_DUST)));

            relations.add(Relationships.SOULFORGED_STEEL
                    .addShape(Relationships.INGOT, new ItemStack(Registrars.Items.SOULFORGED_STEEL_INGOT))
                    .addShape(Relationships.NUGGET, new ItemStack(Registrars.Items.SOULFORGED_STEEL_NUGGET)));


            relations.forEach(consumer);
        }

        @Override
        public void registerShapes(Consumer<Shape> consumer) {

        }
    }

    public static class Recipes extends RecipeProvider {


        public Recipes(DataGenerator generatorIn) {
            super(generatorIn);
        }

        private static ResourceLocation format(IItemProvider provider, String format) {
            ResourceLocation item = provider.asItem().getRegistryName();
            return new ResourceLocation(String.format(format, item.getNamespace(), item.getPath()));
        }

        private static ResourceLocation highEff(IItemProvider provider) {
            return format(provider, "%s:high_efficiency_%s");
        }

        @Override
        public void registerRecipes(Consumer<IFinishedRecipe> consumer) {


            Ingredient INGOTS_SOULFORGED_STEEL = new TagIngredient(Tags.Items.INGOT_SOULFORGED_STEEL);
            Ingredient INGOTS_IRON = new TagIngredient(net.minecraftforge.common.Tags.Items.INGOTS_IRON);
            Ingredient NUGGETS_GOLD = new TagIngredient(net.minecraftforge.common.Tags.Items.NUGGETS_GOLD);
            Ingredient GEMS_LAPIS = new TagIngredient(net.minecraftforge.common.Tags.Items.GEMS_LAPIS);
            Ingredient STICKS = new TagIngredient(net.minecraftforge.common.Tags.Items.RODS_WOODEN);
            Ingredient PLANKS = new TagIngredient(ItemTags.PLANKS);
            Ingredient WOOL = new TagIngredient(ItemTags.WOOL);
            Ingredient STONE = new TagIngredient(net.minecraftforge.common.Tags.Items.STONE);
            Ingredient COBBLESTONE = new TagIngredient(net.minecraftforge.common.Tags.Items.COBBLESTONE);
            Ingredient DUSTS_REDSTONE = new TagIngredient(net.minecraftforge.common.Tags.Items.DUSTS_REDSTONE);

            Ingredient WOOD_MOULDING = new ShapeIngredient(PLANKS, Relationships.BLOCK, Relationships.MOULDING);
            Ingredient WOOD_SIDING = new ShapeIngredient(PLANKS, Relationships.BLOCK, Relationships.SIDING);

            ShapelessRecipeBuilder.shapelessRecipe(Registrars.Items.PADDING)
                    .addIngredient(Registrars.Items.HEMP_CLOTH)
                    .addIngredient(Items.FEATHER)
                    .addCriterion("has_hemp_cloth", hasItem(Registrars.Items.HEMP_CLOTH))
                    .build(consumer);

            ShapelessRecipeBuilder.shapelessRecipe(Registrars.Blocks.UNFIRED_CLAY_BRICK, 1)
                    .addIngredient(Items.CLAY_BALL)
                    .addCriterion("has_clay", hasItem(Items.CLAY_BALL))
                    .build(consumer);

            ShapelessRecipeBuilder.shapelessRecipe(Registrars.Blocks.UNFIRED_NETHERSLUDGE_BRICK, 1)
                    .addIngredient(Registrars.Items.NETHERSLUDGE)
                    .addCriterion("has_nethersludge", hasItem(Registrars.Items.NETHERSLUDGE))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.ANCHOR).key('#', INGOTS_IRON).key('@', Blocks.SMOOTH_STONE).patternLine(" # ").patternLine("@@@").addCriterion("has_iron", this.hasItemProvider(Items.IRON_INGOT)).build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Items.WOODEN_GEAR)
                    .key('#', PLANKS)
                    .key('S', STICKS)
                    .patternLine("S#S")
                    .patternLine("# #")
                    .patternLine("S#S")
                    .addCriterion("has_planks", this.hasItemProvider(ItemTags.PLANKS))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.ROPE)
                    .key('#', Tags.Items.FIBER_HEMP)
                    .patternLine("##")
                    .patternLine("##")
                    .patternLine("##")
                    .addCriterion("has_hemp_fiber", this.hasItemProvider(Tags.Items.FIBER_HEMP))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Items.HEMP_CLOTH)
                    .key('#', Tags.Items.FIBER_HEMP)
                    .patternLine("###")
                    .patternLine("###")
                    .patternLine("###")
                    .addCriterion("has_hemp_fiber", this.hasItemProvider(Tags.Items.FIBER_HEMP))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.VINE_TRAP, 3)
                    .key('#', Blocks.VINE)
                    .patternLine("###")
                    .addCriterion("has_vines", this.hasItemProvider(Blocks.VINE))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.MINING_CHARGE, 1)
                    .key('R', Registrars.Blocks.ROPE)
                    .key('A', Tags.Items.ADHESIVE)
                    .key('D', Registrars.Items.DYNAMITE)
                    .patternLine("RAR")
                    .patternLine("DDD")
                    .patternLine("DDD")
                    .addCriterion("has_dynamite", this.hasItemProvider(Registrars.Items.DYNAMITE))
                    .build(consumer);

            ShapelessRecipeBuilder.shapelessRecipe(Registrars.Items.DYNAMITE, 1)
                    .addIngredient(Items.PAPER)
                    .addIngredient(Items.PAPER)
                    .addIngredient(Items.PAPER)
                    .addIngredient(Registrars.Items.FUSE)
                    .addIngredient(Registrars.Items.BLASTING_OIL)
                    .addIngredient(Tags.Items.DUST_WOOD)
                    .addCriterion("has_blasting_oil", this.hasItemProvider(Registrars.Items.BLASTING_OIL))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.HIBACHI, 1)
                    .key('H', Registrars.Items.CONCENTRATED_HELLFIRE)
                    .key('E', Registrars.Items.ELEMENT)
                    .key('S', STONE)
                    .key('R', DUSTS_REDSTONE)
                    .patternLine("HHH")
                    .patternLine("SES")
                    .patternLine("SRS")
                    .addCriterion("has_hellfire", this.hasItemProvider(Registrars.Items.CONCENTRATED_HELLFIRE))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.MILLSTONE, 1)
                    .key('S', STONE)
                    .key('G', Tags.Items.GEAR_WOOD)
                    .patternLine("SSS")
                    .patternLine("SSS")
                    .patternLine("SGS")
                    .addCriterion("has_wooden_gear", this.hasItemProvider(Tags.Items.GEAR_WOOD))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.HANDCRANK, 1)
                    .key('S', STICKS)
                    .key('#', COBBLESTONE)
                    .key('G', Tags.Items.GEAR_WOOD)
                    .patternLine("  S")
                    .patternLine(" S ")
                    .patternLine("#G#")
                    .addCriterion("has_wooden_gear", this.hasItemProvider(Tags.Items.GEAR_WOOD))
                    .build(consumer);

            Block axle = Registrars.Blocks.AXLE;
            ShapedRecipeBuilder.shapedRecipe(axle, 1)
                    .key('P', PLANKS)
                    .key('R', Registrars.Blocks.ROPE)
                    .patternLine("PRP")
                    .addCriterion("has_rope", this.hasItemProvider(Registrars.Blocks.ROPE))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(axle, 1)
                    .key('P', WOOD_MOULDING)
                    .key('R', Registrars.Blocks.ROPE)
                    .patternLine("PRP")
                    .addCriterion("has_rope", this.hasItemProvider(Registrars.Blocks.ROPE))
                    .build(consumer, highEff(axle));

            Block gearbox = Registrars.Blocks.GEARBOX;

            ShapedRecipeBuilder.shapedRecipe(gearbox, 1)
                    .key('P', PLANKS)
                    .key('G', Tags.Items.GEAR_WOOD)
                    .key('A', Registrars.Blocks.AXLE)
                    .patternLine("PGP")
                    .patternLine("GAG")
                    .patternLine("PGP")
                    .addCriterion("has_axle", this.hasItemProvider(Registrars.Blocks.AXLE))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(gearbox, 1)
                    .key('P', WOOD_SIDING)
                    .key('G', Tags.Items.GEAR_WOOD)
                    .key('A', Registrars.Blocks.AXLE)
                    .patternLine("PGP")
                    .patternLine("GAG")
                    .patternLine("PGP")
                    .addCriterion("has_axle", this.hasItemProvider(Registrars.Blocks.AXLE))
                    .build(consumer, highEff(gearbox));


            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.GEARBOX, 1)
                    .key('B', Registrars.Blocks.GEARBOX_BROKEN)
                    .key('G', Tags.Items.GEAR_WOOD)
                    .patternLine(" G ")
                    .patternLine("GBG")
                    .patternLine(" G ")
                    .addCriterion("has_broken_gearbox", this.hasItemProvider(Registrars.Blocks.GEARBOX_BROKEN))
                    .build(consumer, format(Registrars.Blocks.GEARBOX, "%s:repair_%s"));

            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.CLUTCHBOX, 1)
                    .key('B', Registrars.Blocks.CLUTCHBOX_BROKEN)
                    .key('G', Tags.Items.GEAR_WOOD)
                    .patternLine(" G ")
                    .patternLine("GBG")
                    .patternLine(" G ")
                    .addCriterion("has_broken_clutchbox", this.hasItemProvider(Registrars.Blocks.CLUTCHBOX_BROKEN))
                    .build(consumer, format(Registrars.Blocks.CLUTCHBOX, "%s:repair_%s"));

            ShapedRecipeBuilder.shapedRecipe(Registrars.Items.REDSTONE_LATCH, 1)
                    .key('R', DUSTS_REDSTONE)
                    .key('G', NUGGETS_GOLD)
                    .patternLine(" R ")
                    .patternLine("GGG")
                    .addCriterion("has_gold_nugget", this.hasItemProvider(net.minecraftforge.common.Tags.Items.NUGGETS_GOLD))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.TURNTABLE, 1)
                    .key('W', WOOD_SIDING)
                    .key('S', STONE)
                    .key('G', Tags.Items.GEAR_WOOD)
                    .key('C', Items.CLOCK)
                    .patternLine("WWW")
                    .patternLine("SCS")
                    .patternLine("SGS")
                    .addCriterion("has_clock", this.hasItemProvider(Items.CLOCK))
                    .build(consumer);

            Ingredient TANNED_LEATHER = new TagIngredient(Tags.Items.TANNED_LEATHER);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.BELLOWS, 1)
                    .key('W', WOOD_SIDING)
                    .key('L', TANNED_LEATHER)
                    .key('G', Tags.Items.GEAR_WOOD)
                    .key('B', Registrars.Items.TANNED_LEATHER_BELT)
                    .patternLine("WWW")
                    .patternLine("LLL")
                    .patternLine("BGB")
                    .addCriterion("has_tanned_belt", this.hasItemProvider(Registrars.Items.TANNED_LEATHER_BELT))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.SAW, 1)
                    .key('I', INGOTS_IRON)
                    .key('B', Registrars.Items.TANNED_LEATHER_BELT)
                    .key('G', Tags.Items.GEAR_WOOD)
                    .key('P', PLANKS)
                    .patternLine("III")
                    .patternLine("GBG")
                    .patternLine("PGP")
                    .addCriterion("has_tanned_belt", this.hasItemProvider(Registrars.Items.TANNED_LEATHER_BELT))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.SAW, 1)
                    .key('I', INGOTS_IRON)
                    .key('B', Registrars.Items.TANNED_LEATHER_BELT)
                    .key('G', Tags.Items.GEAR_WOOD)
                    .key('P', WOOD_SIDING)
                    .patternLine("III")
                    .patternLine("GBG")
                    .patternLine("PGP")
                    .addCriterion("has_tanned_belt", this.hasItemProvider(Registrars.Items.TANNED_LEATHER_BELT))
                    .build(consumer, highEff(Registrars.Blocks.SAW));

            ShapedRecipeBuilder.shapedRecipe(Registrars.Items.WIND_SAIL, 1)
                    .key('H', Registrars.Items.HEMP_CLOTH)
                    .key('P', PLANKS)
                    .patternLine("PPP")
                    .patternLine("HHH")
                    .patternLine("HHH")
                    .addCriterion("has_hemp_cloth", this.hasItemProvider(Registrars.Items.HEMP_CLOTH))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.HORIZONTAL_WINDMILL, 1)
                    .key('B', Registrars.Items.WIND_SAIL)
                    .patternLine(" B ")
                    .patternLine("B B")
                    .patternLine(" B ")
                    .addCriterion("has_wind_sail", this.hasItemProvider(Registrars.Items.WIND_SAIL))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.VERTICAL_WINDMILL, 1)
                    .key('B', Registrars.Items.WIND_SAIL)
                    .patternLine("BBB")
                    .patternLine("B B")
                    .patternLine("BBB")
                    .addCriterion("has_wind_sail", this.hasItemProvider(Registrars.Items.WIND_SAIL))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.WATERWHEEL, 1)
                    .key('B', Registrars.Items.WATERWHEEL_PADDLE)
                    .patternLine("BBB")
                    .patternLine("B B")
                    .patternLine("BBB")
                    .addCriterion("has_waterwheel_paddle", this.hasItemProvider(Registrars.Items.WATERWHEEL_PADDLE))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Items.TANNED_LEATHER_BELT, 1)
                    .key('S', Registrars.Items.TANNED_LEATHER_STRAP)
                    .patternLine(" S ")
                    .patternLine("S S")
                    .patternLine(" S ")
                    .addCriterion("has_tanned_leather_strap", this.hasItemProvider(Registrars.Items.TANNED_LEATHER_STRAP))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.PADDING_BLOCK, 1)
                    .key('#', Registrars.Items.PADDING)
                    .patternLine("###")
                    .patternLine("###")
                    .patternLine("###")
                    .addCriterion("has_padding", this.hasItemProvider(Registrars.Items.PADDING))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Items.WATERWHEEL_PADDLE, 1)
                    .key('S', WOOD_SIDING)
                    .key('G', Registrars.Items.GLUE)
                    .patternLine("S  ")
                    .patternLine("SGS")
                    .patternLine("S  ")
                    .addCriterion("has_glue", this.hasItem(Registrars.Items.GLUE))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.CLUTCHBOX, 1)
                    .key('P', PLANKS)
                    .key('G', Tags.Items.GEAR_WOOD)
                    .key('L', Registrars.Items.REDSTONE_LATCH)
                    .patternLine("PGP")
                    .patternLine("GLG")
                    .patternLine("PGP")
                    .addCriterion("has_latch", this.hasItemProvider(Registrars.Items.REDSTONE_LATCH))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.DIRT_SLAB, 4)
                    .key('#', Blocks.DIRT)
                    .patternLine("##")
                    .addCriterion("has_dirt", this.hasItemProvider(Blocks.DIRT))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.CAULDRON)
                    .key('#', INGOTS_IRON)
                    .key('B', Items.BONE)
                    .key('W', Items.WATER_BUCKET)
                    .patternLine("#B#")
                    .patternLine("#W#")
                    .patternLine("###")
                    .addCriterion("has_iron", this.hasItemProvider(net.minecraftforge.common.Tags.Items.INGOTS_IRON))
                    .build(consumer);


            ShapedRecipeBuilder.shapedRecipe(Blocks.DIRT, 1)
                    .key('#', Registrars.Blocks.DIRT_SLAB)
                    .patternLine("#")
                    .patternLine("#")
                    .addCriterion("has_dirt_slab", this.hasItemProvider(Registrars.Blocks.DIRT_SLAB))
                    .build(consumer, new ResourceLocation(References.MODID_MECHANIST, "dirt_slabs_to_dirt"));


            for (Map.Entry<DyeColor, Block> entry : Registrars.Blocks.REDSTONE_LAMPS.entrySet()) {
                DyeColor color = entry.getKey();
                Tag<Item> dye = DyeUtils.getDye(color);
                Block block = entry.getValue();
                ResourceLocation id = new ResourceLocation(References.MODID_MECHANIST, "lamps/with_dye/" + block.getRegistryName().getPath());
                ShapelessRecipeBuilder
                        .shapelessRecipe(block)
                        .addIngredient(new TagIngredient(dye))
                        .addIngredient(Tags.Items.REDSTONE_LAMP)
                        .addCriterion("has_lamp", this.hasItemProvider(Blocks.REDSTONE_LAMP))
                        .build(consumer, id);
                ResourceLocation id2 = new ResourceLocation(References.MODID_MECHANIST, "lamps/stained_panes/" + block.getRegistryName().getPath());
                IItemProvider pane = DyeUtils.getGlassPane(color);
                ShapedRecipeBuilder.shapedRecipe(block)
                        .key('P', pane)
                        .key('F', Registrars.Items.FILAMENT)
                        .key('R', DUSTS_REDSTONE)
                        .patternLine(" P ")
                        .patternLine("PFP")
                        .patternLine(" R ")
                        .addCriterion("has_filament", this.hasItemProvider(Registrars.Items.FILAMENT))
                        .build(consumer, id2);
            }


            ICondition REDSTONE_LAMP_ENABLED = moduleLoaded(References.MODID_MECHANIST, "redstone_lamp");
            ICondition REDSTONE_LAMP_DISABLED = not(REDSTONE_LAMP_ENABLED);


            ResourceLocation redstone_lamp = new ResourceLocation(References.MINECRAFT, Blocks.REDSTONE_LAMP.getRegistryName().getPath());

            ConditionalRecipe.builder()
                    .addCondition(
                            REDSTONE_LAMP_ENABLED
                    )
                    .addRecipe(
                            ShapedRecipeBuilder.shapedRecipe(Blocks.REDSTONE_LAMP)
                                    .key('P', Blocks.GLASS_PANE)
                                    .key('F', Registrars.Items.FILAMENT)
                                    .key('R', DUSTS_REDSTONE)
                                    .patternLine(" P ")
                                    .patternLine("PFP")
                                    .patternLine(" R ")
                                    .addCriterion("has_filament", this.hasItemProvider(Registrars.Items.FILAMENT))
                                    ::build
                    )
                    .addCondition(REDSTONE_LAMP_DISABLED)
                    .addRecipe(
                            ShapedRecipeBuilder.shapedRecipe(Blocks.REDSTONE_LAMP)
                                    .key('R', Items.REDSTONE)
                                    .key('G', Blocks.GLOWSTONE)
                                    .patternLine(" R ")
                                    .patternLine("RGR")
                                    .patternLine(" R ")
                                    .addCriterion("has_glowstone", this.hasItemProvider(Blocks.GLOWSTONE))
                                    ::build
                    )
                    .build(consumer, redstone_lamp);


            ShapedRecipeBuilder.shapedRecipe(Registrars.Items.ENDER_OCULAR)
                    .key('E', Items.ENDER_EYE)
                    .key('G', new TagIngredient(net.minecraftforge.common.Tags.Items.NUGGETS_GOLD))
                    .patternLine("GGG")
                    .patternLine("GEG")
                    .patternLine("GGG")
                    .addCriterion("has_ender_eye", this.hasItemProvider(Items.ENDER_EYE))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Items.IRON_SCREW)
                    .key('I', new TagIngredient(net.minecraftforge.common.Tags.Items.INGOTS_IRON))
                    .patternLine("II ")
                    .patternLine(" II")
                    .patternLine("II ")
                    .addCriterion("has_iron_ingot", this.hasItemProvider(net.minecraftforge.common.Tags.Items.INGOTS_IRON))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Items.TANNED_LEATHER_ARMOR_HELM)
                    .key('L', new TagIngredient(Tags.Items.TANNED_LEATHER))
                    .patternLine("LLL")
                    .patternLine("L L")
                    .addCriterion("has_tanned_leather", this.hasItemProvider(Tags.Items.TANNED_LEATHER))
                    .build(consumer);


            ShapedRecipeBuilder.shapedRecipe(Registrars.Items.TANNED_LEATHER_ARMOR_CHEST)
                    .key('L', new TagIngredient(Tags.Items.TANNED_LEATHER))
                    .patternLine("L L")
                    .patternLine("LLL")
                    .patternLine("LLL")
                    .addCriterion("has_tanned_leather", this.hasItemProvider(Tags.Items.TANNED_LEATHER))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Items.TANNED_LEATHER_ARMOR_LEGS)
                    .key('L', new TagIngredient(Tags.Items.TANNED_LEATHER))
                    .patternLine("LLL")
                    .patternLine("L L")
                    .patternLine("L L")
                    .addCriterion("has_tanned_leather", this.hasItemProvider(Tags.Items.TANNED_LEATHER))
                    .build(consumer);


            ShapedRecipeBuilder.shapedRecipe(Registrars.Items.TANNED_LEATHER_ARMOR_FEET)
                    .key('L', new TagIngredient(Tags.Items.TANNED_LEATHER))
                    .patternLine("L L")
                    .patternLine("L L")
                    .addCriterion("has_tanned_leather", this.hasItemProvider(Tags.Items.TANNED_LEATHER))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Items.WOOL_ARMOR_HELM)
                    .key('L', WOOL)
                    .patternLine("LLL")
                    .patternLine("L L")
                    .addCriterion("has_wool", this.hasItemProvider(ItemTags.WOOL))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Items.WOOL_ARMOR_CHEST)
                    .key('L', WOOL)
                    .patternLine("L L")
                    .patternLine("LLL")
                    .patternLine("LLL")
                    .addCriterion("has_wool", this.hasItemProvider(ItemTags.WOOL))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Items.WOOL_ARMOR_LEGS)
                    .key('L', WOOL)
                    .patternLine("LLL")
                    .patternLine("L L")
                    .patternLine("L L")
                    .addCriterion("has_wool", this.hasItemProvider(ItemTags.WOOL))
                    .build(consumer);


            ShapedRecipeBuilder.shapedRecipe(Registrars.Items.WOOL_ARMOR_FEET)
                    .key('L', WOOL)
                    .patternLine("L L")
                    .patternLine("L L")
                    .addCriterion("has_wool", this.hasItemProvider(ItemTags.WOOL))
                    .build(consumer);

            ShapedRecipeBuilder.shapedRecipe(Registrars.Blocks.SOULFORGE)
                    .key('#', INGOTS_SOULFORGED_STEEL)
                    .patternLine("###")
                    .patternLine(" # ")
                    .patternLine("###")
                    .addCriterion("has_sfs", this.hasItemProvider(Tags.Items.INGOT_SOULFORGED_STEEL))
                    .build(consumer);

            Ingredient shears = new ToolIngredient(ConstantIngredients.SHEARS);
            ToolRecipeBuilder.recipe(new ItemStack(Registrars.Items.LEATHER_SLICE, 2))
                    .tool(shears)
                    .input(Ingredient.fromItems(Items.LEATHER))
                    .sound(SoundEvents.ENTITY_SHEEP_SHEAR)
                    .damageTool()
                    .addCriterion("has_leather", this.hasItemProvider(Items.LEATHER))
                    .build(consumer);

            ToolRecipeBuilder.recipe(new ItemStack(Registrars.Items.SCOURED_LEATHER_SLICE, 2))
                    .tool(shears)
                    .input(Ingredient.fromItems(Registrars.Items.SCOURED_LEATHER))
                    .sound(SoundEvents.ENTITY_SHEEP_SHEAR)
                    .damageTool()
                    .addCriterion("has_scoured_leather", this.hasItemProvider(Registrars.Items.SCOURED_LEATHER))
                    .build(consumer);

            ToolRecipeBuilder.recipe(new ItemStack(Registrars.Items.TANNED_LEATHER_SLICE, 2))
                    .tool(shears)
                    .input(Ingredient.fromItems(Registrars.Items.TANNED_LEATHER))
                    .sound(SoundEvents.ENTITY_SHEEP_SHEAR)
                    .damageTool()
                    .addCriterion("has_tanned_leather", this.hasItemProvider(Registrars.Items.TANNED_LEATHER))
                    .build(consumer);

            ToolRecipeBuilder.recipe(new ItemStack(Registrars.Items.TANNED_LEATHER_STRAP, 2))
                    .tool(shears)
                    .input(Ingredient.fromItems(Registrars.Items.TANNED_LEATHER_SLICE))
                    .sound(SoundEvents.ENTITY_SHEEP_SHEAR)
                    .damageTool()
                    .addCriterion("has_tanned_leather_slice", this.hasItemProvider(Registrars.Items.TANNED_LEATHER_SLICE))
                    .build(consumer);

            SoulforgeShapedRecipeBuilder.recipe(new ItemStack(Registrars.Items.SOULFORGED_STEEL_MATTOCK))
                    .key('S', new TagIngredient(Tags.Items.INGOT_SOULFORGED_STEEL))
                    .key('H', Ingredient.fromItems(Registrars.Items.HAFT))
                    .patternLine(" SSS")
                    .patternLine("S H ")
                    .patternLine("  H ")
                    .patternLine("  H ")
                    .setGroup("soulforge")
                    .addCriterion("has_sfs", hasItemProvider(Tags.Items.INGOT_SOULFORGED_STEEL))
                    .build(consumer);

            SoulforgeShapedRecipeBuilder.recipe(new ItemStack(Registrars.Items.SOULFORGED_STEEL_PICKAXE))
                    .key('S', new TagIngredient(Tags.Items.INGOT_SOULFORGED_STEEL))
                    .key('H', Ingredient.fromItems(Registrars.Items.HAFT))
                    .patternLine("SSS")
                    .patternLine(" H ")
                    .patternLine(" H ")
                    .patternLine(" H ")
                    .setGroup("soulforge")
                    .addCriterion("has_sfs", hasItemProvider(Tags.Items.INGOT_SOULFORGED_STEEL))
                    .build(consumer);

            SoulforgeShapedRecipeBuilder.recipe(new ItemStack(Registrars.Items.SOULFORGED_STEEL_SWORD))
                    .key('S', new TagIngredient(Tags.Items.INGOT_SOULFORGED_STEEL))
                    .key('H', Ingredient.fromItems(Registrars.Items.HAFT))
                    .patternLine("S")
                    .patternLine("S")
                    .patternLine("S")
                    .patternLine("H")
                    .setGroup("soulforge")
                    .addCriterion("has_sfs", hasItemProvider(Tags.Items.INGOT_SOULFORGED_STEEL))
                    .build(consumer);

            SoulforgeShapedRecipeBuilder.recipe(new ItemStack(Registrars.Items.SOULFORGED_STEEL_SHOVEL))
                    .key('S', new TagIngredient(Tags.Items.INGOT_SOULFORGED_STEEL))
                    .key('H', Ingredient.fromItems(Registrars.Items.HAFT))
                    .patternLine("S")
                    .patternLine("H")
                    .patternLine("H")
                    .patternLine("H")
                    .setGroup("soulforge")
                    .addCriterion("has_sfs", hasItemProvider(Tags.Items.INGOT_SOULFORGED_STEEL))
                    .build(consumer);

            SoulforgeShapedRecipeBuilder.recipe(new ItemStack(Registrars.Items.SOULFORGED_STEEL_AXE))
                    .key('S', new TagIngredient(Tags.Items.INGOT_SOULFORGED_STEEL))
                    .key('H', Ingredient.fromItems(Registrars.Items.HAFT))
                    .patternLine("SS")
                    .patternLine("SH")
                    .patternLine(" H")
                    .patternLine(" H")
                    .setGroup("soulforge")
                    .addCriterion("has_sfs", hasItemProvider(Tags.Items.INGOT_SOULFORGED_STEEL))
                    .build(consumer);

            SoulforgeShapedRecipeBuilder.recipe(new ItemStack(Registrars.Items.SOULFORGED_STEEL_HOE))
                    .key('S', new TagIngredient(Tags.Items.INGOT_SOULFORGED_STEEL))
                    .key('H', Ingredient.fromItems(Registrars.Items.HAFT))
                    .patternLine("SS")
                    .patternLine(" H")
                    .patternLine(" H")
                    .patternLine(" H")
                    .setGroup("soulforge")
                    .addCriterion("has_sfs", hasItemProvider(Tags.Items.INGOT_SOULFORGED_STEEL))
                    .build(consumer);

            SoulforgeShapedRecipeBuilder.recipe(new ItemStack(Registrars.Items.SOULFORGED_STEEL_BATTLEAXE))
                    .key('S', new TagIngredient(Tags.Items.INGOT_SOULFORGED_STEEL))
                    .key('H', Ingredient.fromItems(Registrars.Items.HAFT))
                    .patternLine("SSS")
                    .patternLine("SHS")
                    .patternLine(" H ")
                    .patternLine(" H ")
                    .setGroup("soulforge")
                    .addCriterion("has_sfs", hasItemProvider(Tags.Items.INGOT_SOULFORGED_STEEL))
                    .build(consumer);

            SoulforgeShapedRecipeBuilder.recipe(new ItemStack(Registrars.Items.SOULFORGED_STEEL_ARMOR_HELM))
                    .key('S', new TagIngredient(Tags.Items.INGOT_SOULFORGED_STEEL))
                    .key('P', Ingredient.fromItems(Registrars.Items.SOULFORGED_ARMOR_PLATE))
                    .patternLine("SSSS")
                    .patternLine("S  S")
                    .patternLine(" PP ")
                    .setGroup("soulforge")
                    .addCriterion("has_sfs_plating", hasItemProvider(Registrars.Items.SOULFORGED_ARMOR_PLATE))
                    .build(consumer);

            SoulforgeShapedRecipeBuilder.recipe(new ItemStack(Registrars.Items.SOULFORGED_STEEL_ARMOR_CHEST))
                    .key('S', new TagIngredient(Tags.Items.INGOT_SOULFORGED_STEEL))
                    .key('P', Ingredient.fromItems(Registrars.Items.SOULFORGED_ARMOR_PLATE))
                    .patternLine("P  P")
                    .patternLine("SSSS")
                    .patternLine("SSSS")
                    .patternLine("SSSS")
                    .setGroup("soulforge")
                    .addCriterion("has_sfs_plating", hasItemProvider(Registrars.Items.SOULFORGED_ARMOR_PLATE))
                    .build(consumer);

            SoulforgeShapedRecipeBuilder.recipe(new ItemStack(Registrars.Items.SOULFORGED_STEEL_ARMOR_LEGS))
                    .key('S', new TagIngredient(Tags.Items.INGOT_SOULFORGED_STEEL))
                    .key('P', Ingredient.fromItems(Registrars.Items.SOULFORGED_ARMOR_PLATE))
                    .patternLine("SSSS")
                    .patternLine("PSSP")
                    .patternLine("P  P")
                    .patternLine("P  P")
                    .setGroup("soulforge")
                    .addCriterion("has_sfs_plating", hasItemProvider(Registrars.Items.SOULFORGED_ARMOR_PLATE))
                    .build(consumer);

            SoulforgeShapedRecipeBuilder.recipe(new ItemStack(Registrars.Items.SOULFORGED_STEEL_ARMOR_FEET))
                    .key('S', new TagIngredient(Tags.Items.INGOT_SOULFORGED_STEEL))
                    .key('P', Ingredient.fromItems(Registrars.Items.SOULFORGED_ARMOR_PLATE))
                    .patternLine(" SS ")
                    .patternLine(" SS ")
                    .patternLine("SPPS")
                    .setGroup("soulforge")
                    .addCriterion("has_sfs_plating", hasItemProvider(Registrars.Items.SOULFORGED_ARMOR_PLATE))
                    .build(consumer);

            SoulforgeShapedRecipeBuilder.recipe(new ItemStack(Registrars.Items.SOULFORGED_ARMOR_PLATE))
                    .key('S', new TagIngredient(Tags.Items.INGOT_SOULFORGED_STEEL))
                    .key('P', Ingredient.fromItems(Registrars.Items.PADDING))
                    .key('L', Ingredient.fromItems(Registrars.Items.TANNED_LEATHER_STRAP))
                    .patternLine("LSPL")
                    .setGroup("soulforge")
                    .addCriterion("has_sfs", hasItemProvider(Tags.Items.INGOT_SOULFORGED_STEEL))
                    .build(consumer);

            SoulforgeShapedRecipeBuilder.recipe(new ItemStack(Registrars.Items.POLISHED_LAPIS))
                    .key('L', GEMS_LAPIS)
                    .key('G', NUGGETS_GOLD)
                    .key('R', DUSTS_REDSTONE)
                    .patternLine("LLL")
                    .patternLine("LLL")
                    .patternLine("GGG")
                    .patternLine(" R ")
                    .setGroup("soulforge")
                    .addCriterion("has_lapis", hasItemProvider(net.minecraftforge.common.Tags.Items.GEMS_LAPIS))
                    .build(consumer);

            SoulforgeShapedRecipeBuilder.recipe(new ItemStack(Registrars.Blocks.ADVANCED_OBSERVER))
                    .key('L', Registrars.Items.POLISHED_LAPIS)
                    .key('S', STONE)
                    .key('R', DUSTS_REDSTONE)
                    .patternLine("SSLS")
                    .patternLine("LRRS")
                    .patternLine("SRRL")
                    .patternLine("SLSS")
                    .setGroup("soulforge")
                    .addCriterion("has_polished_lapis", hasItemProvider(Registrars.Items.POLISHED_LAPIS))
                    .build(consumer);

            SoulforgeShapedRecipeBuilder.recipe(new ItemStack(Registrars.Blocks.CHOPPING_BLOCK))
                    .key('S', Blocks.SMOOTH_STONE)
                    .patternLine("S  S")
                    .patternLine("SSSS")
                    .setGroup("soulforge")
                    .addCriterion("has_smooth_stone", hasItemProvider(Blocks.SMOOTH_STONE))
                    .build(consumer);

        }


    }
}
