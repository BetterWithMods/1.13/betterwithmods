/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.mechanical.generators.render;

import com.betterwithmods.core.References;
import com.betterwithmods.core.base.game.client.tile.GeometryTileRenderer;
import com.betterwithmods.core.utilties.BannerUtils;
import com.betterwithmods.mechanist.content.mechanical.BaseMechanicalAxisBlock;
import com.betterwithmods.mechanist.content.mechanical.generators.tile.BaseWindmillTile;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.BannerTextures;
import net.minecraft.util.Direction;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.vecmath.Vector3f;

public abstract class BaseWindmillRenderer<T extends BaseWindmillTile> extends GeometryTileRenderer<T> {

    @OnlyIn(Dist.CLIENT)
    public static final BannerTextures.Cache WINDMILLS = BannerUtils.makeCache(References.MODID_MECHANIST, "windmill_", "textures/entity/sails/sail.png", "textures/entity/sails/");

    public abstract void renderSail(T tile, int sail);

    public abstract void renderStructure();


    @Override
    public void renderModel(T tile, double x, double y, double z, float partialTicks, int destroyStage) {
        GlStateManager.color3f(1, 1, 1);
        BlockState state = tile.getWorld().getBlockState(tile.getPos());
        Direction.Axis axis = state.get(BaseMechanicalAxisBlock.AXIS);
        Vector3f axes = getRotationAxes();
        GlStateManager.rotatef(axis == Direction.Axis.X ? 90 : 0, 0, 1, 0);
        double rotation = (tile.getCurrentRotation() + (partialTicks * tile.getPreviousRotation()));
        GlStateManager.rotatef((float) rotation, axes.getX(), axes.getY(), axes.getZ());

        float angle = (360f/tile.getSailCount());
        for (int i = 0; i < tile.getSailCount(); i++) {
            GlStateManager.pushMatrix();
            GlStateManager.rotatef(i * angle, axes.getX(), axes.getY(), axes.getZ());
            GlStateManager.pushMatrix();
            renderSail(tile, i);
            GlStateManager.popMatrix();
            GlStateManager.popMatrix();
        }

        renderStructure();
    }

    public abstract Vector3f getRotationAxes();
}
