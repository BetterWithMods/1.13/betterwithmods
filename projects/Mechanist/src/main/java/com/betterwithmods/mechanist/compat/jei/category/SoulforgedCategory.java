/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.compat.jei.category;

import com.betterwithmods.core.References;
import com.betterwithmods.core.compat.jei.category.BaseCategory;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.compat.jei.MechanistJEI;
import com.betterwithmods.mechanist.content.soulforge.recipes.ISoulforgeRecipe;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.ingredient.IGuiItemStackGroup;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.crafting.IShapedRecipe;

import javax.annotation.Nonnull;
import java.util.List;

public class SoulforgedCategory extends BaseCategory<ISoulforgeRecipe> {
    private static final int craftOutputSlot = 16;
    private static final int craftInputSlot1 = 0;

    public SoulforgedCategory(IGuiHelper guiHelper) {
        super(guiHelper,
                guiHelper.createDrawable(MechanistJEI.Constants.SOULFORGE_BACKGROUND, 0, 0, 133, 71),
                String.format("jei.%s.soulforge", References.MODID_MECHANIST),
                guiHelper.createDrawableIngredient(new ItemStack(Registrars.Blocks.SOULFORGE)),
                MechanistJEI.Constants.SOULFORGE_CATEGORY);
    }


    @Override
    public Class<? extends ISoulforgeRecipe> getRecipeClass() {
        return ISoulforgeRecipe.class;
    }

    @Override
    public void setIngredients(@Nonnull ISoulforgeRecipe recipe, @Nonnull IIngredients ingredients) {
        ingredients.setInputIngredients(recipe.getIngredients());
        ingredients.setOutput(VanillaTypes.ITEM, recipe.getRecipeOutput());
    }

    @Override
    public void setRecipe(IRecipeLayout recipeLayout, @Nonnull ISoulforgeRecipe recipe, @Nonnull IIngredients ingredients) {
        IGuiItemStackGroup stacks = recipeLayout.getItemStacks();

        stacks.init(craftOutputSlot, false, 112, 27);

        for (int y = 0; y < 4; y++) {
            for (int x = 0; x < 4; x++) {
                int index = craftInputSlot1 + x + (y * 4);
                stacks.init(index, true, x * 18, y * 18);
            }
        }

        List<List<ItemStack>> inputs = ingredients.getInputs(VanillaTypes.ITEM);
        List<List<ItemStack>> outputs = ingredients.getOutputs(VanillaTypes.ITEM);
        if (recipe instanceof IShapedRecipe<?>) {
            IShapedRecipe<?> wrapper = (IShapedRecipe<?>) recipe;
            setInputStacks(stacks, inputs, wrapper.getRecipeWidth(), wrapper.getRecipeHeight());
        } else {
            setInputStacks(stacks, inputs);
        }
        stacks.set(craftOutputSlot, outputs.get(0));

    }

    //Copied from CraftingGridHelper
    private void setInputStacks(IGuiItemStackGroup guiItemStacks, List<List<ItemStack>> input) {
        int width, height;
        if (input.size() > 9) {
            width = height = 4;
        } else if (input.size() > 4) {
            width = height = 3;
        } else if (input.size() > 1) {
            width = height = 2;
        } else {
            width = height = 1;
        }

        setInputStacks(guiItemStacks, input, width, height);
    }

    //Copied from CraftingGridHelper
    private void setInputStacks(IGuiItemStackGroup guiItemStacks, List<List<ItemStack>> input, int width, int height) {
        for (int i = 0; i < input.size(); i++) {
            List<ItemStack> recipeItem = input.get(i);
            int index = getCraftingIndex(i, width, height);
            setInput(guiItemStacks, index, recipeItem);
        }
    }


    private int getCraftingIndex(int i, int width, int height) {
        int x = i % width;
        int y = i / width;
        //4 is max width of grid
        return x + (y * 4);
    }

    //Copied from CraftingGridHelper
    private void setInput(IGuiItemStackGroup guiItemStacks, int inputIndex, List<ItemStack> input) {
        guiItemStacks.set(craftInputSlot1 + inputIndex, input);
    }

    //Copied from CraftingGridHelper
    private void setOutput(IGuiItemStackGroup guiItemStacks, List<ItemStack> output) {
        guiItemStacks.set(craftOutputSlot, output);
    }


}
