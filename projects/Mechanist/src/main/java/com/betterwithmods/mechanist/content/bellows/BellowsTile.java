/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.bellows;

import com.betterwithmods.core.api.mech.IPower;
import com.betterwithmods.core.base.game.tile.MechanicalPowerTile;
import com.betterwithmods.core.utilties.DirectionUtils;
import com.betterwithmods.core.utilties.EnvironmentUtils;
import com.betterwithmods.core.utilties.FloatBuilder;
import com.betterwithmods.core.utilties.VectorBuilder;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;

public class BellowsTile extends MechanicalPowerTile {
    public BellowsTile() {
        super(Registrars.Tiles.BELLOWS);
    }

    @Nonnull
    @Override
    public Iterable<Direction> getInputs() {
        return DirectionUtils.HORIZONTAL_SET;
    }

    @Nonnull
    @Override
    public Iterable<Direction> getOutputs() {
        return Collections.emptyList();
    }

    public static final FloatBuilder PITCH_ON = FloatBuilder.builder(1.25f).addRandom(0.1f), PITCH_OFF = FloatBuilder.builder(0.75f).addRandom(0.1f);

    @Override
    public void postCalculate() {
        if (world != null) {
            boolean powered = power.getTorque() > 0;
            BlockState state = world.getBlockState(getPos());
            if (state.get(BellowsBlock.POWERED) != powered) {
                BlockState newState = state.with(BellowsBlock.POWERED, powered);
                if (powered) {
                    world.addBlockEvent(pos, getBlockState().getBlock(), PARTICLES_EVENT, 0);
                    EnvironmentUtils.playSound(world, pos, Registrars.Sounds.BELLOWS_AIR, SoundCategory.BLOCKS, 0.5f, PITCH_ON.build());
                    world.setBlockState(getPos(), newState);
                    BellowsHandler.onCompressed(world, pos, newState);
                } else {
                    EnvironmentUtils.playSound(world, pos, Registrars.Sounds.BELLOWS_AIR, SoundCategory.BLOCKS, 0.5f,  PITCH_OFF.build());
                    world.setBlockState(getPos(), Block.nudgeEntitiesWithNewState(state, newState, world, pos));
                }
            }
        }

    }

    private static final int PARTICLES_EVENT = 1000;

    @Override
    public boolean receiveClientEvent(int id, int type) {
        if(id == PARTICLES_EVENT) {
            BlockState state = world.getBlockState(pos);
            if(state.has(BellowsBlock.FACING)) {
                Direction dir = state.get(BellowsBlock.FACING);
                BlockPos offset = pos.offset(dir);
                BlockPos posLeft = offset.offset(dir.rotateY());
                BlockPos posRight = offset.offset(dir.rotateYCCW());

                EnvironmentUtils.forEachParticle(world, posLeft, ParticleTypes.CLOUD, 5, new VectorBuilder().setGaussian(0.25).offset(0.5), new VectorBuilder().offset(dir, 0.1));
                EnvironmentUtils.forEachParticle(world, offset, ParticleTypes.CLOUD, 5, new VectorBuilder().setGaussian(0.25).offset(0.5), new VectorBuilder().offset(dir, 0.1));
                EnvironmentUtils.forEachParticle(world, posRight, ParticleTypes.CLOUD, 5, new VectorBuilder().setGaussian(0.25).offset(0.5), new VectorBuilder().offset(dir, 0.1));
            }
            return true;
        }
        return false;
    }

    @Nullable
    @Override
    public IPower getOutput(@Nonnull IWorld world, @Nonnull BlockPos pos, @Nonnull Direction facing) {
        return null;
    }

    @Override
    public boolean overpower(World world, BlockPos pos) {
        return false;
    }
}
