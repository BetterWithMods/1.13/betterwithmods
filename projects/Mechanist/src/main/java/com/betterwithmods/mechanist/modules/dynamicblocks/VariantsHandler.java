/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.modules.dynamicblocks;

import com.betterwithmods.core.Relationships;
import com.betterwithmods.core.base.game.block.MaterialKey;
import com.betterwithmods.core.relationary.Shape;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.content.dynamic.CamoItemStackBuilder;
import com.betterwithmods.mechanist.content.dynamic.CamoflageTile;
import com.betterwithmods.mechanist.content.dynamic.TagVariant;
import com.betterwithmods.mechanist.content.dynamic.VariantProvider;
import com.betterwithmods.mechanist.content.dynamic.block.BlockDynamicVariant;
import com.google.common.collect.*;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.event.lifecycle.FMLLoadCompleteEvent;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

public class VariantsHandler {

    private static final Collection<VariantProvider> VARIANT_PROVIDERS = Lists.newArrayList();
    @SuppressWarnings("UnstableApiUsage")
    private final static ListMultimap<MaterialKey, BlockState> MATERIAL_VARIANTS = MultimapBuilder.hashKeys().arrayListValues().build();

    private final static ListMultimap<Block, CamoItemStackBuilder> DYNAMIC_BLOCK_VARIANTS = MultimapBuilder.hashKeys().arrayListValues().build();

    public static Table<MaterialKey, Shape, Block> DYNAMIC_BLOCKS_TABLE;


    public static void onLoadComplete(FMLLoadCompleteEvent event) {

        DYNAMIC_BLOCKS_TABLE = ImmutableTable.<MaterialKey, Shape, Block>builder()
                .put(MaterialKey.WOOD, Relationships.SIDING, Registrars.Blocks.SIDING_WOOD)
                .put(MaterialKey.WOOD, Relationships.MOULDING, Registrars.Blocks.MOULDING_WOOD)
                .put(MaterialKey.WOOD, Relationships.CORNER, Registrars.Blocks.CORNER_WOOD)
                .put(MaterialKey.WOOD, Relationships.TABLE, Registrars.Blocks.TABLE_WOOD)
                .put(MaterialKey.WOOD, Relationships.CHAIR, Registrars.Blocks.CHAIR_WOOD)

                .put(MaterialKey.ROCK, Relationships.SIDING, Registrars.Blocks.SIDING_ROCK)
                .put(MaterialKey.ROCK, Relationships.MOULDING, Registrars.Blocks.MOULDING_ROCK)
                .put(MaterialKey.ROCK, Relationships.CORNER, Registrars.Blocks.CORNER_ROCK)
                .put(MaterialKey.ROCK, Relationships.TABLE, Registrars.Blocks.TABLE_ROCK)
                .put(MaterialKey.ROCK, Relationships.CHAIR, Registrars.Blocks.CHAIR_ROCK)
                .build();


    }

    public static Collection<BlockState> getStatesForBlock(Block block) {
        return getStatesForBlockState(block.getDefaultState());
    }

    public static Collection<BlockState> getStatesForBlockState(BlockState state) {
        return getStatesForMaterial(state.getMaterial());
    }

    public static Collection<BlockState> getStatesForMaterial(Material material) {
        return getStatesForMaterial(MaterialKey.get(material));
    }

    public static Collection<BlockState> getStatesForMaterial(MaterialKey materialKey) {
        if (!MATERIAL_VARIANTS.containsKey(materialKey)) {
            MATERIAL_VARIANTS.putAll(materialKey,
                    VARIANT_PROVIDERS.stream()
                            .map(VariantProvider::variants)
                            .flatMap(Collection::stream)
                            .filter(state -> materialKey.matches(state.getMaterial()))
                            .collect(Collectors.toList()));
        }
        return MATERIAL_VARIANTS.get(materialKey);
    }

    public static void populateDynamicBlockChildren(Block block) {
        if (!DYNAMIC_BLOCK_VARIANTS.containsKey(block)) {
            Collection<BlockState> variants = getStatesForBlock(block);
            DYNAMIC_BLOCK_VARIANTS.putAll(block, variants.stream().map(v -> CamoflageTile.builder(block, v)).collect(Collectors.toList()));
        }
    }

    public static Collection<ItemStack> getChildren(Block block) {
        populateDynamicBlockChildren(block);
        return DYNAMIC_BLOCK_VARIANTS.get(block).stream().map(CamoItemStackBuilder::build).collect(Collectors.toList());
    }

    public static void loadVariantProviders(Collection<? extends String> serializedVariants) {
        VARIANT_PROVIDERS.addAll(serializedVariants.stream().map(VariantsHandler::deserializeVariantProvider).collect(Collectors.toList()));
    }

    private static VariantProvider deserializeVariantProvider(String variant) {
        String[] parts = variant.split(":");

        String type = "block";
        ResourceLocation location = null;
        if (parts.length == 3) {
            type = parts[0];
            location = new ResourceLocation(parts[1], parts[2]);
        } else if (parts.length == 2) {
            location = new ResourceLocation(parts[0], parts[1]);
        }
        if (location != null) {
            switch (type) {
                case "block":
                    return new BlockDynamicVariant(location);
                case "tag":
                    return new TagVariant(location);
            }
        }
        throw new IllegalStateException("Invalid Variant Provider: " + variant);
    }

    public static Collection<BlockState> getAllStates() {
        return Collections.unmodifiableCollection(MATERIAL_VARIANTS.values());
    }
}
