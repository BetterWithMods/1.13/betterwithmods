/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.saw;

import com.betterwithmods.core.References;
import com.betterwithmods.core.Tags;
import com.betterwithmods.core.base.game.ConstantIngredients;
import com.betterwithmods.core.utilties.EnvironmentUtils;
import com.betterwithmods.core.utilties.PlayerUtils;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.storage.loot.LootContext;
import net.minecraft.world.storage.loot.LootParameterSets;
import net.minecraft.world.storage.loot.LootParameters;
import net.minecraft.world.storage.loot.LootTable;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber
public class SawDamageHandler {

    public static final DamageSource SAW_BLADE = new DamageSource("sawBlade");

    private static final float BASE_DAMAGE = 4.0f;

    public static void onEntityCollision(BlockState state, World world, BlockPos pos, Entity entity) {
        if (state.get(SawBlock.POWERED) && entity instanceof LivingEntity) {
            LivingEntity living = (LivingEntity) entity;
            if (living.attackEntityFrom(SAW_BLADE, BASE_DAMAGE)) {
                EnvironmentUtils.playSound(world, pos, Registrars.Sounds.SAW_CUT, SoundCategory.BLOCKS, SawBlock.VOLUME.build(), SawBlock.PITCH_ON.build());
            }
        }
    }

    @SubscribeEvent
    public static void livingDrops(LivingDropsEvent event) {
        LivingEntity entity = (LivingEntity) event.getEntity();
        World world = entity.getEntityWorld();
        if (insideChoppingBlock(world, entity)) {
            dropSawLoot(entity, SAW_BLADE);
        }
    }

    public static ResourceLocation getSawLoot(LivingEntity entity) {
        ResourceLocation registryName = entity.getType().getRegistryName();
        return new ResourceLocation(References.MODID_MECHANIST, "chopping_block/" + registryName.getPath());
    }

    public static void dropSawLoot(LivingEntity entity, DamageSource source) {
        ResourceLocation resourcelocation = getSawLoot(entity);
        LootTable loottable = entity.world.getServer().getLootTableManager().getLootTableFromLocation(resourcelocation);
        LootContext.Builder lootcontext$builder = builder(entity, source);
        loottable.generate(lootcontext$builder.build(LootParameterSets.ENTITY), entity::entityDropItem);
    }

    public static LootContext.Builder builder(LivingEntity entity, DamageSource source) {
        return (new LootContext.Builder((ServerWorld) entity.world)).withRandom(entity.getRNG()).withParameter(LootParameters.THIS_ENTITY, entity).withParameter(LootParameters.POSITION, new BlockPos(entity)).withParameter(LootParameters.DAMAGE_SOURCE, source).withNullableParameter(LootParameters.KILLER_ENTITY, source.getTrueSource()).withNullableParameter(LootParameters.DIRECT_KILLER_ENTITY, source.getImmediateSource());
    }


    public static boolean insideChoppingBlock(World world, Entity entity) {
        BlockPos p = entity.getPosition().add(0, entity.getEyeHeight() - 0.5, 0);
        BlockState inside = world.getBlockState(p);
        return inside.isIn(Tags.Blocks.CHOPPING_BLOCKS);
    }

    public static boolean killedByBattleAxe(World world, Entity entity, DamageSource source) {
        //Battle Axe bonus is separate
        if (source.getTrueSource() instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) source.getTrueSource();
            return PlayerUtils.isHolding(player, ConstantIngredients.BATTLEAXES);
        }
        return false;
    }
}
