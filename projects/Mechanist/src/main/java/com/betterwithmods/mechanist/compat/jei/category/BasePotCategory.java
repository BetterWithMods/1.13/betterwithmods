/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.compat.jei.category;

import com.betterwithmods.core.References;
import com.betterwithmods.core.api.crafting.result.IResult;
import com.betterwithmods.core.compat.jei.CoreJEI;
import com.betterwithmods.core.compat.jei.IconDrawable;
import com.betterwithmods.core.compat.jei.category.AbstractInventoryMultiplexCategory;
import com.betterwithmods.mechanist.content.pots.BasePotRecipe;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.mojang.datafixers.util.Pair;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawableAnimated;
import mezz.jei.api.gui.ingredient.IGuiIngredientGroup;
import mezz.jei.api.gui.ingredient.IGuiItemStackGroup;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class BasePotCategory<T extends BasePotRecipe<T>> extends AbstractInventoryMultiplexCategory<T> {
    private final Cache<Pair<Integer, Integer>, IDrawableAnimated> ANIMATED_CACHE = CacheBuilder.newBuilder().build();
    private final ResourceLocation background;
    private final Class<T> recipeClass;

    public BasePotCategory(IGuiHelper guiHelper, ResourceLocation background, String type, Block blockIcon, ResourceLocation uid, Class<T> recipeClass, int heat) {
        super(guiHelper, guiHelper.createDrawable(background, 0, 0, 165, 57),
                String.format("jei.%s.%s", References.MODID_MECHANIST, type),
                IconDrawable.builder(guiHelper, new ItemStack(blockIcon), background, 166, (heat -1) * 14, 14,14),
                uid);
        this.background = background;
        this.recipeClass = recipeClass;
    }

    @Override
    public void draw(T recipe, double mouseX, double mouseY) {
        int flame = (recipe.getRecipeHeat() * 14) - 14;
        try {
            IDrawableAnimated animation = ANIMATED_CACHE.get(Pair.of(recipe.getRecipeTime(), recipe.getRecipeHeat()),
                    () -> this.guiHelper.drawableBuilder(background, 166, flame, 14, 14).buildAnimated(recipe.getRecipeTime(), IDrawableAnimated.StartDirection.BOTTOM, false));
            animation.draw(77, 22);
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Nonnull
    @Override
    public Class<? extends T> getRecipeClass() {
        return recipeClass;
    }

    @Override
    public void setRecipe(IRecipeLayout layout, @Nonnull T recipe, @Nonnull IIngredients ingredients) {
        IGuiItemStackGroup guiItemStacks = layout.getItemStacks();
        IGuiIngredientGroup<IResult<ItemStack>> guiResults = layout.getIngredientsGroup(CoreJEI.STACK_RESULT);
        createSlotsGrid(guiItemStacks, true, 9, 3, 0, 7, 2);
        guiItemStacks.set(ingredients);
        createSlotsGrid(guiResults, false, 9, 3, 9, 106, 3);
        guiResults.set(ingredients);

        List<List<IResult<ItemStack>>> resultList = ingredients.getOutputs(CoreJEI.STACK_RESULT);
        if (resultList.size() > 9) {
            List<List<IResult<ItemStack>>> subList = resultList.subList(8, resultList.size());
            List<IResult<ItemStack>> results = subList.stream().flatMap(List::stream).collect(Collectors.toList());
            guiResults.set(18, results);
        }
    }

}
