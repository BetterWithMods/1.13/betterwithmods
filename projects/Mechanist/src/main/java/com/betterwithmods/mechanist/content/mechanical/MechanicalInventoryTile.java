/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.mechanical;

import com.betterwithmods.core.api.mech.IPower;
import com.betterwithmods.core.base.game.tile.MechanicalPowerTile;
import com.betterwithmods.core.utilties.ItemUtils;
import com.betterwithmods.core.utilties.inventory.InventoryUtils;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.function.Predicate;

public abstract class MechanicalInventoryTile extends MechanicalPowerTile {

    protected final LazyOptional<ItemStackHandler> optionalInventory = LazyOptional.of(this::getInventory);

    public MechanicalInventoryTile(TileEntityType<?> tileEntityTypeIn) {
        super(tileEntityTypeIn);
        inputs = f -> true;
    }

    @Nonnull
    private final ItemStackHandler inventory = createInventory();

    protected Predicate<Direction> inputs;

    @Nonnull
    public abstract ItemStackHandler createInventory();

    @Nonnull
    public ItemStackHandler getInventory() {
        return inventory;
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        if (inputs.test(side) && cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.orEmpty(cap, optionalInventory.cast());
        }
        return super.getCapability(cap, side);
    }

    @Nonnull
    @Override
    public Iterable<Direction> getOutputs() {
        return Collections.emptyList();
    }

    @Nullable
    @Override
    public IPower getOutput(@Nonnull IWorld world, @Nonnull BlockPos pos, @Nonnull Direction facing) {
        return null;
    }


    @Override
    public CompoundNBT write(CompoundNBT compound) {
        compound.put("inventory", inventory.serializeNBT());
        return super.write(compound);
    }

    @Override
    public void read(CompoundNBT compound) {
        if (compound.contains("inventory")) {
            inventory.deserializeNBT(compound.getCompound("inventory"));
        }
        super.read(compound);
    }

    @Override
    public void onRemoved() {
        super.onRemoved();
        InventoryUtils.drop(inventory, world,pos, ItemUtils.EJECT_OFFSET);
    }
}
