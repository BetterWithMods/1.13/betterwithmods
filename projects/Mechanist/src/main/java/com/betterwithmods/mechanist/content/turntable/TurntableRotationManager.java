/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.turntable;

import com.betterwithmods.core.api.blocks.IRotationAttachment;
import com.betterwithmods.core.utilties.DirectionUtils;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import net.minecraft.block.*;
import net.minecraft.state.properties.AttachFace;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.EnumMap;
import java.util.Set;
import java.util.function.BiFunction;

public class TurntableRotationManager {


    public static void onRotate(World world, BlockPos turntable, Rotation rotation, BiFunction<Integer, BlockPos, Boolean> action) {
        int i = 0;
        for (BlockPos pos = turntable.up(); i < 2; i++, pos = pos.up()) {
            BlockState state = world.getBlockState(pos);
            if (!canTransferRotation(world, pos, state)) {
                break;
            }
            rotateBlock(world, pos, state, rotation);
            rotateAttachments(world, pos, rotation);
            if (action.apply(i, pos))
                break;
        }

    }

    private static void rotateAttachments(World world, BlockPos pos, Rotation rotation) {
        EnumMap<Direction, BlockState> attachments = Maps.newEnumMap(Direction.class);
        for (Direction dir : DirectionUtils.HORIZONTAL_SET) {
            BlockPos offset = pos.offset(dir);
            BlockState attached = world.getBlockState(offset);
            if (isAttachment(attached, dir)) {
                attachments.put(dir, attached);
                world.removeBlock(offset, false);
            }
        }
        for (Direction dir : attachments.keySet()) {
            BlockState attached = attachments.get(dir);
            Direction newDir = DirectionUtils.rotation(dir, rotation);
            BlockPos newPos = rotateAround(pos, newDir);

            //Drop attach if it can't be placed in new spot
            if (!world.getBlockState(newPos).getMaterial().isReplaceable()) {
                Block.spawnDrops(attached, world, pos.offset(dir));
            } else {
                world.setBlockState(newPos, attached.rotate(world, pos, rotation));
            }
        }
    }

    private static BlockPos rotateAround(BlockPos centerPos, Direction facing) {
        return centerPos.add(facing.getXOffset(), 0, facing.getZOffset());
    }


    public static boolean canTransferRotation(World world, BlockPos pos, BlockState state) {
        return !state.isAir(world, pos);
    }

    public static void rotateBlock(World world, BlockPos pos, BlockState state, Rotation rotation) {
        world.setBlockState(pos, state.rotate(world, pos, rotation));
    }


    public static boolean isAttachment(BlockState state, Direction direction) {
        return state != null && ATTACHMENTS.stream().anyMatch(i -> i.test(state, direction));
    }

    private static final Set<IRotationAttachment> ATTACHMENTS = Sets.newHashSet();

    static {
        ATTACHMENTS.add(TurntableRotationManager::lever);
        ATTACHMENTS.add(TurntableRotationManager::torch);
        ATTACHMENTS.add(TurntableRotationManager::redstoneTorch);
    }

    public static boolean lever(BlockState state, Direction direction) {
        return state.getBlock() instanceof LeverBlock && state.get(BlockStateProperties.HORIZONTAL_FACING) == direction && state.get(LeverBlock.FACE) == AttachFace.WALL;
    }

    public static boolean torch(BlockState state, Direction direction) {
        return state.getBlock() instanceof WallTorchBlock && state.get(BlockStateProperties.HORIZONTAL_FACING) == direction;
    }

    public static boolean redstoneTorch(BlockState state, Direction direction) {
        return state.getBlock() instanceof RedstoneWallTorchBlock && state.get(BlockStateProperties.HORIZONTAL_FACING) == direction;
    }

}
