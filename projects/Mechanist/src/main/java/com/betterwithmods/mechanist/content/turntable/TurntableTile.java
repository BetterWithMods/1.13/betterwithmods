/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.turntable;

import com.betterwithmods.core.api.mech.IPower;
import com.betterwithmods.core.base.game.tile.MechanicalPowerTile;
import com.betterwithmods.core.impl.crafting.multiplex.MultiplexRecipeManager;
import com.betterwithmods.core.utilties.MiscUtils;
import com.betterwithmods.core.utilties.Timer;
import com.betterwithmods.mechanist.Mechanist;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.EnumSet;

public class TurntableTile extends MechanicalPowerTile implements ITickableTileEntity {

    private final Timer[] timers = new Timer[]{new Timer(), new Timer()};

    public TurntableTile() {
        super(Registrars.Tiles.TURNTABLE);
    }

    @Nonnull
    @Override
    public Iterable<Direction> getInputs() {
        return EnumSet.of(Direction.DOWN);
    }

    @Nonnull
    @Override
    public Iterable<Direction> getOutputs() {
        return Collections.emptyList();
    }

    @Override
    public void postCalculate() {

    }

    @Nullable
    @Override
    public IPower getOutput(@Nonnull IWorld world, @Nonnull BlockPos pos, @Nonnull Direction facing) {
        return null;
    }

    @Override
    public boolean overpower(World world, BlockPos pos) {
        return false;
    }

    private int delay;

    private void noRecipe(int i) {
        Timer timer = timers[i];
        if (timer != null) {
            timer.reset();
        }
    }


    @Override
    public void tick() {
        if (world != null) {
            if (power.getTorque() > 0) {
                if (delay <= 0) {

                    TurntableRotationManager.onRotate(world, pos, Rotation.CLOCKWISE_90,
                            (i, pos) -> {
                                Timer timer = timers[i];
                                TurntableRecipeContext context = new TurntableRecipeContext(timer.getTime());
                                return MultiplexRecipeManager.get(world.getServer())
                                        .map(m ->
                                                MiscUtils.ifIsPresentOrElse(
                                                        m.getRecipe(Registrars.MultiplexRecipeTypes.TURNTABLE, world, pos, context),
                                                        r -> {
                                                            timer.update(r);
                                                            r.craft(world, pos, context);
                                                            timer.incrementTime(1);
                                                        }, () -> noRecipe(i)))
                                        .orElse(false);
                            });

                    BlockState state = world.getBlockState(pos);
                    if (state.has(TurntableBlock.TICK_RATE)) {
                        delay = state.get(TurntableBlock.TICK_RATE).getTicks();
                    } else {
                        Mechanist.LOGGER.error("Invalid Turntable BlockState, Please report this.");
                        delay = 10;
                    }


                }
                delay--;
            }
        }
    }

    private static final String DELAY = "delay";
    private static final String TIMER = "timer";

    @Nonnull
    @Override
    public CompoundNBT write(CompoundNBT compound) {

        for (int i = 0; i < timers.length; i++) {
            compound.put(String.format("%s%d", TIMER, i), timers[i].serializeNBT());
        }
        compound.putInt(DELAY, delay);
        return super.write(compound);
    }

    @Override
    public void read(CompoundNBT compound) {
        for (int i = 0; i < timers.length; i++) {
            timers[i].deserializeNBT(compound.getCompound(String.format("%s%d", TIMER, i)));
        }
        if (compound.contains(DELAY)) {
            delay = compound.getInt(DELAY);
        }
        super.read(compound);
    }
}
