/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.filtered_hopper;

import com.betterwithmods.core.References;
import com.betterwithmods.core.base.game.container.InventoryContainer;
import com.betterwithmods.core.base.game.container.SlotTransformation;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.IIntArray;
import net.minecraft.util.IntArray;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nullable;

public class FilteredHopperContainer extends InventoryContainer {
    public static final ResourceLocation FILTER = new ResourceLocation(References.MODID_MECHANIST, "filter");

    private final ItemStackHandler filterInventory;

    public FilteredHopperContainer(int windowId, PlayerEntity player) {
        this(windowId, player, new ItemStackHandler(18), new ItemStackHandler(1), new IntArray(1));
    }

    public FilteredHopperContainer(int windowId, PlayerEntity player, ItemStackHandler inventory, ItemStackHandler filterInventory, @Nullable IIntArray data) {
        super(Registrars.Containers.FILTERED_HOPPER, windowId, player, inventory, data);
        this.filterInventory = filterInventory;
        addSlots(FILTER, filterInventory, 1, 1, 0, 80, 37);
        addSlots(CONTAINER, inventory, 18, 2, 0, 8, 60);
        addPlayerInventory(0, 8, 111, 58);
        addSlotTransformation(new SlotTransformation(FILTER, PLAYER));
        addSlotTransformation(new SlotTransformation(PLAYER, CONTAINER));
        addSlotTransformation(new SlotTransformation(CONTAINER, PLAYER));
    }

    public boolean isPowered() {
        return this.data.get(0) == 1;
    }


    public ItemStackHandler getFilterInventory() {
        return filterInventory;
    }
}
