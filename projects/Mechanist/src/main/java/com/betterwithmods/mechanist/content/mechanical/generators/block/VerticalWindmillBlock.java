/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.mechanical.generators.block;

import com.betterwithmods.mechanist.content.mechanical.BaseMechanicalAxisBlock;
import net.minecraft.block.BlockState;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IWorldReader;

public class VerticalWindmillBlock extends BaseWindmillBlock {
    public VerticalWindmillBlock(Properties properties) {
        super(properties);
    }

    @Override
    public boolean isValid(IWorldReader world, BlockPos pos, BlockState currentState) {
        return super.isValid(world, pos, currentState) && hasStructuralAxles(world, pos);
    }

    @Override
    public boolean allowAxis(Direction.Axis axis) {
        return axis.isVertical();
    }

    public static VoxelShape[] AXIS_SHAPES = new VoxelShape[]{
            VoxelShapes.empty(),
            VoxelShapes.create(-3, -3, -3, 4, 4, 4),/*y*/
            VoxelShapes.empty(),
    };

    @Override
    public VoxelShape[] shapes() {
        return AXIS_SHAPES;

    }

    public boolean hasStructuralAxles(IWorldReader world, BlockPos center) {
        for (int i = -3; i <= 3; i++) {
            BlockState state = world.getBlockState(center.add(0, i, 0));
            if (!(state.getBlock() instanceof BaseMechanicalAxisBlock))
                return false;
        }
        return true;
    }

}
