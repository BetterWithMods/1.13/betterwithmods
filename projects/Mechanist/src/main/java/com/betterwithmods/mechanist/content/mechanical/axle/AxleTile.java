/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.mechanical.axle;

import com.betterwithmods.core.api.mech.IPower;
import com.betterwithmods.core.base.game.tile.MechanicalPowerTile;
import com.betterwithmods.core.impl.Power;
import com.betterwithmods.core.utilties.DirectionUtils;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.content.mechanical.BaseMechanicalAxisBlock;
import net.minecraft.block.BlockState;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class AxleTile extends MechanicalPowerTile {


    public AxleTile() {
        super(Registrars.Tiles.AXLE);
    }

//    @Override
//    public boolean hasFastRenderer() {
//        return true;
//    }

    private Direction.Axis getAxis() {
        BlockState state = world.getBlockState(pos);
        if(state.getBlock() instanceof BaseMechanicalAxisBlock)
            return state.get(BaseMechanicalAxisBlock.AXIS);
        return null;
    }

    @Nonnull
    @Override
    public Iterable<Direction> getInputs() {
        return DirectionUtils.fromAxis(getAxis());
    }

    @Nonnull
    @Override
    public Iterable<Direction> getOutputs() {
        return getInputs();
    }

    private boolean onAxis(Direction facing) {
        return facing != null && facing.getAxis().equals(getAxis());
    }

    @Override
    protected boolean isMechanicalSide(Direction facing) {
        return onAxis(facing);
    }


    @Nullable
    @Override
    public IPower getOutput(@Nonnull IWorld world, @Nonnull BlockPos pos, @Nonnull Direction facing) {
        if (onAxis(facing)) {
            return power;
        }
        return null;
    }

    @Nullable
    @Override
    public IPower getMaximumInput(@Nonnull Direction facing) {
        return null;
    }

    @Nullable
    @Override
    public IPower getMinimumInput() {
        //minimum distance is 1, if 0 it will overpower
        return new Power(0, 1);
    }

    @Override
    public void postCalculate() {
        BlockState state = world.getBlockState(pos);
        if(state.getBlock() instanceof BaseMechanicalAxisBlock) {
            world.setBlockState(pos, state.with(BaseMechanicalAxisBlock.POWERED, power.getTorque() > 0));
        }
    }

    @Override
    public boolean overpower(World world, BlockPos pos) {
        world.destroyBlock(pos, true);
        return true;
    }
}

