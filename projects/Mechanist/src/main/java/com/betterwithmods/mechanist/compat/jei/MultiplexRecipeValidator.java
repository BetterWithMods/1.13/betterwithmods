/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.compat.jei;

import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipe;
import com.betterwithmods.core.impl.crafting.multiplex.MultiplexRecipeManager;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.content.filtered_hopper.recipes.HopperRecipe;
import com.betterwithmods.mechanist.content.kiln.KilnRecipe;
import com.betterwithmods.mechanist.content.millstone.MillstoneRecipe;
import com.betterwithmods.mechanist.content.pots.IHeatedRecipe;
import com.betterwithmods.mechanist.content.pots.cauldron.CauldronRecipe;
import com.betterwithmods.mechanist.content.pots.crucible.CrucibleRecipe;
import com.betterwithmods.mechanist.content.saw.SawRecipe;
import com.betterwithmods.mechanist.content.soulforge.SoulforgeCrafting;
import com.betterwithmods.mechanist.content.soulforge.recipes.ISoulforgeRecipe;
import com.betterwithmods.mechanist.content.turntable.TurntableRecipe;
import com.betterwithmods.mechanist.handlers.HeatHandler;
import com.google.common.collect.Lists;
import net.minecraft.client.Minecraft;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.RecipeManager;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MultiplexRecipeValidator {

    public MultiplexRecipeValidator() {
    }

    public Results getRecipes() {
        Predicate<IMultiplexRecipe<?, ?>> validator = (recipe) -> true;
        Predicate<IHeatedRecipe> normalHeat = r -> r.getRecipeHeat().equals(HeatHandler.NORMAL_HEAT);
        Predicate<IHeatedRecipe> stokedHeat = r -> r.getRecipeHeat().equals(HeatHandler.STOKED_HEAT);
        MultiplexRecipeManager multiplexRecipeManager = MultiplexRecipeManager.CLIENT_INSTANCE;
        List<MillstoneRecipe> millstoneRecipes = multiplexRecipeManager.getRecipes(Registrars.MultiplexRecipeTypes.MILLSTONE)
                .values()
                .stream()
                .filter(validator)
                .collect(Collectors.toList());

        List<CauldronRecipe> cauldronRecipes = multiplexRecipeManager.getRecipes(Registrars.MultiplexRecipeTypes.CAULDRON)
                .values()
                .stream()
                .filter(normalHeat)
                .collect(Collectors.toList());

        List<CrucibleRecipe> crucibleRecipes = multiplexRecipeManager.getRecipes(Registrars.MultiplexRecipeTypes.CRUCIBLE)
                .values()
                .stream()
                .filter(normalHeat)
                .collect(Collectors.toList());

        List<CauldronRecipe> stokedCauldronRecipes = multiplexRecipeManager.getRecipes(Registrars.MultiplexRecipeTypes.CAULDRON)
                .values()
                .stream()
                .filter(stokedHeat)
                .collect(Collectors.toList());

        List<CrucibleRecipe> stokedCrucibleRecipes = multiplexRecipeManager.getRecipes(Registrars.MultiplexRecipeTypes.CRUCIBLE)
                .values()
                .stream()
                .filter(stokedHeat)
                .collect(Collectors.toList());

        List<SawRecipe> sawRecipes = multiplexRecipeManager.getRecipes(Registrars.MultiplexRecipeTypes.SAW)
                .values()
                .stream()
                .map(IMultiplexRecipe::getDisplayRecipes)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        List<KilnRecipe> kilnRecipes = multiplexRecipeManager.getRecipes(Registrars.MultiplexRecipeTypes.KILN)
                .values()
                .stream()
                .map(IMultiplexRecipe::getDisplayRecipes)
                .flatMap(Collection::stream)
                .filter(normalHeat).collect(Collectors.toList());

        List<KilnRecipe> stokedKilnRecipes = multiplexRecipeManager.getRecipes(Registrars.MultiplexRecipeTypes.KILN)
                .values()
                .stream()
                .map(IMultiplexRecipe::getDisplayRecipes)
                .flatMap(Collection::stream)
                .filter(stokedHeat).collect(Collectors.toList());

        List<TurntableRecipe> turntableRecipes = multiplexRecipeManager.getRecipes(Registrars.MultiplexRecipeTypes.TURNTABLE)
                .values()
                .stream()
                .map(IMultiplexRecipe::getDisplayRecipes)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        ClientWorld world = Minecraft.getInstance().world;
        RecipeManager recipeManager = world.getRecipeManager();

        List<ISoulforgeRecipe> soulforgeRecipes = Lists.newArrayList();
        for (IRecipe<?> recipe : recipeManager.getRecipes()) {
            if (recipe.getType().equals(SoulforgeCrafting.SOULFORGE)) {
                soulforgeRecipes.add((ISoulforgeRecipe) recipe);
            }
        }

        List<HopperRecipe> hopperRecipes = multiplexRecipeManager.getRecipes(Registrars.MultiplexRecipeTypes.FILTERED_HOPPER)
                .values()
                .stream()
                .map(IMultiplexRecipe::getDisplayRecipes)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        return new Results(millstoneRecipes, cauldronRecipes, crucibleRecipes, stokedCauldronRecipes, stokedCrucibleRecipes, sawRecipes, kilnRecipes, stokedKilnRecipes, turntableRecipes, soulforgeRecipes, hopperRecipes);
    }

    public class Results {
        private final List<MillstoneRecipe> millstoneRecipes;
        private final List<CauldronRecipe> cauldronRecipes;
        private final List<CrucibleRecipe> crucibleRecipes;
        private final List<CauldronRecipe> stokedCauldronRecipes;
        private final List<CrucibleRecipe> stokedCrucibleRecipes;
        private final List<SawRecipe> sawRecipes;
        private final List<KilnRecipe> kilnRecipes;
        private final List<KilnRecipe> stokedKilnRecipes;
        private final List<TurntableRecipe> turntableRecipes;
        private final List<ISoulforgeRecipe> soulforgeRecipes;
        private final List<HopperRecipe> hopperRecipes;

        Results(List<MillstoneRecipe> millstoneRecipes, List<CauldronRecipe> cauldronRecipes, List<CrucibleRecipe> crucibleRecipes, List<CauldronRecipe> stokedCauldronRecipes, List<CrucibleRecipe> stokedCrucibleRecipes, List<SawRecipe> sawRecipes, List<KilnRecipe> kilnRecipes, List<KilnRecipe> stokedKilnRecipes, List<TurntableRecipe> turntableRecipes, List<ISoulforgeRecipe> soulforgeRecipes, List<HopperRecipe> hopperRecipes) {
            this.millstoneRecipes = millstoneRecipes;
            this.cauldronRecipes = cauldronRecipes;
            this.crucibleRecipes = crucibleRecipes;
            this.stokedCauldronRecipes = stokedCauldronRecipes;
            this.stokedCrucibleRecipes = stokedCrucibleRecipes;
            this.sawRecipes = sawRecipes;
            this.kilnRecipes = kilnRecipes;
            this.stokedKilnRecipes = stokedKilnRecipes;
            this.turntableRecipes = turntableRecipes;
            this.soulforgeRecipes = soulforgeRecipes;
            this.hopperRecipes = hopperRecipes;
        }

        public List<MillstoneRecipe> getMillstoneRecipes() {
            return millstoneRecipes;
        }

        public List<CauldronRecipe> getCauldronRecipes() {
            return cauldronRecipes;
        }

        public List<CrucibleRecipe> getCrucibleRecipes() {
            return crucibleRecipes;
        }

        public List<CauldronRecipe> getStokedCauldronRecipes() {
            return stokedCauldronRecipes;
        }

        public List<CrucibleRecipe> getStokedCrucibleRecipes() {
            return stokedCrucibleRecipes;
        }

        public List<SawRecipe> getSawRecipes() {
            return sawRecipes;
        }

        public List<KilnRecipe> getKilnRecipes() {
            return kilnRecipes;
        }

        public List<KilnRecipe> getStokedKilnRecipes() {
            return stokedKilnRecipes;
        }

        public List<TurntableRecipe> getTurntableRecipes() {
            return turntableRecipes;
        }

        public List<ISoulforgeRecipe> getSoulforgeRecipes() {
            return soulforgeRecipes;
        }

        public List<HopperRecipe> getHopperRecipes() {
            return hopperRecipes;
        }
    }

}
