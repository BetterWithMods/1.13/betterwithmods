/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.compat.jei;

import com.betterwithmods.core.References;
import com.betterwithmods.core.api.crafting.result.IResult;
import com.betterwithmods.core.compat.jei.helpers.ResultHelper;
import com.betterwithmods.core.compat.jei.render.ResultRenderer;
import com.betterwithmods.core.impl.souls.SoulStack;
import com.betterwithmods.core.utilties.StackEjector;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.compat.jei.category.*;
import com.betterwithmods.mechanist.content.dynamic.CamoflageBlock;
import com.betterwithmods.mechanist.content.filtered_hopper.FilteredHopperScreen;
import com.betterwithmods.mechanist.content.filtered_hopper.recipes.result.EjectResult;
import com.betterwithmods.mechanist.content.filtered_hopper.recipes.result.SoulResult;
import com.betterwithmods.mechanist.content.millstone.MillstoneContainer;
import com.betterwithmods.mechanist.content.millstone.MillstoneScreen;
import com.betterwithmods.mechanist.content.pots.cauldron.CauldronContainer;
import com.betterwithmods.mechanist.content.pots.cauldron.CauldronRecipe;
import com.betterwithmods.mechanist.content.pots.cauldron.CauldronScreen;
import com.betterwithmods.mechanist.content.pots.crucible.CrucibleContainer;
import com.betterwithmods.mechanist.content.pots.crucible.CrucibleRecipe;
import com.betterwithmods.mechanist.content.pots.crucible.CrucibleScreen;
import com.betterwithmods.mechanist.content.soulforge.SoulforgeScreen;
import com.betterwithmods.mechanist.handlers.HeatHandler;
import com.google.common.collect.Lists;
import mezz.jei.api.IModPlugin;
import mezz.jei.api.JeiPlugin;
import mezz.jei.api.helpers.IJeiHelpers;
import mezz.jei.api.ingredients.IIngredientType;
import mezz.jei.api.registration.*;
import net.minecraft.block.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nonnull;
import java.util.Arrays;

@JeiPlugin
public class MechanistJEI implements IModPlugin {
    private static final ResourceLocation PLUGIN_UID = new ResourceLocation(References.MODID_MECHANIST, "jei_plugin");


    public static IIngredientType<IResult<StackEjector>> EJECT_RESULT = () -> EjectResult.class;
    public static IIngredientType<IResult<SoulStack>> SOUL_RESULT = () -> SoulResult.class;

    @Override
    public void registerIngredients(IModIngredientRegistration registration) {
        registration.register(EJECT_RESULT, Lists.newArrayList(), new ResultHelper<>(), new ResultRenderer<>());
        registration.register(SOUL_RESULT, Lists.newArrayList(), new ResultHelper<>(), new ResultRenderer<>());
    }

    @Override
    public void registerItemSubtypes(ISubtypeRegistration registration) {
        registration.useNbtForSubtypes(Arrays.stream(CamoflageBlock.getAll()).map(IItemProvider::asItem).toArray(Item[]::new));
    }

    @Override
    public void registerCategories(IRecipeCategoryRegistration registration) {
        IJeiHelpers helpers = registration.getJeiHelpers();
        registration.addRecipeCategories(new MillstoneCategory(helpers.getGuiHelper()));
        registration.addRecipeCategories(new SawCategory(helpers.getGuiHelper()));
        registration.addRecipeCategories(new BasePotCategory<>(helpers.getGuiHelper(), Constants.CAULDRON_BACKGROUND, "cauldron", Registrars.Blocks.CAULDRON, Constants.CAULDRON_CATEGORY, CauldronRecipe.class, HeatHandler.NORMAL_HEAT));
        registration.addRecipeCategories(new BasePotCategory<>(helpers.getGuiHelper(), Constants.CRUCIBLE_BACKGROUND, "crucible", Registrars.Blocks.CRUCIBLE, Constants.CRUCIBLE_CATEGORY, CrucibleRecipe.class, HeatHandler.NORMAL_HEAT));
        registration.addRecipeCategories(new BasePotCategory<>(helpers.getGuiHelper(), Constants.CAULDRON_BACKGROUND, "stoked_cauldron", Registrars.Blocks.CAULDRON, Constants.STOKED_CAULDRON_CATEGORY, CauldronRecipe.class, HeatHandler.STOKED_HEAT));
        registration.addRecipeCategories(new BasePotCategory<>(helpers.getGuiHelper(), Constants.CRUCIBLE_BACKGROUND, "stoked_crucible", Registrars.Blocks.CRUCIBLE, Constants.STOKED_CRUCIBLE_CATEGORY, CrucibleRecipe.class, HeatHandler.STOKED_HEAT));
        registration.addRecipeCategories(new KilnCategory(helpers.getGuiHelper(), "kiln", HeatHandler.NORMAL_HEAT, Constants.KILN_CATEGORY));
        registration.addRecipeCategories(new KilnCategory(helpers.getGuiHelper(), "stoked_kiln", HeatHandler.STOKED_HEAT, Constants.STOKED_KILN_CATEGORY));
        registration.addRecipeCategories(new TurntableCategory(helpers.getGuiHelper()));
        registration.addRecipeCategories(new SoulforgedCategory(helpers.getGuiHelper()));
        registration.addRecipeCategories(new FilteredHopperCategory(helpers.getGuiHelper()));
    }

    @Override
    public void registerRecipes(IRecipeRegistration registration) {
        MultiplexRecipeValidator validator = new MultiplexRecipeValidator();
        MultiplexRecipeValidator.Results results = validator.getRecipes();

        registration.addRecipes(results.getCauldronRecipes(), Constants.CAULDRON_CATEGORY);
        registration.addRecipes(results.getCrucibleRecipes(), Constants.CRUCIBLE_CATEGORY);
        registration.addRecipes(results.getStokedCauldronRecipes(), Constants.STOKED_CAULDRON_CATEGORY);
        registration.addRecipes(results.getStokedCrucibleRecipes(), Constants.STOKED_CRUCIBLE_CATEGORY);
        registration.addRecipes(results.getMillstoneRecipes(), Constants.MILLSTONE_CATEGORY);
        registration.addRecipes(results.getSawRecipes(), Constants.SAW_CATEGORY);
        registration.addRecipes(results.getKilnRecipes(), Constants.KILN_CATEGORY);
        registration.addRecipes(results.getStokedKilnRecipes(), Constants.STOKED_KILN_CATEGORY);
        registration.addRecipes(results.getTurntableRecipes(), Constants.TURNTABLE_CATEGORY);
        registration.addRecipes(results.getSoulforgeRecipes(), Constants.SOULFORGE_CATEGORY);
        registration.addRecipes(results.getHopperRecipes(), Constants.FILTERED_HOPPER_CATEGORY);
    }

    @Override
    public void registerGuiHandlers(IGuiHandlerRegistration registration) {
        registration.addRecipeClickArea(MillstoneScreen.class, 80, 18, 14, 14, Constants.MILLSTONE_CATEGORY);
        registration.addRecipeClickArea(CauldronScreen.class, 81, 19, 14, 14, Constants.CAULDRON_CATEGORY, Constants.STOKED_CAULDRON_CATEGORY);
        registration.addRecipeClickArea(CrucibleScreen.class, 81, 19, 14, 14, Constants.CRUCIBLE_CATEGORY, Constants.STOKED_CRUCIBLE_CATEGORY);
        registration.addRecipeClickArea(SoulforgeScreen.class, 90, 44, 22, 15, Constants.SOULFORGE_CATEGORY);
        registration.addRecipeClickArea(FilteredHopperScreen.class, 81, 18, 14, 14, Constants.FILTERED_HOPPER_CATEGORY);
    }

    @Override
    public void registerRecipeTransferHandlers(IRecipeTransferRegistration registration) {
        registration.addRecipeTransferHandler(MillstoneContainer.class, Constants.MILLSTONE_CATEGORY, 0, 3, 3, 36);
        registration.addRecipeTransferHandler(CrucibleContainer.class, Constants.CRUCIBLE_CATEGORY, 0, 27, 27, 36);
        registration.addRecipeTransferHandler(CrucibleContainer.class, Constants.STOKED_CRUCIBLE_CATEGORY, 0, 27, 27, 36);
        registration.addRecipeTransferHandler(CauldronContainer.class, Constants.CAULDRON_CATEGORY, 0, 27, 27, 36);
        registration.addRecipeTransferHandler(CauldronContainer.class, Constants.STOKED_CAULDRON_CATEGORY, 0, 27, 27, 36);
//        registration.addRecipeTransferHandler(FilteredHopperContainer.class, Constants.FILTERED_HOPPER_CATEGORY, );
//        registration.addRecipeTransferHandler(CauldronContainer.class, Constants.STOKED_CAULDRON_CATEGORY, 0, 27, 27, 36);
//        registration.addRecipeTransferHandler(SoulforgeContainer.class, Constants.SOULFORGE_CATEGORY, 0, 27, 27, 36);
    }

    @Override
    public void registerRecipeCatalysts(IRecipeCatalystRegistration registration) {
        registration.addRecipeCatalyst(new ItemStack(Registrars.Blocks.SAW), Constants.SAW_CATEGORY);
        registration.addRecipeCatalyst(new ItemStack(Registrars.Blocks.MILLSTONE), Constants.MILLSTONE_CATEGORY);
        registration.addRecipeCatalyst(new ItemStack(Registrars.Blocks.CAULDRON), Constants.CAULDRON_CATEGORY, Constants.STOKED_CAULDRON_CATEGORY);
        registration.addRecipeCatalyst(new ItemStack(Registrars.Blocks.CRUCIBLE), Constants.CRUCIBLE_CATEGORY, Constants.STOKED_CRUCIBLE_CATEGORY);
        registration.addRecipeCatalyst(new ItemStack(Registrars.Blocks.TURNTABLE), Constants.TURNTABLE_CATEGORY);
        registration.addRecipeCatalyst(new ItemStack(Registrars.Blocks.SOULFORGE), Constants.SOULFORGE_CATEGORY);
        registration.addRecipeCatalyst(new ItemStack(Registrars.Blocks.FILTERED_HOPPER), Constants.FILTERED_HOPPER_CATEGORY);
        registration.addRecipeCatalyst(new ItemStack(Blocks.BRICKS), Constants.KILN_CATEGORY, Constants.STOKED_KILN_CATEGORY);
    }

    @Nonnull
    @Override
    public ResourceLocation getPluginUid() {
        return PLUGIN_UID;
    }


    public static class Constants {
        public static ResourceLocation CRUCIBLE_CATEGORY = new ResourceLocation(References.MODID_MECHANIST, "crucible");
        public static ResourceLocation STOKED_CRUCIBLE_CATEGORY = new ResourceLocation(References.MODID_MECHANIST, "stoked_crucible");
        public static ResourceLocation CRUCIBLE_BACKGROUND = new ResourceLocation(References.MODID_MECHANIST, "textures/gui/crucible/jei.png");

        public static ResourceLocation CAULDRON_CATEGORY = new ResourceLocation(References.MODID_MECHANIST, "cauldron");
        public static ResourceLocation STOKED_CAULDRON_CATEGORY = new ResourceLocation(References.MODID_MECHANIST, "stoked_cauldron");
        public static ResourceLocation CAULDRON_BACKGROUND = new ResourceLocation(References.MODID_MECHANIST, "textures/gui/cauldron/jei.png");

        public static ResourceLocation MILLSTONE_CATEGORY = new ResourceLocation(References.MODID_MECHANIST, "millstone");
        public static ResourceLocation MILLSTONE_BACKGROUND = new ResourceLocation(References.MODID_MECHANIST, "textures/gui/millstone/jei.png");

        public static ResourceLocation SAW_CATEGORY = new ResourceLocation(References.MODID_MECHANIST, "saw");
        public static ResourceLocation SAW_BACKGROUND = new ResourceLocation(References.MODID_MECHANIST, "textures/gui/saw/jei.png");

        public static ResourceLocation SOULFORGE_CATEGORY = new ResourceLocation(References.MODID_MECHANIST, "soulforge");
        public static ResourceLocation SOULFORGE_BACKGROUND = new ResourceLocation(References.MODID_MECHANIST, "textures/gui/soulforge/jei.png");

        public static ResourceLocation TURNTABLE_CATEGORY = new ResourceLocation(References.MODID_MECHANIST, "turntable");
        public static ResourceLocation TURNTABLE_BACKGROUND = new ResourceLocation(References.MODID_MECHANIST, "textures/gui/turntable/jei.png");

        public static ResourceLocation KILN_CATEGORY = new ResourceLocation(References.MODID_MECHANIST, "kiln");
        public static ResourceLocation STOKED_KILN_CATEGORY = new ResourceLocation(References.MODID_MECHANIST, "stoked_kiln");
        public static ResourceLocation KILN_BACKGROUND = new ResourceLocation(References.MODID_MECHANIST, "textures/gui/kiln/jei.png");

        public static ResourceLocation FILTERED_HOPPER_CATEGORY = new ResourceLocation(References.MODID_MECHANIST, "filtered_hopper");
        public static ResourceLocation FILTERED_HOPPER_BACKGROUND = new ResourceLocation(References.MODID_MECHANIST, "textures/gui/filtered_hopper/jei.png");
    }
}
