/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.filtered_hopper;

import com.betterwithmods.core.References;
import com.betterwithmods.core.Tags;
import com.google.common.collect.Maps;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tags.Tag;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.ResourceLocation;

import java.util.Map;
import java.util.Optional;

public class FilterHandler {

    private static final Map<Item, Tag<Item>> FILTERS = Maps.newHashMap();

    private static ResourceLocation findTagPath(Item item) {
        ResourceLocation itemName = item.getRegistryName();
        String namespace = itemName.getNamespace();
        String path = itemName.getPath();
        return new ResourceLocation(References.MODID_MECHANIST, String.format("filters/%s/%s", namespace, path));
    }

    public static void findFilterTag(Item item) {
        ResourceLocation tagPath = findTagPath(item);
        Tag<Item> tag = Tags.Items.tag(tagPath);
        //Avoid empty tags
        if (!isEmpty(tag)) {
            FILTERS.put(item, tag);
        } else {
            FILTERS.remove(item);
        }
    }

    private static boolean isEmpty(Tag<? extends IItemProvider> tag) {
        return tag.getAllElements().isEmpty() || tag.getEntries().isEmpty();
    }

    public static Optional<Tag<Item>> getFilter(ItemStack stack) {
        Item item = stack.getItem();
        findFilterTag(item);
        if (FILTERS.containsKey(item)) {
            return Optional.of(FILTERS.get(item));
        }
        return Optional.empty();
    }

}
