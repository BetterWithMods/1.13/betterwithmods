/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.dispenser;

import com.betterwithmods.core.References;
import com.betterwithmods.core.base.game.tile.InventoryTile;
import com.betterwithmods.core.utilties.inventory.InventoryUtils;
import com.betterwithmods.core.utilties.inventory.ItemSlot;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.IIntArray;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class AdvancedDispenserTile extends InventoryTile<ItemStackHandler> implements INamedContainerProvider {

    private int cursor;

    public AdvancedDispenserTile() {
        super(Registrars.Tiles.ADVANCED_DISPENSER);
    }

    @Nonnull
    @Override
    public ItemStackHandler createInventory() {
        return new ItemStackHandler(16);
    }

    public static final String CURSOR = "cursor";

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        compound.putInt(CURSOR, cursor);
        return super.write(compound);
    }

    @Override
    public void read(CompoundNBT compound) {
        if (compound.contains(CURSOR)) {
            cursor = compound.getInt(CURSOR);
        }
        super.read(compound);
    }

    public void setCursor(int cursor) {
        this.cursor = cursor;
    }

    public int getCursor() {
        return cursor;
    }

    public void incrementCursor() {
        int cursor = getCursor();

        IItemHandler inventory = getInventory();

        Iterable<ItemSlot> slots = InventoryUtils.iterable(inventory, cursor + 1);

        for (ItemSlot slot : slots) {
            if (!slot.isEmpty()) {
                setCursor(slot.getSlot());
                return;
            }
        }
    }

    @Override
    public ITextComponent getDisplayName() {
            return new TranslationTextComponent(String.format("container.%s.advanced_dispenser", References.MODID_MECHANIST));
        }

    @Nullable
    @Override
    public Container createMenu(int windowId, PlayerInventory playerInventory, PlayerEntity playerIn) {
        return new AdvancedDispenserContainer(windowId,playerIn, getInventory());
    }


    private final IIntArray extraData = new IIntArray() {
        @Override
        public int get(int index) {
            switch(index) {
                case 1:
                    return cursor;
            }
            return 0;
        }

        @Override
        public void set(int index, int data) {
            switch(index) {
                case 1:
                    cursor = data;
            }
        }

        @Override
        public int size() {
            return 1;
        }
    };
}
