/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.handlers;

import com.betterwithmods.core.utilties.EntityUtils;
import com.betterwithmods.core.utilties.EnvironmentUtils;
import com.betterwithmods.core.utilties.inventory.InventoryUtils;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.World;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;

import java.util.List;

public class ItemCollector {

    private VoxelShape collectionArea;
    private LazyOptional<? extends IItemHandler> inventory;

    public ItemCollector(VoxelShape collectionArea, LazyOptional<? extends IItemHandler> inventory) {
        this.collectionArea = collectionArea;
        this.inventory = inventory;
    }

    public void tick(World world, BlockPos pos) {
        pickupItems(world, pos);
    }

    public List<ItemEntity> getCollected(World world, BlockPos pos, VoxelShape area) {
        return EntityUtils.getEntitesInShape(world, pos, area, ItemEntity.class, ItemEntity::isAlive);
    }

    public VoxelShape getCollectionArea() {
        return collectionArea;
    }

    public LazyOptional<? extends IItemHandler> getInventory() {
        return inventory;
    }

    public void pickupItems(World world, BlockPos pos){
        getInventory().ifPresent(inventory -> getCollected(world, pos, getCollectionArea()).forEach(item -> pickupItem(world, pos, item, inventory)));
    }

    public void pickupItem(World world, BlockPos pos, ItemEntity entity, IItemHandler inventory) {
        if (InventoryUtils.addEntityItem(inventory, entity)) {
            EnvironmentUtils.playSound(world, pos, SoundEvents.ENTITY_ITEM_PICKUP, SoundCategory.BLOCKS, 1, 1);
        }
    }
}
