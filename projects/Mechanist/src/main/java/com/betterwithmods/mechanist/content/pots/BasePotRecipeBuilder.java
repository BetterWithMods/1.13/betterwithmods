/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.pots;

import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipeSerializer;
import com.betterwithmods.core.api.crafting.output.IOutput;
import com.betterwithmods.core.api.crafting.result.IResult;
import com.betterwithmods.core.generator.multiplex.FinishedMultiplexRecipe;
import com.betterwithmods.core.generator.multiplex.IFinishedMultiplexRecipe;
import com.betterwithmods.core.impl.crafting.input.InventoryInput;
import com.betterwithmods.core.impl.crafting.output.OutputList;
import com.betterwithmods.core.utilties.DeserializeUtils;
import com.betterwithmods.mechanist.content.millstone.MillstoneRecipe;
import com.google.common.collect.Lists;
import com.google.gson.JsonObject;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;

import java.util.function.Consumer;

public abstract class BasePotRecipeBuilder {

    protected ResourceLocation id;
    protected NonNullList<Ingredient> inputs = NonNullList.create();
    protected IOutput outputs;
    protected int heatLevel;
    protected int recipeTime;

    public BasePotRecipeBuilder id(ResourceLocation id) {
        this.id = id;
        return this;
    }

    public BasePotRecipeBuilder id(String modid, String name) {
        return id(new ResourceLocation(modid, name));
    }

    public BasePotRecipeBuilder recipeTime(int recipeTime) {
        this.recipeTime = recipeTime;
        return this;
    }

    public BasePotRecipeBuilder heatLevel(int heatLevel) {
        this.heatLevel = heatLevel;
        return this;
    }


    public BasePotRecipeBuilder inputs(NonNullList<Ingredient> inputs) {
        this.inputs.addAll(inputs);
        return this;
    }

    public BasePotRecipeBuilder inputs(Ingredient... ingredients) {
        this.inputs.addAll(Lists.newArrayList(ingredients));
        return this;
    }

    public BasePotRecipeBuilder input(Ingredient input) {
        this.inputs.add(input);
        return this;
    }

    public BasePotRecipeBuilder input(IItemProvider provider) {
        return this.input(Ingredient.fromItems(provider));
    }

    public BasePotRecipeBuilder outputs(IOutput outputs) {
        this.outputs = outputs;
        return this;
    }

    public BasePotRecipeBuilder outputs(IResult... outputs) {
        this.outputs(new OutputList(Lists.newArrayList(outputs)));
        return this;
    }

    public abstract void build(Consumer<IFinishedMultiplexRecipe> consumer);


    protected class Result extends FinishedMultiplexRecipe {

        private InventoryInput<MillstoneRecipe> input;
        private int recipeTime;
        private int heatLevel;

        public Result(IMultiplexRecipeSerializer<?> serializer, ResourceLocation id, NonNullList<Ingredient> inputs, IOutput outputs, int recipeTime, int heatLevel) {
            super(serializer, id, outputs);
            this.input = new InventoryInput<>(inputs);
            this.heatLevel = heatLevel;
            this.recipeTime = recipeTime;
        }

        @Override
        public void write(JsonObject jsonObject) {
            DeserializeUtils.writeInput(this.input, jsonObject);
            DeserializeUtils.writeOutput(this.outputs, jsonObject);
            jsonObject.addProperty("recipeTime", recipeTime);
            jsonObject.addProperty("heatLevel", heatLevel);
        }
    }

}
