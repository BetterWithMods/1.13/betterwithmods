/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.turntable;

import com.betterwithmods.core.api.crafting.input.IInput;
import com.betterwithmods.core.api.crafting.input.IRecipeContext;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipeSerializer;
import com.betterwithmods.core.api.crafting.output.IOutput;
import com.betterwithmods.core.base.game.recipes.multiplex.ITimedRecipe;
import com.betterwithmods.core.base.game.recipes.multiplex.state.StateMultiplexRecipe;
import com.betterwithmods.core.utilties.ItemUtils;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class TurntableRecipe extends StateMultiplexRecipe<TurntableRecipe> implements ITimedRecipe, IStagedRecipe {

    private Integer recipeTime;

    private BlockState recipeStage;

    public TurntableRecipe(ResourceLocation id, IInput<BlockState, TurntableRecipe> inputs, IOutput outputs, Integer recipeTime, BlockState recipeStage) {
        super(Registrars.MultiplexRecipeTypes.TURNTABLE, id, inputs, outputs);
        this.recipeTime = recipeTime;
        this.recipeStage = recipeStage;
    }

    @Override
    public IMultiplexRecipeSerializer<?> getSerializer() {
        return Registrars.MultiplexRecipeSerializers.TURNTABLE;
    }

    @Override
    public void onCraft(World world, BlockPos pos, IRecipeContext<BlockState, TurntableRecipe> context) {
        ItemUtils.ejectStackOffset(world, pos, outputs.getOutputs(context.getState(world,pos), ItemStack.class));
    }

    @Override
    public Integer getRecipeTime() {
        return recipeTime;
    }

    @Override
    public BlockState getRecipeStage() {
        return recipeStage;
    }

    @Override
    public void consume(World world, BlockPos pos, IRecipeContext context) {
        if(recipeStage != null) {
            world.setBlockState(pos, recipeStage);
        } else {
            world.removeBlock(pos, false);
        }
        context.onCrafted();
    }
}
