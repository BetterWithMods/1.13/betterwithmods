/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.dynamic;

import com.betterwithmods.core.base.game.block.BlockBase;
import com.google.common.collect.Sets;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.stats.Stats;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.Set;

public abstract class CamoflageBlock extends BlockBase {

    private static Set<Block> CAMO_BLOCKS = Sets.newHashSet();

    public static Set<Block> getCamoBlocks() {
        return CAMO_BLOCKS;
    }

    public static Block[] getAll() {
        Block[] array = new Block[CAMO_BLOCKS.size()];
        CAMO_BLOCKS.toArray(array);
        return array;
    }

    public CamoflageBlock(Properties properties) {
        super(properties);
        if(isCamoTile()) {
            CAMO_BLOCKS.add(this);
        }
    }

    @Override
    public void fillItemGroup(ItemGroup group, NonNullList<ItemStack> items) {
        super.fillItemGroup(group,items);
    }

    @Override
    public ItemStack getPickBlock(BlockState state, @Nullable RayTraceResult target, IBlockReader world, BlockPos pos, PlayerEntity player) {
        TileEntity tile = world.getTileEntity(pos);
        return tile instanceof CamoflageTile ? ((CamoflageTile) tile).getItemStack(state) : ItemStack.EMPTY;
    }

    @Override
    public ITextComponent getNameTextComponent() {
        return super.getNameTextComponent();
    }

    public BlockState getParent(ItemStack stack) {
        CompoundNBT tag = stack.getChildTag("BlockEntityTag");
        if (tag != null && tag.contains("parent")) {
            return NBTUtil.readBlockState(tag.getCompound("parent"));
        }
        return null;
    }

    @Override
    public void harvestBlock(World world, PlayerEntity player, BlockPos pos, BlockState state, @Nullable TileEntity te, ItemStack stack) {
        if (te instanceof CamoflageTile) {
            spawnAsEntity(world, pos, ((CamoflageTile) te).getItemStack(state));
            player.addStat(Stats.BLOCK_MINED.get(this));
        } else {
            super.harvestBlock(world, player, pos, state, null, stack);
        }
    }

    @Override
    public void onBlockPlacedBy(World world, BlockPos pos, BlockState state, @Nullable LivingEntity placer, ItemStack stack) {
        TileEntity tile = world.getTileEntity(pos);
        if (tile instanceof CamoflageTile) {
            ((CamoflageTile) tile).loadFromItemStack(stack);
        }
    }

    public boolean isCamoTile() {
        return true;
    }
}
