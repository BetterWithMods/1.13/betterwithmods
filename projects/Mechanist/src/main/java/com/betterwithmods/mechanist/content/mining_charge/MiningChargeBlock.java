/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.mining_charge;

import com.betterwithmods.core.base.game.ConstantIngredients;
import com.betterwithmods.core.base.game.block.BlockBase;
import com.betterwithmods.core.utilties.EnvironmentUtils;
import com.betterwithmods.core.utilties.ItemUtils;
import com.betterwithmods.core.utilties.WorldUtils;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.Explosion;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class MiningChargeBlock extends BlockBase {

    private static final BooleanProperty UNSTABLE = BlockStateProperties.UNSTABLE;
    public static final DirectionProperty FACING = BlockStateProperties.FACING;

    private static VoxelShape[] FACING_SHAPES = new VoxelShape[]{
            Block.makeCuboidShape(0, 8, 0, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 16, 8, 16),
            Block.makeCuboidShape(0, 0, 8, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 16, 16, 8),
            Block.makeCuboidShape(8, 0, 0, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 8, 16, 16),
    };


    public MiningChargeBlock(Block.Properties builder) {
        super(builder);
        this.setDefaultState(this.getDefaultState().with(UNSTABLE, false).with(FACING, Direction.UP));
        setRenderLayer(BlockRenderLayer.CUTOUT);
    }


    @Override
    public boolean isValidPosition(BlockState state, IWorldReader world, BlockPos pos) {
        Direction facing = state.get(FACING).getOpposite();
        BlockPos offset = pos.offset(facing);
        return WorldUtils.isSideSolid(world, offset, facing.getOpposite());
    }


    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return getDefaultState().with(FACING, context.getFace());
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader world, BlockPos pos, ISelectionContext context) {
        Direction facing = state.get(FACING);
        return FACING_SHAPES[facing.getIndex()];
    }

    @Override
    public void onBlockAdded(BlockState state, World world, BlockPos pos, BlockState oldState, boolean isMoving) {
        if (oldState.getBlock() != state.getBlock()) {
            if (world.isBlockPowered(pos)) {
                this.explode(world, pos);
                world.removeBlock(pos, false);
            }
        }
    }


    @Override
    public void neighborChanged(BlockState state, World world, BlockPos pos, Block fromBlock, BlockPos fromPos, boolean idk) {
        if (world.isBlockPowered(pos)) {
            this.explode(world, pos);
            world.removeBlock(pos, false);
        }
        super.neighborChanged(state, world, pos, fromBlock, fromPos, idk);
    }

    public void onBlockHarvested(World world, BlockPos pos, BlockState state, PlayerEntity player) {
        if (!world.isRemote() && !player.isCreative() && state.get(UNSTABLE)) {
            this.explode(world, pos);
        }

        super.onBlockHarvested(world, pos, state, player);
    }

    public void onExplosionDestroy(World world, BlockPos pos, Explosion explosionIn) {
        explode(world, pos, explosionIn.getExplosivePlacedBy());
    }

    public void explode(World world, BlockPos p_196534_2_) {
        this.explode(world, p_196534_2_, null);
    }

    private void explode(World world, BlockPos pos, @Nullable LivingEntity exploder) {
        if (!world.isRemote) {
            BlockState state = world.getBlockState(pos);
            Direction facing = Direction.UP;
            if (state.getBlock() == this)
                facing = state.get(FACING);
            MiningChargeEntity entity = new MiningChargeEntity(world, pos, facing, exploder);
            world.addEntity(entity);
            EnvironmentUtils.playSound(world,pos, SoundEvents.ENTITY_TNT_PRIMED, SoundCategory.BLOCKS, 1,1);
        }
    }


    public boolean onBlockActivated(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockRayTraceResult hit) {
        ItemStack itemstack = player.getHeldItem(hand);
        Item item = itemstack.getItem();
        if (ConstantIngredients.FIRESTARTER.test(itemstack)) {
            this.explode(world, pos, player);
            world.setBlockState(pos, Blocks.AIR.getDefaultState(), 11);
            if (item.isDamageable()) {
                ItemUtils.damageItem(itemstack, player, hand, 1);
            } else {
                itemstack.shrink(1);
            }
            return true;
        }
        return false;
    }

    public void onEntityCollision(BlockState state, World world, BlockPos pos, Entity entityIn) {
        if (!world.isRemote && entityIn instanceof AbstractArrowEntity) {
            AbstractArrowEntity entityarrow = (AbstractArrowEntity) entityIn;
            Entity entity = entityarrow.getShooter();
            if (entityarrow.isBurning()) {
                this.explode(world, pos, entity instanceof LivingEntity ? (LivingEntity) entity : null);
                world.removeBlock(pos, true);
            }
        }
    }


    public boolean canDropFromExplosion(Explosion explosionIn) {
        return false;
    }

    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(UNSTABLE).add(FACING);
    }
}
