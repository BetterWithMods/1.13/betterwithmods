/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.soulforge;

import com.betterwithmods.core.References;
import com.betterwithmods.core.base.game.ListenableItemStackHandler;
import com.betterwithmods.core.base.game.tile.InventoryTile;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.IWorldPosCallable;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class SoulforgeTile extends InventoryTile<ListenableItemStackHandler> implements INamedContainerProvider {
    public SoulforgeTile() {
        super(Registrars.Tiles.SOULFORGE);
    }

    private ItemStackHandler resultInventory = new ItemStackHandler(1);

    @Nonnull
    @Override
    public ListenableItemStackHandler createInventory() {
        return new ListenableItemStackHandler(16);
    }

    @Override
    public ITextComponent getDisplayName() {
        return new TranslationTextComponent(String.format("container.%s.soulforge", References.MODID_MECHANIST));
    }

    public ItemStackHandler getResultInventory() {
        return resultInventory;
    }

    @Nullable
    @Override
    public Container createMenu(int id, PlayerInventory playerInventory, PlayerEntity player) {
        return new SoulforgeContainer(id, player, this.getInventory(), getResultInventory(), IWorldPosCallable.of(world, pos), null);
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        compound.put("result", resultInventory.serializeNBT());
        return super.write(compound);
    }

    @Override
    public void read(CompoundNBT compound) {
        if (compound.contains("result")) {
            resultInventory.deserializeNBT(compound.getCompound("result"));
        }
        super.read(compound);
    }
}
