/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.planter;

import com.betterwithmods.core.base.game.ConstantIngredients;
import com.betterwithmods.core.base.game.block.ISimpleActivation;
import com.betterwithmods.core.utilties.BlockUtils;
import com.betterwithmods.core.utilties.ItemUtils;
import com.betterwithmods.core.utilties.PlayerUtils;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.Hand;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.World;
import net.minecraftforge.common.PlantType;


public enum PlanterType implements ISimpleActivation, IStringSerializable {
    EMPTY(new PlantType[0], new ItemStack(Blocks.AIR), Ingredient.EMPTY, IBooleanFunction.ONLY_FIRST, PlanterType::activateEmpty),
    DIRT(new PlantType[]{PlantType.Plains}, new ItemStack(Blocks.DIRT), IBooleanFunction.OR, PlanterType::usingShovel, PlanterType::till),
    GRASS(new PlantType[]{PlantType.Plains}, new ItemStack(Blocks.DIRT), Ingredient.fromItems(Blocks.GRASS_BLOCK), IBooleanFunction.OR, PlanterType::usingShovel, PlanterType::till),
    FARMLAND(new PlantType[]{PlantType.Crop, PlantType.Plains}, new ItemStack(Blocks.DIRT), Ingredient.EMPTY, IBooleanFunction.OR, PlanterType::usingShovel),
    FERTILIZED_FARMLAND(new PlantType[]{PlantType.Crop, PlantType.Plains}, new ItemStack(Blocks.DIRT), Ingredient.EMPTY, IBooleanFunction.OR, PlanterType::usingShovel),
    SAND(new PlantType[]{PlantType.Desert, PlantType.Beach}, new ItemStack(Blocks.SAND), IBooleanFunction.OR, PlanterType::usingShovel),
    RED_SAND(new PlantType[]{PlantType.Desert, PlantType.Beach}, new ItemStack(Blocks.RED_SAND), IBooleanFunction.OR, PlanterType::usingShovel),
    SOUL_SAND(new PlantType[]{PlantType.Nether}, new ItemStack(Blocks.SOUL_SAND), IBooleanFunction.OR, PlanterType::usingShovel),
    WATER(new PlantType[]{PlantType.Water}, ItemStack.EMPTY, Ingredient.fromItems(Items.WATER_BUCKET), IBooleanFunction.ONLY_FIRST, PlanterType::bucket),
    MYCELIUM(new PlantType[]{PlantType.Cave}, new ItemStack(Blocks.DIRT), Ingredient.fromItems(Blocks.MYCELIUM), IBooleanFunction.OR, PlanterType::usingShovel),
    PODZOL(new PlantType[]{PlantType.Cave}, new ItemStack(Blocks.DIRT), Ingredient.fromItems(Blocks.PODZOL), IBooleanFunction.OR, PlanterType::usingShovel);

    public static final PlanterType[] VALUES = values();


    private Ingredient ingredient;
    private ItemStack stack;
    private VoxelShape shape;

    private ISimpleActivation[] activations;
    private PlantType[] plantTypes;

    PlanterType(PlantType[] plantTypes, ItemStack stack, IBooleanFunction combine, ISimpleActivation... activations) {
        this(plantTypes, stack, Ingredient.fromStacks(stack), combine, activations);
    }

    PlanterType(PlantType[] plantTypes, ItemStack stack, Ingredient ingredient, IBooleanFunction combine, ISimpleActivation... activations) {
        this.ingredient = ingredient;
        this.plantTypes = plantTypes;
        this.stack = stack;
        this.shape = VoxelShapes.combineAndSimplify(PlanterBlock.PLANTER, PlanterBlock.INSIDE, combine);
        this.activations = activations;
    }


    public VoxelShape getShape() {
        return shape;
    }

    public ItemStack getStack() {
        return this.stack;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public PlantType[] getPlantTypes() {
        return plantTypes;
    }

    @Override
    public boolean onBlockActivated(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand) {
        for (ISimpleActivation a : this.activations)
            if (a.onBlockActivated(state, world, pos, player, hand))
                return true;
        return false;
    }

    public static boolean usingShovel(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand) {
        return removeUsing(ConstantIngredients.SHOVEL, state, world, pos, player, hand);
    }

    public static boolean removeUsing(Ingredient using, BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand) {
        PlanterBlock planter = (PlanterBlock) state.getBlock();
        PlanterType type = planter.getType();
        ItemStack held = PlayerUtils.getHeld(player, hand);
        if (using.test(held)) {
            world.playSound(player, pos, SoundEvents.BLOCK_GRAVEL_BREAK, SoundCategory.BLOCKS, 1, 1);
            if (!world.isRemote)
                ItemUtils.ejectStackExact(world, pos, type.getStack());

            planter.setType(state, world, pos, EMPTY);
            return true;
        }
        return false;
    }


    public static boolean till(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand) {
        PlanterBlock planter = (PlanterBlock) state.getBlock();

        ItemStack held = PlayerUtils.getHeld(player, hand);
        if (ConstantIngredients.HOES.test(held)) {
            world.playSound(player, pos, SoundEvents.ITEM_HOE_TILL, SoundCategory.BLOCKS, 1, 1);
            planter.setType(state, world, pos, FARMLAND);
            return true;
        }
        return false;
    }


    public static boolean bucket(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand) {
        PlanterBlock planter = (PlanterBlock) state.getBlock();

        ItemStack held = PlayerUtils.getHeld(player, hand);
        if (Items.BUCKET.equals(held.getItem())) {
            world.playSound(player, pos, SoundEvents.ITEM_BUCKET_FILL, SoundCategory.BLOCKS, 1, 1);
            planter.setType(state, world, pos, EMPTY);
            player.setHeldItem(hand, new ItemStack(Items.WATER_BUCKET));
            return true;
        }
        return false;
    }


    public static boolean activateEmpty(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand) {
        PlanterBlock planter = (PlanterBlock) state.getBlock();
        PlanterType type = planter.getType();
        if (type != EMPTY)
            return false;
        ItemStack held = PlayerUtils.getHeld(player, hand);
        if (held.isEmpty())
            return false;

        for (PlanterType newType : VALUES) {
            if (newType.getIngredient().test(held)) {

                if(!held.getContainerItem().isEmpty()) {
                    player.setHeldItem(hand, held.getContainerItem());
                } else {
                    ItemUtils.getBlock(newType.getStack()).ifPresent(block -> BlockUtils.playPlaceSound(world, pos, player, block));
                    held.shrink(1);
                }

                planter.setType(state, world, pos, newType);
                return true;
            }
        }
        return false;
    }


    @Override
    public String getName() {
        return name().toLowerCase();
    }
}
