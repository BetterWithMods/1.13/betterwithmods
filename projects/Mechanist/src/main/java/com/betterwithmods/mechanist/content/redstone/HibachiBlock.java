/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.redstone;

import com.betterwithmods.core.Tags;
import com.betterwithmods.core.base.game.block.DigitalRedstoneBlock;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.FireBlock;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class HibachiBlock extends DigitalRedstoneBlock {

    public static final DirectionProperty FACING = BlockStateProperties.FACING;

    public HibachiBlock(Properties properties) {
        super(properties);
        this.setDefaultState(this.getDefaultState().with(FACING, Direction.UP));
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(FACING);
    }

    public Direction getFront(IBlockReader world, BlockPos pos) {
        return world.getBlockState(pos).get(FACING);
    }

    public BlockPos getFrontPos(IBlockReader world, BlockPos pos) {
        return pos.offset(getFront(world, pos));
    }

    public void makeFlame(World world, BlockPos pos) {
        BlockPos front = getFrontPos(world, pos);
        if (world.isAirBlock(front)) {
            BlockPos below = front.down();
            BlockState belowState = world.getBlockState(below);
            //TODO allow hibachis to place side fires
            if (Block.hasSolidSide(belowState, world, below, Direction.UP)) {
                world.playSound(null, pos, SoundEvents.ITEM_FIRECHARGE_USE, SoundCategory.BLOCKS, 1.0F, (world.rand.nextFloat() - world.rand.nextFloat()) * 0.2F + 1.0F);
                world.setBlockState(front, ((FireBlock) (Blocks.FIRE)).getStateForPlacement(world, pos));
            }
        }
    }

    @Override
    public void onPowered(BlockState state, World world, BlockPos pos) {
        makeFlame(world, pos);
    }

    @Override
    public void onUnpowered(BlockState state, World world, BlockPos pos) {
        if (isValidFlame(world, pos)) {
            BlockPos front = getFrontPos(world, pos);
            world.removeBlock(front, false);
        }
    }

    @Override
    public boolean isFireSource(BlockState state, IBlockReader world, BlockPos pos, Direction side) {
        BlockState fire = world.getBlockState(pos.offset(side));
        if (fire.getBlock().equals(Registrars.Blocks.STOKED_FIRE))
             return false;
        return isLit(world, pos);
    }

    public boolean isValidFlame(IBlockReader world, BlockPos pos) {
        BlockState state = world.getBlockState(getFrontPos(world, pos));
        return state.isIn(Tags.Blocks.NORMAL_FIRE) || state.isIn(Tags.Blocks.STOKED_FIRE);
    }

    public BlockState getStateForPlacement(BlockItemUseContext context) {
        Direction facing = context.getNearestLookingDirection().getOpposite();
        return this.getDefaultState().with(FACING, facing);
    }

    @Override
    protected void onChanged(BlockState state, World world, BlockPos pos, Block blockIn, BlockPos fromPos) {
        if (world.isRemote)
            return;
        if (isLit(world, pos)) {
            makeFlame(world, pos);
        }
    }

}
