/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.dirt_slab;

import com.betterwithmods.mechanist.Registrars;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;

import java.util.Random;

public class DirtSlab extends SnowyDirtSlabBlock {
    public DirtSlab(Properties builder) {
        super(builder);
    }


    @Override
    public void tick(BlockState state, World world, BlockPos pos, Random random) {
        super.tick(state,world,pos,random);

        if (!world.isRemote) {
            if (world.getLight(pos.up()) >= 9) {
                for (int i = 0; i < 4; ++i) {
                    BlockPos blockpos = pos.add(random.nextInt(3) - 1, random.nextInt(5) - 3, random.nextInt(3) - 1);
                    if (world.isBlockPresent(blockpos)) {
                        if (world.getBlockState(blockpos).getBlock() == Blocks.GRASS_BLOCK && canGrassGrow(world, blockpos)) {
                            world.setBlockState(pos, Registrars.Blocks.GRASS_SLAB.getDefaultState().with(SNOWY, state.get(SNOWY)));
                            return;
                        }
                    }
                }
            }
        }
    }

    private static boolean canGrassGrow(IWorldReader world, BlockPos pos) {
        BlockPos blockpos = pos.up();
        return world.getLight(blockpos) >= 4 && world.getBlockState(blockpos).getOpacity(world, blockpos) < world.getMaxLightLevel() && !world.getFluidState(blockpos).isTagged(FluidTags.WATER);
    }
}
