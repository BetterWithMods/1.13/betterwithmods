/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.millstone;

import com.betterwithmods.core.References;
import com.betterwithmods.core.base.game.recipes.multiplex.inventory.TimedInventoryRecipeContext;
import com.betterwithmods.core.impl.crafting.multiplex.MultiplexRecipeManager;
import com.betterwithmods.core.utilties.*;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.content.mechanical.MechanicalInventoryTile;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.IIntArray;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class MillstoneTile extends MechanicalInventoryTile implements ITickableTileEntity, INamedContainerProvider {

    private final Timer timer = new Timer();

    public MillstoneTile() {
        super(Registrars.Tiles.MILLSTONE);
    }

    @Nonnull
    @Override
    public ItemStackHandler createInventory() {
        return new ItemStackHandler(3);
    }

    @Nonnull
    @Override
    public Iterable<Direction> getInputs() {
        return DirectionUtils.fromAxis(Direction.Axis.Y);
    }

    @Override
    public void postCalculate() {
    }

    @Override
    public boolean overpower(World world, BlockPos pos) {
        return false;
    }

    public static final FloatBuilder VOLUME = FloatBuilder.builder(0.5f).addRandom(0.1F),
            PITCH = FloatBuilder.builder(0.5f).addRandom(0.1F);

    @Override
    public void tick() {
        if (world != null) {
            if (power.getTorque() > 0) {
                EnvironmentUtils.playRandomSound(world, pos, Registrars.Sounds.MILLSTONE_GRIND, SoundCategory.BLOCKS, VOLUME.build(), PITCH.build(), 0.05d);

                //TODO stop if too many sides blocked
                TimedInventoryRecipeContext<MillstoneRecipe> context = new TimedInventoryRecipeContext<>(timer.getTime());
                MultiplexRecipeManager.get(world.getServer())
                        .ifPresent(m -> MiscUtils.ifPresentOrElse(m.getRecipe(Registrars.MultiplexRecipeTypes.MILLSTONE, world, pos, context),
                                r -> {
                                    timer.update(r);
                                    r.craft(world, pos, context);
                                    timer.incrementTime(1);
                                },
                                timer::reset
                        ));
            }
        }
    }

    public static final String TIMER = "timer";

    @Override
    public void read(CompoundNBT compound) {
        timer.deserializeNBT(compound.getCompound(TIMER));
        super.read(compound);
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        compound.put(TIMER, timer.serializeNBT());
        return super.write(compound);
    }

    @Override
    public ITextComponent getDisplayName() {
        return new TranslationTextComponent(String.format("container.%s.millstone", References.MODID_MECHANIST));
    }

    @Nullable
    @Override
    public Container createMenu(int id, PlayerInventory inventory, PlayerEntity player) {
        return new MillstoneContainer(id, player, getInventory(), data);
    }


    public final IIntArray data = new IIntArray() {

        @Override
        public int get(int index) {
            switch (index) {
                case 0:
                    return MillstoneTile.this.timer.getTime().get();
                case 1:
                    return MillstoneTile.this.timer.getGoalTime().get();
                case 2:
                    return MillstoneTile.this.power.getTorque();
                default:
                    return -1;
            }

        }

        @Override
        public void set(int index, int value) {
            switch (index) {
                case 0:
                    MillstoneTile.this.timer.setTime(value);
                    break;
                case 1:
                    break;
            }
        }

        @Override
        public int size() {
            return 3;
        }
    };
}
