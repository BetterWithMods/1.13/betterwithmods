/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.mining_charge;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import net.minecraft.block.BlockState;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Collection;
import java.util.Set;
import java.util.function.BiFunction;

public class MiningChargeHandler {

    private static final BlockPos[][] OFFSETS_BY_AXIS = new BlockPos[][]{
            generateRing((i, j) -> new BlockPos(0, i, j)), //X
            generateRing((i, j) -> new BlockPos(i, 0, j)), //Y
            generateRing((i, j) -> new BlockPos(i, j, 0)), //Z
    };

    public static Iterable<BlockPos> getOffsetsAround(BlockPos center, Direction facing, int distance) {
        Direction.Axis axis = facing.getAxis();
        BlockPos[] positions = OFFSETS_BY_AXIS[axis.ordinal()];
        Collection<Iterable<BlockPos>> collection = Lists.newArrayList();
        for (BlockPos pos : positions) {
            BlockPos offset = center.add(pos);
            collection.add(BlockPos.getAllInBoxMutable(offset, offset.offset(facing, distance)));
        }
        return Iterables.concat(collection);
    }

    public void onExplode(World world, BlockPos pos, Direction facing) {
        world.playSound(null,pos, SoundEvents.ENTITY_GENERIC_EXPLODE, SoundCategory.BLOCKS, 1.0F, 1.0F);

        Direction explodeDir = facing.getOpposite();
        BlockPos center = pos.offset(explodeDir);

        Iterable<BlockPos> positions = Iterables.concat(
                getOffsetsAround(pos, explodeDir, 2),
                BlockPos.getAllInBoxMutable(center, center.offset(explodeDir, 2)) //centerBlocks
        );

        for (BlockPos position : positions) {
            BlockState state = world.getBlockState(position);
            float hardness = state.getBlockHardness(world,position);
            if(hardness >= 0 && hardness <= 10f)
                 world.destroyBlock(position, true);
        }
    }


    public static BlockPos[] generateRing(BiFunction<Integer, Integer, BlockPos> provider) {
        Set<BlockPos> positions = Sets.newHashSet();
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (i == 0 && j == 0)
                    continue;
                positions.add(provider.apply(i, j));
            }
        }
        return positions.toArray(new BlockPos[8]);
    }

}
