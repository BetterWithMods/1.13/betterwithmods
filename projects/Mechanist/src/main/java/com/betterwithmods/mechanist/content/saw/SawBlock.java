/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.saw;

import com.betterwithmods.core.base.game.block.BlockBase;
import com.betterwithmods.core.utilties.FloatBuilder;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class SawBlock extends BlockBase {

    public static final BooleanProperty POWERED = BlockStateProperties.POWERED;
    public static final DirectionProperty FACING = BlockStateProperties.FACING;

    public SawBlock(Properties properties) {
        super(properties);
        setDefaultState(getDefaultState().with(POWERED, false).with(FACING, Direction.UP));
        setRenderLayer(BlockRenderLayer.CUTOUT);
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return this.getDefaultState().with(FACING, context.getNearestLookingDirection().getOpposite());
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(POWERED).add(FACING);
    }

    public static final FloatBuilder VOLUME = FloatBuilder.builder(0.5f).addRandom(0.1F),
            PITCH_ON = FloatBuilder.builder(2).addRandom(0.1F),
            PITCH_OFF = FloatBuilder.builder(0.75f).addRandom(0.1F);

//
//    private static final VoxelShape BLADE_Y = Block.makeCuboidShape(3, 0.01, 7.875, 13, 16, 8.125);
//    private static final VoxelShape BLADE_H = Block.makeCuboidShape(3, 0.01, 7.875, 13, 16, 8.125);

    private static final VoxelShape[] COLLISION_SHAPES = new VoxelShape[]{
            Block.makeCuboidShape(0, 4, 0, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 16, 12, 16),
            Block.makeCuboidShape(0, 0, 4, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 16, 16, 12),
            Block.makeCuboidShape(4, 0, 0, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 12, 16, 16),
    };

    private static final VoxelShape[] SHAPES = new VoxelShape[]{
            Block.makeCuboidShape(0, 4, 0, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 16, 12, 16),
            Block.makeCuboidShape(0, 0, 4, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 16, 16, 12),
            Block.makeCuboidShape(4, 0, 0, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 12, 16, 16),
    };

    @Override
    public VoxelShape getCollisionShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        int i = state.get(FACING).getIndex();
        return COLLISION_SHAPES[i];
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader world, BlockPos pos, ISelectionContext context) {
        int i = state.get(FACING).getIndex();
        return SHAPES[i];
    }

    @Override
    public void onEntityCollision(BlockState state, World worldIn, BlockPos pos, Entity entityIn) {
        SawDamageHandler.onEntityCollision(state, worldIn, pos, entityIn);
    }
}
