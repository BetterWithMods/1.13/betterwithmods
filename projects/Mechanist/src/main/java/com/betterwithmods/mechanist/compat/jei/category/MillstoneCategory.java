/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.compat.jei.category;

import com.betterwithmods.core.References;
import com.betterwithmods.core.api.crafting.result.IResult;
import com.betterwithmods.core.compat.jei.CoreJEI;
import com.betterwithmods.core.compat.jei.category.AbstractInventoryMultiplexCategory;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.compat.jei.MechanistJEI;
import com.betterwithmods.mechanist.content.millstone.MillstoneRecipe;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawableAnimated;
import mezz.jei.api.gui.ingredient.IGuiIngredientGroup;
import mezz.jei.api.gui.ingredient.IGuiItemStackGroup;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.item.ItemStack;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class MillstoneCategory extends AbstractInventoryMultiplexCategory<MillstoneRecipe> {
    private static final Cache<Integer, IDrawableAnimated>  ANIMATED_CACHE = CacheBuilder.newBuilder().build();

    public MillstoneCategory(IGuiHelper guiHelper) {
        super(guiHelper, guiHelper.createDrawable(MechanistJEI.Constants.MILLSTONE_BACKGROUND, 0, 0, 149, 32),
                String.format("jei.%s.millstone", References.MODID_MECHANIST),
                guiHelper.createDrawableIngredient(new ItemStack(Registrars.Blocks.MILLSTONE)),
                MechanistJEI.Constants.MILLSTONE_CATEGORY);
    }

    @Nonnull
    @Override
    public Class<? extends MillstoneRecipe> getRecipeClass() {
        return MillstoneRecipe.class;
    }

    @Override
    public void draw(MillstoneRecipe recipe, double mouseX, double mouseY) {
        try {
            IDrawableAnimated animation = ANIMATED_CACHE.get(recipe.getRecipeTime(), () -> this.guiHelper.drawableBuilder(MechanistJEI.Constants.MILLSTONE_BACKGROUND, 150, 0, 14, 14).buildAnimated(recipe.getRecipeTime(), IDrawableAnimated.StartDirection.BOTTOM, false));
            animation.draw(68,8);
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setRecipe(@Nonnull IRecipeLayout layout, @Nonnull MillstoneRecipe recipe, @Nonnull IIngredients ingredients) {
        IGuiItemStackGroup guiItemStacks = layout.getItemStacks();
        IGuiIngredientGroup<IResult<ItemStack>> guiResults = layout.getIngredientsGroup(CoreJEI.STACK_RESULT);
        createSlotsHorizontal(guiItemStacks, true, 3, 0, 7,7);
        guiItemStacks.set(ingredients);
        createSlotsHorizontal(guiResults, false, 3, 3, 90, 8);
        guiResults.set(ingredients);

        List<List<IResult<ItemStack>>> resultList = ingredients.getOutputs(CoreJEI.STACK_RESULT);
        if(resultList.size() > 3) {
            List<List<IResult<ItemStack>>> subList = resultList.subList(2, resultList.size());
            List<IResult<ItemStack>> results = subList.stream().flatMap(List::stream).collect(Collectors.toList());
            guiResults.set(5, results);
        }

    }


}
