/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.handlers;

import com.betterwithmods.core.Tags;
import com.betterwithmods.core.base.setup.ISetup;
import com.betterwithmods.core.utilties.WorldUtils;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.content.kiln.KilnBlock;
import net.minecraft.block.BlockState;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.IWorldReader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import static com.betterwithmods.core.utilties.DirectionUtils.ALL_EXCEPT_DOWN;

public class KilnStructureHandler implements ISetup {

    public KilnStructureHandler() {
        MinecraftForge.EVENT_BUS.register(this);
    }


    @SubscribeEvent
    public void onBlockPlaced(BlockEvent.EntityPlaceEvent event) {
        IWorld world = event.getWorld();
        BlockPos kilnPos = event.getPos();
        BlockPos heatPos = kilnPos.down();

        if (isValidKiln(world, kilnPos, heatPos)) {
            formKiln(world, kilnPos);
        }
    }

    @SubscribeEvent
    public void onNeighborUpdated(BlockEvent.NeighborNotifyEvent event) {
        IWorld world = event.getWorld();
        BlockPos kilnPos = event.getPos().up();
        BlockState heatState = event.getState();
        if (isValidKiln(world, kilnPos, heatState)) {
            formKiln(world, kilnPos);
        }
    }

    public static boolean isValidKiln(IWorldReader world, BlockPos kilnPos, BlockPos heatPos) {
        return isValidKiln(world, kilnPos, world.getBlockState(heatPos));
    }

    public static boolean isValidKiln(IWorldReader world, BlockPos kilnPos, BlockState heatState) {
        int heat = HeatHandler.getHeat(heatState);
        if (heat > 0) {
            BlockPos center = kilnPos.up();
            BlockState state = world.getBlockState(kilnPos);
            if (isKilnBlock(world, kilnPos, state, Direction.DOWN)) {
                int i = 0;
                for (Direction dir : ALL_EXCEPT_DOWN) {
                    BlockPos offset = center.offset(dir);
                    if (isKilnBlock(world, offset, world.getBlockState(offset), dir))
                        i++;
                }
                return i >= 3;
            }
        }
        return false;
    }

    @SuppressWarnings("ConstantConditions")
    public static boolean isKilnBlock(IWorldReader world, BlockPos pos, BlockState state, Direction direction) {
        if (state.getBlock() != Registrars.Blocks.KILN) {
            if (!state.isNormalCube(world, pos) && direction != Direction.DOWN) {
                if (!WorldUtils.isSideSolid(world, pos, direction.getOpposite()))
                    return false;
            }
            return state.isIn(Tags.Blocks.KILN_BLOCKS);
        }
        return true;
    }

    public static boolean formKiln(IWorld world, BlockPos kilnPos) {
        BlockState parent = world.getBlockState(kilnPos);
        return KilnBlock.place(world, kilnPos, parent);
    }
}
