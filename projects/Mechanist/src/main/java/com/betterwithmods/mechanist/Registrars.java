/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist;

import com.betterwithmods.core.References;
import com.betterwithmods.core.api.crafting.input.IInputSerializer;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipeSerializer;
import com.betterwithmods.core.api.crafting.multiplex.IMultiplexRecipeType;
import com.betterwithmods.core.api.crafting.result.IResultSerializer;
import com.betterwithmods.core.base.game.ConstantProperties;
import com.betterwithmods.core.base.game.block.BlockBase;
import com.betterwithmods.core.base.game.item.ArmorItem;
import com.betterwithmods.core.base.game.item.AxeItem;
import com.betterwithmods.core.base.game.item.PickaxeItem;
import com.betterwithmods.core.base.game.item.*;
import com.betterwithmods.core.base.registration.*;
import com.betterwithmods.core.base.registration.custom.InputSerializerRegistrar;
import com.betterwithmods.core.base.registration.custom.MultiplexRecipeSerializerRegistrar;
import com.betterwithmods.core.base.registration.custom.MultiplexRecipeTypeRegistrar;
import com.betterwithmods.core.base.registration.custom.ResultSerializerRegistrar;
import com.betterwithmods.core.impl.crafting.multiplex.MultiplexRecipeType;
import com.betterwithmods.mechanist.blocks.ChoppingBlock;
import com.betterwithmods.mechanist.blocks.PaddingBlock;
import com.betterwithmods.mechanist.blocks.StokedFireBlock;
import com.betterwithmods.mechanist.blocks.VineTrapBlock;
import com.betterwithmods.mechanist.content.baking.RawCakeBlock;
import com.betterwithmods.mechanist.content.baking.RawCookieBlock;
import com.betterwithmods.mechanist.content.baking.RawFlourBlock;
import com.betterwithmods.mechanist.content.bellows.BellowsBlock;
import com.betterwithmods.mechanist.content.bellows.BellowsTile;
import com.betterwithmods.mechanist.content.bricks.UnfiredBrickBlock;
import com.betterwithmods.mechanist.content.detector.DetectorBlock;
import com.betterwithmods.mechanist.content.detector.DetectorTile;
import com.betterwithmods.mechanist.content.dirt_slab.DirtSlab;
import com.betterwithmods.mechanist.content.dirt_slab.GrassPathSlabBlock;
import com.betterwithmods.mechanist.content.dirt_slab.GrassSlabBlock;
import com.betterwithmods.mechanist.content.dispenser.AdvancedDispenserBlock;
import com.betterwithmods.mechanist.content.dispenser.AdvancedDispenserContainer;
import com.betterwithmods.mechanist.content.dispenser.AdvancedDispenserTile;
import com.betterwithmods.mechanist.content.dynamic.CamoflageBlock;
import com.betterwithmods.mechanist.content.dynamic.CamoflageBlockItem;
import com.betterwithmods.mechanist.content.dynamic.CamoflageTile;
import com.betterwithmods.mechanist.content.dynamic.block.BarkBlock;
import com.betterwithmods.mechanist.content.dynamic.block.furniture.ChairBlock;
import com.betterwithmods.mechanist.content.dynamic.block.furniture.TableBlock;
import com.betterwithmods.mechanist.content.dynamic.block.mini.CornerBlock;
import com.betterwithmods.mechanist.content.dynamic.block.mini.MouldingBlock;
import com.betterwithmods.mechanist.content.dynamic.block.mini.SidingBlock;
import com.betterwithmods.mechanist.content.dynamite.DynamiteEntity;
import com.betterwithmods.mechanist.content.dynamite.DynamiteItem;
import com.betterwithmods.mechanist.content.filtered_hopper.FilteredHopperBlock;
import com.betterwithmods.mechanist.content.filtered_hopper.FilteredHopperContainer;
import com.betterwithmods.mechanist.content.filtered_hopper.FilteredHopperTile;
import com.betterwithmods.mechanist.content.filtered_hopper.recipes.HopperInputSerializer;
import com.betterwithmods.mechanist.content.filtered_hopper.recipes.HopperRecipe;
import com.betterwithmods.mechanist.content.filtered_hopper.recipes.HopperRecipeSerializer;
import com.betterwithmods.mechanist.content.filtered_hopper.recipes.HopperState;
import com.betterwithmods.mechanist.content.filtered_hopper.recipes.result.EjectResultSerializer;
import com.betterwithmods.mechanist.content.filtered_hopper.recipes.result.SoulResultSerializer;
import com.betterwithmods.mechanist.content.hemp.HempBottomBlock;
import com.betterwithmods.mechanist.content.hemp.HempTopBlock;
import com.betterwithmods.mechanist.content.kiln.KilnBlock;
import com.betterwithmods.mechanist.content.kiln.KilnRecipe;
import com.betterwithmods.mechanist.content.kiln.KilnRecipeSerializer;
import com.betterwithmods.mechanist.content.kiln.KilnTile;
import com.betterwithmods.mechanist.content.mechanical.axle.AxleBlock;
import com.betterwithmods.mechanist.content.mechanical.axle.AxleTile;
import com.betterwithmods.mechanist.content.mechanical.generators.block.*;
import com.betterwithmods.mechanist.content.mechanical.generators.tile.*;
import com.betterwithmods.mechanist.content.mechanical.joint.*;
import com.betterwithmods.mechanist.content.millstone.*;
import com.betterwithmods.mechanist.content.mining_charge.MiningChargeBlock;
import com.betterwithmods.mechanist.content.mining_charge.MiningChargeEntity;
import com.betterwithmods.mechanist.content.planter.PlanterBlock;
import com.betterwithmods.mechanist.content.planter.PlanterType;
import com.betterwithmods.mechanist.content.planter.UnfiredPlanterBlock;
import com.betterwithmods.mechanist.content.pots.BasePotRecipeSerializer;
import com.betterwithmods.mechanist.content.pots.cauldron.CauldronBlock;
import com.betterwithmods.mechanist.content.pots.cauldron.CauldronContainer;
import com.betterwithmods.mechanist.content.pots.cauldron.CauldronRecipe;
import com.betterwithmods.mechanist.content.pots.cauldron.CauldronTile;
import com.betterwithmods.mechanist.content.pots.crucible.*;
import com.betterwithmods.mechanist.content.redstone.AdvancedObserverBlock;
import com.betterwithmods.mechanist.content.redstone.HibachiBlock;
import com.betterwithmods.mechanist.content.rope.AnchorBlock;
import com.betterwithmods.mechanist.content.rope.RopeBlock;
import com.betterwithmods.mechanist.content.saw.SawBlock;
import com.betterwithmods.mechanist.content.saw.SawRecipe;
import com.betterwithmods.mechanist.content.saw.SawRecipeSerializer;
import com.betterwithmods.mechanist.content.saw.SawTile;
import com.betterwithmods.mechanist.content.soulforge.SoulforgeBlock;
import com.betterwithmods.mechanist.content.soulforge.SoulforgeContainer;
import com.betterwithmods.mechanist.content.soulforge.SoulforgeTile;
import com.betterwithmods.mechanist.content.soulforge.recipes.SoulforgeShapedRecipe;
import com.betterwithmods.mechanist.content.turntable.TurntableBlock;
import com.betterwithmods.mechanist.content.turntable.TurntableRecipe;
import com.betterwithmods.mechanist.content.turntable.TurntableRecipeSerializer;
import com.betterwithmods.mechanist.content.turntable.TurntableTile;
import com.betterwithmods.mechanist.content.urn.SoulUrnBlock;
import com.betterwithmods.mechanist.content.urn.SoulUrnTile;
import com.betterwithmods.mechanist.content.urn.UnfiredUrnBlock;
import com.betterwithmods.mechanist.content.urn.UrnBlock;
import com.betterwithmods.mechanist.content.vase.UnfiredVaseBlock;
import com.betterwithmods.mechanist.items.BWMArmorMaterial;
import com.betterwithmods.mechanist.items.MattockItem;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.RedstoneLampBlock;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.*;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.particles.BasicParticleType;
import net.minecraft.particles.ParticleType;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.ObjectHolder;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;


@SuppressWarnings("ALL")
@Mod.EventBusSubscriber
public class Registrars {
    public static Registrars INSTANCE;

    public static final CreativeTab ITEM_GROUP = new CreativeTab(References.MODID_MECHANIST, "items", () -> new ItemStack(Items.HEMP_LEAF));
    public static final CreativeTab MINI_BLOCKS = new CreativeTab(References.MODID_MECHANIST, "miniblocks",
            () -> CamoflageTile.fromParent(Blocks.SIDING_WOOD, net.minecraft.block.Blocks.OAK_PLANKS.getDefaultState()));

    private static String MODID;
    private static IItemTier SOULFORGED_STEEL = new ToolTier(4, 2250, 10f, 3, 22, () -> Ingredient.fromItems(Items.SOULFORGED_STEEL_INGOT));
    public static BlockRegistrar BLOCKS;

    public Registrars(String modid, Logger logger) {
        MODID = modid;
        INSTANCE = this;
        BLOCKS = new BlockRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<Block> registry) {
                register(registry, "padding_block", new PaddingBlock(ConstantProperties.WOOL));
                register(registry, "hemp_plant_bottom", new HempBottomBlock(ConstantProperties.PLANT_PROPERTIES));
                register(registry, "hemp_plant_top", new HempTopBlock(ConstantProperties.PLANT_PROPERTIES));
                register(registry, "cauldron", new CauldronBlock(ConstantProperties.CAULDRON));
                register(registry, "crucible", new CrucibleBlock(ConstantProperties.CRUCIBLE));
                register(registry, "hibachi", new HibachiBlock(ConstantProperties.REDSTONE_DEVICE));
                register(registry, "advanced_observer", new AdvancedObserverBlock(Block.Properties.create(Material.ROCK).hardnessAndResistance(3.0F)));
                register(registry, "dirt_slab", new DirtSlab(ConstantProperties.DIRT.tickRandomly()));
                register(registry, "grass_slab", new GrassSlabBlock(ConstantProperties.GRASS.tickRandomly()));
                register(registry, "grass_path_slab", new GrassPathSlabBlock(ConstantProperties.GRASS));
                register(registry, "mining_charge", new MiningChargeBlock(ConstantProperties.MINING_CHARGE));
                register(registry, "rope", new RopeBlock(ConstantProperties.ROPE));
                register(registry, "anchor", new AnchorBlock(ConstantProperties.ANCHOR));
                register(registry, "vine_trap", new VineTrapBlock(ConstantProperties.PLANT_PROPERTIES));
                register(registry, "axle", new AxleBlock(ConstantProperties.WOOD));
                register(registry, "clutchbox", new ClutchboxBlock(ConstantProperties.WOOD));
                register(registry, "gearbox", new GearboxBlock(ConstantProperties.WOOD));
                register(registry, "handcrank", new HandcrankBlock(ConstantProperties.ROCK));
                register(registry, "clutchbox_broken", new ClutchboxBrokenBlock(ConstantProperties.WOOD));
                register(registry, "gearbox_broken", new GearboxBroken(ConstantProperties.WOOD));
                register(registry, "millstone", new MillstoneBlock(ConstantProperties.ROCK));
                register(registry, "creative_generator", new CreativeGeneratorBlock(ConstantProperties.WOOD));
                register(registry, "horizontal_windmill", new HorizontalWindmillBlock(ConstantProperties.WOOD));
                register(registry, "vertical_windmill", new VerticalWindmillBlock(ConstantProperties.WOOD));
                register(registry, "waterwheel", new WaterwheelBlock(ConstantProperties.WOOD));
                register(registry, "detector", new DetectorBlock(ConstantProperties.ROCK));
                register(registry, "advanced_dispenser", new AdvancedDispenserBlock(ConstantProperties.ROCK));
                register(registry, "turntable", new TurntableBlock(ConstantProperties.ROCK));
                register(registry, "bellows", new BellowsBlock(Block.Properties.create(Material.WOOD, MaterialColor.WOOD).hardnessAndResistance(2.0F, 3.0F).sound(SoundType.WOOD)));
                register(registry, "unfired_crucible", new UnfiredCrucibleBlock(Block.Properties.create(Material.CLAY).hardnessAndResistance(0.6F).sound(SoundType.GROUND)));
                register(registry, "unfired_planter", new UnfiredPlanterBlock(Block.Properties.create(Material.CLAY).hardnessAndResistance(0.6F).sound(SoundType.GROUND)));
                register(registry, "unfired_vase", new UnfiredVaseBlock(Block.Properties.create(Material.CLAY).hardnessAndResistance(0.6F).sound(SoundType.GROUND)));
                register(registry, "unfired_urn", new UnfiredUrnBlock(Block.Properties.create(Material.CLAY).hardnessAndResistance(0.6F).sound(SoundType.GROUND)));
                register(registry, "unfired_clay_brick", new UnfiredBrickBlock(Block.Properties.create(Material.CLAY).hardnessAndResistance(0.6F).sound(SoundType.GROUND)));
                register(registry, "unfired_nethersludge_brick", new UnfiredBrickBlock(Block.Properties.create(Material.CLAY).hardnessAndResistance(0.6F).sound(SoundType.GROUND)));
                register(registry, "urn", new UrnBlock(Block.Properties.create(Material.ROCK, MaterialColor.ADOBE).hardnessAndResistance(1.25F, 4.2F)));
                register(registry, "flour", new RawFlourBlock(Block.Properties.create(Material.CAKE).hardnessAndResistance(0.6F).sound(SoundType.CLOTH)));
                register(registry, "raw_cake", new RawCakeBlock(Block.Properties.create(Material.CAKE).hardnessAndResistance(0.6F).sound(SoundType.CLOTH)));
                register(registry, "raw_cookie", new RawCookieBlock(Block.Properties.create(Material.CAKE).hardnessAndResistance(0.6F).sound(SoundType.CLOTH)));
                register(registry, "white_stone", new BlockBase(ConstantProperties.ROCK));
                register(registry, "white_cobblestone", new BlockBase(ConstantProperties.ROCK));
                register(registry, "soulforge", new SoulforgeBlock(Block.Properties.create(Material.IRON, MaterialColor.IRON).hardnessAndResistance(5.0F, 6.0F).sound(SoundType.METAL)));
                register(registry, "filtered_hopper", new FilteredHopperBlock(ConstantProperties.WOOD));

                for (DyeColor color : DyeColor.values()) {
                    Blocks.REDSTONE_LAMPS.put(color,
                            register(registry, String.format("%s_redstone_lamp", color.getTranslationKey()), new RedstoneLampBlock(ConstantProperties.LAMP))
                    );
                }

                for (PlanterType type : PlanterType.VALUES) {
                    Blocks.PLANTERS.add(
                            register(registry, String.format("%s_planter", type.getName()), new PlanterBlock(type, ConstantProperties.PLANTER))
                    );
                }

                ///Dynamic Blocks
                register(registry, "kiln", new KilnBlock(Block.Properties.create(Material.ROCK, MaterialColor.RED).noDrops().hardnessAndResistance(2.0F, 6.0F)));
                register(registry, "bark", new BarkBlock(ConstantProperties.WOOD));
                register(registry, "table_wood", new TableBlock(ConstantProperties.WOOD));
                register(registry, "table_rock", new TableBlock(ConstantProperties.ROCK));
                register(registry, "siding_wood", new SidingBlock(ConstantProperties.WOOD));
                register(registry, "siding_rock", new SidingBlock(ConstantProperties.ROCK));
                register(registry, "moulding_wood", new MouldingBlock(ConstantProperties.WOOD));
                register(registry, "moulding_rock", new MouldingBlock(ConstantProperties.ROCK));
                register(registry, "corner_wood", new CornerBlock(ConstantProperties.WOOD));
                register(registry, "corner_rock", new CornerBlock(ConstantProperties.ROCK));
                register(registry, "chair_wood", new ChairBlock(ConstantProperties.WOOD));
                register(registry, "chair_rock", new ChairBlock(ConstantProperties.ROCK));
                register(registry, "soul_urn", new SoulUrnBlock(ConstantProperties.ROCK));
                register(registry, "saw", new SawBlock(ConstantProperties.WOOD));
                register(registry, "chopping_block", new ChoppingBlock(ConstantProperties.ROCK));
                register(registry, "stoked_fire", new StokedFireBlock(Block.Properties.create(Material.FIRE, MaterialColor.TNT).doesNotBlockMovement().tickRandomly().hardnessAndResistance(0, 0).lightValue(15).sound(SoundType.CLOTH).lootFrom(net.minecraft.block.Blocks.FIRE)));
            }


        };
        new ItemRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<Item> registry) {
                register(registry, "wooden_gear", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "hemp_leaf", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "hemp_fiber", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "hemp_cloth", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "filament", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "element", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "concentrated_hellfire", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "tallow", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "leather_slice", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "tanned_leather", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "tanned_leather_belt", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "tanned_leather_strap", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "tanned_leather_slice", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "scoured_leather", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "scoured_leather_slice", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soulforged_steel_ingot", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soulforged_steel_nugget", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soulforged_armor_plate", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "diamond_ingot", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "blasting_oil", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "saw_dust", new BaseItem(new ItemProperties().group(ITEM_GROUP).burnTime(200)));
                register(registry, "fuse", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "glue", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soap", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "brimstone", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "coal_dust", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "netherrack_dust", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "ender_slag", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "hellfire_dust", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "haft", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "padding", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "chain_mail", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "broadhead", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "redstone_latch", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soul_flux", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "niter", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "polished_lapis", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "potash", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soul_dust", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "ender_ocular", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "iron_screw", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "waterwheel_paddle", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "wind_sail", new BaseItem(new ItemProperties().group(ITEM_GROUP)));
                register(registry, "nethercoal", new BaseItem(ConstantProperties.NETHERCOAL));
                register(registry, "nethersludge", new BaseItem(new ItemProperties().group(ITEM_GROUP)));



                register(registry, "soulforged_steel_armor_helm", new ArmorItem(BWMArmorMaterial.SOULFORGED_STEEL, EquipmentSlotType.HEAD, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soulforged_steel_armor_chest", new ArmorItem(BWMArmorMaterial.SOULFORGED_STEEL, EquipmentSlotType.CHEST, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soulforged_steel_armor_legs", new ArmorItem(BWMArmorMaterial.SOULFORGED_STEEL, EquipmentSlotType.LEGS, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soulforged_steel_armor_feet", new ArmorItem(BWMArmorMaterial.SOULFORGED_STEEL, EquipmentSlotType.FEET, new ItemProperties().group(ITEM_GROUP)));

                register(registry, "tanned_leather_armor_helm", new ArmorItem(BWMArmorMaterial.TANNED_LEATHER, EquipmentSlotType.HEAD, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "tanned_leather_armor_chest", new ArmorItem(BWMArmorMaterial.TANNED_LEATHER, EquipmentSlotType.CHEST, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "tanned_leather_armor_legs", new ArmorItem(BWMArmorMaterial.TANNED_LEATHER, EquipmentSlotType.LEGS, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "tanned_leather_armor_feet", new ArmorItem(BWMArmorMaterial.TANNED_LEATHER, EquipmentSlotType.FEET, new ItemProperties().group(ITEM_GROUP)));

                register(registry, "wool_armor_helm", new ArmorItem(BWMArmorMaterial.WOOL, EquipmentSlotType.HEAD, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "wool_armor_chest", new ArmorItem(BWMArmorMaterial.WOOL, EquipmentSlotType.CHEST, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "wool_armor_legs", new ArmorItem(BWMArmorMaterial.WOOL, EquipmentSlotType.LEGS, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "wool_armor_feet", new ArmorItem(BWMArmorMaterial.WOOL, EquipmentSlotType.FEET, new ItemProperties().group(ITEM_GROUP)));

                register(registry, "dynamite", new DynamiteItem(new ItemProperties().group(ITEM_GROUP)));


                TieredItem pickaxe = (TieredItem) register(registry, "soulforged_steel_pickaxe", new PickaxeItem(SOULFORGED_STEEL, 1, -2.8F, new ItemProperties().group(ITEM_GROUP)));
                TieredItem shovel = (TieredItem) register(registry, "soulforged_steel_shovel", new ShovelItem(SOULFORGED_STEEL, 1.5F, -3.0F, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soulforged_steel_sword", new SwordItem(SOULFORGED_STEEL, 4, -2.4F, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soulforged_steel_axe", new AxeItem(SOULFORGED_STEEL, 5.0F, -3.0F, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soulforged_steel_hoe", new HoeItem(SOULFORGED_STEEL, 0, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soulforged_steel_mattock", new MattockItem(SOULFORGED_STEEL, 1, -2.8F, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soulforged_steel_battleaxe", new BattleAxeItem(SOULFORGED_STEEL, 5, -2.4F, new ItemProperties().group(ITEM_GROUP)));

                for (Block block : Blocks.REDSTONE_LAMPS.values()) {
                    register(registry, block, ITEM_GROUP);
                }
                for (Block block : Blocks.PLANTERS) {
                    register(registry, block, ITEM_GROUP);
                }
                register(registry, "hemp_seed", new BlockNamedItem(Blocks.HEMP_PLANT_BOTTOM, new ItemProperties().group(ITEM_GROUP)));
                register(registry, Blocks.VINE_TRAP, ITEM_GROUP);
                register(registry, Blocks.HIBACHI, ITEM_GROUP);
                register(registry, Blocks.ADVANCED_OBSERVER, ITEM_GROUP);
                register(registry, Blocks.CAULDRON, ITEM_GROUP);
                register(registry, Blocks.CRUCIBLE, ITEM_GROUP);
                register(registry, Blocks.DIRT_SLAB, ITEM_GROUP);
                register(registry, Blocks.GRASS_SLAB, ITEM_GROUP);
                register(registry, Blocks.MINING_CHARGE, ITEM_GROUP);
                register(registry, Blocks.ROPE, ITEM_GROUP);
                register(registry, Blocks.ANCHOR, ITEM_GROUP);
                register(registry, Blocks.AXLE, ITEM_GROUP);
                register(registry, Blocks.CLUTCHBOX, ITEM_GROUP);
                register(registry, Blocks.GEARBOX, ITEM_GROUP);
                register(registry, Blocks.CLUTCHBOX_BROKEN, ITEM_GROUP);
                register(registry, Blocks.GEARBOX_BROKEN, ITEM_GROUP);
                register(registry, Blocks.MILLSTONE, ITEM_GROUP);
                register(registry, Blocks.CREATIVE_GENERATOR, ITEM_GROUP);
                register(registry, Blocks.HORIZONTAL_WINDMILL, ITEM_GROUP);
                register(registry, Blocks.VERTICAL_WINDMILL, ITEM_GROUP);
                register(registry, Blocks.WATERWHEEL, ITEM_GROUP);
                register(registry, Blocks.HANDCRANK, ITEM_GROUP);
                register(registry, Blocks.PADDING_BLOCK, ITEM_GROUP);
                register(registry, Blocks.DETECTOR, ITEM_GROUP);
                register(registry, Blocks.ADVANCED_DISPENSER, ITEM_GROUP);
                register(registry, Blocks.SOUL_URN, ITEM_GROUP);
                register(registry, Blocks.SAW, ITEM_GROUP);
                register(registry, Blocks.CHOPPING_BLOCK, ITEM_GROUP);
                register(registry, Blocks.BELLOWS, ITEM_GROUP);
                register(registry, Blocks.TURNTABLE, ITEM_GROUP);
                register(registry, Blocks.UNFIRED_CRUCIBLE, ITEM_GROUP);
                register(registry, Blocks.UNFIRED_PLANTER, ITEM_GROUP);
                register(registry, Blocks.UNFIRED_VASE, ITEM_GROUP);
                register(registry, Blocks.UNFIRED_URN, ITEM_GROUP);
                register(registry, Blocks.UNFIRED_CLAY_BRICK, ITEM_GROUP);
                register(registry, Blocks.UNFIRED_NETHERSLUDGE_BRICK, ITEM_GROUP);
                register(registry, Blocks.URN, ITEM_GROUP);
                register(registry, Blocks.FLOUR, ITEM_GROUP);
                register(registry, Blocks.RAW_CAKE, ITEM_GROUP);
                register(registry, Blocks.RAW_COOKIE, ITEM_GROUP);
                register(registry, Blocks.WHITE_STONE, ITEM_GROUP);
                register(registry, Blocks.WHITE_COBBLESTONE, ITEM_GROUP);
                register(registry, Blocks.SOULFORGE, ITEM_GROUP);
                register(registry, Blocks.FILTERED_HOPPER, ITEM_GROUP);

                register(registry, CamoflageBlockItem::new, Blocks.BARK, MINI_BLOCKS);
                register(registry, CamoflageBlockItem::new, Blocks.TABLE_WOOD, MINI_BLOCKS);
                register(registry, CamoflageBlockItem::new, Blocks.TABLE_ROCK, MINI_BLOCKS);
                register(registry, CamoflageBlockItem::new, Blocks.SIDING_WOOD, MINI_BLOCKS);
                register(registry, CamoflageBlockItem::new, Blocks.SIDING_ROCK, MINI_BLOCKS);
                register(registry, CamoflageBlockItem::new, Blocks.MOULDING_WOOD, MINI_BLOCKS);
                register(registry, CamoflageBlockItem::new, Blocks.MOULDING_ROCK, MINI_BLOCKS);
                register(registry, CamoflageBlockItem::new, Blocks.CORNER_WOOD, MINI_BLOCKS);
                register(registry, CamoflageBlockItem::new, Blocks.CORNER_ROCK, MINI_BLOCKS);
                register(registry, CamoflageBlockItem::new, Blocks.CHAIR_WOOD, MINI_BLOCKS);
                register(registry, CamoflageBlockItem::new, Blocks.CHAIR_ROCK, MINI_BLOCKS);
            }
        };

        new EntityRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<EntityType<?>> registry) {
                register(registry, "betterwithmods_mechanist:mining_charge", EntityType.Builder
                        .<MiningChargeEntity>create(MiningChargeEntity::new, EntityClassification.MISC)
                        .setTrackingRange(64)
                        .setUpdateInterval(1)
                        .setShouldReceiveVelocityUpdates(true)
                        .setCustomClientFactory(((spawnEntity, world) -> {
                            return Entities.MINING_CHARGE.create(world);
                        }))
                        .immuneToFire()
                        .size(0.98F, 0.98F)
                );

                register(registry, "betterwithmods_mechanist:dynamite", EntityType.Builder
                        .<DynamiteEntity>create(DynamiteEntity::new, EntityClassification.MISC)
                        .setTrackingRange(64)
                        .setUpdateInterval(1)
                        .setShouldReceiveVelocityUpdates(true)
                        .immuneToFire()
                        .setCustomClientFactory((spawnEntity, world) -> {
                            return Entities.DYNAMITE.create(world);
                        })
                        .size(0.98F, 0.98F)
                );
            }
        };

        new TileEntityRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<TileEntityType<?>> registry) {
                register(registry, "camoflage", TileEntityType.Builder.create(CamoflageTile::new, CamoflageBlock.getAll()).build(null));
                register(registry, "axle", TileEntityType.Builder.create(AxleTile::new, Blocks.AXLE).build(null));
                register(registry, "creative_generator", TileEntityType.Builder.create(CreativeGeneratorTile::new, Blocks.CREATIVE_GENERATOR).build(null));
                register(registry, "horizontal_windmill", TileEntityType.Builder.create(HorizontalWindmillTile::new, Blocks.HORIZONTAL_WINDMILL).build(null));
                register(registry, "vertical_windmill", TileEntityType.Builder.create(VerticalWindmillTile::new, Blocks.VERTICAL_WINDMILL).build(null));
                register(registry, "waterwheel", TileEntityType.Builder.create(WaterwheelTile::new, Blocks.WATERWHEEL).build(null));
                register(registry, "gearbox", TileEntityType.Builder.create(GearboxTile::new, Blocks.GEARBOX).build(null));
                register(registry, "clutchbox", TileEntityType.Builder.create(ClutchboxTile::new, Blocks.CLUTCHBOX).build(null));
                register(registry, "handcrank", TileEntityType.Builder.create(HandcrankTile::new, Blocks.HANDCRANK).build(null));
                register(registry, "cauldron", TileEntityType.Builder.create(CauldronTile::new, Blocks.CAULDRON).build(null));
                register(registry, "crucible", TileEntityType.Builder.create(CrucibleTile::new, Blocks.CRUCIBLE).build(null));
                register(registry, "detector", TileEntityType.Builder.create(DetectorTile::new, Blocks.DETECTOR).build(null));
                register(registry, "advanced_dispenser", TileEntityType.Builder.create(AdvancedDispenserTile::new, Blocks.ADVANCED_DISPENSER).build(null));
                register(registry, "soul_urn", TileEntityType.Builder.create(SoulUrnTile::new, Blocks.SOUL_URN).build(null));
                register(registry, "saw", TileEntityType.Builder.create(SawTile::new, Blocks.SAW).build(null));
                register(registry, "millstone", TileEntityType.Builder.create(MillstoneTile::new, Blocks.MILLSTONE).build(null));
                register(registry, "bellows", TileEntityType.Builder.create(BellowsTile::new, Blocks.BELLOWS).build(null));
                register(registry, "kiln", TileEntityType.Builder.create(KilnTile::new, Blocks.KILN).build(null));
                register(registry, "turntable", TileEntityType.Builder.create(TurntableTile::new, Blocks.TURNTABLE).build(null));
                register(registry, "soulforge", TileEntityType.Builder.create(SoulforgeTile::new, Blocks.SOULFORGE).build(null));
                register(registry, "filtered_hopper", TileEntityType.Builder.create(FilteredHopperTile::new, Blocks.FILTERED_HOPPER).build(null));
            }
        };

        new SoundRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<SoundEvent> registry) {
                register(registry, "block.mechanical.creak");
                register(registry, "block.saw.cut");
                register(registry, "block.millstone.grind");
                register(registry, "block.bellows.air");
                register(registry, "block.handcrank.click");
            }
        };

        new ContainerRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<ContainerType<?>> registry) {
                register(registry, "cauldron", CauldronContainer::new);
                register(registry, "crucible", CrucibleContainer::new);
                register(registry, "millstone", MillstoneContainer::new);
                register(registry, "advanced_dispenser", AdvancedDispenserContainer::new);
                register(registry, "soulforge", SoulforgeContainer::new);
                register(registry, "filtered_hopper", FilteredHopperContainer::new);
            }
        };


        new MultiplexRecipeTypeRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<IMultiplexRecipeType<?, ?>> registry) {
                register(registry, "turntable", new MultiplexRecipeType<BlockState, TurntableRecipe>());
                register(registry, "kiln", new MultiplexRecipeType<BlockState, KilnRecipe>());
                register(registry, "saw", new MultiplexRecipeType<BlockState, SawRecipe>());
                register(registry, "millstone", new MultiplexRecipeType<IItemHandlerModifiable, MillstoneRecipe>());
                register(registry, "cauldron", new MultiplexRecipeType<IItemHandlerModifiable, CauldronRecipe>());
                register(registry, "crucible", new MultiplexRecipeType<IItemHandlerModifiable, CrucibleRecipe>());
                register(registry, "filtered_hopper", new MultiplexRecipeType<HopperState, HopperRecipe>());
            }
        };

        new MultiplexRecipeSerializerRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<IMultiplexRecipeSerializer<?>> registry) {
                register(registry, "turntable", new TurntableRecipeSerializer());
                register(registry, "saw", new SawRecipeSerializer());
                register(registry, "kiln", new KilnRecipeSerializer());
                register(registry, "millstone", new MillstoneRecipeSerializer());
                register(registry, "cauldron", new BasePotRecipeSerializer<>(CauldronRecipe::new));
                register(registry, "crucible", new BasePotRecipeSerializer<>(CrucibleRecipe::new));
                register(registry, "filtered_hopper", new HopperRecipeSerializer());
            }
        };


        new ParticleTypeRegistrar(modid, logger) {

            @Override
            public void registerAll(IForgeRegistry<ParticleType<?>> registry) {
                ParticleType<BasicParticleType> CAULDRON_STEAM = new BasicParticleType(true);
                register(registry, "cauldron_steam", CAULDRON_STEAM);

            }
        };

        new RecipeSerializerRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<IRecipeSerializer<?>> registry) {
                register(registry, "soulforge_shaped", new SoulforgeShapedRecipe.Serializer());
            }
        };

        new InputSerializerRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<IInputSerializer<?, ?>> registry) {
                register(registry, "filtered_hopper", new HopperInputSerializer());
            }
        };


        new ResultSerializerRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<IResultSerializer<?>> registry) {
                register(registry, "eject", new EjectResultSerializer());
                register(registry, "souls", new SoulResultSerializer());
            }
        };
    }


    @ObjectHolder(References.MODID_MECHANIST)
    public static class Blocks {
        public static final Block HEMP_PLANT_BOTTOM = null;
        public static final Block HEMP_PLANT_TOP = null;
        public static final Block HIBACHI = null;
        public static final Block CAULDRON = null;
        public static final Block CRUCIBLE = null;
        public static final Block ADVANCED_OBSERVER = null;
        public static Map<DyeColor, Block> REDSTONE_LAMPS = Maps.newHashMap();
        public static List<Block> PLANTERS = Lists.newArrayList();
        public static final Block ANCHOR = null;
        public static final Block ROPE = null;
        public static final Block MINING_CHARGE = null;
        public static final Block EMPTY_PLANTER = null;
        public static final Block GRASS_PLANTER = null;
        public static final Block WATER_PLANTER = null;
        public static final Block DIRT_SLAB = null;
        public static final Block GRASS_SLAB = null;
        public static final Block GRASS_PATH_SLAB = null;
        public static final Block MYCELIUM_SLAB = null;
        public static final Block VINE_TRAP = null;
        public static final Block BARK = null;
        public static final Block TABLE_WOOD = null;
        public static final Block TABLE_ROCK = null;
        public static final Block SIDING_WOOD = null;
        public static final Block SIDING_ROCK = null;
        public static final Block MOULDING_WOOD = null;
        public static final Block MOULDING_ROCK = null;
        public static final Block CORNER_WOOD = null;
        public static final Block CORNER_ROCK = null;
        public static final Block CHAIR_WOOD = null;
        public static final Block CHAIR_ROCK = null;
        public static final Block AXLE = null;
        public static final Block CLUTCHBOX = null;
        public static final Block GEARBOX = null;
        public static final Block CLUTCHBOX_BROKEN = null;
        public static final Block GEARBOX_BROKEN = null;
        public static final Block MILLSTONE = null;
        public static final Block CREATIVE_GENERATOR = null;
        public static final Block HORIZONTAL_WINDMILL = null;
        public static final Block VERTICAL_WINDMILL = null;
        public static final Block WATERWHEEL = null;
        public static final Block HANDCRANK = null;
        public static final Block PADDING_BLOCK = null;
        public static final Block DETECTOR = null;
        public static final Block ADVANCED_DISPENSER = null;
        public static final Block SOUL_URN = null;
        public static final Block SAW = null;
        public static final Block CHOPPING_BLOCK = null;
        public static final Block STOKED_FIRE = null;
        public static final Block BELLOWS = null;
        public static final Block KILN = null;
        public static final Block TURNTABLE = null;
        public static final Block UNFIRED_CRUCIBLE = null;
        public static final Block UNFIRED_PLANTER = null;
        public static final Block UNFIRED_VASE = null;
        public static final Block UNFIRED_URN = null;
        public static final Block UNFIRED_CLAY_BRICK = null;
        public static final Block UNFIRED_NETHERSLUDGE_BRICK = null;
        public static final Block URN = null;
        public static final Block FLOUR = null;
        public static final Block RAW_CAKE = null;
        public static final Block RAW_COOKIE = null;
        public static final Block WHITE_STONE = null;
        public static final Block WHITE_COBBLESTONE = null;
        public static final Block SOULFORGE = null;
        public static final Block FILTERED_HOPPER = null;
    }


    @ObjectHolder(References.MODID_MECHANIST)
    public static class Items {
        public static final Item HEMP_SEED = null;
        public static final Item HEMP_LEAF = null;
        public static final Item HEMP_FIBER = null;
        public static final Item HEMP_CLOTH = null;
        public static final Item WOODEN_GEAR = null;
        public static final Item FILAMENT = null;
        public static final Item ELEMENT = null;
        public static final Item CONCENTRATED_HELLFIRE = null;
        public static final Item SOULFORGED_STEEL_INGOT = null;
        public static final Item SOULFORGED_STEEL_NUGGET = null;
        public static final Item SOAP = null;
        public static final Item BRIMSTONE = null;
        public static final Item COAL_DUST = null;
        public static final Item NETHERRACK_DUST = null;
        public static final Item ENDER_SLAG = null;
        public static final Item SOUL_FLUX = null;
        public static final Item HELLFIRE_DUST = null;
        public static final Item HAFT = null;
        public static final Item PADDING = null;
        public static final Item SAW_DUST = null;
        public static final Item DYNAMITE = null;
        public static final Item GLUE = null;
        public static final Item NITER = null;
        public static final Item DIAMOND_INGOT = null;
        public static final Item BLASTING_OIL = null;
        public static final Item FUSE = null;
        public static final Item TALLOW = null;
        public static final Item REDSTONE_LATCH = null;
        public static final Item WIND_SAIL = null;
        public static final Item WATERWHEEL_PADDLE = null;
        public static final Item POTASH = null;
        public static final Item ENDER_OCULAR = null;
        public static final Item IRON_SCREW = null;
        public static final Item NETHERSLUDGE = null;
        public static final Item SCOURED_LEATHER = null;
        public static final Item TANNED_LEATHER = null;
        public static final Item LEATHER_SLICE = null;
        public static final Item SCOURED_LEATHER_SLICE = null;
        public static final Item SOULFORGED_ARMOR_PLATE = null;
        public static final Item POLISHED_LAPIS = null;


        public static final Item SOULFORGED_STEEL_PICKAXE = null;
        public static final Item SOULFORGED_STEEL_HOE = null;
        public static final Item SOULFORGED_STEEL_AXE = null;
        public static final Item SOULFORGED_STEEL_SHOVEL = null;
        public static final Item SOULFORGED_STEEL_MATTOCK = null;
        public static final Item SOULFORGED_STEEL_BATTLEAXE = null;
        public static final Item SOULFORGED_STEEL_SWORD = null;
        public static final Item TANNED_LEATHER_SLICE = null;
        public static final Item TANNED_LEATHER_STRAP = null;
        public static final Item TANNED_LEATHER_BELT = null;
        public static final Item TANNED_LEATHER_ARMOR_HELM = null;
        public static final Item TANNED_LEATHER_ARMOR_CHEST = null;
        public static final Item TANNED_LEATHER_ARMOR_LEGS = null;
        public static final Item TANNED_LEATHER_ARMOR_FEET = null;
        public static final Item SOULFORGED_STEEL_ARMOR_HELM = null;
        public static final Item SOULFORGED_STEEL_ARMOR_CHEST = null;
        public static final Item SOULFORGED_STEEL_ARMOR_LEGS = null;
        public static final Item SOULFORGED_STEEL_ARMOR_FEET = null;
        public static final Item WOOL_ARMOR_HELM = null;
        public static final Item WOOL_ARMOR_CHEST = null;
        public static final Item WOOL_ARMOR_LEGS = null;
        public static final Item WOOL_ARMOR_FEET = null;

    }

    @ObjectHolder(References.MODID_MECHANIST)
    public static class Entities {
        public static final EntityType<MiningChargeEntity> MINING_CHARGE = null;
        public static final EntityType<DynamiteEntity> DYNAMITE = null;
    }

    @ObjectHolder(References.MODID_MECHANIST)
    public static class Tiles {
        public static final TileEntityType<CamoflageTile> CAMOFLAGE = null;
        public static final TileEntityType<AxleTile> AXLE = null;
        public static final TileEntityType<CreativeGeneratorTile> CREATIVE_GENERATOR = null;
        public static final TileEntityType<HorizontalWindmillTile> HORIZONTAL_WINDMILL = null;
        public static final TileEntityType<VerticalWindmillTile> VERTICAL_WINDMILL = null;
        public static final TileEntityType<WaterwheelTile> WATERWHEEL = null;
        public static final TileEntityType<GearboxTile> GEARBOX = null;
        public static final TileEntityType<ClutchboxTile> CLUTCHBOX = null;
        public static final TileEntityType<HandcrankTile> HANDCRANK = null;
        public static final TileEntityType<SawTile> SAW = null;
        public static final TileEntityType<CrucibleTile> CRUCIBLE = null;
        public static final TileEntityType<CauldronTile> CAULDRON = null;
        public static final TileEntityType<MillstoneTile> MILLSTONE = null;
        public static final TileEntityType<DetectorTile> DETECTOR = null;
        public static final TileEntityType<AdvancedDispenserTile> ADVANCED_DISPENSER = null;
        public static final TileEntityType<SoulUrnTile> SOUL_URN = null;
        public static final TileEntityType<BellowsTile> BELLOWS = null;
        public static final TileEntityType<KilnTile> KILN = null;
        public static final TileEntityType<TurntableTile> TURNTABLE = null;
        public static final TileEntityType<SoulforgeTile> SOULFORGE = null;
        public static final TileEntityType<FilteredHopperTile> FILTERED_HOPPER = null;
    }


    public static class Sounds {
        @ObjectHolder(References.MODID_MECHANIST + ":block.mechanical.creak")
        public static final SoundEvent MECHANICAL_CREAK = null;

        @ObjectHolder(References.MODID_MECHANIST + ":block.saw.cut")
        public static final SoundEvent SAW_CUT = null;

        @ObjectHolder(References.MODID_MECHANIST + ":block.millstone.grind")
        public static final SoundEvent MILLSTONE_GRIND = null;

        @ObjectHolder(References.MODID_MECHANIST + ":block.bellows.air")
        public static final SoundEvent BELLOWS_AIR = null;

        @ObjectHolder(References.MODID_MECHANIST + ":block.handcrank.click")
        public static final SoundEvent HANDCRANK_CLICK = null;
    }

    @ObjectHolder(References.MODID_MECHANIST)
    public static class Containers {
        public static final ContainerType<CauldronContainer> CAULDRON = null;
        public static final ContainerType<CrucibleContainer> CRUCIBLE = null;
        public static final ContainerType<MillstoneContainer> MILLSTONE = null;
        public static final ContainerType<AdvancedDispenserContainer> ADVANCED_DISPENSER = null;
        public static final ContainerType<SoulforgeContainer> SOULFORGE = null;
        public static final ContainerType<FilteredHopperContainer> FILTERED_HOPPER = null;
    }

    @ObjectHolder(References.MODID_MECHANIST)
    public static class MultiplexRecipeTypes {
        public static final MultiplexRecipeType<BlockState, TurntableRecipe> TURNTABLE = null;
        public static final MultiplexRecipeType<BlockState, KilnRecipe> KILN = null;
        public static final MultiplexRecipeType<BlockState, SawRecipe> SAW = null;
        public static final MultiplexRecipeType<IItemHandlerModifiable, MillstoneRecipe> MILLSTONE = null;
        public static final MultiplexRecipeType<IItemHandlerModifiable, CauldronRecipe> CAULDRON = null;
        public static final MultiplexRecipeType<IItemHandlerModifiable, CrucibleRecipe> CRUCIBLE = null;
        public static final MultiplexRecipeType<HopperState, HopperRecipe> FILTERED_HOPPER = null;
    }

    @ObjectHolder(References.MODID_MECHANIST)
    public static class MultiplexRecipeSerializers {
        public static final TurntableRecipeSerializer TURNTABLE = null;
        public static final SawRecipeSerializer SAW = null;
        public static final KilnRecipeSerializer KILN = null;
        public static final MillstoneRecipeSerializer MILLSTONE = null;
        public static final BasePotRecipeSerializer<CauldronRecipe> CAULDRON = null;
        public static final BasePotRecipeSerializer<CrucibleRecipe> CRUCIBLE = null;
        public static final HopperRecipeSerializer FILTERED_HOPPER = null;
    }

    @ObjectHolder(References.MODID_MECHANIST)
    public static class RecipeSerializers {
        public static final IRecipeSerializer<SoulforgeShapedRecipe> SOULFORGE_SHAPED = null;
    }

    @ObjectHolder(References.MODID_MECHANIST)
    public static class Particles {
        public static final BasicParticleType CAULDRON_STEAM = null;
    }

    @ObjectHolder(References.MODID_MECHANIST)
    public static class InputSerializers {
        public static final HopperInputSerializer FILTERED_HOPPER = null;
    }

    @ObjectHolder(References.MODID_MECHANIST)
    public static class ResultSerializers {
        public static final EjectResultSerializer EJECT = null;
        public static final SoulResultSerializer SOULS = null;

    }


}
