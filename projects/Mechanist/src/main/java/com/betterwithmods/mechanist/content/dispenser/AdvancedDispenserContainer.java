/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.dispenser;

import com.betterwithmods.core.base.game.container.InventoryContainer;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraftforge.items.ItemStackHandler;

public class AdvancedDispenserContainer extends InventoryContainer {

    public AdvancedDispenserContainer(int id, PlayerEntity player) {
        super(Registrars.Containers.ADVANCED_DISPENSER, id, player, new ItemStackHandler(16), null);
    }

    public AdvancedDispenserContainer(int windowId, PlayerEntity player, ItemStackHandler inventory) {
        super(Registrars.Containers.ADVANCED_DISPENSER, windowId, player, inventory, null);

        addSlots(CONTAINER,this.inventory, 16, 4, 0, 53, 17);
        addPlayerInventory(0, 8, 102, 58);
    }

}
